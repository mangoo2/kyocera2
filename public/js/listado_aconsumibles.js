var base_url = $('#base_url').val();
var table_equipo;
var perfilid=$('#perfilid_select').val();
$(document).ready(function() {
    $('.chosen-select').chosen({width: "100%"});
    table_equipo=$("#tabla_consumibles").DataTable();
    ocultarbodegas(perfilid);
});
function ocultarbodegas(per){
    if(per!=1){
        //$('.bodegaId_1').remove();//boulevard
        //$('.bodegaId_8').remove();//tamaulipas
        //$('.bodegaId_10').remove();//alt tlaxcala
        //$('.bodegaId_5').remove();//tlaxcala

        $('.bodegaId_2').remove();//seminuevos
        $('.bodegaId_7').remove();//ADHL
        $('.bodegaId_9').remove();//Dimpresion
        $('.bodegaId_11').remove();//Dimplesion puebla
        $('.addnuevoserie').remove();
    }
}
function loadtable(){
    var tipo = $('#tipoconsulta option:selected').val();
    if (tipo==0) {
        $('.titulocra').html('');
        limpiatable();
    }
    if (tipo==1) {
        $('.titulocra').html('Consumible');
        datos_consumibles();
    }
    if (tipo==2) {
        $('.titulocra').html('Refacciones');
        datos_refacciones();
    }
    if (tipo==3) {
        $('.titulocra').html('Accesorios');
        datos_accesorios();
    }
    if (tipo==4) {
        $('.titulocra').html('Equipo');
        datos_equipo();
    }
}
function limpiatable(){
    table_equipo.destroy();
    table_equipo=$("#tabla_consumibles").DataTable().clear();
}
function datos_consumibles(){
    table_equipo.destroy();
    var bodega=$('#bodegaId option:selected').val();
    table_equipo = $('#tabla_consumibles').DataTable({
        pagingType: 'input',
        //stateSave: true,
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Aconsumibles/consumibles_bodega",
            type: "post",
            "data": {
                "bodegaId": bodega
            },
            error: function() {
                $("#tabla_consumibles").css("display", "none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "bodega"},
            {"data": "parte"},
            {"data": "modelo"},
            {"data": "total",
                render: function(data,type,row){ 
                    var html='';
                        if (row.total>0) {
                            var total=row.total;
                            if(perfilid>1){
                                total=parseInt(total)-parseInt(row.resguardar_cant);
                            }
                            html=total;
                        }else{
                            html=0;
                        }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html='<div class="div_button">';
                        if (row.total>0) {
                            if(perfilid==1){
                            var nombre="'"+row.modelo+"'";
                                html+='<button class="btn-bmz waves-effect waves-light " title="Traspaso"\
                                 onclick="traspasar('+row.consubId+','+row.bodegaId+','+row.total+','+nombre+')">\
                                 <i class="material-icons left" style="margin: 0px;">reply</i></button>';
                                html+='<button class="btn-bmz waves-effect waves-light editarcons_'+row.consubId+'" \
                                        data-modelo="'+row.modelo+'" title="Editar" \
                                        onclick="editarcons('+row.consubId+','+row.total+')">\
                                        <i class="material-icons left" style="margin: 0px;">create</i></button>';
                                html+='<button class="btn-bmz waves-effect waves-light editarcons_'+row.consubId+'" \
                                        data-modelo="'+row.modelo+'" title="Eliminar" \
                                        onclick="deletecons('+row.consubId+')">\
                                        <i class="material-icons left" style="margin: 0px;">delete</i></button>';
                                //===============================================
                                    if(row.resguardar_cant>0){
                                        var resclass='has-badge-rounded has-badge-danger notificacion_tipped';
                                        var resclass_data=' data-badge="'+row.resguardar_cant+'" '
                                    }else{
                                        var resclass='';
                                        var resclass_data='';
                                    }
                                    html+='<button class="btn-bmz waves-effect waves-light '+resclass+' " title="Resguardar/liberar" onclick="resguardar_cons('+row.consubId+','+row.resguardar_cant+')" '+resclass_data+' >';
                                                if(row.resguardar_cant==0){
                                                    html+='<i class="fas fa-lock-open"></i>';
                                                }else{
                                                    html+='<i class="fas fa-lock"></i>';
                                                }
                                    html+='</button>';
                                //============================================
                            }
                        }else{
                            html+='<div class="card-content red accent-2 white-text"><h4 style="margin:0px;">Resurtir</h4></div>\
                                  <div class="card-content consultconsupendient consultconsupendient_'+row.id+'" data-id="'+row.id+'"></div>';
                        }
                            html+='<div class="card-content verificarprioridad verificarprioridad_'+row.id+'" data-id="'+row.id+'"></div>';
                        html+='</div>';
                    return html;
                }
            },
          ],
        order: [[3, "desc"]]
        
    }).on('draw',function(){
        consultconsupendient();
        //verificarprioridadfuc();
    });
}
function datos_refacciones(){
    table_equipo.destroy();
    var bodega=$('#bodegaId option:selected').val();
    table_equipo = $('#tabla_consumibles').DataTable({
        pagingType: 'input',
        //stateSave: true,
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Aconsumibles/refacciones_bodega",
            type: "post",
            "data": {
                "bodegaId": bodega
            },
            error: function() {
                $("#tabla_consumibles").css("display", "none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "bodega"},
            {"data": "codigo"},
            {"data": "nombre"},
            {"data": "cantidad",
                render: function(data,type,row){ 
                    var html='';
                    if (row.cantidad>0) {
                        var total = row.cantidad;
                        if(perfilid>1){
                            total= parseInt(total)-parseInt(row.resguardar_cant);
                        }
                        html=total;
                    }else{
                        html=0;
                    }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html='<div class="div_button">';
                    if (row.cantidad>0) {
                        if(perfilid==1){
                            if(row.con_serie==0){
                                html+='<button class="btn-bmz waves-effect waves-light editarrefacc_'+row.serieId+'" \
                                        data-modelo="'+row.nombre+'" title="Editar" \
                                        onclick="editarrefacc('+row.serieId+','+row.cantidad+')">\
                                        <i class="material-icons left" style="margin: 0px;">create</i></button>';
                                html+='<button class="btn-bmz waves-effect waves-light editarrefacc_'+row.serieId+'" \
                                        data-modelo="'+row.nombre+'" title="Eliminar" \
                                        onclick="deleterefacc('+row.serieId+')">\
                                        <i class="material-icons left" style="margin: 0px;">delete</i></button>';
                            }
                        }
                    }else{
                        html+='<div class="card-content red accent-2 white-text"><h4 style="margin:0px;">Resurtir</h4></div>\
                              <div class="card-content consultrefapendient consultrefapendient_'+row.id+'" data-id="'+row.id+'"></div>';
                    }
                        html+='<div class="card-content verificarprioridad verificarprioridad_'+row.id+'" data-id="'+row.id+'"></div>';
                    html+='</div>';
                    return html;
                }
            },
          ],
        order: [[0, "desc"]],
        "columnDefs": [
            {
                //"visible": false,
                "searchable": false,
                "targets": 3,
            }
        ]
        
    }).on('draw',function(){
        consultrefapendient();
        //verificarprioridadfuc();
    });
}
function datos_accesorios(){ 
    table_equipo.destroy();
    var bodega=$('#bodegaId option:selected').val();
    table_equipo = $('#tabla_consumibles').DataTable({
        pagingType: 'input',
        //stateSave: true,
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Aconsumibles/accesorio_bodega",
            type: "post",
            "data": {
                "bodegaId": bodega
            },
            error: function() {
                $("#tabla_consumibles").css("display", "none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "bodega"},
            {"data": "no_parte"},
            {"data": "nombre"},
            {"data": "stock",
                render: function(data,type,row){ 
                    var html='';
                    if(row.stock1>0){
                        var stock1=row.stock1;
                    }else{
                        var stock1=0;
                    }
                    if(row.stock2>0){
                        var stock2=row.stock2;
                    }else{
                        var stock2=0;
                    }
                    if(row.resguardar_cant>0){
                        var resguardar_cant=row.resguardar_cant;
                    }else{
                        var resguardar_cant=0;
                    }
                    if ((parseFloat(stock1)+(parseFloat(stock2)))>0) {
                        if(perfilid>1){
                            html=parseFloat(stock1)+parseFloat(stock2)-parseFloat(resguardar_cant);
                        }else{
                            html=parseFloat(stock1)+parseFloat(stock2);
                        }
                        
                        html+='<!--stock1:'+stock1+' stock2:'+stock2+' resguardar_cant:'+resguardar_cant+'-->';
                    }else{
                        html=0;
                    }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html='<div class="div_button">';
                    if (row.stock>0) {

                    }else{
                        html+='<div class="card-content red accent-2 white-text"><h4 style="margin:0px;">Resurtir</h4></div>\
                              <div class="card-content consultaccependient consultaccependient_'+row.id+'" data-id="'+row.id+'"></div>';
                    }
                        html+='<div class="card-content verificarprioridad verificarprioridad_'+row.id+'" data-id="'+row.id+'"></div>';
                     html+='</div>';
                    return html;
                }
            },
          ],
        order: [[3, "desc"]]
    }).on('draw',function(){
        consultaccependient();
        //verificarprioridadfuc();
    });
}
function datos_equipo(){
    table_equipo.destroy();
    var bodega=$('#bodegaId option:selected').val();
    table_equipo = $('#tabla_consumibles').DataTable({
        pagingType: 'input',
        //stateSave: true,
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Aconsumibles/equipo_bodega",
            type: "post",
            "data": {
                "bodegaId": bodega
            },
            error: function() {
                $("#tabla_consumibles").css("display", "none");
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "bodega"},
            {"data": "noparte"},
            {"data": "modelo"},
            {"data": "stock",
                render: function(data,type,row){ 
                    var html='';
                    if (row.stock>0) {
                        var total=row.stock;
                        if(perfilid>1){
                            total=parseInt(total)-parseInt(row.resguardar_cant);
                        }
                        html=total;
                    }else{
                        html=0;
                    }
                    return html;
                }

            },
            {"data": null,
                render: function(data,type,row){ 
                        var html='<div class="div_button">';
                        if (row.stock>0) {

                        }else{
                            html+='<div class="card-content red accent-2 white-text"><h4 style="margin:0px;">Resurtir</h4></div>\
                                  <div class="card-content consultequipopendient consultequipopendient_'+row.id+'" data-id="'+row.id+'"></div>';
                        }
                            html+='<div class="card-content verificarprioridad verificarprioridad_'+row.id+'" data-id="'+row.id+'"></div>';
                        html+='</div>';

                    return html;
                }
            },
          ],
        order: [[3, "desc"]]
    }).on('draw',function(){
        consultequipopendient();
        //verificarprioridadfuc();
    });
}
function traspasar(id,bodega,total,nombre){
    $('.productotras').html(nombre);
    $('#total').val(total);
    $('#trasserieid').val(id);
    $('#bodegao').val(bodega).prop('disabled', false).trigger("chosen:updated");
    $('#modal_traspasos').modal();
    $('#modal_traspasos').modal('open');
    var tipov=$('#bodegao option:selected').data('bodegatipo');
    $('.bodegatipo_1').prop('disabled', true).trigger("chosen:updated");
    $('.bodegatipo_2').prop('disabled', true).trigger("chosen:updated");
    console.log('tipov_'+tipov);
    if(tipov==1){
        $('.bodegatipo_1').prop('disabled', false).trigger("chosen:updated");
        console.log('tipov1');
    }
    if(tipov==2){
        $('.bodegatipo_2').prop('disabled', false).trigger("chosen:updated");
        console.log('tipov2');
    }
    setTimeout(function(){ 
        $('#bodegao').prop('disabled', true).trigger("chosen:updated");
        $('#bodeganew option[value="'+bodega+'"]').prop('disabled', true).trigger("chosen:updated");
    }, 1000);
    
}
function aceptar_traspaso(){
    var bodeganew = $('#bodeganew option:selected').val();
    var cantidad=$('#cantidad').val();
    var total = $('#total').val();
    var motivo = $('#motivo1').val();
    if(bodeganew>0){
        if(motivo!=''){
            if (parseFloat(cantidad)>0&&parseFloat(cantidad)<=parseFloat(total)) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Aconsumibles/traspasar',
                    data: {
                        serieid: $('#trasserieid').val(),
                        cantidad: cantidad,
                        bodega:$('#bodeganew option:selected').val(),
                        motivo:motivo,
                        bodegao:$('#bodegao option:selected').val(),
                        modelo:$('.modelotras1').html(),
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            swal("Error", "404", "error");
                        },
                        500: function(){
                            swal("Error", "500", "error"); 
                        }
                    },
                    success:function(data){
                        $('#modal_traspasos').modal('close');
                        datos_consumibles();
                    }
                });
            }else{
                swal("Error", "No se aceptan números negativos y/o mayor al disponible", "error");
            }
        }else{
            toastr["warning"]("Agregue un motivo del traspaso", "Alerta!");
        }
    }else{
        toastr["warning"]("Seleccione Bodega valida");
    }
    
}
function editarcons(id,total){
    var modelo = $('.editarcons_'+id).data('modelo');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Cantidad a editar de '+modelo+' <br><input type="number" placeholder="Cantidad" id="cantidadedit" value="'+total+'" class="name form-control-bmz" style="width: 90%;"required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var cant=$('#cantidadedit').val();
                if (cant>=0) {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Aconsumibles/editarconsumible",
                        data: {
                            idcons:id,
                            cantidad:cant
                        },
                        success: function (response){
                                $.alert({boxWidth: '30%',useBootstrap: false,
                                    title: 'Hecho!',content: 'Cantidad editada'}); 
                                    datos_consumibles();
                                
                        },
                        error: function(response){
                            notificacion(1);  
                        }
                    });
                    
                }else{
                    notificacion(3);
                }
            },
            cancelar: function (){
            }
        }
    });
}
function deletecons(id){
    var modelo = $('.editarcons_'+id).data('modelo');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea Eliminar el stock de '+modelo+' de la bodega actual?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Aconsumibles/deleteconsumible",
                        data: {
                            idcons:id
                        },
                        success: function (response){
                                $.alert({boxWidth: '30%',useBootstrap: false,
                                    title: 'Echo!',content: 'Cantidad Eliminada'}); 
                                    datos_consumibles();
                                
                        },
                        error: function(response){
                            notificacion(1);    
                        }
                    });
            },
            cancelar: function () {
                
            }
        }
    });
}
function editarrefacc(id,total){
    var modelo = $('.editarrefacc_'+id).data('modelo');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Cantidad a editar de '+modelo+' <br>'+
                 '<input type="number" placeholder="Cantidad" id="cantidadedit" value="'+total+'" class="name form-control-bmz" style="width: 90%;"required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var cant=$('#cantidadedit').val();
                if (cant>=0) {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Aconsumibles/editarrefacciones",
                        data: {
                            idcons:id,
                            cantidad:cant
                        },
                        success: function (response){
                                $.alert({boxWidth: '30%',useBootstrap: false,
                                    title: 'Echo!',content: 'Cantidad editada'}); 
                                    datos_refacciones();    
                        },
                        error: function(response){
                            notificacion(1);    
                        }
                    });
                    
                }else{
                    notificacion(3);
                }
            },
            cancelar: function (){
            }
        }
    });
}
function deleterefacc(id){
    var modelo = $('.editarrefacc_'+id).data('modelo');
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea Eliminar el stock de '+modelo+' de la bodega actual?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Aconsumibles/deleterefacciones",
                        data: {
                            idcons:id
                        },
                        success: function (response){
                                $.alert({boxWidth: '30%',useBootstrap: false,
                                    title: 'Echo!',content: 'Cantidad Eliminada'}); 
                                    datos_refacciones();
                                
                        },
                        error: function(response){ 
                            notificacion(1); 
                        }
                    });
            },
            cancelar: function (){
                
            }
        }
    });
}
function consultconsupendient(){
    var DATAf  = [];
    $(".consultconsupendient").each(function() {
        item = {};
        item ["id"] = $(this).data('id');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Aconsumibles/consultconsupendient",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".consultconsupendient_"+item.idconsu).html('Pendiente por recibir :'+item.pendientes);
                
            });
        }
    });
}
function consultrefapendient(){
    var DATAf  = [];
    $(".consultrefapendient").each(function() {
        item = {};
        item ["id"] = $(this).data('id');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Aconsumibles/consultrefapendient",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".consultrefapendient_"+item.idref).html('Pendiente por recibir :'+item.pendientes);
                
            });
        }
    });
}
function consultaccependient(){
    var DATAf  = [];
    $(".consultaccependient").each(function() {
        item = {};
        item ["id"] = $(this).data('id');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Aconsumibles/consultaccependient",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".consultaccependient_"+item.idacc).html('Pendiente por recibir :'+item.pendientes);
                
            });
        }
    });
}
function consultequipopendient(){
    var DATAf  = [];
    $(".consultequipopendient").each(function() {
        item = {};
        item ["id"] = $(this).data('id');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Aconsumibles/consultequipopendient",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".consultequipopendient_"+item.ideq).html('Pendiente por recibir :'+item.pendientes);
                
            });
        }
    });
}
/*
function verificarprioridadfuc(){
    var DATAf  = [];
    $(".verificarprioridad").each(function() {
        item = {};
        item ["id"] = $(this).data('id');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Aconsumibles/verificarprioridad",
        data: {
            datos:aInfo,
            tipo:$('#tipoconsulta option:selected').val()
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".verificarprioridad_"+item.ideq).html('Marcado como prioritario');
                
            });
        }
    });
}
*/
function resguardar_cons(serie,cant){

    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Confirma resguardar stock?<br><label>Cantidad</label><input type="number" value="'+cant+'" class="form-control-bmz "  id="cant_reguardar">',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var can_r=$('#cant_reguardar').val();
                //===========================
                    $.ajax({
                        type:'POST',
                        url: base_url+'Series/resguardar_cons',
                        data: {
                            serie: serie,
                            cant:can_r
                        },
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr["error"]("Error 404", "Alerta!");
                            },
                            500: function(){
                                toastr["error"]("Error 500", "Alerta!");
                            }
                        },
                        success:function(data){
                            loadtable();
                            toastr["success"]("Realizado");
                            
                            
                        }
                    });
                //===========================
            },
            cancelar: function (){
                
            }
        }
    });
}