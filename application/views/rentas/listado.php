<style type="text/css">
   .redcolor{
    background: #ee6b0b  !important;
   }
   table td{
    font-size: 12px;
    padding: 5px 7px;
   }
</style>
<div style="height: 0px;width: 0px;">
  <input type="password" name="password" style="position: inherit;">
  <input type="text" name="user" style="position: inherit;">
</div>
<!-- START CONTENT -->
    <section id="content">
    	<!--start container-->
      	<div class="container">
      		<div class="section">
<div class="card-panel">
      		<input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">

      		<div id="table-datatables">
      			<br>
                <h4 class="header">Listado Solicitudes de Contrato en Renta</h4>
                <div class="row">
                  <div class="col s4">
                    <select id="tipo" class="browser-default form-control-bmz" onchange="load()">
                      <option value="1">Activo</option>
                      <option value="2">Activo con series pendientes</option>
                      <option value="0">Eliminado</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">

                    <table id="tabla_solicitudcontrato" class="responsive-table display" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>IdContrato</th>
                          <th>Cliente</th>
                          <th>Atención</th>
                          <th>Folio</th>
                          <th>Pre factura</th>
                          <td></td>
                          <td>Acciones</td> 
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>  
               </div>
        </section>
<div id="modalprefacturash" class="modal">
    <div class="modal-content ">
      <h4>Prefacturas</h4>
      <div class="row">
        <div class="col s12 m12 l12 historialprefacturas">
        </div>
      </div>
        
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>
    </div>
  </div>
</div>