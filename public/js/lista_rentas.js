var base_url = $('#base_url').val();
var table;
$(document).ready(function() {	
    table=$("#tabla_solicitudcontrato").DataTable();
    $('.chosen-select').chosen({width: "100%"});
    load();
});
function load(){
    table.destroy();
    var tipo=$('#tipo option:selected').val();
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table = $('#tabla_solicitudcontrato').DataTable({
        //pagingType: 'full_numbers',
           //pagingType: 'input',
           //"sPaginationType": "listbox",
        stateSave: true,
        responsive: true,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            },
        "ajax": {
            "url": base_url+"index.php/Rentas/getListadocontratosRentasIncompletas",
            type: "post",
            "data": {
                "tipo": tipo
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "idcontrato"},
            {"data": "empresa"},
            {"data": "atencion"},
            {"data": "folio"},
            {"data": "id",
                render:function(data,type,row){
                    var html='';
                    if(row.estatus == 2 || row.estatus == 0){
                         if (row.prefactura==1) {
                            var colorbtton='green';
                        }else{
                            var colorbtton='blue';
                        }
                        html='<a class="btn-floating '+colorbtton+' tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalle('+row.id+')">\
                                    <i class="material-icons">assignment</i>\
                                  </a>';
                        html+='<a class="btn-floating amber tooltipped" data-position="top" data-delay="50" data-tooltip="Prefactura" onclick="detalleh('+row.id+')">\
                                    <i class="material-icons">assignment</i>\
                                  </a>';
                    }else{    
                        html='<a class="btn-floating blue tooltipped" disabled>\
                                    <i class="material-icons">assignment</i>\
                                  </a>';
                    }
                    return html;
                }
            }, 
            {"data": null,
                 render:function(data,type,row){
                    return '<img src="'+base_url+'app-assets/images/kyorent.png" style="width:100px;" >';
                }
            }, 
            {"data": "id",
                render:function(data,type,row){
                    var html='<!--'+row.estatus+'-->';
                    if(data==1){
                        html+='<a class="waves-effect green btn-bmz" href="'+base_url+'index.php/Rentas/contrato/'+row.id+'"> Completar Venta &nbsp;</a>';    
                    }else {
                        if(row.estatus == 2){
                            html+='<a class="waves-effect green btn-bmz" href="'+base_url+'index.php/Rentas/contrato/'+row.id+'">Contrato generado</a>';
                            html+='<a class="btn-floating green tooltipped edit" href="'+base_url+'index.php/Contratos/contrato/'+row.id+'?edit=1"><i class="material-icons">mode_edit</i></a>';
                        }else if(row.estatus != 0){
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Rentas/contrato/'+row.id+'">Generar contrato</a>';        
                        }
                        
                    } 
                    //if (row.estatus!=0) {
                    html+='<a class="btn-floating waves-effect waves-light red accent-2"><i class="material-icons" onclick="cancelarcs('+row.id+')">clear</i></a>';
                    //}
                    

                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        "dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            
        ],
        // Cambiamos lo principal a Español
        
        rowCallback: function (row, data) {
        //if it is not the summation row the row should be selectable
            if (data.estatus == 2 ) {
                $(row).addClass('redcolor');
                //$('.red_color').css("background","red");
            }
        }
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_solicitudcontrato_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    table.search(searchv).draw();
}
function detalle(id){
    window.open(base_url+"RentasCompletadas/view/"+id, "Prefactura", "width=780, height=612");
}
function cancelarcs(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Cancelar la solicitud de  Contrato?<br>Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                var pass=$('#contrasena').val();
                if (pass!='') {
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Sistema/solicitarpermiso",
                        data: {
                            pass:pass
                        },
                        success: function (response){
                                var respuesta = parseInt(response);
                                if (respuesta==1) {
                                    $.ajax({
                                        type:'POST',
                                        url: base_url+"index.php/Rentas/cancelar",
                                        data: {
                                            contratoId:id
                                        },
                                        success: function (response){
                                                $.alert({
                                                    boxWidth: '30%',
                                                    useBootstrap: false,
                                                    title: 'Éxito!',
                                                    content: 'Contrato Cancelado!'});
                                                load();
                                        },
                                        error: function(response){
                                            notificacion(1);   
                                             
                                        }
                                    });
                                }else{
                                    notificacion(2);
                                }
                        },
                        error: function(response){
                            notificacion(1);   
                        }
                    });
                    
                }else{
                    notificacion(0);
                }
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}
function detalleh(idr){
    $('#modalprefacturash').modal();
    $('#modalprefacturash').modal('open');
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Rentas/historialprefacturas",
        data: {
            idrenta:idr
        },
        success: function (response){
            $('.historialprefacturas').html(response);
            $('#historialprefacturas').DataTable();
        },
        error: function(response){
            notificacion(1);  
             
        }
    });
}
function prefacturah(idrenta,idhistorial){
    window.open(base_url+"RentasCompletadas/viewh/"+idrenta+'/'+idhistorial, "Prefactura", "width=780, height=612");
}
function eliminarpreh(idpreh){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar la prefactura?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Contratos/eliminarprehistorial",
                    data: {
                        id:idpreh
                    },
                    success: function (response){
                            $.alert({
                                boxWidth: '30%',
                                useBootstrap: false,
                                title: 'Éxito!',
                                content: 'prefactura eliminada!'});
                            load();
                    },
                    error: function(response){
                        notificacion(1);  
                         
                    }
                });
   
                
            },
            cancelar: function () 
            {
                
            }
        }
    });
}