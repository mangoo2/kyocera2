<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Mapa con rutas por calles</title>
    <!--<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@3.2.12/dist/leaflet-routing-machine.css" />-->
    <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/leaflet2/leaflet.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>public/plugins/leaflet2/leaflet-routing-machine.css" />
    <style>
        body{
            margin: 0px;
        }
        #map {
            height: 99vh;
            width: 100%;
        }
    </style>
</head>
<body>
    <div id="map"></div>

    <!--<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-routing-machine@3.2.12/dist/leaflet-routing-machine.js"></script>-->
    <script src="<?php echo base_url();?>public/plugins/leaflet2/leaflet.js"></script>
    <script src="<?php echo base_url();?>public/plugins/leaflet2/leaflet-routing-machine.js"></script>

    <script>
        // Crear el mapa
        var map = L.map('map').setView([20.6844, -103.4028], 13);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
        }).addTo(map);

        // Coordenadas de cada ruta
        var ruta1Coords = [];//clientes
        var ruta2Coords = [];//tecnicos
        <?php if($result->num_rows()==0){ ?>
            var ruta2Coords = [
                { coords: [19.08782533671139, -98.22777569573624], mensaje: "Sin registros" }
                [19.08782533671139, -98.22777569573624, "Sin registros", "https://altaproductividadapr.com/public/img/file-info_40446.png"]
            ];
        <?php } ?>
        /*
        var ruta1Coords = [
            [20.6844, -103.4028, "Inicio de la ruta 1", "https://altaproductividadapr.com/public/img/edificio.png"],
            [20.6743708, -103.3683999, "Punto intermedio 1", "https://altaproductividadapr.com/public/img/edificio.png"],
            [20.6773472, -103.34908399999999, "Punto intermedio 2", "https://altaproductividadapr.com/public/img/edificio.png"],
            [20.6793534, -103.4230303, "Final de la ruta 1", "https://altaproductividadapr.com/public/img/edificio.png"]
        ];

        var ruta2Coords = [
            [20.6844, -103.4028, "Inicio de la ruta 2", "https://altaproductividadapr.com/public/img/file-info_40446.png"],
            [20.6743708, -103.3683999, "Punto intermedio 1", "https://altaproductividadapr.com/public/img/file-info_40446.png"],
            [20.6773472, -103.34908399999999, "Punto intermedio 2", "https://altaproductividadapr.com/public/img/file-info_40446.png"],
            [20.6793534, -103.4230303, "Final de la ruta 2", "https://altaproductividadapr.com/public/img/file-info_40446.png"]
        ];
        */
        <?php if(count($arrayubicli)>0){ ?>
            var ruta1Coords = [
                <?php foreach ($arrayubicli as $itemc) { ?>
                    [<?php echo $itemc['latitud'] ?>, <?php echo $itemc['longitud'] ?>, '<?php echo str_replace("'", "", $itemc['message']) ?>', "https://altaproductividadapr.com/public/img/edificio.png"],
                <?php } ?>
            ]
        <?php } ?>
        <?php if($result->num_rows()>0){ ?>
            var ruta2Coords = [
                <?php foreach ($result->result() as $item) { ?>
                  [<?php echo $item->latitud; ?>, <?php echo $item->longitud; ?>, '<?php echo str_replace("'", "", $item->reg) ?>', "https://altaproductividadapr.com/public/img/file-info_40446.png"],
                <?php } ?>
            ];
        <?php } ?>
        

        // Función para crear la ruta en el mapa
        function crearRuta(routeCoords, color) {
            // Configurar los puntos de la ruta para enrutamiento
            var waypoints = routeCoords.map(coord => L.latLng(coord[0], coord[1]));

            // Crear la ruta en el mapa usando OSRM y Leaflet Routing Machine
            L.Routing.control({
                waypoints: waypoints,
                router: L.Routing.osrmv1({
                    serviceUrl: 'https://router.project-osrm.org/route/v1'
                }),
                createMarker: function(i, waypoint, n) {
                    let coord = routeCoords[i];
                    let iconUrl = coord[3];
                    let message = coord[2];

                    // Crear un ícono personalizado para cada punto
                    let icon = L.icon({
                        iconUrl: iconUrl,
                        iconSize: [25, 25]
                    });

                    return L.marker(waypoint.latLng, { icon: icon })
                        .bindPopup(message, { autoClose: false, closeOnClick: false });
                },
                lineOptions: {
                    styles: [{ color: color, opacity: 0.7, weight: 5 }]
                },
                addWaypoints: false,
                draggableWaypoints: false,
                show: false  // Ocultar el panel de instrucciones
            }).addTo(map);
        }

        // Generar las dos rutas en el mapa
        crearRuta(ruta1Coords, 'red');  // Ruta 1 en color rojo
        crearRuta(ruta2Coords, 'blue'); // Ruta 2 en color azul
    </script>
</body>
</html>
