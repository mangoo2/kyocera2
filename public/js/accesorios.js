var base_url =$('#base_url').val();
var tableload;
var equipo_infog;
$(document).ready(function($) {
	tableload=$('#tabla_accesorios').DataTable();
	loadtable();
	var formulario_catalogos_accesorios = $('#catalogos_accesorios_form');
	formulario_catalogos_accesorios.validate({
            rules:{
                nombre: {
                    required: true
                },
                no_parte: {
                    required: true
                },
                stock: {
                    required: true
                },
                costo: {
                    required: true
                }
            },
            // Para mensajes personalizados
            messages:{
                nombre:{
                    required: "Ingrese un nombre"
                },
                no_parte:{
                    required: "Ingrese un número de parte"
                },
                stock:{
                    required: "Ingrese un número de stock"
                },
                costo:{
                    required: "Ingrese un costo"
                }
            },
        errorElement : 'div',
            // Si están correctos los campos, procesamos el formulario mediante Ajax
        submitHandler: function (form){
            var datos = formulario_catalogos_accesorios.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'index.php/Configuraciones/insertarCatalogoAccesorios',
                data: datos,
                success:function(data){
                    if(data==1){
                        swal({ title: "Error",
                            text: "No. parte ya existe, ingrese otro diferente.",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    }
                    // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                    else if(data==2){
                        swal({ title: "Éxito",
                            text: "Se insertó accesorio",
                            type: 'success',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                        //$('#catalogos_accesorios_form')[0].reset();
                        setTimeout(function(){
                            cancelar();
                        loadtable();
                      },1000);
                    }else{
                        swal({ title: "Error",
                            text: "Los datos del son erroneos",
                            type: 'error',
                            showCancelButton: false,
                            allowOutsideClick: false,
                        });
                    }
                }
            });
            
            
        }
    
    });
    filesupload();
    $('.modal').modal();
});
function loadtable(){
	tableload.destroy();
	tableload=$("#tabla_accesorios").DataTable({
    //stateSave: true,
    responsive: !0,
    "bProcessing": true,
    "serverSide": true,
    search: {
                return: true
            },
    "ajax": {
       "url": base_url+"Accesorios/getlistaccesorios",
       type: "post",
       "data": function(d){
        // d.bodegaId = $('#bodegaId option:selected').val()
        },
        error: function(){
           $("#table").css("display","none");
        }
    },
    "columns": [
        {"data": "id"},
        {"data": "nombre"},
        {"data": "no_parte"},
        {"data": "stock"},
        {"data": "costo"},
        {"data": "observaciones"},
        {"data": "destacado",
                render:function(data,type,row){ 
                    var html='';
                    if(row.destacado==1){
                        var startchecked='checked';
                    }else{
                        var startchecked='';
                    }
                    html='<input class="star_f soloadministradores" type="checkbox" title="Favorito" id="star_factura_'+row.id+'" onclick="favorito('+row.id+')" '+startchecked+'>';
                    return html;
                }
            },
        {"data": null,
            "render": function ( data, type, row, meta ) {
                var html="";
                html+=" <a class='btn btn-floating waves-effect waves-light red soloadministradores' onclick='deleteaccesorio("+row.id+")'><i class='material-icons'>delete_forever</i></a>";  
                html+=" <a class='btn btn-floating waves-effect waves-light green editaccesorio_"+row.id+" soloadministradores'\
                        data-nombre='"+row.nombre+"'\
                        data-noparto='"+row.no_parte+"'\
                        data-costo='"+row.costo+"'\
                        data-observaciones='"+row.observaciones+"'\
                        onclick='editaccesorio("+row.id+")'><i class='material-icons'>mode_edit</i></a>"; 
                html+=' <a class="btn-floating blue tooltipped infogen_'+row.id+'" data-modelo="'+row.nombre+'" data-position="top" data-delay="50" data-tooltip="Informacion General" onclick="infogen('+row.id+')"><i class="fa fa-laptop fa-fw"></i></a>';
                    if(row.paginaweb==1){
                        var viewweb='checked';
                    }else{
                        var viewweb='';
                    } 
                html+='<div class="switch"><br>\
                                  <label class="soloadministradores">Mostrar\
                                    <input type="checkbox" name="bloqueo" id="mostrarweb_'+row.id+'" wtx-context="7579CA6E-0857-4E6C-B2D3-9042AE775C00" onclick="mostrarweb('+row.id+')" '+viewweb+'>\
                                    <span class="lever"></span>\
                                  </label>\
                            </div>';       
            return html;
            }
        },
    ],
    "columnDefs": [ 
            {
                "targets": [ 3 ],
                "visible": false,
                "searchable": false
            }
        ],
    "order": [[ 0, "desc" ]],
    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
  }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')

    });
}
function searchtable_v(){
    var contenedor = document.getElementById('tabla_accesorios_filter');

    // Selecciona el input dentro del contenedor
    var input = contenedor.querySelector('input.form-control-bmz');

    // Obtén el valor del input
    var searchv = input.value;

    // Muestra el valor en la consola
    console.log(searchv);
    //var searchv =$("input[type=search]").val();
    tableload.search(searchv).draw();
}
function deleteaccesorio(id){
	$.confirm({ 
        boxWidth: '30%', 
        useBootstrap: false, 
        icon: 'fa fa-warning', 
        title: 'Atención!', 
        content: '¿Está seguro de Eliminar este accesorio?', 
        type: 'red', 
        typeAnimated: true, 
        buttons: { 
            // Si le da click al botón de "confirmar" 
            confirmar: function (){ 
                $.ajax({
			      type: "POST",
			      url: base_url+"index.php/Configuraciones/eliminar_accesorio",
			      data:{id:id},
			      success: function(data){
			      	loadtable();
			      }
			    });
            }, 
            // Si le da click a "cancelar" 
            cancelar: function (){ 
                 
            } 
        }
    }); 
	
}
function validar_accesorio(){
   $.ajax({
        type:'POST',
        url: base_url+'index.php/Configuraciones/verificaraccesorio',
        data: {datos:$('#no_parte').val()},
        success:function(data)
        {
            if(data == 1){
                swal({ title: "Error",
                    text: "No. parte ya existe, ingrese otro diferente.",
                    type: 'error',
                    showCancelButton: false,
                    allowOutsideClick: false,
                });
            }
        }
    });
}
function editaccesorio(id){
    var nombre  = $('.editaccesorio_'+id).data('nombre');
    var noparto = $('.editaccesorio_'+id).data('noparto');
    var costo   = $('.editaccesorio_'+id).data('costo');
    var observaciones = $('.editaccesorio_'+id).data('observaciones');
    $('#id').val(id);
    $('#nombre').val(nombre);
    $('#no_parte').val(noparto);
    $('#costo').val(costo);
    $('#observaciones').val(observaciones);
    $('.addaccesorios').html('Editar');
    $('.divaddaccesorios').html('<a class="btn-bmz btn-floating waves-effect waves-light red" onclick="cancelar()" style="width: 100px;">Cancelar</a>');
    $('#nombre').focus();
}
function cancelar(){
    $('#id').val(0);
    $('#nombre').val('');
    $('#no_parte').val('');
    $('#costo').val('');
    $('#observaciones').val('');
    $('.addaccesorios').html('Guardar');
    $('.divaddaccesorios').html('');
}
//========================
$(document).ready(function($) {
    $('#save_fc').click(function(event) {
        var datos = $('#form_catacteristicas').serialize();
                
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Accesorios/insertaActualizacaracteristicas",
            data: datos,
            success:function(data){  
                catacte_data_view(equipo_infog);
                toastr["success"]("Se ha Procesado con exito", "éxito");
                fc_limpiar();
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
            }
        });
    });   
});
function favorito(id){
    setTimeout(function(){ 
        var statusfav=$('#star_factura_'+id).is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+'Accesorios/favorito',
            data: {
                equipo:id,
                status:statusfav
            },
            statusCode:{
                404: function(data){
                    toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                },
                500: function(){
                    toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                }
            },
            success:function(data){
                
               
            }
        });
    }, 1000);
}
function mostrarweb(id){
    setTimeout(function(){ 
        var paginaweb = $('#mostrarweb_'+id).is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Accesorios/mostrarweb",
            data: {
                    idequipo:id,
                    mostrar:paginaweb
                },
            success: function (response){
                
            },
            error: function(response){
                toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
            }
        });
    }, 1000);
}
function infogen(id){
    $('#modalinfoequipo').modal('open');
    var modelo=$('.infogen_'+id).data('modelo');
    $('#idequipofc').val(id);
    $('.equipo_modelo').html(modelo);
    equipo_infog=id;
    upload_data_view(equipo_infog);
    catacte_data_view(equipo_infog);
}
function upload_data_view(id){
    console.log(id);
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Accesorios/viewimages',
        data: {idequipo:id},
        success:function(data){
            $('.galeriimg').html(data);
            $('.materialboxed').materialbox();
        }
    });
}
function catacte_data_view(id){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Accesorios/viewcaracte',
        data: {idequipo:id},
        success:function(data){
            $('.lis_catacter').html(data);
        }
    });
}
function filesupload(){
    $("#files").fileinput({
        showCaption: true,
        showUpload: true,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","png","webp","jpeg"],
        browseLabel: 'Seleccionar documentos',
        uploadUrl: base_url+'Accesorios/imagenes_multiple',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            'cer': '<i class="fas fa-file-invoice"></i>',
        },
        uploadExtraData: function (previewId, index) {
        var info = {
                    idequipo:equipo_infog
                };
        return info;
      }
    }).on('filebatchuploadcomplete', function(event, files, extra) {
      //location.reload();
        upload_data_view(equipo_infog);
    }).on('filebatchuploadsuccess', function(event, files, extra) {
      //location.reload();
      //toastr.success('Se cargo el Certificado Correctamente','Hecho!');
        upload_data_view(equipo_infog);
    });
}
function deleteimg(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la imagen? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Accesorios/deleteimg/",
                    data: {idequipo:id},
                    success: function (response){
                        toastr["success"]("Se ha eliminado con exito", "éxito");
                        upload_data_view(equipo_infog);
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}
function fc_limpiar(){
    $('#idfc').val(0);
    $('#descripcion').val('');
    $('#name').val('');
    $('#general').prop('checked',false);
    $('#orden').val('');
    $('#cancelar_fc').hide('slow');
}
function delete_ec(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar la caracteristica? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"index.php/Equipos/deleteec",
                    data: {idequipo:id},
                    success: function (response){
                        toastr["success"]("Se ha eliminado con exito", "éxito");
                        catacte_data_view(equipo_infog);
                    },
                    error: function(response){
                        toastr["error"]("Algo salió mal, intente de nuevo o contacte al administrador del sistema", "Advertencia");
                    }
                });
            },
            cancelar: function (){
                
            }
        }
    });
}
function editar_ec(id){
    $('#cancelar_fc').show('slow');
    var name = $('.eq_cact_'+id).data('name');
    var descripcion = $('.eq_cact_'+id).data('descripcion');
    var general = $('.eq_cact_'+id).data('general');
    var orden = $('.eq_cact_'+id).data('orden');

    $('#idfc').val(id);
    $('#name').val(name);
    $('#descripcion').val(descripcion);
    if(general==1){
        $('#general').prop('checked',true);    
    }else{
        $('#general').prop('checked',false);
    }
    $('#orden').val(orden);
}