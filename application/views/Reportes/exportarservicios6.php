<?php 
	date_default_timezone_set('America/Mexico_City');
    $this->fechahoy = date('Y-m-d G:i:s');
    $this->fechaactual = date('Y-m-d');
    $this->horaactual = date('G:i:s');

    
	header("Pragma: public");
	header("Expires: 0");
	$filename = "Reporte_AST_".$this->fechaactual.".xls";
	header("Content-type: application/x-msdownload");
	header("Content-Disposition: attachment; filename=$filename");
	header("Pragma: no-cache");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	
	
?>
<table border="1">
	<tr>
		<th colspan="10"><?php echo utf8_decode('REPORTE: ANÁLISIS DE SERVICIO TECNICO');?></th>
	</tr>
	<tr>
		<th colspan="10"></th>
	</tr>
	<tr>
		<td>Fecha</td>
		<td>Asesor Tecnico</td>
		<td>Tota de servicios atendidos (Preventivos+Correctivos+Instalaciones)</td>
		<td>Num de Servicios Preventivos</td>
		<td><?php echo utf8_decode('Número de Servicios Correctivos');?></td>
		<td><?php echo utf8_decode('Número de Instalaciones');?></td>
		<td><?php echo utf8_decode('Reincidencia (más de un correctivo a cliente y serie de eq en menos de 30 días)');?></td>
		<td>Tiempo promedio en Servicios Preventivos (mins)</td>
		<td>Tiempo promedio en Servicios Correctivos (mins)</td>
		<td>Confiabilidad (reincidencia / servicio correctivo)</td>
	</tr>
	<?php foreach ($resultstec as $item) { 
		$personalId=$item->personalId;
		//14 Servicios Preventivos
		 			 
		$result_sp = $this->ModeloAsignacion->getlistadocalentario($fechainicio,$fechafin,14,$personalId,0,0,0,$personalId,0,0);
		$num_sp=$result_sp->num_rows();
		//16 Servicios Correctivos
		$result_sc = $this->ModeloAsignacion->getlistadocalentario($fechainicio,$fechafin,16,$personalId,0,0,0,$personalId,0,0);
		$num_sc=$result_sc->num_rows();
		//17 Servicios Instalaciones
		$result_si = $this->ModeloAsignacion->getlistadocalentario($fechainicio,$fechafin,17,$personalId,0,0,0,$personalId,0,0);
		$num_si=$result_si->num_rows();
	?>
        <tr>
			<td><?php echo $fechainicio.' a '.$fechafin;?></td>
			<td><?php echo utf8_decode($item->tecnico);?></td>
			<td><?php echo $num_sp+$num_sc+$num_si;?></td>
			<td><?php echo $num_sp;?></td>
			<td><?php echo $num_sc;?></td>
			<td><?php echo $num_si;?></td>
			<td></td>
			<td><?php if($num_sp>0){
				$numero=array();
				foreach ($result_sp->result() as $item_sp) {
					if($item_sp->tipo==1){ //contrato
						$asignacionId=$item_sp->asignacionId;
						$serviciosresult=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$asignacionId));
						foreach ($serviciosresult->result() as $item_time) {
							if($item_time->horaserinicio!=''){
								$diferencia=$this->ModeloCatalogos->diferenciaentrehoras($item_time->horaserinicio,$item_time->horaserfin);
								//echo '<!--'.$item_time->horaserinicio.'/'.$item_time->horaserfin.'(1 '.$asignacionId.') '.$diferencia.'-->';
								$numero[]=$diferencia;
							}
							// code...
						}
					}
					if($item_sp->tipo==2){ //poliza
						$asignacionId=$item_sp->asignacionId;
						$serviciosresult=$this->ModeloCatalogos->db14_getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$asignacionId));
						foreach ($serviciosresult->result() as $item_time) {
							if($item_time->horaserinicio!=''){
								$diferencia=$this->ModeloCatalogos->diferenciaentrehoras($item_time->horaserinicio,$item_time->horaserfin);
								//echo '<!--'.$item_time->horaserinicio.'/'.$item_time->horaserfin.'(2 '.$asignacionId.')-->';
								$numero[]=$diferencia;
							}
							// code...
						}
					}
					if($item_sp->tipo==3){ //cliente
						$asignacionId=$item_sp->asignacionId;
						$serviciosresult=$this->ModeloCatalogos->db3_getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$asignacionId));
						foreach ($serviciosresult->result() as $item_time) {
							if($item_time->horaserinicio!=''){
								$diferencia=$this->ModeloCatalogos->diferenciaentrehoras($item_time->horaserinicio,$item_time->horaserfin);
								//echo '<!--'.$item_time->horaserinicio.'/'.$item_time->horaserfin.'(3 '.$asignacionId.')-->';
								$numero[]=$diferencia;
							}
							// code...
						}

					}
					if($item_sp->tipo==4){ //venta
						$asignacionId=$item_sp->asignacionId;
						$serviciosresult=$this->ModeloCatalogos->db6_getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$asignacionId));
						foreach ($serviciosresult->result() as $item_time) {
							if($item_time->horaserinicio!=''){
								$diferencia=$this->ModeloCatalogos->diferenciaentrehoras($item_time->horaserinicio,$item_time->horaserfin);
								//echo '<!--'.$item_time->horaserinicio.'/'.$item_time->horaserfin.'(4 '.$asignacionId.')-->';
								$numero[]=$diferencia;
							}
							// code...
						}
					}
				}
				
				if(count($numero)>0){
					$promedio=array_sum($numero)/count($numero);
					echo round($promedio);
				}else{
					echo 0;
				}
			}else{echo 0;}?></td>
			<td><?php if($num_sc>0){
				$numero=array();
				foreach ($result_sc->result() as $item_sc) {
					if($item_sc->tipo==1){ //contrato
						$asignacionId=$item_sc->asignacionId;
						$serviciosresult=$this->ModeloCatalogos->db10_getselectwheren('asignacion_ser_contrato_a_e',array('asignacionId'=>$asignacionId));
						foreach ($serviciosresult->result() as $item_time) {
							if($item_time->horaserinicio!=''){
								$diferencia=$this->ModeloCatalogos->diferenciaentrehoras($item_time->horaserinicio,$item_time->horaserfin);
								//echo $item_time->horaserinicio.'/'.$item_time->horaserfin.'(1 '.$asignacionId.') '.$diferencia;
								$numero[]=$diferencia;
							}
							// code...
						}
					}
					if($item_sc->tipo==2){ //poliza
						$asignacionId=$item_sc->asignacionId;
						$serviciosresult=$this->ModeloCatalogos->db13_getselectwheren('asignacion_ser_poliza_a_e',array('asignacionId'=>$asignacionId));
						foreach ($serviciosresult->result() as $item_time) {
							if($item_time->horaserinicio!=''){
								$diferencia=$this->ModeloCatalogos->diferenciaentrehoras($item_time->horaserinicio,$item_time->horaserfin);
								//echo '<!--'.$item_time->horaserinicio.'/'.$item_time->horaserfin.'(2 '.$asignacionId.')-->';
								$numero[]=$diferencia;
							}
							// code...
						}
					}
					if($item_sc->tipo==3){ //cliente
						$asignacionId=$item_sc->asignacionId;
						$serviciosresult=$this->ModeloCatalogos->db2_getselectwheren('asignacion_ser_cliente_a_d',array('asignacionId'=>$asignacionId));
						foreach ($serviciosresult->result() as $item_time) {
							if($item_time->horaserinicio!=''){
								$diferencia=$this->ModeloCatalogos->diferenciaentrehoras($item_time->horaserinicio,$item_time->horaserfin);
								//echo '<!--'.$item_time->horaserinicio.'/'.$item_time->horaserfin.'(3 '.$asignacionId.')-->';
								$numero[]=$diferencia;
							}
							// code...
						}

					}
					if($item_sc->tipo==4){ //venta
						$asignacionId=$item_sc->asignacionId;
						$serviciosresult=$this->ModeloCatalogos->getselectwheren('asignacion_ser_venta_a',array('asignacionId'=>$asignacionId));
						foreach ($serviciosresult->result() as $item_time) {
							if($item_time->horaserinicio!=''){
								$diferencia=$this->ModeloCatalogos->diferenciaentrehoras($item_time->horaserinicio,$item_time->horaserfin);
								//echo '<!--'.$item_time->horaserinicio.'/'.$item_time->horaserfin.'(4 '.$asignacionId.')-->';
								$numero[]=$diferencia;
							}
							// code...
						}
					}
				}
				if(count($numero)>0){
					$promedio=array_sum($numero)/count($numero);
					echo round($promedio);
				}else{
					echo 0;
				}
			}else{echo 0;}?></td>
			<td></td>
		</tr>
    <?php } ?>
	
</table>