// Guardamos la URL base
var base_url = $('#base_url').val();
var idUsuario = $('#idUsuario').val();
var perfilid = $('#perfilid').val();
$(document).ready(function(){

	table = $('#tabla_clientes_1').DataTable();
    $('.chosen-select').chosen({width: "100%"});
	load();
    /*
	// Mostrar mini modal de acciones
    $('#tabla_clientes tbody').on('click', 'a.minimodal', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var data = row.data();

        $('.modal').modal({
            dismissible: true
        });
        //call the specific div (modal)
        $('#modalAcciones').modal('open');
        $('#idClienteModal').val(data.id);
        $('#empresa').val(data.empresa);
    });
    */
    // Listener para Editar 
    $('#lista_editar').on('click', function () {
        var id = $('#idClienteModal').val();
        window.location = 'Clientes/edicion/'+id;
    });
    // Listener para Visualizar 
    $('#lista_visualizar').on('click', function (){
        var id = $('#idClienteModal').val();
        window.location = 'Clientes/visualizar/'+id;
    });
    // Listener para eliminar 
    $('#lista_eliminar').on('click', function (){
        var id = $('#idClienteModal').val();
        confirmaEliminarCliente(id);
    });
    // ventas 
    $('#ventas').on('click', function (){   
        var idCliente = $('#idClienteModal').val();
        
        if(perfilid==11){
            window.location = base_url+'index.php/Ventasd/add/'+idCliente;
        }else{
            window.location = base_url+'index.php/Ventas/add/'+idCliente;
        }
        
    });
    // Listener para cotizar
    $('#lista_cotiza').on('click', function (){   
        var idCliente = $('#idClienteModal').val();
        window.location = base_url+'index.php/Cotizaciones/altaCotizacion/'+idCliente;
    });
    // Listener para listado de cotizaciones
    $('#lista_cotiza_historico').on('click', function (){   
        var idCliente = $('#idClienteModal').val();
        window.location = base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+idCliente;
    });
    // Listener para agendar llamadas 
    $('#lista_llamadas').on('click', function (){
        var id = $('#idClienteModal').val();
        window.location = 'Crm/agendaLlamadas/'+id;
    });
    // Listener para agendar correos 
    $('#lista_correos').on('click', function (){
        var id = $('#idClienteModal').val();
        window.location = 'Crm/agendaCorreos/'+id;
    });
    // Listener para agendar visitas 
    $('#lista_visitas').on('click', function (){
        var id = $('#idClienteModal').val();
        window.location = 'Crm/agendaVisitas/'+id;
    });
    // Listener para agendar llamadas 
    $('#lista_agenda').on('click', function (){
        var id = $('#idClienteModal').val();
        window.location = 'Crm/revisarCalendario/'+id;
    });
    // Listener para revisar timeline 
    $('#lista_timeline').on('click', function (){
        var id = $('#idClienteModal').val();
        verTimeline(id);
    });
    // Descargar tabla
    $('#descargarExcel').on('click', function (){
         
        window.open(base_url+'Clientes/clientexls', '_blank');        
    });
});

function confirmaEliminarCliente(idCliente){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de Eliminar este Cliente? El cambio será irreversible',
        type: 'red',
        typeAnimated: true,
        buttons: 
        {
            confirmar: function (){
                $.ajax({
                    url: base_url+"index.php/clientes/eliminarClientes/"+idCliente,
                    success: function (response){
                        if(response==1){
                            fuctionalert('Éxito!','Cliente Eliminado!'); 
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/clientes"; 
                            }, 2000);
                            
                        }else{
                            fuctionalert('Error!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');  
                            setTimeout(function(){ 
                                window.location.href = base_url+"index.php/clientes"; 
                            }, 2000);      
                        }
                    },
                    error: function(response){
                        fuctionalert('Error!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');   
                        setTimeout(function(){ 
                                window.location.href = base_url+"index.php/clientes";
                            }, 2000);  
                    }
                });
            },
            cancelar: function () 
            {
            }
        }
    });
}
function load() {
    // Destruye la tabla y la crea de nuevo con los datos cargados 
    table.destroy();
    table = $('#tabla_clientes').DataTable({
        //pagingType: 'full_numbers',
           pagingType: 'input',
           //"sPaginationType": "listbox",
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        search: {
                return: true
            }, 
        "ajax": {
            "url": base_url+"index.php/Clientes/getListadoClientesPorUsuario_asic/"+idUsuario,
            type: "post"
        },
        "columns": [
            {"data": "id"},
            {"data": null,
                 render:function(data,type,row){
                    var html='';
                        html+=row.empresa;
                        html+='<span class="ont_datosfiscales ont_datosfiscales_'+row.id+'" data-cli="'+row.id+'"></span>';
                        /*
                        if(row.razones!='null' && row.razones!=null){
                            if(row.empresa!=row.razones){
                                html+=' / '+row.razones; 
                            }
                           
                        }*/
                        
                    return html;
                }
            },
            {"data": "email"},
            {"data": "estado", visible: false},
            {"data": "municipio"},
            {"data": "giro"},
            {"data": "observaciones"},
            {"data": "nombre"},             
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<a class="btn-floating red tooltipped minimodal cliente_row_'+row.id+'" data-position="top" data-delay="50" data-tooltip="Acciones" data-empresa="'+row.empresa+'" onclick="modalacciones('+row.id+')">\
                                    <i class="material-icons">build</i>\
                                  </a>';
                    if(row.bloqueo==1){
                        html+='<i class="fas fa-user-lock fa-fw clientebloqueado"></i>';
                    }
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": false
            }
        ]
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        consultar_datos_fiscales();
    });
}
function searchtable_v(){
    //var searchv =$("input[type=search]").val();
    var searchv = $("#tabla_clientes_filter input[type=search]").val();
    table.search(searchv).draw();
}
function selectcotiza(){
    var selection=$('#cotizar option:selected').val(); 
      if(selection==1){
        $('.select_familia').css('display','none');
      }else if(selection==2){
        $('.select_familia').css('display','inline');
      }
}
function verTimeline(idCliente){
    $.dialog({
            theme: 'supervan',
            title: 'Timeline',
            content: 'url: '+base_url+'index.php/prospectos/getInfo/'+idCliente,
            animation: 'scale',
            columnClass: 'medium',
            closeAnimation: 'scale',
            backgroundDismiss: true,
        });
}
function fuctionalert(titles,contents){
    $.alert({
        boxWidth: '30%',
        useBootstrap: false,
        title: titles,
        content: contents});
}
function search_cotizacion(){
    $('#modalsearchcoti').modal();
    $('#modalsearchcoti').modal('open');
}
function search_cot(){
    $('body').loader('show');
     $.ajax({
        type:'POST',
        url: base_url+"index.php/clientes/search_cot",
        data: {
          cot:$('#search_cont').val()
        },
        success: function (response){
            $('.table_searcho_cot').html(response);
            $('body').loader('hide');
        },
        error: function(response){
            fuctionalert('Error!','Algo salió mal, intente de nuevo o contacte al administrador del sistema');   
             
        }
    });
}
function table_search_cli(idcliente){
    table.search( idcliente ).draw();
}
function modalacciones(id){
    $('.modal').modal({
        dismissible: true
    });
    var empre = $('.cliente_row_'+id).data('empresa');
    //call the specific div (modal)
    $('#modalAcciones').modal('open');
    $('#idClienteModal').val(id);
    $('#empresa').val(empre);
}
function consultar_datos_fiscales(){
    var DATAf  = [];
    $(".ont_datosfiscales").each(function() {
        item = {};
        item ["cli"] = $(this).data('cli');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    if(DATAf.length>0){
        $.ajax({
            type:'POST',
            url: base_url+"Clientes/consult_df",
            data: {
                datos:aInfo
            },
            success: function (data){
                console.log(data);
                var array = $.parseJSON(data);
                console.log(array);
                $.each(array, function(index, item) {
                    $('.ont_datosfiscales_'+item.id).html(' / '+item.razon_social);
                });
            }
        });
    }
}