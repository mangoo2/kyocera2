var base_url = $('#base_url').val();
var tableu;
var tables;
$(document).ready(function() {	
	tables = $('#tabla_servicio').DataTable();
    tableu = $('#tabla_unidades').DataTable();
    loadtables();
    loadtableu();
});
function loadtables(){
	tables.destroy();
	tables = $('#tabla_servicio').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Conf_facturacion/getlistservicios",
            type: "post",
            "data": {
                'activo':0
            },
        },
        "columns": [
            {"data": "Clave"},
            {"data": "nombre"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if (row.activo==1) {
                    	var servicioc='checked';
                    }else{
                    	var servicioc='';
                    }
                    html+='<input type="checkbox" class="filled-in" id="ser_c_'+row.ServiciosId+'" onclick="activardesactivars('+row.ServiciosId+')" '+servicioc+'>';
                    html+='<label for="ser_c_'+row.ServiciosId+'"></label>';
                    return html;
                }
            },
        ],
        "order": [[ 2, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        
    });
}
function loadtableu(){
	tableu.destroy();
	tableu = $('#tabla_unidades').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Conf_facturacion/getlistunidades",
            type: "post",
            "data": {
                'activo':0
            },
        },
        "columns": [
            {"data": "Clave"},
            {"data": "nombre"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if (row.activo==1) {
                    	var unidadc='checked';
                    }else{
                    	var unidadc='';
                    }
                    html+='<input type="checkbox" class="filled-in" id="unidad_c_'+row.UnidadId+'" onclick="activardesactivaru('+row.UnidadId+')" '+unidadc+'>';
                    html+='<label for="unidad_c_'+row.UnidadId+'"></label>';
                    return html;
                }
            },
        ],
        "order": [[ 2, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]],
        
    });
}
function activardesactivars(id){
	var activo = $('#ser_c_'+id).is(':checked')==true?1:0;
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Conf_facturacion/adservicio",
        data: {
            ServiciosId:id,
            activo:activo
        },
        success:function(response){  
            
        }
    });
}
function activardesactivaru(id){
	var activo = $('#unidad_c_'+id).is(':checked')==true?1:0;
	$.ajax({
        type:'POST',
        url: base_url+"index.php/Conf_facturacion/adunidad",
        data: {
            UnidadId:id,
            activo:activo
        },
        success:function(response){  
            
        }
    });
}