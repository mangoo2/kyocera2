<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Itinerario</title>
		<meta content="width=device-width,initial-scale=1.0" name=viewport>
		<meta name="theme-color" content="#e31a2f" />
		<meta name="description" content="Sistema Kyocera">
		<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
		<!--<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/fullcalendar-6.1.8/dist/index.global.js"></script>-->
		<link href='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/fullcalendar.min.css' rel='stylesheet' />
		<link href='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<script src='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/lib/moment.min.js'></script>
		<script src='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/lib/jquery.min.js'></script>
		<script src='<?php echo base_url()?>app-assets/vendors/fullcalendar-3.2.0/fullcalendar.min.js'></script>
		<link href="<?php echo base_url()?>public/plugins/note/bootstrap.min.css" rel="stylesheet">
		<script src="<?php echo base_url()?>public/plugins/note/bootstrap.min.js"></script>
		<!--<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/fullcalendar-scheduler@1.10.4/dist/scheduler.min.js"></script>-->
		<script type="text/javascript" src="<?php echo base_url()?>public/plugins/fullcalendar/scheduler.min.js"></script>
		<!--<script type="text/javascript" src="https://static.cloudflareinsights.com/beacon.min.js"></script>-->
		<script type="text/javascript" src="<?php echo base_url()?>public/plugins/fullcalendar/beacon.min.js"></script>
		<input type="hidden" id="base_url" value="<?php echo base_url();?>">
		<style type="text/css">
			.fc-scroller.fc-time-grid-container{height: inherit !important;}
			.fc-listDay-view .fc-scroller{min-height: 371px !important;}
			.fc-list-table td {padding: 8px 7px;}
			.fc table {font-size: 12px;}
		</style>
		<script>
		var base_url = $('#base_url').val()
		$(document).ready(function($) {
			var tecnicoid = $('#tecinfog option:selected').val();
			$('#servicioscal').fullCalendar({
			defaultView: 'listDay',
			minTime: "07:00",
			maxTime: "20:00",
			header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listDay',
			},
		
			events: function(start, end, timezone, callback) {
				console.log(start.unix());
				console.log(end.unix());
			$.ajax({
			url: base_url+'Generales/lisitinerario',
			dataType: 'json',
			type:'POST',
			data: {
			start: start.unix(),
			end: end.unix(),
							tecnicoid:$('#tecinfog option:selected').val()
			},
			success: function(doc) {
				
			var datos=doc;
			var events = [];
			datos.forEach(function(r){
				console.log(r);
				console.log(r.descripcion);
		events.push({
				asignacionId: r.id,
				title: r.descripcion+' ('+r.nombre+' '+r.apellido_paterno+' '+r.apellido_materno+')',
				start: r.fecha+'T'+r.hora_inicio,
				end: r.fecha+'T'+r.hora_fin
				});
				
			});
			callback(events);
			}
			});
			},
			eventRender: function(event, element){
				
			}
			});
		});
		function tecinfog(){
			$('#servicioscal').fullCalendar( 'refetchEvents' );
		}
		setInterval(function(){
		tecinfog();
		},1800000);
		</script>
	</head>
	<body>
		<label for="tecinfog">Tecnico</label>
		<select class="browser-default form-control" id="tecinfog" onchange="tecinfog()">
			<option value="0">Todos</option>
			<?php
			$resultstec=$this->Configuraciones_model->view_session_searchTecnico();
			foreach ($resultstec as $item) { 
				echo '<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
			} ?>
		</select>
		<!--<div id='calendar'></div>-->
		<div id='servicioscal'></div>
	</body>
</html>