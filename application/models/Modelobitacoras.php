<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelobitacoras extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->DB11 = $this->load->database('other16_db', TRUE);
        $this->DB12 = $this->load->database('other12_db', TRUE);
    }
    //$this->Modelobitacoras->Insert(array('contenido'=>'','nombretabla'=>'nombretabla','idtable'=>'','tipo'=>'','personalId'=>$this->idpersonal));
    //Insert
    //update
    //delete
    function Insert($data){
        $this->DB11->insert('bitacora',$data);
        $id=$this->DB11->insert_id();
        return $id;
    }
    function Insertsession($data){
        $this->DB12->insert('bitacorasession',$data);
        $id=$this->DB12->insert_id();
        return $id;
    }
    public function Totalbitacora(){
        $this->DB11->select('COUNT(*) as total');
        $this->DB11->from('bitacora');
        $query=$this->DB11->get();
        return $query->row()->total;
    }
    function Getbitacora($params){
            $columns = array( 
                0=>'bit.bitacoraId',
                1=>'bit.contenido',
                2=>'bit.nombretabla',
                3=>'bit.idtable',
                4=>'bit.tipo',
                5=>'bit.reg',
                6=>'per.nombre'
            );

            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('bitacora bit');
            $this->db->join('personal per', 'per.personalId=bit.personalId');  
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    } 	
}

?>