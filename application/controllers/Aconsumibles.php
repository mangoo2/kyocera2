<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aconsumibles extends CI_Controller {
    public function __construct()     {
        parent::__construct();
        //$this->load->model('Clientes_model', 'model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloaconsumibles');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=44;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 43 es el id del submenu
            if ($permiso==0) {
                //redirect('Login');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    // Listado de series
    function index(){
        $data['MenusubId']=$this->submenu;
        $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,44);// 43 es el id del submenu
        if ($permiso==0) {
            redirect('Sistema');
        }
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            //$data['bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));

            //unset($_SESSION['session_bodegas']);
            /*
            if(isset($_SESSION['session_bodegas'])){
                $session_bodegas=$_SESSION['session_bodegas'];
            }else{
                unset($_SESSION['session_bodegas']);
                $session_bodegas=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
                $_SESSION['session_bodegas']=$session_bodegas->result();
            }
            */
            $session_bodegas=$this->ModeloCatalogos->view_session_bodegas();
            //var_dump($session_bodegas);
            $data['bodegasresult']= $session_bodegas;

            $data['totalconsumible']=$this->Modeloaconsumibles->totalconsumible();
            $data['totalrefacciones']=$this->Modeloaconsumibles->totalrefacciones();
            $data['totalaccesorio']=$this->Modeloaconsumibles->totalaccesorio();
            $data['totalequipos']=$this->Modeloaconsumibles->totalequipos();
            $data['perfilid']=$this->perfilid;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('series/listadoc');
            $this->load->view('footer');
            $this->load->view('series/listadoc_js');
    }
    
    function add(){
        $data['MenusubId']=$this->submenu;
        //$data['bodegas']=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
        //$data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();
        $strq="SELECT * from bodegas where activo=1";
        $resul=$this->db->query($strq);
        $data['bodegas']=$resul->result();

        $data['consumibles']=$this->ModeloCatalogos->getselectwheren('consumibles',array('status' => 1));
        $data['refacciones']=$this->ModeloCatalogos->getselectwheren('refacciones',array('status' => 1));
        $data['accesorios']=$this->ModeloCatalogos->getselectwheren('catalogo_accesorios',array('status' => 1));

        $this->load->view('header');
        $this->load->view('main',$data);
        $this->load->view('series/addcc');
        $this->load->view('footer');
        $this->load->view('series/addccjs');
    }
    function addconsumibles(){
        $data = $this->input->post(); 
        $arrayconsumibles = $data['arrayconsumibles'];
        $arrayrefacciones = $data['arrayrefacciones'];
        $arrayaccesorios = $data['arrayaccesorios'];

       $DATAc = json_decode($arrayconsumibles); 
       for ($i=0;$i<count($DATAc);$i++) {
            $whereconsu = array( 
                'bodegaId' => $DATAc[$i]->bodega, 
                'consumiblesId' => $DATAc[$i]->consumible, 
                'status'=>1 
            );  
            $resultconsu = $this->ModeloCatalogos->getselectwherenall('consumibles_bodegas',$whereconsu); 
            $consubId=0; 
            foreach ($resultconsu as $item) { 
                $consubId=$item->consubId; 
            } 
            if ($consubId==0) {
                $detallesconsumibles = array(
                        'bodegaId'=>$DATAc[$i]->bodega,
                        'consumiblesId'=>$DATAc[$i]->consumible,
                        'total'=>$DATAc[$i]->cantidad
                    );
                $idnew=$this->ModeloCatalogos->Insert('consumibles_bodegas',$detallesconsumibles);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se inserto nuevo consumible','nombretabla'=>'consumibles_bodegas','idtable'=>$idnew,'tipo'=>'Insert','personalId'=>$this->idpersonal));
                
                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>4,'id_item'=>0,'modeloId'=>$DATAc[$i]->consumible,'modelo'=>'','serieId'=>0,'bodega_d'=>$DATAc[$i]->bodega,'clienteId'=>0,'cantidad'=>$DATAc[$i]->cantidad));
            }else{
                $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','+',$DATAc[$i]->cantidad,'bodegaId',$DATAc[$i]->bodega,'consumiblesId',$DATAc[$i]->consumible,'status',1);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se actualizo consumible (se agregaron '.$DATAc[$i]->cantidad.')','nombretabla'=>'consumibles_bodegas','idtable'=>$consubId,'tipo'=>'Update','personalId'=>$this->idpersonal));
                //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                $this->ModeloCatalogos->Insert('bitacora_movimientos',array('entrada_salida'=>1,'descripcion_evento'=>'Entrada directa','personal'=>$this->idpersonal,'tipo'=>4,'id_item'=>0,'modeloId'=>$DATAc[$i]->consumible,'modelo'=>'','serieId'=>0,'bodega_d'=>$DATAc[$i]->bodega,'clienteId'=>0,'cantidad'=>$DATAc[$i]->cantidad));
            }

        }
        $DATAr = json_decode($arrayrefacciones); 
       for ($j=0;$j<count($DATAr);$j++) {
            $whereconsu = array( 
                'bodega' => $DATAr[$j]->bodega, 
                'refaccion' => $DATAr[$j]->refacciones, 
                'activo'=>1 
            );  
            $resultconsur = $this->ModeloCatalogos->getselectwherenall('inventario_refacciones',$whereconsu); 
            $consubId=0; 
            foreach ($resultconsur as $item) { 
                $consubId=$item->id; 
            } 
            if ($consubId==0) {
                $detallesconsumibles = array(
                        'bodega'=>$DATAr[$j]->bodega,
                        'refaccion'=>$DATAr[$j]->refacciones,
                        'stock'=>$DATAr[$j]->cantidad
                    );
                $idnew=$this->ModeloCatalogos->Insert('inventario_refacciones',$detallesconsumibles);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se inserto nueva refaccion','nombretabla'=>'inventario_refacciones','idtable'=>$idnew,'tipo'=>'Insert','personalId'=>$this->idpersonal));
            }else{
                $this->ModeloCatalogos->updatestock3('inventario_refacciones','stock','+',$DATAr[$j]->cantidad,'bodega',$DATAr[$j]->bodega,'refaccion',$DATAr[$j]->refacciones,'activo',1);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se actualizo refaccion (se agregaron '.$DATAr[$j]->cantidad.')','nombretabla'=>'inventario_refacciones','idtable'=>$consubId,'tipo'=>'Update','personalId'=>$this->idpersonal));
            }

        }
        $DATAac = json_decode($arrayaccesorios); 
       for ($x=0;$x<count($DATAac);$x++) {
            $whereconsu = array( 
                'bodega' => $DATAac[$x]->bodega, 
                'accesoriosid' => $DATAac[$x]->accesorios, 
                'activo'=>1 
            );  
            $resultconsu = $this->ModeloCatalogos->getselectwherenall('inventario_accesorios',$whereconsu); 
            $consubId=0; 
            foreach ($resultconsu as $item) { 
                $consubId=$item->id; 
            } 
            if ($consubId==0) {
                $detallesconsumibles = array(
                        'bodega'=>$DATAac[$x]->bodega,
                        'accesoriosid'=>$DATAac[$x]->accesorios,
                        'stock'=>$DATAac[$x]->cantidad
                    );
                $idnew=$this->ModeloCatalogos->Insert('inventario_accesorios',$detallesconsumibles);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se inserto nueva accesorio','nombretabla'=>'inventario_accesorios','idtable'=>$idnew,'tipo'=>'Insert','personalId'=>$this->idpersonal));
            }else{
                $this->ModeloCatalogos->updatestock3('inventario_accesorios','stock','+',$DATAac[$x]->cantidad,'bodega',$DATAac[$x]->bodega,'accesoriosid',$DATAac[$x]->accesorios,'status',1);
                //$this->Modelobitacoras->Insert(array('contenido'=>'Se actualizo accesorio (se agregaron '.$DATAac[$x]->cantidad.')','nombretabla'=>'inventario_accesorios','idtable'=>$consubId,'tipo'=>'Update','personalId'=>$this->idpersonal));
            }

        }
    }
    
    public function consumibles_bodega() {
        $params = $this->input->post();
        $params['perfilid']=$this->perfilid;
        $serie = $this->Modeloaconsumibles->getconsumibles_bodega($params);
        $serietotal = $this->Modeloaconsumibles->totalconsumiblesbodega($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($serietotal),  
            "recordsFiltered" => intval($serietotal),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    } 
    public function refacciones_bodega() {
        $params = $this->input->post();
        $params['perfilid']=$this->perfilid;
        $serie = $this->Modeloaconsumibles->getrefacciones_bodega($params);
        $serietotal = $this->Modeloaconsumibles->totalrefaccionesbodega($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($serietotal),  
            "recordsFiltered" => intval($serietotal),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    } 
    public function accesorio_bodega() {
        $params = $this->input->post();
        $params['perfilid']=$this->perfilid;
        $serie = $this->Modeloaconsumibles->getaccesorio_bodega($params);
        $serietotal = $this->Modeloaconsumibles->totalaccesoriobodega($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($serietotal),  
            "recordsFiltered" => intval($serietotal),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    } 
    public function equipo_bodega() {
        $params = $this->input->post();
        $params['perfilid']=$this->perfilid;
        $serie = $this->Modeloaconsumibles->getequipos_bodega($params); 
        $serietotal = $this->Modeloaconsumibles->totalequiposbodega($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($serietotal),  
            "recordsFiltered" => intval($serietotal),
            "data"            => $serie->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    } 
    function traspasar(){
        $serieid = $this->input->post('serieid');
        $cantidad = $this->input->post('cantidad');
        $bodega = $this->input->post('bodega');

        $bodegao = $this->input->post('bodegao');
        $modelo = $this->input->post('modelo');
        $motivo = $this->input->post('motivo');


        $wherec=array('consubId'=>$serieid);
        $resultcb=$this->ModeloCatalogos->getselectwheren('consumibles_bodegas',$wherec);
        foreach ($resultcb->result() as $item) {
            $rt_resultcb=$item->bodegaId;
            $rt_consumiblesId=$item->consumiblesId;
            $rt_total=$item->total;
        }
        if ($rt_total==$cantidad) {
            $whereconsu = array( 
                'bodegaId' => $bodega, 
                'consumiblesId' => $rt_consumiblesId, 
                'status'=>1 
            );  
            $resultconsu = $this->ModeloCatalogos->getselectwherenall('consumibles_bodegas',$whereconsu); 
            $consubId=0; 
            foreach ($resultconsu as $item) { 
                $consubId=$item->consubId; 
            } 
            if ($consubId==0) {
                $this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('bodegaId'=>$bodega),array('consubId'=>$serieid));
            }else{
                $this->ModeloCatalogos->updatestock3('consumibles_bodegas','total','+',$cantidad,'bodegaId',$bodega,'consumiblesId',$rt_consumiblesId,'status',1);
                $this->ModeloCatalogos->getdeletewheren('consumibles_bodegas',array('consubId'=>$serieid));
            } 

        }else{
            $this->ModeloCatalogos->updatestock('consumibles_bodegas','total','-',$cantidad,'consubId',$serieid);
            $whereconsu = array( 
                'bodegaId' => $bodega, 
                'consumiblesId' => $rt_consumiblesId, 
                'status'=>1 
            );  
            $resultconsu = $this->ModeloCatalogos->getselectwherenall('consumibles_bodegas',$whereconsu); 
            $consubId=0; 
            foreach ($resultconsu as $item) { 
                $consubId=$item->consubId; 
            } 
            if ($consubId>=1) { 
                $this->ModeloCatalogos->updatestock('consumibles_bodegas','total','+',$cantidad,'consubId',$consubId);
            }else{
                $detallesconsumibles = array( 
                    'bodegaId'=>$bodega, 
                    'consumiblesId'=>$rt_consumiblesId, 
                    'total'=>$cantidad); 
                $this->ModeloCatalogos->Insert('consumibles_bodegas',$detallesconsumibles); 
            }

        } 
        $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'modeloId'=>$rt_consumiblesId,'modelo'=>$modelo,'tipo'=>4,'cantidad'=>$cantidad,'serieId'=>$serieid,'entrada_salida'=>2,'bodega_o'=>$bodegao,'bodega_d'=>$bodega,'personal'=>$this->idpersonal,'descripcion_evento'=>'Traspaso, Motivo: '.$motivo.''));
        //$this->Modelobitacoras->Insert(array('contenido'=>'Se realizo un traspaso de consumible','nombretabla'=>'consumibles_bodegas','idtable'=>$rt_consumiblesId,'tipo'=>'Update','personalId'=>$this->idpersonal));   
    }
    function stockconsumiblesid(){
        $id =$this->input->post('id');
        $total = $this->Modeloaconsumibles->totalconsumibleid($id);
        echo $total;
    }
    function stockconsumiblesid2(){
        $id =$this->input->post('id');
        $bodega =$this->input->post('bodega');
        $total = $this->Modeloaconsumibles->totalconsumibleid2($id,$bodega);
        echo $total;
    }
    function stockrefaccionid(){
        $id =$this->input->post('id');
        $total = $this->Modeloaconsumibles->totalrefaccionesid($id);
        echo $total;
    }
    function stockrefaccionid2(){
        $id =$this->input->post('id');
        $bodega =$this->input->post('bodega');
        $total = $this->Modeloaconsumibles->totalrefaccionesid2($id,$bodega);
        echo $total;
    }
    function stockaccesorioid(){
        $id =$this->input->post('id');
        $total = $this->Modeloaconsumibles->totalaccesorioid($id);
        echo $total;
    }
    function editarconsumible(){
        $params = $this->input->post();
        $idcons = $params['idcons'];
        $total = $params['cantidad'];
        $this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total'=>$total),array('consubId'=>$idcons));
    }
    function deleteconsumible(){
        $params = $this->input->post();
        $idcons = $params['idcons'];
        $this->ModeloCatalogos->updateCatalogo('consumibles_bodegas',array('total'=>0),array('consubId'=>$idcons));
    }
    function editarrefacciones(){
        $params = $this->input->post();
        $idcons = $params['idcons'];
        $total = $params['cantidad'];
        $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('cantidad'=>$total),array('serieId'=>$idcons));
    }
     function deleterefacciones(){
        $params = $this->input->post();
        $idcons = $params['idcons'];
        $this->ModeloCatalogos->updateCatalogo('series_refacciones',array('cantidad'=>0),array('serieId'=>$idcons));
    }
    function consultconsupendient(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $ventas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $id=$DATA[$i]->id;
            $result=$this->Modeloaconsumibles->servicioscontratosactivos($id);
            $pendientes=0;
            foreach ($result->result() as $item) {
                $pendientes=$pendientes+($item->cantidad-$item->recibidos);
                
            }
            if($pendientes>0){
                $ventas[]=array('idconsu'=>$id,'pendientes'=>$pendientes);
            }
        }
        echo json_encode($ventas);

    }
    function consultrefapendient(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $ventas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $id=$DATA[$i]->id;
            $result=$this->Modeloaconsumibles->consultrefapendient($id);
            $pendientes=0;
            foreach ($result->result() as $item) {
                $pendientes=$pendientes+($item->cantidad-$item->entraron);
                
            }
            if($pendientes>0){
                $ventas[]=array('idref'=>$id,'pendientes'=>$pendientes);
            }
        }
        echo json_encode($ventas);
    }
    function consultaccependient(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $ventas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $id=$DATA[$i]->id;
            $result=$this->Modeloaconsumibles->consultaccependient($id);
            $pendientes=0;
            foreach ($result->result() as $item) {
                $pendientes=$pendientes+($item->cantidad-$item->entraron);
                
            }
            if($pendientes>0){
                $ventas[]=array('idacc'=>$id,'pendientes'=>$pendientes);
            }
        }
        echo json_encode($ventas);
    }
    function consultequipopendient(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $ventas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $id=$DATA[$i]->id;
            $result=$this->Modeloaconsumibles->consultequipopendient($id);
            $pendientes=$result->num_rows();
            if($pendientes>0){
                $ventas[]=array('ideq'=>$id,'pendientes'=>$pendientes);
            }
        }
        echo json_encode($ventas);
    }
    /*
    function verificarprioridad(){
        $datos = $this->input->post('datos');
        $tipo = $this->input->post('tipo');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $ventas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $id=$DATA[$i]->id;
            if($tipo==1){// consumible
                $result=$this->Modeloaconsumibles->verificarprioridad_c($id);
                $pendientes=$result->num_rows();
                if($pendientes>0){
                    $ventas[]=array('ideq'=>$id,'pendientes'=>$pendientes);
                }
                $result=$this->Modeloaconsumibles->verificarprioridad_c2($id);
                $pendientes=$result->num_rows();
                if($pendientes>0){
                    $ventas[]=array('ideq'=>$id,'pendientes'=>$pendientes);
                }
            }
            if($tipo==2){// Refacciones
                $result=$this->Modeloaconsumibles->verificarprioridad_r($id);
                $pendientes=$result->num_rows();
                if($pendientes>0){
                    $ventas[]=array('ideq'=>$id,'pendientes'=>$pendientes);
                }
            }
            if($tipo==3){//accesorios
                $result=$this->Modeloaconsumibles->verificarprioridad_a($id);
                $pendientes=$result->num_rows();
                if($pendientes>0){
                    $ventas[]=array('ideq'=>$id,'pendientes'=>$pendientes);
                }
            }
            if($tipo==4){//Equipo
                $result=$this->Modeloaconsumibles->verificarprioridad_e($id);
                $pendientes=$result->num_rows();
                if($pendientes>0){
                    $ventas[]=array('ideq'=>$id,'pendientes'=>$pendientes);
                }
            }
            
        }
        echo json_encode($ventas);
    }
    */
    
}
?>
