<?php
require_once dirname(__FILE__) . '/TCPDF3/examples/tcpdf_include.php'; 
require_once dirname(__FILE__) . '/TCPDF3/tcpdf.php';
$GLOBALS['configuracionCotizacion']=$configuracionCotizacion;
$GLOBALS["fecha_crea"]=$fecha_crea;
$GLOBALS["fecha_asig"]=$fecha_asig;
$GLOBALS["tipo_serv"]=$tipo_serv;
$GLOBALS["responsable_asig"]=$responsable_asig;
$GLOBALS["hora_asig"]=$hora_asig;
$GLOBALS["prioridad"]=$prioridad;
$GLOBALS["descripcion_serv_fall"]=$descripcion_serv_fall;
$GLOBALS["modelo"]=$modelo;
$GLOBALS["serie"]=$serie;
$GLOBALS["direcc_inst_serv"]=$direcc_inst_serv;
$GLOBALS["tel"]=$tel;
$GLOBALS["cel"]=$cel;
$GLOBALS["correo"]=$correo;
$GLOBALS["docum_acce"]=$docum_acce;
$GLOBALS["observa_edi"]=$observa_edi;
$GLOBALS["lectu_ini"]=$lectu_ini;
$GLOBALS["km_veh_i"]=$km_veh_i;
$GLOBALS["lectur_fin"]=$lectur_fin;
$GLOBALS["km_veh_f"]=$km_veh_f;
$GLOBALS["asesor_ser"]=$asesor_ser;
$GLOBALS["gerencia"]=$gerencia;
$GLOBALS["firma"]=$firma;
$GLOBALS["comentarioejecutiva"]=$comentarioejecutiva;
$GLOBALS["comentariotecnico"]=$comentariotecnico;
$GLOBALS["nombre_empresa"]=$nombre_empresa;
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
      /// datos completos
      $html = '';
   
      $this->writeHTML($html, true, false, true, false, '');
  }

    // Page footer
  public function Footer() {
    $html = '';

      $html .= '<table width="100%" border="0">
                  <tr>
                    <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
                  </tr>
                </table>';
      $this->writeHTML($html, true, false, true, false, '');
  }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Cuestionario');
$pdf->SetTitle('Cuestionario');
$pdf->SetSubject('Cuestionario');
$pdf->SetKeywords('Cuestionario');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '10', '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin("15");

// set auto page breaks
$pdf->SetAutoPageBreak(true, '0');

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage('P', 'A4');
$html='';
// Encabezado
$html.= '<table>
          <tr>
            <td width="25%">
              <img src="'.base_url().'app-assets/images/'.$GLOBALS['configuracionCotizacion']->logo1.'" style="padding: 1px;">
            </td>
            <td width="50%">
              '.$GLOBALS['configuracionCotizacion']->textoHeader.'
            </td>
            <td width="25%"><br><br>
              <img class="" src="'.base_url().'app-assets/images/1024_kyocera_logo_mds.png" alt="materialize logo">
            </td>
          </tr>
        </table>';
        // datos completos        
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000; text-align: center;"><span style="font-weight: bold;">INFORMACIÓN GENERAL DEL SERVICIO</span></td>
          </tr>
        </table>'; 
// datos completos        
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Fecha de creación: '.date('d-m-Y',strtotime($GLOBALS["fecha_crea"])).'</span></td>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Responsable asignado: '.$GLOBALS["responsable_asig"].'</span></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Fecha asignación: '.date('d-m-Y',strtotime($GLOBALS["fecha_asig"])).'</span></td>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Hora de asignación: '.date('g:i a',strtotime($GLOBALS["hora_asig"])).'</span></td>
          </tr>
          <tr>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Tipo de servicio: '.$GLOBALS["tipo_serv"].'</span></td>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Prioridad: '.$GLOBALS["prioridad"].'</span></td>
          </tr>
        </table>'; 
// datos en proceso   
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000; text-align: center;"><span style="font-weight: bold;">INFORMACIÓN DEL EQUIPO</span></td>
          </tr>
        </table>'; 

$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px;"><span style="font-weight: bold;">Descripción del servicio ó falla: '.$GLOBALS["descripcion_serv_fall"].'</span></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"></td>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"></td>
          </tr>
          <tr>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Modelo: '.$GLOBALS["modelo"].'</span></td>
            <td style="width: 50%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Serie: '.$GLOBALS["serie"].'</span></td>
          </tr>
        </table>'; 
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000; text-align: center;"><span style="font-weight: bold;">INFORMACIÓN DEL CLIENTE</span></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px;">
              <span style="font-weight: bold;">Nombre: '.$GLOBALS["nombre_empresa"].'</span>
            </td>
          </tr>
          <tr>
            <td style="width: 100%; font-size: 11px;">
              <span style="font-weight: bold;">Dirección de instalación/servicio: '.$GLOBALS["direcc_inst_serv"].'</span>
              
              ';
              $datosconsumiblesfolio = $this->ModeloServicio->getfoliosserviciosserie($idpdf,$serieId);
              if ($datosconsumiblesfolio->num_rows()>0) {
                $html.=' <p> <b> Consumibles</b></p>';
              }
    $html.='<ul>';
              foreach ($datosconsumiblesfolio->result() as $item) {
                $html.='<li> Modelo: '.$item->modelo.' parte:'.$item->parte.' Folio:'.$item->foliotext.'</li>';
              }
  $html.='    </ul>
            </td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"></td>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"></td>
            <td style="width: 40%; font-size: 11px; border-bottom: 1px solid #000000;"></td>
          </tr>
          <tr>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Teléfono: '.$GLOBALS["tel"].'</span></td>
            <td style="width: 30%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Celular: '.$GLOBALS["cel"].'</span></td>
            <td style="width: 40%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Correo electrónico: '.$GLOBALS["correo"].'</span></td>
          </tr>
        </table>'; 
$html.='<table width="100%">
          <tr>
            <td style="width: 100%; font-size: 11px;"><span style="font-weight: bold;">Documentos y accesorios para el ingreso: '.$GLOBALS["docum_acce"].'</span></td>
          </tr>
        </table>';
$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
// datos en procesos          
$html.='<table width="100%">
          <tr>
            <td style="width: 50%; font-size: 11px;"><span style="font-weight: bold;">Observaciones tecnico: <br>'.$GLOBALS["comentariotecnico"].'</span>
            </td>
            <td style="width: 50%; font-size: 11px;"><span style="font-weight: bold;">Observaciones ejecutiva: <br>'.$GLOBALS["comentarioejecutiva"].'</span>
            </td>
          </tr>
        </table>'; 

$html.='<table width="100%">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td style="width: 100%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;"></span></td>
          </tr>
        </table>';
$imgtext_title='x';
$imgtext='x';
//if(isset($GLOBALS["firma"])){
  if($GLOBALS["firma"]==''){
    $imgtext_title='Nombre de quien recibió';
    $imgtext=$qrecibio;
  }else{
    $fh = fopen(base_url()."uploads/firmasservicios/".$GLOBALS["firma"], 'r') or die("Se produjo un error al abrir el archivo");
        $linea = fgets($fh);
        fclose($fh);  
        $linea=preg_replace('#^data:image/[^;]+;base64,#', '', $linea) ;
      $imgtext_title='Firma del cliente';
      $imgtext='<img src="@'.$linea.'" width="355" height="160" style="border:dotted 1px black;">';
  }
//}   
$html.='<table width="100%" border="0">
          <tr><td style="width: 100%;"></td></tr> 
          <tr>
            <td rowspan="4" style="width: 30%; font-size: 11px;"><span style="font-weight: bold; text-align: center;">'.$imgtext_title.'</span>
            <div style="text-align:center;margin-top:0.05cm;padding:0.1cm;">';
    $html.=$imgtext;
            
            $produccion=$GLOBALS["lectur_fin"]-$GLOBALS["lectu_ini"];
    $html.='</div>
            </td>
            <td style="width: 25%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Lectura inicial: '.$GLOBALS["lectu_ini"].' </span></td>
            <td style="width: 25%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Lectura final: '.$GLOBALS["lectur_fin"].'</span>

            </td>
            <td style="font-size: 11px; border-bottom: 1px solid #000000;">
            <b>Produccion: '.$produccion.'</b>
            </td>
          </tr>
          <tr><td style="width: 70%;"></td></tr> 
          <tr>
            <td style="width: 35%; font-size: 11px; "><span style="font-weight: bold;">KM (Vehículo) inicial: '.$GLOBALS["km_veh_i"].'</span></td>
            <td style="width: 35%; font-size: 11px; "><span style="font-weight: bold;">KM (Vehículo): '.$GLOBALS["km_veh_f"].'</span></td>
            <td>
            </td>
          </tr>
          <tr><td style="width: 70%;"><br></td></tr> 
          <tr>
            <td style="width: 30%; font-size: 11px;"><span style="font-weight: bold; text-align: center;"></span></td>
            <td style="width: 70%; font-size: 11px; border-bottom: 1px solid #000000;"><span style="font-weight: bold;">Asesor de servicio: '.$GLOBALS["asesor_ser"].'</span></td>
          </tr>
          <tr><td style="width: 100%;"></td></tr> 
          
        </table>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Captura.pdf', 'I');

?>