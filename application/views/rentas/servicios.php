<?php 
  if ($perfilid==1 or $perfilid==8 or $perfilid==10) {
    $ocultotecnico='';
    $idpersonals=0;
  }else{
    $ocultotecnico='style="display: none;"';
    $idpersonals=$idpersonal;
  }
?>
<style type="text/css">
  table td{font-size: 12px;padding: 6px 7px;}
  .ocultarfuturaeliminacion{display: none;}
  [data-badge]::after {top: 13px !important;right: 23px !important;}
  #infordatac,#infordatap,#infordatae,#infordatav,.dataTables_length,.dataTables_info,.dataTables_paginate{display: none !important; }
  @media only screen and (max-width: 450px){
    #modalinfoincidencias, #modalitinerario{width: 99%;}
    .modal .modal-content{padding: 3px;}
    .equipo_modelo{text-align: center;}
    #table_inci{font-size: 10px;}
    .btn-accion{margin-bottom: 5px;}
    .jconfirm-box.jconfirm-hilight-shake{width: 90% !important;}
    table.dataTable.dtr-inline.collapsed>tbody>tr>td.dtr-control, table.dataTable.dtr-inline.collapsed>tbody>tr>th.dtr-control {padding-left: 27px;}
    table.dataTable tbody th, table.dataTable tbody td {padding: 7px 5px;}
    .vinc_c{margin-left: 10px;margin-right: 0px !important;}
    #modalitinerario{top: 3% !important;max-height: 88% !important;}
    .dataTables_filter{display: none;}
  }
  <?php if($fechahoy==date('Y-m-d')){ ?>

  <?php }else{ ?>
    .ocutarfechadiferente{display: none;}
  <?php } ?>
  .in_hora{margin-bottom: 0px !important;}
  .iti_dir{font-size: 11px;color: #a9abae;}
  .in_hora_f{display: none;}
  .error{color: red;}
  .emoji_triste, .emoji_feliz{height: 48px;width: 48px;}
  .emoji_triste{
    background: url(<?php echo base_url()?>public/img/triste.jpg);
    background-size: 100%;background-repeat: no-repeat;background-position: center;
  }
  .emoji_feliz{
    background: url(<?php echo base_url()?>public/img/feliz.jpg);
    background-size: 100%;background-repeat: no-repeat;background-position: center;
  }
  table .emoji_triste{
    background: url(<?php echo base_url()?>public/img/triste.jpg);
    background-size: 85%;background-repeat: no-repeat;background-position: center;
  }
  table .emoji_feliz{
    background: url(<?php echo base_url()?>public/img/feliz.jpg);
    background-size: 85%;background-repeat: no-repeat;background-position: center;
  }
  .new_servicio{background-color: #ff9c1a2e !important;}
  .vinc_c{margin-right: 10px;margin-top: 10px;}
  .vinc_c.clicked{background-color: #ff9800;border-color: #ff9800;}
  .vinc_group{margin-top: 10px;min-width: 293px;}
  .vinc_groupp{width: 111px;}
  .ocultar_tec{display: none;}
  .in_hora_i{margin-bottom: 5px !important;}
  .hrs_info{font-size: 11px;margin: 0px;}
</style>
<!-- START CONTENT -->
<section id="content" width="100%">
  <!--start container-->
  <div class="container" width="100%">
    <div class="section">
      <div class="row">
        <div class="col s12" align="right">
          <button  class="btn-bmz divmenugroup-btn"  title="Reporte servicios" onclick="obtenerserviciosreport()" style="margin-left: 8px;"><i class="fas fa-file-signature"></i></button>
          <button data-activates="chat-out5" class="btn-bmz chat-collapse  chat-out5  divmenugroup-btn"  title="Productos" onclick="div_view_pre(1)" style="margin-left: 8px;"><i class="fas fa-tasks"></i></button>
          <button data-activates="chat-out4" class="btn-bmz chat-collapse  chat-out4  divmenugroup-btn"  title="Prefacturas" onclick="div_view_pre(0)" style="margin-left: 8px;"><i class="fas fa-file-invoice"></i></button>
          <button data-activates="chat-out3" class="btn-bmz chat-collapse  chat-out3  divmenugroup-btn"  title="Refacciones"><i class="fas fa-wrench"></i></button>
          <?php 
            //$status_tec=' b-btn-secondary ';
            //$res_tec = $this->ModeloServicio->view_estatus_tecnicos(date('Y-m-d'),0);
            //if($res_tec->num_rows()>0){
              //$status_tec=' b-btn-danger ';
              ?>
                <script type="text/javascript">
                  $(document).ready(function($) {
                    //viewstatustec();
                  });
                </script>
              <?php
            //}
          ?>
          <span class="btn-notificacionstado"></span>      
        </div>
      </div>
      <div class="row">
        <div class="col s2 m2 2" style="text-align: center;"><a href="<?php echo base_url().'index.php/Configuracionrentas/servicios?fech='.date("Y-m-d", strtotime($fechahoy. "-1 day"));?>"><i class="far fa-arrow-alt-circle-left" style="font-size: 41px;"></i></a></div>
        <div class="col s8 m8 8" style="text-align: center;"><h3><?php echo $dial;?></h3><input type="input"  id="calendarinput" value="<?php echo $fechahoy?>"  style="display: none;"></div>
        <div class="col s2 m2 2" style="text-align: center;"><a href="<?php echo base_url();?>index.php/Configuracionrentas/servicios?fech=<?php echo date("Y-m-d", strtotime($fechahoy. "+1 day"))?>"><i class="far fa-arrow-alt-circle-right" style="font-size: 41px;"></i></a></div> 
      </div>
      <div class="row">
        <div class="col s12" <?php echo $ocultotecnico;?> >
          <label for="tecnico">Seleccionar Tecnico</label><select id="tecnico" name="planta_sucursal" class="browser-default form-control-bmz" onchange="cargainforma()"><?php foreach ($resultstec as $item) { ?><option value="<?php echo $item->personalId;?>" <?php if ($idpersonals==$item->personalId) { echo 'selected';} ?> ><?php echo $item->tecnico;?></option><?php } ?></select>
        </div>
      </div>
      <div class="row">
        <div class="col s12 m12 24">
          <div class="card-panel">
            <div class="row">
              <div class="col s12 infoser" style="margin-bottom: 5px;"><a class="btn waves-effect waves-light btn-bmz col s12 has-badge-rounded has-badge-danger infordatac" id="infordatac" data-badge="0">Rentas/contratos</a></div><div class="col s12 infoser" style="margin-bottom: 5px;"><a class="btn waves-effect waves-light btn-bmz col s12 has-badge-rounded has-badge-danger infordatap" id="infordatap" data-badge="0">Pólizas</a></div><div class="col s12 infoser" style="margin-bottom: 5px;"><a class="btn waves-effect waves-light btn-bmz col s12 has-badge-rounded has-badge-danger infordatae" id="infordatae" data-badge="0">Eventos</a></div><div class="col s12 infoser" style="margin-bottom: 5px;"><a class="btn waves-effect waves-light btn-bmz col s12 has-badge-rounded has-badge-danger infordatav" id="infordatav" data-badge="0">Ventas</a></div>
              <div class="col s12 infoser" style="margin-bottom: 5px;">
                <button class="btn waves-effect waves-light btn-bmz col s12 has-badge-rounded has-badge-danger infordatag" id="infordatag" data-badge="0" data-itinerario="0">Servicios</button>
                <a class="b-btn b-btn-primary" onclick="itinerario()" title="Itinerario" style="margin-top: 15px; width: 100%;"><i class="fas fa-list-ul"></i> Itinerario</a>
              </div>
            </div>
            <div class="row">
              <div class="col s12">
                <table class="table" id="tableunidades"><thead><tr><th>Modelo</th><th>Marca</th><th>Placas</th><th></th></tr></thead><tbody></tbody></table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

<div id="modalinfoincidencias" class="modal">
  <div class="modal-content ">
    <h4 class="equipo_modelo">Incidencias</h4>
    <div class="row"> 
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#lisincidencias">Incidencias</a></li>
          <li class="tab col s3"><a  href="#regincidencia">Registrar</a></li>
        </ul>
      </div>
      <div id="lisincidencias" class="">
        <div class="col s12 addtablelistincidencias"></div>
      </div>
      <div id="regincidencia">
        <div class="col s12"><input type="file" id="files" name="files[]" multiple > </div>
        <div class="col s12"><textarea class="form-control-bmz" id="detalle" placeholder="Descripcion del incidente" style="height:auto !important;"></textarea></div>
        <div class="col s12" ><a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow " id="registrarincidente">Registrar</a></div>
      </div>       
    </div>                   
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>    
  </div>
</div>

<div id="modalitinerario" class="modal">
  <div class="modal-content ">
    <h4 class="equipo_modelo">Itinerario</h4>
    <div class="row"> 
      <form id="formser"><table class="table striped" id="table_clientesininerario"><tbody class="tbcliii"></tbody></table></form>
      <div class="col s6 m3">
        <a class="btn-bmz green gradient-shadow ocutarfechadiferente" id="registraritinerario">Registrar</a>
        <!--<a class="btn-bmz blue gradient-shadow" onclick="limpiaritinerario()">limpiar</a>-->
      </div>     
    </div>
    <div class="row">
      <form class="col s12 tableitinerario" id="form_t_itinerario"></form>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>    
  </div>
</div>