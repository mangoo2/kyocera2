<input type="hidden" id="perfilid" value="<?php echo $perfilid; ?>">
<style type="text/css">
  <?php if($perfilid!=1){ ?>
    .soloadministradores{
      display: none;
    }
  <?php } ?>
  #detalle{
    margin-top: 10px;
    height: auto !important;
  }
  .kv-file-zoom{
    display: none;
  }
</style>
<?php 
  $my_date = new DateTime();

  $my_date->modify('first day of this month');
  $primerdia = $my_date->format('Y-m-d');

  $my_date->modify('last day of this month');
  $ultimodia = $my_date->format('Y-m-d');
?>
<!-- START CONTENT -->
    <section id="content">
      <!--start container-->
        <div class="container">
          <div class="section">

          <input id="base_url" type="hidden" name="base_url" class="infogen" value="<?php echo base_url(); ?>"  data-primerdia="<?php echo $primerdia;?>" data-ultimodia="<?php echo $ultimodia;?>"
          >

          <div id="table-datatables">
            <br>
                <h4 class="header">Listado de unidades</h4>
                <div class="row " >
                  
                  <div class="col s10  soloadministradores">
                    <a class="b-btn b-btn-success" href="<?php echo base_url(); ?>index.php/Listado_unidades/registro">Nuevo</a>
                  </div>
                  <div class="col s2  soloadministradores">
                    <a class="b-btn b-btn-success" onclick="reporte()">Reporte</a>
                  </div>

                  <div class="col s12">
                
                    <table id="tabla_registros" class="table" cellspacing="0">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Foto</th>
                          <th>Modelo</th>
                          <th>Marca</th>
                          <th>Placas</th>
                          <th>Últ.km</th>
                          <th>Tipo.U</th>
                          <th>Vigencia de póliza</th>
                          <th>Próximo servicio</th>
                          <th>Tecnico asignado</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>  
<div id="modalinfoequipo" class="modal">
  <div class="modal-content ">
    <h4 class="equipo_modelo">equipo_modelo</h4>
    
    <div class="row"> 
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#test1">Imagenes</a></li>
          <li class="tab col s3"><a  href="#test2">Caracteristicas</a></li>
        </ul>
      </div>
      <div id="test1" class="">
        <div class="col s12">
          <!--<input type="file" id="files" name="files[]" multiple accept="image/*"> -->
        </div>
        <div class="col s12 galeriimg" style="margin-top: 10px;"></div>
      </div>
      <div id="test2">
        <div >
          <form id="form_catacteristicas">
            <input type="hidden" name="id" id="idfc">
            <input type="hidden" name="idequipo" id="idequipofc">
            <div class="col s2">
              <label>Tipo</label>
              <input type="text" name="name" id="name" class="form-control-bmz">
            </div>
            <div class="col s2">
              <label>Descripcion</label>
              <input type="text" name="descripcion" id="descripcion" class="form-control-bmz">
            </div>
            <div class="col s2">
              <input type="checkbox" class="filled-in" id="general" name="general" value="1">
              <label for="general" style="margin-top: 21px;">General</label>
            </div>
            <div class="col s2">
              <label>Orden</label>
              <input type="number" name="orden" id="orden" class="form-control-bmz">
            </div>
            <div class="col s2">
              <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow" id="save_fc" style="margin-top: 21px;">Guardar</a>
              <a class="btn-bmz waves-effect waves-light gradient-45deg-red-teal gradient-shadow" id="cancelar_fc" onclick="fc_limpiar()" style="display: none;">Cancelar</a>
            </div>
          </form>
        </div>
        <div class="lis_catacter">
          
        </div>
      </div>       
    </div>                   
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>    
  </div>
</div>


<div id="modalinfoincidencias" class="modal">
  <div class="modal-content ">
    <h4 class="equipo_modelo">Incidencias</h4>
    
    <div class="row"> 
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s3"><a class="active" href="#lisincidencias">Incidencias</a></li>
          <li class="tab col s3"><a  href="#regincidencia">Registrar</a></li>
        </ul>
      </div>
      <div id="lisincidencias" class="">
        <div class="col s12 addtablelistincidencias">
          
        </div>
        
      </div>
      <div id="regincidencia">
        <div class="col s12">
          <input type="file" id="files" name="files[]" multiple > 
        </div>
        <div class="col s12">
          <textarea class="form-control-bmz" id="detalle" placeholder="Descripcion del incidente" style="height:auto !important;"></textarea>
        </div>
        <div class="col s12" >
          <a class="btn-bmz waves-effect waves-light gradient-45deg-green-teal gradient-shadow " id="registrarincidente">Registrar</a>
        </div>
        
      </div>       
    </div>                   
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>    
  </div>
</div>
<div id="modalitinerario" class="modal">
  <div class="modal-content ">
    <h4 class="equipo_modelo">Itinerario</h4>
    <div class="row">
      <div class="col s12 tableitinerario">
      </div>
    </div>

  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-red gray btn-flat ">Cerrar</a>    
  </div>
</div>
<div class="row" style="display:none;">
  <select class="form-control-bmz browser-default" id="se_eje">
    <option value="0">Todo</option>
    <?php foreach ($resultstec as $item) { 
      echo '<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
      }  ?>
  </select>
  <select class="form-control-bmz browser-default" id="se_uni">
    <option value="0">Todo</option>
    <?php foreach ($resultsuni->result() as $item) { 
      echo '<option value="'.$item->id.'">'.$item->modelo.'</option>';
      }  ?>
  </select>
</div>