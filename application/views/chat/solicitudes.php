<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<script type="text/javascript" src="<?php echo base_url(); ?>app-assets/vendors/jquery-3.3.1.min.js"></script>
	<link href="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.min.css" rel="stylesheet"  >
    <script src="<?php echo base_url()?>public/plugins/bootstrap/bootstrap.bundle.min.js"  ></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/style_chat.css?v=<?php echo date('Ymd');?>">  
    <script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/loading/jquery.loading.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loading/demo.css">
	<link href="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
	<script src="<?php echo base_url(); ?>app-assets/vendors/confirm/jquery-confirm.min.js"></script>
	<style type="text/css">
		.jconfirm-box.jconfirm-hilight-shake.jconfirm-type-red.jconfirm-type-animated{
			width: 90% !important;
		}
	</style>
    <script type="text/javascript">
    	$(document).ready(function($) {
    		$('.vinculo_a').click(function(event) {
    			var url =$(this).data('url');
    			var ini =$(this).data('ini');
    			if(ini==0){
	    			if(url!='#'){
	    				//$('body').loading({theme: 'dark',message: 'Procesando...'});
	    				$.confirm({
					        boxWidth: '30%',
					        useBootstrap: false,
					        icon: 'fa fa-warning',
					        title: 'Atención!',
					        content: 'Confirma inicializar el chat?',
					        type: 'red',
					        typeAnimated: true,
					        buttons:{
					            confirmar: function (){
					            	console.log(url);
					                 $(location).attr('href',url);
					                    
					            },
					            cancelar: function () {
					                
					            }
					        }
					    });		
	    			}
	    		}else{
	    			$(location).attr('href',url);
	    		}
    			
    			
    		});
    	});
    </script>
</head>
<body class="container">
	<?php 
		$html='';
		$num_reg=$result->num_rows()+$result1->num_rows();
		if($num_reg==0){
			$html.='<div class="row">
			<div class="col-md-12 min-height">

				<a class="option_ct bloques_cli shadowx " style="text-align:center" >
					<span style="color:red"><b>Sin solicitudes</b></span>
					
				</a>
			</div>
		</div>';
		}
		foreach ($result->result() as $item) {
			$html.='<div class="row">
			<div class="col-md-12 min-height">

				<a class="option_ct bloques_cli shadowx vinculo_a" data-url="'.base_url().'Rchat/confirm/'.$item->id.'/'.$dep.'/'.$per.'?sis=1" data-ini="0">
					<div class="cls_reg"><span >'.$item->reg.'</span></div>
					<span style="color:red">'.$item->nombre.'</span><br><span style="color:#817b7b">'.$item->motivo.'</span>
					
				</a>
			</div>
		</div>';
		}
		foreach ($result1->result() as $item) {
			$html.='<div class="row">
			<div class="col-md-12 min-height">

				<a class="option_ct bloques_cli iniciada shadowx vinculo_a" data-url="'.base_url().'Rchat/confirm/'.$item->id.'/'.$dep.'/'.$per.'?sis=1" data-ini="1">
					<div class="cls_reg"><span >'.$item->reg.'</span></div>
					<span style="color:red">'.$item->nombre.'</span><br><span style="color:#817b7b">'.$item->motivo.'</span>
					
				</a>
			</div>
		</div>';
		}
		echo $html;
	?>
</body>
</html>