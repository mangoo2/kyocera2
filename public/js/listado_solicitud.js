var base_url = $('#base_url').val();
var table_tipo;
var numsuma = 0;
$(document).ready(function() {
    //$('.chosen-select').chosen({width: "100%"});
    table_tipo=$(".tabla_compras").DataTable();
    setTimeout(function(){ 
      datos_compras();
    }, 1000);
    
    
});
function datos_compras(){
  var fecha = $('#fecha').val();
    var tipo = $('#tipo option:selected').val();
    var prioridad = $('#prioridad option:selected').val();
  table_tipo.destroy();
  table_tipo=$(".tabla_compras").DataTable({
    responsive: !0,
    "bProcessing": true,
    "serverSide": true,
    search: {
                return: true
            }, 
    "ajax": {
       "url": base_url+"index.php/Compras/getDatacomprassol",
       type: "post",
       "data": {
                "tipo": tipo,
                "prioridad": prioridad,
        },
        error: function(){
           $("#tabla_compras").css("display","none");
        }
    },
    "columns": [
        {"data": null,
            render: function(data,type,row){ 
                var html = "";
                    if(row.tipo==1){
                        html ='Consumible';
                    }
                    if(row.tipo==2){
                        html ='Accesorios';
                    }
                    if(row.tipo==3){
                        html ='Equipos';
                    }
                    if(row.tipo==4){
                        html ='Refacción';
                    }
                return html;
            }
        },
        {"data": 'cantidad'},
        {"data": 'modelo'},
        {"data": null,
            render: function(data,type,row){ 
                var html = "";
                if(row.prioridad>0){
                    html = "Prioridad:"+row.prioridad;
                }

                    //html = row.personal+' '+row.apellido_paterno+' '+row.apellido_materno;
                return html;
            }
        },
        {"data": null,
            render: function(data,type,row){ 
                var html = "";
                    html += '<div class="botes_group">';
                    //html +=row.idCotizaciones;
                    var idCoti = row.idCotizaciones;
                    var arr = idCoti.split(',');
                    for (var i = 0; i < arr.length; i++) {
                        //html+=arr[i];
                        html+='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Vizualizar" href="'+base_url+'index.php/Cotizaciones/documento/'+arr[i]+'" title="Cotizacion: '+arr[i]+'" target="_black">'+arr[i]+'</a> ';
                    }
                    html += '</div>';
                return html;
            }
        },
        {"data": null,
          render: function(data,type,row){ 
            var html='';
            html='<div class="inventarios inventarios_'+row.tipo+'_'+row.idpro+'" data-tipo="'+row.tipo+'" data-pro="'+row.idpro+'">';
            return html;
          }
        },
    ],
    "order": [[ 0, "desc" ]],
  }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_v()"></i>')
        verificarstock();
    });
}
function searchtable_v(){
    var searchv =$("input[type=search]").val();
    table_tipo.search(searchv).draw();
}
function verificarstock(){
    var DATAf  = [];
    $(".inventarios").each(function() {
        item = {};
        item ["tipo"] = $(this).data('tipo');
        item ["pro"] = $(this).data('pro');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    //console.log(aInfo);
    var datos=aInfo;
    
    $.ajax({
        type:'POST',
        url: base_url+"Compras/verificarstockpro",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            $.each(array, function(index, item) {
                    $(".inventarios_"+item.tipo+"_"+item.pro).html(item.stock);
                
            });
        }
    });
}