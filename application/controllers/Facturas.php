<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Facturas extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->submenu=63;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 52 es el id del submenu
            if ($permiso==0) {
                redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    function index(){
        $data['MenusubId']=$this->submenu;
            $this->load->view('header');
            $this->load->view('main');
            $this->load->view('expofacturas/exportfacturas');
            $this->load->view('footer');
    }
    function exportar($fechaini,$fechafin,$tiporeporte,$cli=0,$emp,$tpago=0){
        //http://localhost/kyocera2/index.php/Facturaslis/exportar/2022-02-01/2022-02-28/0
        $params['emp']=$emp;
        $params['finicio']=$fechaini;
        $params['ffin']=$fechafin;
        $params['tiporeporte']=$tiporeporte;
        $params['cli']=$cli;
        $getdata = $this->Modelofacturas->getfacturasexport($params);
        $data['getdata']=$getdata;
        $data['tpago']=$tpago;
        $this->load->view('expofacturas/exportfacturasexpo',$data);
    }
    function exportarrc($fechaini,$fechafin,$tiporeporte,$emp){
        //http://localhost/kyocera2/index.php/Facturaslis/exportar/2022-02-01/2022-02-28/0
        $params['emp']=$emp;
        $params['finicio']=$fechaini;
        $params['ffin']=$fechafin;
        $params['tiporeporte']=$tiporeporte;
        $getdata = $this->Modelofacturas->getfacturasexportc($params);
        $data['getdata']=$getdata;
        $this->load->view('expofacturas/exportfacturasexpoc',$data);
    }
    function exportarfc($fechaini,$fechafin,$emp){
        $params['emp']=$emp;
        $params['finicio']=$fechaini;
        $params['ffin']=$fechafin;
        $getdata = $this->Modelofacturas->getfacturasexportarfc($params);
        $data['getdata']=$getdata;
        $this->load->view('expofacturas/facturasexportarfc',$data);
    }
    function refacturarperiodo($periodo,$contrato,$factura){

        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura',array('factura_renta'=>0,'factura_excedente'=>0),array('prefId'=>$periodo));
        $this->ModeloCatalogos->updateCatalogo('alta_rentas_prefactura_d',array('statusfacturar'=>0,'statusfacturae'=>0),array('prefId'=>$periodo));

        $resultf=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
        foreach ($resultf->result() as $item) {
            $uuid=$item->uuid;
        }
        $urlredirigir="Configuracionrentas/Facturar/$periodo/$contrato?facid=$factura&uuid=$uuid";
        redirect($urlredirigir);
    }
    function obtenerfoliouuid(){
        $factura = $this->input->post('idfactura');
        $resultf=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
        $uuid='';
        foreach ($resultf->result() as $item) {
            $uuid=$item->uuid;
        }
        $resultf=$this->ModeloCatalogos->getselectwheren('f_facturas',array('f_r_uuid'=>$uuid));
        $uuidnew='';
        foreach ($resultf->result() as $item) {
            $uuidnew=$item->uuid;
        }
        echo $uuidnew;
    }
    


    
}
?>
