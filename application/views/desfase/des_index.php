<style type="text/css">
  #t_iti td{
    font-size: 13px;
  }
  .info_clipro{
    float: inline-end;
  }
  .h_hrs{
    color: grey;
    font-size: 12px;
  }
</style>
<!-- START CONTENT -->
    <section id="content">
    	<!--start container-->
      	<div class="container">
      		<div class="section">
      		    <div>
                <h4 class="header">Tablero de desfase</h4>
                <div class="row">
                  <form id="form_reporte">
                    <div class="col s3">
                      <label>Ejecutivo</label>
                      <select class="browser-default form-control-bmz" id="eje" name="eje" required>
                        <option value="">Seleccionar tecnico</option>
                        <?php foreach ($personalrows as $item) { 
                            echo '<option value="'.$item->personalId.'">'.$item->tecnico.'</option>';
                        } ?>
                      </select>
                    </div>
                    <div class="col s3">
                      <label>Fecha Inicial</label>
                      <input type="date" class="form-control-bmz" id="fecha" name="fecha" value="<?php echo date('Y-m-d');?>" min="<?php echo date("Y-m-d",strtotime(date('Y-m-d')."- 1 month")); ?>" required onchange="calcu_mes()">
                    </div>
                    <div class="col s3">
                      <label>Fecha final</label>
                      <input type="date" class="form-control-bmz" id="fechaf" name="fechaf" value="<?php echo date('Y-m-d');?>" max="<?php echo date('Y-m-d');?>" required onchange="calcu_mes()">
                    </div>
                  </form>

                  <div class="col s3">
                    <button class="b-btn b-btn-success" onclick="generar()" style="margin-top: 20px;" title="Consultar desfaces">Consultar</button>
                  </div>
                </div>
                <div class="carga_info">
                  
                </div>
              </div>
          </div>  
        </div>
    </section>
