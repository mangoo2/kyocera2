// Obtener la BASE URL
var base_url = $('#base_url').val();
// Saber si es solo Visualización
var tipoVista = $("#tipoVista").val();


$(document).ready(function(){
    select_tipo();
    $('.chosen-select').chosen({width: "90%"});
    // Si el tipo de vista no es Alta, mostramos los datos de usuario
    if(tipoVista != 1){
        muestraDatosUsuario();
    }
    
    // Conforme cambie el tipo de Empleado, mostraremos o no los campos extras
    $( "#tipo" ).change(function() {
      muestraDatosUsuario();
    });

    // Muestra el Modal para restablecer contraseña
    $( "#botonRestablece" ).click(function() {
        $('.modal').modal({
            dismissible: true
        });
        $('#modalRestableceContra').modal('open');
    });

    // Ejecuta la acción de restablecer la contraseña
    $( "#guardaContrasenaRestablecida" ).click(function() {
        restableceContrasena();
    });

    // En caso de que sea solo visualizar, deshabilita todo
    if(tipoVista == 3)
    {
        $("input").prop('disabled', true);
        $("textarea").prop('disabled', true);
        $("select").prop('disabled', true);
        $("checkbox").prop('disabled', true);
        $("button").prop('disabled', true);
    }
});

// Muestra u oculta los datos de usuario, dependiendo el tipo de empleado que se registra
function muestraDatosUsuario(){
    if($( "#tipo" ).val()==1){
        $('#datosUsuario').attr('style','display: none');
    }else{
        $('#datosUsuario').attr('style','display: inline');
    }
}

// Obtiene la nueva contraseña del usuario para restablecerla
function restableceContrasena(){
    var idUsuario = $('#usuarioId').val();
    var contrasenaRestablecida = $('#contrasena_restablecida').val();

    if(contrasenaRestablecida.trim()==''){
        $.alert({
            title: 'Atención!',
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            type: 'red',
            typeAnimated: true,
            content: 'El campo está vacio. Si deseea restablecer la contraseña ingrese un valor.',
        });
    }else{
        var datos = {   idUsuario: idUsuario,
                        contrasenaRestablecida: contrasenaRestablecida
                };
        $.ajax({
            type:'POST',
            url: base_url+'index.php/empleados/restableceContrasena',
            data: datos,
            success:function(response){
                // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                if(response!=0){
                    swal({ title: "Éxito",
                        text: "La contrasena se ha reseteado de manera correcta",
                        type: 'success',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    });
                }
                // En caso contrario, se notifica
                else
                {
                    swal({ title: "Error",
                        text: "La contrasena no pudo resetearse. Contacte al administrador del sistema",
                        type: 'error',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    });
                }
            }
        });
    }
    
}
function formsave(){
    var formsave = $('#empleados-form');
    if(formsave.valid()){
        var datos = formsave.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'index.php/empleados/insertaActualizaEmpleado',
            data: datos,
            success:function(data){
                // Si existe el usuario, mostramos el mensaje de entrada y redirigimos
                if(parseInt(data)>0){
                    swal({ title: "Éxito",
                        text: "El empleado se ha registrado de manera correcta",
                        type: 'success',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    });
                    
                    setTimeout(function(){ 
                        window.location.href = base_url+'index.php/empleados'; 
                    }, 2000);
                }
                // En caso contrario, se notifica
                else{
                    swal({ title: "Error",
                        text: "El empleado no pudo guardarse. Contacte al administrador del sistema",
                        type: 'error',
                        showCancelButton: false,
                        allowOutsideClick: false,
                    });
                }
            }
        });
    }
}
function select_tipo(){
    var tipo = $('#tipo option:selected').val();
    if(tipo==1){
        $("#usuario").prop('required', false);
        $("#contrasena").prop('required', false);
        $("#perfilId").prop('required', false);
    }
    if(tipo==2){
        $("#usuario").prop('required', true);
        $("#contrasena").prop('required', true);
        $("#perfilId").prop('required', true);
    }
}