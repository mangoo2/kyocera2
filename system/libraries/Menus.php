<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	function __construct(){}

	class CI_Menus
	{
		public function __construct(){
			$this->CI =& get_instance();
		}
		
		public function getMenus($idPerfil)
		{
			$this->CI->load->model('login_model');

			$menus = $this->CI->login_model->getMenus($idPerfil);

	        $submenusArray = array();
	        // Obtenemos y asignamos los submenus 
	        foreach ($menus as $item)
	        {
	            array_push($submenusArray, $this->CI->login_model->submenus($idPerfil,$item->MenuId));
	        }
	        // Los asignamos al arary que se envía a la vista
	        $data['menus'] = $menus;
	        $data['submenusArray'] = $submenusArray;

			return $data;
		}
		
	}
?> 

