<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Empleados_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

 	public function getListadoEmpleados()
    {
        $sql = "SELECT
				personal.personalId,
				personal.nombre,
				personal.sexo,
				personal.email,
				personal.apellido_materno
				FROM personal
				WHERE estatus!=0";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insertaEmpleado($data) 
    {
        $this->db->insert('personal', $data);
        return $this->db->insert_id();
    }

    public function getEmpleadoPorId($idEmpleado)
    {
        $sql = "SELECT
                e.*,
                u.UsuarioID,
                u.perfilId,
                u.Usuario,
                u.contrasena
                FROM personal as e
                LEFT JOIN usuarios as u 
                ON e.personalId=u.personalId
                WHERE e.personalId=".$idEmpleado;
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function actualizaEmpleado($data, $id) 
    {
        $this->db->set($data);
        $this->db->where('personalId', $id);
        return $this->db->update('personal');
    }

    public function insertaUsuario($data) 
    {
        $this->db->insert('usuarios', $data);
        return $this->db->insert_id();
    }
   
    public function actualizaUsuario($data, $id) 
    {
        $this->db->set($data);
        $this->db->where('UsuarioID', $id);
        return $this->db->update('usuarios');
    }
}