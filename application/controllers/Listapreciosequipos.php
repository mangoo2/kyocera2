<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Listapreciosequipos extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('equipos_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->load->library('menus');
        $this->submenu=18;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 43 es el id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('login');
        }
    }

    // Listado de Precios de Equipos
    function index(){
        $data['MenusubId']=$this->submenu;
            // Obtiene los menús disponibles del sistema dependiendo el perfil, ayudado de la libreria "MENUS"
            $menus = $this->menus->getMenus($this->session->userdata('perfilid'));
            $data['menus'] = $menus['menus'];
            $data['submenusArray'] = $menus['submenusArray'];
            // Cargamos las vistas
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('preciosequipos/listado');
            $this->load->view('preciosequipos/listado_js');
            $this->load->view('footer');    
    }

    // Obtener el JSON del listado de equipos para enviar a la tabla
    function getListaPreciosEquipo(){
        $precios = $this->equipos_model->getListaPreciosEquipo();
        $json_data = array("data" => $precios);
        echo json_encode($json_data);
    }

    // Actualizar un precio que venga de la tabla
    function actualizaPrecio(){
        $data = $this->input->post();
        // La variable KEY traerá el ID del equipo a actualizar
        $key = key($data["data"]);

        // Comparamos si lo que trae es el precio en Dólares, para hacer los cálculos
        if(key($data["data"][$key])=='precioDolares') {
            // Obtenemos el Tipo de Cambio
            $cambio = $this->equipos_model->getTipoCambioEquipos();
            
            // Obtenemos los descuentos
            $descuentos = $this->equipos_model->getDescuentosEquipos();
            $descuentoEquipos = $descuentos[0]->descuento;

            // Convertimos a Pesos 
            $tipoCambio = $cambio[0]->cambio;
            $precio = $data["data"][$key]['precioDolares'];
            $precioPesos =  (($precio*$tipoCambio)*(100-$descuentoEquipos))/100;


            // Obtenemos los porcentajes de Ganancia para equipos
            $porcentajesGanancia = $this->equipos_model->getPorcentajeGananciaEquipos();
            // Guardamos cada uno de los porcentajes correspondientes
            $porcentajeVenta = $porcentajesGanancia[0]->gananciaVenta;
            $porcentajeRenta = $porcentajesGanancia[0]->gananciaRenta;
            $porcentajePoliza = $porcentajesGanancia[0]->gananciaPoliza;
            $porcentajeRevendedor = $porcentajesGanancia[0]->gananciaRevendedor;    

            // Hacemos los descuentos sobre los precios con Ganancia
            $totalDescuento = ($descuentoEquipos * $precioPesos) / 100;
            $precioConDescuentoVenta = $precioPesos/((100-$porcentajeVenta)/100);
            $precioConDescuentoRenta = $precioPesos/(1-($porcentajeRenta/100));
            $precioConDescuentoPoliza = $precioPesos/(1-($porcentajePoliza/100));
            $precioConDescuentoRevendedor = $precioPesos/(1-($porcentajeRevendedor/100));;
            
            //$data["data"][$key]['costo_venta'] = $precioConDescuentoVenta;
            //$data["data"][$key]['costo_renta'] = $precioConDescuentoRenta;
            //$data["data"][$key]['costo_poliza'] = $precioConDescuentoPoliza;
            //$data["data"][$key]['costo_revendedor'] = $precioConDescuentoRevendedor;
            $data["data"][$key]['costo_pesos'] = $precioPesos;
            //$data["data"][$key]['porcentaje_ganancia'] = $porcentajeVenta;
            $data["data"][$key]['descuento'] = $totalDescuento;
        }

        // Se actualiza elregistro        
        $idActualizado = $this->equipos_model->update_equipo($data["data"][$key], $key);
        $this->Modelobitacoras->Insert(array('contenido'=>'Se Actualizo Lista de precios equipos','nombretabla'=>'equipos','idtable'=>0,'tipo'=>'Update','personalId'=>$this->idpersonal)); 

        // Sí se actualizó bien, se regresan los datos a la tabla
        if($idActualizado==$key){
            // Se ibtienene todos los datos del listado
            $preciosActualizados = $this->equipos_model->getPreciosEquipoPorId($idActualizado);
            $preciosActualizadosAux = (array) $preciosActualizados[0];
            
            // Se arma el array
            $dataRetorno = array($preciosActualizadosAux ); 
            
            // Se crea el data y se regresa en JSON
            $datosRetorno = array("data" => $dataRetorno);
            echo json_encode($datosRetorno);
        }
    }

}