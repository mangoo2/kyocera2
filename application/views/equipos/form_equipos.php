<?php 
// Validamos si es creación o edición, para así cargar los datos respectivamente
if(isset($equipo))
{
    $idEquipo = $equipo->id;
    $modelo = $equipo->modelo;
    $especificaciones = $equipo->especificaciones;
    $foto = $equipo->foto;
    $especificaciones_tecnicas = $equipo->especificaciones_tecnicas;
    $costo_venta = $equipo->costo_venta;
    $costo_renta = $equipo->costo_renta;
    $categoriaId = $equipo->categoriaId;
    $costo_poliza = $equipo->costo_poliza;
    $stock = $equipo->stock;
    $idfamilia= $equipo->idfamilia; 
    $excedentecolor = $equipo->excedentecolor;
    //$costo_total = $equipo->costo_total;
    $costo_toner_black=$equipo->costo_toner_black;
    $rendimiento_toner_black=$equipo->rendimiento_toner_black;
    $costo_toner_cmy=$equipo->costo_toner_cmy;
    $rendimiento_toner_cmy=$equipo->rendimiento_toner_cmy;
    $costo_unidad_imagen=$equipo->costo_unidad_imagen;
    $rendimiento_unidad_imagen=$equipo->rendimiento_unidad_imagen;
    $costo_garantia_servicio=$equipo->costo_garantia_servicio;
    $rendimiento_garantia_servicio=$equipo->rendimiento_garantia_servicio;
    $costo_pagina_monocromatica=$equipo->costo_pagina_monocromatica;
    $costo_pagina_color=$equipo->costo_pagina_color;

    $costo_revendedor = $equipo->costo_revendedor;
    $pag_monocromo_incluidos = $equipo->pag_monocromo_incluidos;
    $pag_color_incluidos = $equipo->pag_color_incluidos;
    $excedente = $equipo->excedente;
    $noparte = $equipo->noparte;
}
else
{
    $idEquipo = 0;
    $modelo = '';
    $especificaciones = '';
    $foto = '';
    $especificaciones_tecnicas = '';
    $costo_venta = 0;
    $costo_renta = 0;
    $categoriaId = 0;
    $stock = 0;
    $idfamilia = 0;
    //$costo_total = 0;
    $costo_toner_black=0;
    $rendimiento_toner_black=0;
    $costo_toner_cmy=0;
    $rendimiento_toner_cmy=0;
    $costo_unidad_imagen=0;
    $rendimiento_unidad_imagen=0;
    $costo_garantia_servicio=0;
    $rendimiento_garantia_servicio=0;
    $costo_pagina_monocromatica=0;
    $costo_pagina_color=0;

    $costo_poliza = 0;
    $costo_revendedor = 0;
    $pag_monocromo_incluidos = 0;
    $pag_color_incluidos = 0;
    $excedente = 0;
    $excedentecolor = 0;
    $noparte = 0;
} 

if($tipoVista==1)
{
  $label = 'Formulario de Alta de Equipo';
}
else if($tipoVista==2)
{
  $label = 'Formulario de Edición de Equipo';
}
else if($tipoVista==3)
{
  $label = 'Formulario de Visualización de Equipo';
}
?>
<style type="text/css">
  <?php if($this->perfilid!=1){ ?>
    .soloadministradores{
      display: none;
    }
  <?php } ?>
  .eliminacionproxima{
    display: none;
  }
</style>
<!-- START CONTENT --> 
<section id="content" width="100%">
    <!--start container-->
    <div class="container" width="100%">
      <div class="section">
        <h4 class="caption"><?php echo $label; ?>.</h4>
          <div class="row">
            <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
            <input id="tipoVista" type="hidden" name="tipoVista" value="<?php echo $tipoVista; ?>">
            <!-- FORMULARIO -->
               

                  <div class="col s12 m12 24">

                    <div class="card-panel">

                      <div class="row">
                         <form class="form" id="equipos_form" method="post">
            
                          <?php if(isset($equipo)) { ?>
                            <input id="idEquipo" name="idEquipo" type="hidden" value="<?php echo $idEquipo; ?>">
                          <?php } ?>

                          <div class="row">
                            <div class="input-field col s3">
                               <div class="fileinput fileinput-new" data-provides="fileinput">
                                 <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 230px; height: 210px; border-radius: 12px;">
                                <?php if ($foto!=''){ ?>
                                  <input type="file" id="foto" name="foto" class="dropify" data-default-file="<?php echo base_url(); ?>uploads/equipos/<?php echo $foto; ?>">
                                  <a class="btn btn-info" href="<?php echo base_url(); ?>uploads/equipos/<?php echo $foto; ?>" target="_blank">visualizar</a>
                                <?php }else{?>  
                                  <input type="file" id="foto" name="foto" class="dropify">
                                <?php }?>
                                </div>
                               </div>
                            </div>

                             <div class="input-field col s6">
                              <i class="material-icons prefix">local_library</i>
                              <input id="modelo" name="modelo" type="text" placeholder="Modelo" value="<?php echo $modelo; ?>" onchange="verificarmodelo(<?php echo $equipotipo; ?>)">
                             </div>
                             <div class="input-field col s3">
                              <i class="material-icons prefix">local_library</i>
                              <input id="noparte" name="noparte" type="text" value="<?php echo $noparte; ?>" onchange="verificarmodelo2(<?php echo $equipotipo; ?>)">
                              <label for="pag_monocromo_incluidos">Número de parte</label>
                             </div>
                             <br>
                            <div class="input-field col s9">
                              <i class="material-icons prefix">question_answer</i>
                              <textarea id="especificaciones" name="especificaciones" class="materialize-textarea"><?php echo $especificaciones; ?></textarea>
                              <label for="especificaciones">Especificaciones</label>
                            </div>
                            <br>
                            <div class="input-field col s9">
                              <i class="material-icons prefix">question_answer</i>
                              <textarea id="especificaciones_tecnicas" name="especificaciones_tecnicas" class="materialize-textarea"><?php echo $especificaciones_tecnicas; ?></textarea>
                              <label for="especificaciones_tecnicas">Especificaciones técnicas</label>
                            </div>
                          </div>
                          <!--
                          <hr class="divider"><h4 class="caption">Costos</h4></hr>

                          <div class="row">
                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_venta" name="costo_venta" type="number" step="any" value="<?php echo $costo_venta; ?>">
                              <label for="costo_venta">Costos venta</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_revendedor" name="costo_revendedor" type="number" step="any" value="<?php echo $costo_revendedor; ?>" >
                              <label for="costo_revendedor">Costo revendedor</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_renta" name="costo_renta"  type="number" step="any" value="<?php echo $costo_renta; ?>" >
                              <label for="costo_renta">Costo renta</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_poliza" name="costo_poliza"  type="number" step="any" value="<?php echo $costo_poliza; ?>" >
                              <label for="costo_poliza">Costo Póliza</label>
                            </div>

                          </div>
                          -->
                          <div class="row">
                            

                            <div class="input-field col s3">
                              <i class="material-icons prefix">description</i>
                              <input id="pag_monocromo_incluidos" name="pag_monocromo_incluidos" type="number" step="any" value="<?php echo $pag_monocromo_incluidos; ?>">
                              <label for="pag_monocromo_incluidos">Páginas monocromáticas incluidas</label>
                            </div>
                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="excedente" name="excedente" type="number" step="any" value="<?php echo $excedente; ?>">
                              <label for="excedente">Excedente monocromáticas $</label>
                            </div>
                            <div class="input-field col s3">
                              <i class="material-icons prefix">description</i>
                              <input id="pag_color_incluidos" name="pag_color_incluidos"  type="number" step="any" value="<?php echo $pag_color_incluidos; ?>">
                              <label for="pag_color_incluidos">Pag a color incluidas</label>
                            </div>
                            <div class="input-field col s3">
                              <i class="material-icons prefix">description</i>
                              <input id="excedentecolor" name="excedentecolor"  type="number" step="any" value="<?php echo $excedentecolor; ?>">
                              <label for="excedentecolor">Excedente color $</label>
                            </div>
                            <!--
                            <div class="input-field col s2">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="pág_color_incluidos" name="pág_color_incluidos"  type="text">
                              <label for="pág_color_incluidos">Execente $</label>
                            </div>
                            -->
                          </div>
                        
                        <!------ fin form --->
                        <hr class="divider"></hr>
                        
                        <div class="row">
                            <br>
                            <div class="input-field col s4">
                              <i class="material-icons prefix">local_library</i>
                             <select id="categoriaId" name="categoriaId" class="browser-default chosen-select">
                                  <option value="" disabled selected>Selecciona una opción</option>
                                        <?php foreach($categoria_equipo as $item)
                                        { 
                                        ?>
                                          <option value="<?php echo $item->id?>" <?php if($item->id==$categoriaId) echo "selected";   ?> ><?php echo $item->nombre; ?></option>
                                        <?php }?>
                              </select>
                              <label for="categoria" class="active">Categoria</label>
                            </div>

                            <div class="input-field col s4" style="display: none">
                              <i class="material-icons prefix">chrome_reader_mode</i>
                              <input id="stock" name="stock"  type="text" value="<?php echo $stock; ?>">
                              <label for="stock">Stock</label>
                            </div>
                          
                            <div class="input-field col s4" style="display: none;">
                              <i class="material-icons prefix">person</i>

                              <select id="idfamilia" name="idfamilia" class="browser-default chosen-select">
                                    <option value="0"selected>Selecciona una opción</option>
                                    <?php foreach($familia as $item)
                                    { 
                                    ?>
                                      <option value="<?php echo $item->id?>" <?php if($item->id==$idfamilia) echo "selected";   ?>><?php echo $item->nombre; ?></option>
                                    <?php }?>
                              </select>
                               <label for="estado" class="active">Familia</label>
                            </div>
                          </div>
                        </form>
                        <hr class="divider"><h4 class="caption">Información Extra</h4></hr>
                        <div class="row">
                          <div class="input-field col s4">
                              <i class="material-icons prefix">local_library</i>
                             <select id="consumible" name="consumible" class="browser-default chosen-select">
                                  <option value="0">Selecciona una opción</option>
                                        <?php foreach($consumibles->result() as $item){ ?>
                                          <option value="<?php echo $item->id?>"  ><?php echo $item->modelo; ?></option>
                                        <?php }?>
                              </select>
                              <label for="categoria" class="active">Consumible</label>
                          </div>
                          <div class="col s2">
                            <a class="btn btn-floating waves-effect waves-light cyan btn_agregarconsumible"><i class="material-icons">add</i></a>
                          </div>
                        </div>
                        
                           <div class="row">
                          <table id="ttableconsumibles" class="bordered">
                            <tbody id="tableconsumibles"> 
                            <?php if ($idEquipo != 0){ ?>  
                              <?php 
                                $detallescon= $this->ModeloCatalogos->consumibleagregado($idEquipo);
                                foreach ($detallescon->result() as $item) {
                                  if ($item->tipo==5) {
                                    $styledisplay='';
                                    $vrendimiento_unidad_imagen=$item->rendimiento_unidad_imagen;
                                    $vcosto_unidad_imagen=$item->costo_unidad_imagen;
                                  }else{
                                    $styledisplay='style="display:none"';
                                    //$vrendimiento_unidad_imagen=0;
                                    $vrendimiento_unidad_imagen=$item->rendimiento_unidad_imagen;
                                    $vcosto_unidad_imagen=0;
                                  }
                                  $datoconsumible=$item->id.",'".$item->modelo."'";
                                  echo '<tr class="class_trs_'.$item->id.'">
                                            <td>
                                                <div class="row">
                                                    <input id="consumibleid"  type="hidden" value="'.$item->id.'">
                                                    <input id="consumible_selected"  type="hidden" value="'.$item->idconsumible.'">
                                                    <div class="col s3"><b>'.$item->modelo.'</b></div>
                                                    <div class="col s3">
                                                      <a class="btn-floating red tooltipped elimina soloadministradores" data-position="top" data-delay="50" data-tooltip="Eliminar"  data-tooltip-id="fed519cf-dba7-8626-e402-306629f7d7d1" onclick="deleteconsumible(1,'.$datoconsumible.')" >
                                                          <i class="material-icons">delete_forever</i>
                                                      </a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s3">
                                                      <i class="material-icons prefix">attach_money</i>
                                                        <input id="costo_toner" name="costo_toner"  type="number" class="inputplace" value="'.$item->poliza.'" readonly>
                                                      <label for="costo_toner_black">Precio toner</label>
                                                    </div>
                                                    <div class="input-field col s3">
                                                      <i class="material-icons prefix">invert_colors</i>
                                                      <input id="rendimiento_toner_black" name="rendimiento_toner_black"  type="number" value="'.$item->rendimiento_toner.'" data-valor="'.$item->rendimiento_toner.'" readonly>
                                                      <label for="rendimiento_toner_black">Rendimiento toner</label>
                                                    </div>
                                                    <div class="input-field col s3 " >
                                                      <i class="material-icons prefix">invert_colors</i>
                                                      <input id="rendimiento_unidad_imagen" name="rendimiento_unidad_imagen"  type="number" value="'.$vrendimiento_unidad_imagen.'" readonly>
                                                      <label for="rendimiento_unidad_imagen">Rendimiento unidad imagen</label>
                                                    </div>
                                                    <div class="input-field col s3 eliminacionproxima" '.$styledisplay.'>
                                                      <i class="material-icons prefix">attach_money</i>
                                                      <input id="costo_unidad_imagen" name="costo_unidad_imagen"  type="number" value="'.$vcosto_unidad_imagen.'">
                                                      <label for="costo_unidad_imagen">Costo unidad imagen</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-field col s3 eliminacionproxima">
                                                      <i class="material-icons prefix">attach_money</i>
                                                      <input id="costo_garantia_servicio" name="costo_garantia_servicio"  type="number" value="'.$item->costo_garantia_servicio.'">
                                                      <label for="costo_garantia_servicio">Costo garantia servicio</label>
                                                    </div>
                                                    <div class="input-field col s3 eliminacionproxima">
                                                      <i class="material-icons prefix">invert_colors</i>
                                                      <input id="rendimiento_garantia_servicio" name="rendimiento_garantia_servicio"  type="number" value="'.$item->rendimiento_garantia_servicio.'">
                                                      <label for="rendimiento_garantia_servicio">Rendimiento garantia servicio</label>
                                                    </div>
                                                    <div class="input-field col s3">
                                                      <i class="material-icons prefix">attach_money</i>
                                                      <input id="costo_pagina_monocromatica" name="costo_pagina_monocromatica"  type="number" value="'.$item->costo_pagina_monocromatica.'">
                                                      <label for="costo_pagina_monocromatica">Costo por página mocromática</label>
                                                    </div>
                                                    <div class="input-field col s3">
                                                      <i class="material-icons prefix">attach_money</i>
                                                      <input id="costo_pagina_color" name="costo_pagina_color"  type="number" value="'.$item->costo_pagina_color.'">
                                                      <label for="costo_pagina_color">Costo por página a color</label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>';
                                }
                              ?>
                              
 <?php  }  ?>
                            </tbody>
                          </table>
                          
                        </div>
                          
                       
                       
                        <!--
                        <div class="row">
                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_toner_black" name="costo_toner_black"  type="number" value="<?php echo $costo_toner_black ?>">
                              <label for="costo_toner_black">Costo toner black</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">invert_colors</i>
                              <input id="rendimiento_toner_black" name="rendimiento_toner_black"  type="number" value="<?php echo  $rendimiento_toner_black ?>">
                              <label for="rendimiento_toner_black">Rendimiento toner black</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_toner_cmy" name="costo_toner_cmy"  type="number" value="<?php echo $costo_toner_cmy ?>">
                              <label for="costo_toner_cmy">Costo toner cmy</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">invert_colors</i>
                              <input id="rendimiento_toner_cmy" name="rendimiento_toner_cmy"  type="number" value="<?php echo $rendimiento_toner_cmy ?>">
                              <label for="rendimiento_toner_cmy">Rendimiento toner cmy</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_unidad_imagen" name="costo_unidad_imagen"  type="number" value="<?php echo $costo_unidad_imagen ?>">
                              <label for="costo_unidad_imagen">Costo unidad imagen</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">invert_colors</i>
                              <input id="rendimiento_unidad_imagen" name="rendimiento_unidad_imagen"  type="number" value="<?php echo $rendimiento_unidad_imagen ?>">
                              <label for="rendimiento_unidad_imagen">Rendimiento unidad imagen</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_garantia_servicio" name="costo_garantia_servicio"  type="number" value="<?php echo $costo_garantia_servicio ?>">
                              <label for="costo_garantia_servicio">Costo garantia servicio</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">invert_colors</i>
                              <input id="rendimiento_garantia_servicio" name="rendimiento_garantia_servicio"  type="number" value="<?php echo $rendimiento_garantia_servicio ?>">
                              <label for="rendimiento_garantia_servicio">Rendimiento garantia servicio</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_pagina_monocromatica" name="costo_pagina_monocromatica"  type="number" value="<?php echo $costo_pagina_monocromatica ?>">
                              <label for="costo_pagina_monocromatica">Costo por página mocromática</label>
                            </div>

                            <div class="input-field col s3">
                              <i class="material-icons prefix">attach_money</i>
                              <input id="costo_pagina_color" name="costo_pagina_color"  type="number" value="<?php echo $costo_pagina_color ?>">
                              <label for="costo_pagina_color">Costo por página a color</label>
                            </div>

                        </div>
                          -->


                        <hr class="divider"><h4 class="caption">Accesorios</h4></hr>
                        <div class="row">
                            <?php  
                                $i=0;
                                $size=1;
                                do{
                                ?>
                                <div  class="border" id="datos1">
                                  <div class="input-field col s10">
                                    <i class="material-icons prefix">account_circle</i>
                                    <select id="accesorio" name="accesorio" class="browser-default chosen-select">
                                    <option value="" disabled selected>Selecciona una opción</option>
                                    <?php foreach($accesorio as $item)
                                    { 
                                    ?>
                                      <option value="<?php echo $item->id?>"><?php echo $item->nombre; ?>  No.Parte: <?php echo $item->no_parte; ?>  Costo: $<?php echo $item->costo; ?></option>
                                    <?php }?>
                                    </select>
                                  <label for="accesorio" class="active" >Nombre</label>
                                  </div>
                          
                                  <div class="input-field col s2 div_btn">
                                  <?php if($i==0){ ?>
                                  <a class="btn btn-floating waves-effect waves-light cyan btn_agregar"> <i class="material-icons">add</i></a>
                                  <?php } else {?>
                                  <a class="btn btn-floating waves-effect waves-light red btn_eliminar"><i class="material-icons">delete_forever</i></a>
                                  <?php }?>
                                  </div>
                                </div>
                                <?php 
                                $i++;
                                }while($i<$size);
                                ?>
                        </div>


                        <?php 
                        if($tipoVista!=1)
                        {
                        ?> 
                        <h4 class="caption">Tabla Accesorios</h4>
                        <div class="row">   
                           <div class="col s12">
                                <table class="table table-striped table-condensed table-hover table-responsive" id="tabla_accesorios_equipo" name="tabla_accesorios_equipo">
                                  <thead>
                                      <tr>
                                          <th>#</th>
                                          <th>Nombre</th>
                                          <th>Stock</th>
                                          <th>Precio</th>
                                          <th>Equipo</th>
                                          <th>Acciones</th>
                                      </tr>
                                  </thead>

                                  <tbody >
                                  </tbody>
                                  
                              </table> 
                            </div>
                        </div>
                        <?php } ?>

                        <br><br><br>
                          <div class="row">
                            <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right formsubmitsave" type="submit">Guardar
                                <i class="material-icons right">send</i>
                              </button>
                            </div>
                          </div>
                        
                        
                      </div>

                    </div>

                  </div>
                
              </div>
            </div>
          </div>
        </section>