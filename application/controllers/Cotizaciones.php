<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cotizaciones extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Login_model');
        $this->load->model('Cotizaciones_model');
        $this->load->model('Equipos_model');
        $this->load->model('Polizas_model');
        $this->load->model('Clientes_model');
        $this->load->model('Prospectos_model');
        $this->load->model('Consumibles_model');
        $this->load->model('Refacciones_model');
        $this->load->model('ConfiguracionDocumentos_Model');
        $this->load->model('Ventas_model');
        $this->load->model('Rentas_model');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('ModeloGeneral');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechahoy = date('Y-m-d');
        // Librería para mostrar los menús del sistema
        $this->load->library('menus');
        if ($this->session->userdata('logeado')!=true) {
            redirect('Sistema');
        }else{
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal=$this->session->userdata('idpersonal');
        }

    }
    // Vista para crear Cotización
    function altaCotizacion($idCliente){  
        if($this->session->userdata('logeado')==true){
            $telefono='';//telefono
            $aten = ''; // atencion para
            $data['datoscontacto']=$this->ModeloCatalogos->db13_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente,'activo'=>1));
            $data['idCliente'] = $idCliente;
            $data['equipos'] = $this->Equipos_model->getEquiposParaSelect();  
            $data['polizas'] = $this->Polizas_model->getListadoPolizas();      
            //$data['familia'] = $this->Clientes_model->getDataFamilia();
            $data['equiposimg'] = $this->Clientes_model->getDataEquipo2();
            $datosCliente = $this->Clientes_model->getClientePorId($idCliente);
            $data['nombreCliente'] = $datosCliente->empresa;
            $data['emailCliente'] = $datosCliente->email;
            $data['condicion'] = $datosCliente->condicion;
            $data['aaa'] = $datosCliente->aaa;
            $data['idpersonal']=$this->idpersonal;
            // Cliente
            $atenciona = $datosCliente->atencionpara;
            if($atenciona != ''){
                $data['atencionpara'] = $datosCliente->atencionpara;
            }else{
                $person_contacto = $this->Clientes_model->getClienteContactoPorId($idCliente); 
                foreach ($person_contacto as $item) {
                    $aten = $item->persona_contacto;
                } 
                if($aten !=''){
                    $data['atencionpara'] = $aten;
                }else{
                    $data['atencionpara'] = '';
                }
            }
            // prospecto
            $atencionp = $datosCliente->puesto_contacto;
            if($atencionp != ''){
                $data['atencionpara'] = $datosCliente->puesto_contacto;
            }else{
                $person_contacto = $this->Clientes_model->getClienteContactoPorId($idCliente); 
                foreach ($person_contacto as $item) {
                    $aten = $item->persona_contacto;
                }
                if($aten !=''){
                    $data['atencionpara'] = $aten;
                }else{
                    $data['atencionpara'] = '';
                } 
            }
            $where  = array('idCliente' => $idCliente,'status'=>1);
            $datosClientet = $this->ModeloCatalogos->db13_getselectwheren('cliente_has_telefono',$where);
            foreach ($datosClientet->result() as $item) {
                $telefono=$item->tel_local;
            }
            $data['telefonoCliente'] = $telefono;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('cotizacion/form_cotizacion');
            $this->load->view('cotizacion/form_cotizacion_js');
            $this->load->view('footerck'); 
            $this->load->view('info_m_servisios'); 
        }
        else
        {
            redirect('login');
        }       
    }
    // Lista de Cotizaciones, por Cliente
    function listaCotizacionesPorCliente($idCliente=0){
        $data['MenusubId']=68;
        if($this->session->userdata('logeado')==true){
            $data['resultpolizas']=$this->ModeloCatalogos->db13_getselectwheren('polizas',array('estatus'=>1)); 
            $data['idCliente'] = $idCliente;
            if($idCliente>0){
                $datosCliente = $this->Clientes_model->getClientePorId($idCliente);
                $data['nombreCliente'] = $datosCliente->empresa;
                $data['bloqueo'] = $datosCliente->bloqueo;
                $data['fechaini']=date("Y-m-d",strtotime($this->fechahoyc."- 12 month"));;
            }else{
                $data['nombreCliente'] = '';
                $data['bloqueo'] = 0;
                $data['fechaini']=date("Y-m-d",strtotime($this->fechahoyc."- 8 month")); 
            }
            
            $data['equipos'] = $this->Equipos_model->getEquiposParaSelect();
            $data['label'] = 'Cliente';
            //$data['bodegas']=$this->ModeloCatalogos->db13_getselectwheren('bodegas',array('activo' => 1));
            $data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();
            $data['datoscontacto']=$this->ModeloCatalogos->db13_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idCliente,'activo'=>1));
            $data['perfirma']=$this->ModeloCatalogos->seleccionejecutivofirma();
            $data['perfilid']=$this->perfilid;
            $data['personalrows']=$this->ModeloGeneral->listadopersonal();
            $data['idpersonal']=$this->idpersonal;
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('cotizacion/listado');
            
            $this->load->view('footer'); 
            $this->load->view('cotizacion/listado_js');
            $this->load->view('info_m_servisios');
        }
        else{
            redirect('login');
        }
    }
    function datoscontacto(){
        $cli = $this->input->post('cli');
        $datoscontacto=$this->ModeloCatalogos->db13_getselectwheren('cliente_datoscontacto',array('clienteId'=>$cli,'activo'=>1));
        $html='<option data-atencionpara="" data-email="" data-telefono="">Seleccione</option>';
        foreach ($datoscontacto->result() as $item) {
            $html.='<option data-atencionpara="'.$item->atencionpara.'" data-email="'.$item->email.'" data-telefono="'.$item->telefono.'">'.$item->atencionpara.'</option>';
        } 
        echo $html; 
    }
    // Lista de Cotizaciones, por Prospecto
    function listaCotizacionesPorProspecto($idProspectos){
        if($this->session->userdata('logeado')==true){
            $data['resultpolizas']=$this->ModeloCatalogos->db13_getselectwheren('polizas',array('estatus'=>1));
            //$data['bodegas']=$this->ModeloCatalogos->db13_getselectwheren('bodegas',array('activo' => 1));
            $data['bodegas']=$this->ModeloCatalogos->view_session_bodegas();
            $data['idCliente'] = $idProspectos;
            $data['equipos'] = $this->Equipos_model->getEquiposParaSelect();
            $datosProspecto = $this->Prospectos_model->getProspectoPorId($idProspectos);
            $data['bloqueo'] = $datosProspecto->bloqueo;
            $data['nombreCliente'] = $datosProspecto->empresa;
            $data['label'] = 'Prospecto';
            $data['datoscontacto']=$this->ModeloCatalogos->db13_getselectwheren('cliente_datoscontacto',array('clienteId'=>$idProspectos,'activo'=>1));
            $data['perfirma']=$this->ModeloCatalogos->seleccionejecutivofirma();
            $data['perfilid']=$this->perfilid;
            $data['idpersonal']=$this->idpersonal;
            $data['fechaini']=date("Y-m-d",strtotime($this->fechahoyc."- 8 month")); 
            $data['personalrows']=$this->ModeloGeneral->listadopersonal();
            $this->load->view('header');
            $this->load->view('main',$data);
            $this->load->view('cotizacion/listado',$data);
            $this->load->view('cotizacion/listado_js');
            $this->load->view('footer');
        }
        else
        {
            redirect('login');
        }
    }
    // Obtener el listado de las cotizaciones por Cliente
    function getListaCotizacionesPorCliente(){
        //log_message('error','control');
        $idCliente = $this->input->post('idCliente');
        $denegada = $this->input->post('denegada');
        $cotizaciones = $this->Cotizaciones_model->getListaCotizacionesPorCliente($idCliente,$denegada);
        $json_data = array("data" => $cotizaciones);
        echo json_encode($json_data);
    }
    function obtenercotizaciones(){
        $params =$this->input->post();
        $cotizaciones = $this->Cotizaciones_model->getListaCotizaciones($params);
        $html='';

        $html.='<table id="tabla_cotizaciones" class="responsive-table display" cellspacing="0">';
                $html.='<thead>';
                  $html.='<tr>';
                    $html.='<th>#</th>';
                    $html.='<th>Atención</th>';
                    $html.='<th>Cotización</th>';
                    $html.='<th>Tipo</th>';
                    $html.='<th>Estatus</th>';
                    $html.='<th>Fecha</th>';
                    $html.='<th>Acciones</th>';
                  $html.='</tr>';
                $html.='</thead>';
                $html.='<tbody>';
                foreach ($cotizaciones->result() as $item) {
                    $html.='<tr>';
                    $html.='<td>'.$item->id.'</td>';
                    $html.='<td>'.$item->atencion.'</td>';
                    if($item->tipo==1){
                        $tipocot ='Equipos';    
                    }else if($item->tipo==2){
                        $tipocot ='Consumibles';    
                    }else if($item->tipo==3){
                        $tipocot ='Refacciones';    
                    }else if($item->tipo==4){
                        $tipocot ='Pólizas';    
                    } 
                    if($item->tipog!=null){
                        if($item->tipog!='0'){
                            $tipocot='';

                            $tipos = $item->tipog;
                            // Convertir la cadena en un array utilizando el método split(',')
                            $tiposArray = explode(',',$tipos);

                            // Iterar sobre el array usando un bucle for
                            for ($i = 0; $i < count($tiposArray); $i++) {
                                // Acceder al elemento actual usando tiposArray[i]
                                //console.log(tiposArray[i]);
                                if($tiposArray[$i]==1){
                                    $tipocot.='<br>Equipos';    
                                }else if($tiposArray[$i]==2){
                                    $tipocot.='<br>Consumibles';    
                                }else if($tiposArray[$i]==3){
                                    $tipocot.='<br>Refacciones';    
                                }else if($tiposArray[$i]==4){
                                    $tipocot.='<br>Pólizas';    
                                }else if($tiposArray[$i]==5){
                                    $tipocot.='<br>Accesorios';    
                                }
                            }
                        }
                    }
                    $html.='<td>'.$tipocot.'</td>';
                    $tipo_vrp='';
                    if($item->rentaVentaPoliza==1){
                        $tipo_vrp='Venta';    
                    }else if($item->rentaVentaPoliza==2){
                        $tipo_vrp='Renta';    
                    }else if($item->rentaVentaPoliza==3){
                        $tipo_vrp='Póliza';    
                    }else{
                        $tipo_vrp='N/A';
                    }
                    $html.='<td>'.$tipo_vrp.'</td>';

                    $tipo_estatus='';
                    if ($item->denegada==0) {
                        if($item->estatus==1){
                            $tipo_estatus='Pendiente';    
                        }else if($item->estatus==2){
                            $tipo_estatus='Autorizada';    
                        } 
                    }else{
                        $tipo_estatus='Eliminada';
                    }
                    $html.='<td>'.$tipo_estatus.'</td>';
                    $html.='<td>'.$item->reg.'</td>';
                    $btns='';
                    if($item->estatus==1){
                        if ($item->denegada==0) {
                            $btns.='<a class="btn-floating orange  tooltipped" data-position="top" data-delay="50" data-tooltip="Autorizar" onclick="autorizar('.$item->id.','.$item->tipo.')"><i class="material-icons">done</i></a>';
                            $btns.='<a class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Editar" onclick="editar('.$item->id.','.$item->tipo.')"><i class="material-icons">mode_edit</i></a>';
                            $btns.='<a class="btn-floating waves-effect waves-light red accent-2 tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar" onclick="denegarc('.$item->id.')"><i class="material-icons">clear</i></a>';
                          }else{
                            $btns.='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Vizualizar" onclick="editar('.$item->id.','.$item->tipo.')"><i class="material-icons">remove_red_eye</i></a>';
                            $btns.='<a class="cotizacion_c_'.$item->id.' btn-floating waves-effect waves-light red accent-2 tooltipped" data-position="top" data-delay="50" data-tooltip="Motivo" data-motivo="'.$item->motivodenegada.'" onclick="motivod('.$item->id.')"> <i class="material-icons">assignment</i></a>';
                          }
                        
                    }else{
                        
                    }
                        $btns.='<a class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Vizualizar" onclick="editar('.$item->id.','.$item->tipo.')"> <i class="material-icons">remove_red_eye</i> </a>';

                    $html.='<td>'.$btns.'</td>';
                  $html.='</tr>';
                }
                $html.='</tbody>';
              $html.='</table>';
        echo $html;
    }
    public function getlistcotizaciones() {
        $params = $this->input->post();
        $getdata = $this->Cotizaciones_model->getlistcotizaciones($params);
        $totaldata= $this->Cotizaciones_model->getlistcotizaciones_total($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    // Función para autorizar una cotización
    function autorizarCotizacion(){
        $data = $this->input->post();
        $datosDeCotizacion = $this->Cotizaciones_model->getDatosCotizacion($data['idCotizacion']);
        $rentaVentaPoliza = $datosDeCotizacion[0]->rentaVentaPoliza;
        $tipoDeCotizacion = $datosDeCotizacion[0]->tipo;
        unset($datosDeCotizacion[0]->priorizar_cot);
        unset($datosDeCotizacion[0]->tipog);
        // Si el tipo de la cotización es 1, es de EQUIPOS
        if($datosDeCotizacion[0]->tipo==1)
        {
            // Se actualiza la cotización al estatus de "Aprobada"
            $datosActualizadosCotizacion = array('estatus' => 2);
            $actualizado = $this->Cotizaciones_model->actualizaCotizacion($datosActualizadosCotizacion,$data['idCotizacion']);

            // Verificamos que sea para VENTA (1), RENTA (2) o PÓLIZA (3)
            // Ventas
            if($rentaVentaPoliza==1)
            {
                //log_message('error', 'entra tipoventa: 1');
                // Se elimina el ID del registro de la cotización de los datos que usaremos
                unset($datosDeCotizacion[0]->id);
                // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
                unset($datosDeCotizacion[0]->estatus);
                unset($datosDeCotizacion[0]->tipotoner);
                // Se elimina el tipo de "rentaVentaPoliza"
                unset($datosDeCotizacion[0]->rentaVentaPoliza);
                unset($datosDeCotizacion[0]->propuesta);
                unset($datosDeCotizacion[0]->denegada);
                unset($datosDeCotizacion[0]->motivodenegada);

                // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
                $datosDeCotizacion[0]->estatus = 1;
                $datosDeCotizacion[0]->id_personal = $this->idpersonal;
                // Se indica solo como información, el id de la cotización que va con el proceso
                $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
                $datosDeCotizacion[0]->reg = $this->fechahoyc;
                
                $idVenta = $this->Ventas_model->insertarVenta($datosDeCotizacion[0]);

                // Debemos dejar solo el equipo que eligió y eliminar los otros dos de la cotización
                // Obtenemos los detalles de la cotización que se va a autorizar
                $detallesCotizacionEquipos = $this->Cotizaciones_model->getDetallesCotizacionEquiposPorIdCotizacion($data['idCotizacion']);
                
                // Buscamos el registro que corresponda con el ID Detalle que viene del formuario
                foreach ($detallesCotizacionEquipos as $detalle) {
                    if($detalle->id==$data['idDetalle'])
                    {
                        $registroElegido = $detalle;

                        // Se elimina el ID del registro de la cotización de los datos que usaremos
                        unset($detalle->id);
                        // 
                        unset($detalle->idCotizacion);
                        unset($detalle->tipotoner);
                        $detalle->idVenta = $idVenta;
                        $detalle->serie_bodega = $data['serie'];
                        $detalle->cantidad = $data['cantidade'];
                                    
                        $ventaDetalle = $this->Ventas_model->insertarDetallesVentasEquipos($detalle);
                        $this->ModeloCatalogos->updatestock('equipos','stock','-','stock','id',$detalle->idEquipo);
                        if ($data['acce']!=0) {
                            $idaccesorio=$data['acce'];
                            $dataacce['id_venta']=$idVenta;
                            $dataacce['id_equipo']=$detalle->idEquipo;
                            $dataacce['id_accesorio']=$data['acce'];
                            $dataacce['serie_bodega']=$data['serieacc'];
                            $dataacce['cantidad']=$data['cantidada'];
                            $dataacce['costo']=$this->ModeloCatalogos->costoaccesorio($idaccesorio);
                            $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataacce);
                        }
                        if ($data['consumible']!=0) {
                            //=========================================
                            $wherecc = array('id_consumibles' =>$data['consumible'],'id_cotizacion'=>$data['idCotizacion']);
                            $resultconsu=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_detallesEquipos_consumibles',$wherecc);
                            $costo_toner=0;
                            $rendimiento_toner=0;
                            $rendimiento_unidad_imagen=0;

                            foreach ($resultconsu->result() as $itemc) {
                                $costo_toner =$itemc->costo_toner;
                                $rendimiento_toner =$itemc->rendimiento_toner;
                                $rendimiento_unidad_imagen = $itemc->rendimiento_unidad_imagen;
                            }
                            //===============================================
                            $dataaccee['idventa']=$idVenta;
                            $dataaccee['idequipo']=$detalle->idEquipo;
                            $dataaccee['id_consumibles']=$data['consumible'];
                            $dataaccee['costo_toner']=$costo_toner;
                            $dataaccee['rendimiento_toner']=$rendimiento_toner;
                            $dataaccee['rendimiento_unidad_imagen']=$rendimiento_unidad_imagen;
                            
                            $dataaccee['cantidad']=$data['cantidadc'];
                            $this->ModeloCatalogos->Insert('ventas_has_detallesequipos_consumible',$dataaccee);
                        }
                        
                    }
                }

            }
            // Rentas
            else if($rentaVentaPoliza==2)
            {
                //log_message('error', 'entra tipoventa: 2 rentas');
                // Se elimina el ID del registro de la cotización de los datos que usaremos
                unset($datosDeCotizacion[0]->id);
                // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
                unset($datosDeCotizacion[0]->estatus);
                // Se elimina el tipo de "rentaVentaPoliza"
                unset($datosDeCotizacion[0]->rentaVentaPoliza);
                // Se elimina el "tipo" , ya que sabemos que es un ingreso de Equipos
                unset($datosDeCotizacion[0]->tipo);
                unset($datosDeCotizacion[0]->propuesta);
                unset($datosDeCotizacion[0]->denegada);
                unset($datosDeCotizacion[0]->motivodenegada);

                // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
                $datosDeCotizacion[0]->estatus = 1;
                // Se indica solo como información, el id de la cotización que va con el proceso
                $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
                $datosDeCotizacion[0]->id_personal=$this->idpersonal;
                $datosDeCotizacion[0]->reg = $this->fechahoyc;
                // Aunque la variable se llame idVenta, es el ID de la renta
                $idVenta = $this->Rentas_model->insertarRenta($datosDeCotizacion[0]);
                
                // Debemos dejar solo el equipo que eligió y eliminar los otros dos de la cotización
                // Obtenemos los detalles de la cotización que se va a autorizar
                $detallesCotizacionEquipos = $this->Cotizaciones_model->getDetallesCotizacionEquiposPorIdCotizacion($data['idCotizacion']);

                // Buscamos el registro que corresponda con el ID Detalle que viene del formuario
                foreach ($detallesCotizacionEquipos as $detalle){
                    //log_message('error', 'entra a result detallesCotizacionEquipos');
                    if($detalle->id==$data['idDetalle']){
                        $registroElegido = $detalle;

                        // Se elimina el ID del registro de la cotización de los datos que usaremos
                        unset($detalle->id);
                        // 
                        unset($detalle->idCotizacion);

                        $detalle->idRenta = $idVenta;
                        $detalle->serie_bodega=$data['serie'];
                        $detalle->cantidad = $data['cantidade'];
                        $ventaDetalle = $this->Rentas_model->insertarDetallesRentasEquipos($detalle);
                        $this->ModeloCatalogos->updatestock('equipos','stock','-','stock','id',$detalle->idEquipo);
                        if ($data['acce']!=0) {
                            //log_message('error', 'entra a acce '.$data['acce']);
                            $idaccesorio=$data['acce'];
                            $dataacce['idrentas']=$idVenta;
                            $dataacce['id_equipo']=$detalle->idEquipo;
                            $dataacce['id_accesorio']=$data['acce'];
                            $dataacce['serie_bodega']=$data['serieacc'];
                            $dataacce['cantidad']=$data['cantidada'];
                            $dataacce['costo']=$this->ModeloCatalogos->costoaccesorio($idaccesorio);
                            $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataacce);
                        }
                        if ($data['consumible']!=0) {
                            //log_message('error', 'entra a acce '.$data['consumible']);
                            //=========================================
                            $wherecc = array('id_consumibles' =>$data['consumible'],'id_cotizacion'=>$data['idCotizacion']);
                            $resultconsu=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_consumibles',$wherecc);
                            $costo_toner=0;
                            $rendimiento_toner=0;
                            $rendimiento_unidad_imagen=0;

                            foreach ($resultconsu->result() as $itemc) {
                                $costo_toner =$itemc->costo_toner;
                                $rendimiento_toner =$itemc->rendimiento_toner;
                                $rendimiento_unidad_imagen = $itemc->rendimiento_unidad_imagen;
                            }
                            //===============================================
                            $dataaccee['idrentas']=$idVenta;
                            $dataaccee['idequipo']=$detalle->idEquipo;
                            $dataaccee['id_consumibles']=$data['consumible'];
                            $dataaccee['costo_toner']=$costo_toner;
                            $dataaccee['rendimiento_toner']=$rendimiento_toner;
                            $dataaccee['rendimiento_unidad_imagen']=$rendimiento_unidad_imagen;
                            
                            $dataaccee['cantidad']=$data['cantidadc'];
                            $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$dataaccee);
                        }

                    }
                }
            }
            // Póliza
            else if($rentaVentaPoliza==3)
            {

                // Se elimina el ID del registro de la cotización de los datos que usaremos
                unset($datosDeCotizacion[0]->id);
                // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
                unset($datosDeCotizacion[0]->estatus);
                // Se elimina el tipo de "rentaVentaPoliza"
                unset($datosDeCotizacion[0]->rentaVentaPoliza);
                unset($datosDeCotizacion[0]->propuesta);
                unset($datosDeCotizacion[0]->denegada);
                unset($datosDeCotizacion[0]->motivodenegada);
                unset($datosDeCotizacion[0]->v_ser_tipo);
                unset($datosDeCotizacion[0]->v_ser_id);

                // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
                $datosDeCotizacion[0]->estatus = 1;
                // Se indica solo como información, el id de la cotización que va con el proceso
                $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
                $datosDeCotizacion[0]->reg = $this->fechahoyc;
                // Aunque la variable se llame idVenta, es el ID de la Póliza Creada
                $idVenta = $this->Polizas_model->insertaPolizaCreada($datosDeCotizacion[0]);

                // Debemos dejar solo el equipo que eligió y eliminar los otros dos de la cotización
                // Obtenemos los detalles de la cotización que se va a autorizar
                $detallesCotizacionEquipos = $this->Cotizaciones_model->getDetallesCotizacionEquiposPorIdCotizacion($data['idCotizacion']);
                
                // Buscamos el registro que corresponda con el ID Detalle que viene del formuario
                foreach ($detallesCotizacionEquipos as $detalle) 
                {

                    if($detalle->id==$data['idDetalle'])
                    {
                        $registroElegido = $detalle;

                        // Se elimina el ID del registro de la cotización de los datos que usaremos
                        unset($detalle->id);
                        // 
                        unset($detalle->idCotizacion);

                        $detalle->idPoliza = $idVenta;
                        
                        $polizaDetalle = $this->Polizas_model->insertaDetallesPolizaCreada($detalle);
                    }
                }
            }
        }
        // Si el tipo de la cotización es 2 o 3 es REFACCIÓN o CONSUMIBLE
        else if($datosDeCotizacion[0]->tipo==2 || $datosDeCotizacion[0]->tipo==3)
        {
            /*
                base para autorisar consumible
            // Se actualiza la cotización al estatus de "Aprobada"
            $datosActualizadosCotizacion = array('estatus' => 2);
            $actualizado = $this->Cotizaciones_model->actualizaCotizacion($datosActualizadosCotizacion,$data['idCotizacion']);

            // Se elimina el ID del registro de la cotización de los datos que usaremos
            unset($datosDeCotizacion[0]->id);
            // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
            unset($datosDeCotizacion[0]->estatus);
            // Se elimina el tipo de "rentaVentaPoliza"
            unset($datosDeCotizacion[0]->rentaVentaPoliza);

            // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
            $datosDeCotizacion[0]->estatus = 1;
            $datosDeCotizacion[0]->id_personal = $this->idpersonal;
            // Se indica solo como información, el id de la cotización que va con el proceso
            $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
            
            $idVenta = $this->Ventas_model->insertarVenta($datosDeCotizacion[0]);
            */ 
        }
        // Si el tipo de la cotización es 4 es Pólizas
        else if($datosDeCotizacion[0]->tipo==4)
        {
            /*
            // Se actualiza la cotización al estatus de "Aprobada"
            $datosActualizadosCotizacion = array('estatus' => 2);
            $actualizado = $this->Cotizaciones_model->actualizaCotizacion($datosActualizadosCotizacion,$data['idCotizacion']);

            // Se elimina el ID del registro de la cotización de los datos que usaremos
            unset($datosDeCotizacion[0]->id);
            // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
            unset($datosDeCotizacion[0]->estatus);
            // Se elimina el tipo de "rentaVentaPoliza"
            unset($datosDeCotizacion[0]->rentaVentaPoliza);

            // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
            $datosDeCotizacion[0]->estatus = 1;
            // Se indica solo como información, el id de la cotización que va con el proceso
            $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];

            // Aunque la variable se llame idVenta, es el ID de la Póliza Creada
            $idVenta = $this->Polizas_model->insertaPolizaCreada($datosDeCotizacion[0]);

            // Debemos dejar solo la póliza que eligió 
            // Obtenemos los detalles de la cotización que se va a autorizar
            $detallesCotizacionPolizas = $this->Cotizaciones_model->getDetallesCotizacionPolizasPorIdCotizacion($data['idCotizacion']);
            
            // Buscamos el registro que corresponda con el ID Detalle que viene del formuario
            foreach ($detallesCotizacionPolizas as $detalle) 
            {

                if($detalle->id==$data['idDetalle'])
                {
                    $registroElegido = $detalle;

                    // Se elimina el ID del registro de la cotización de los datos que usaremos
                    unset($detalle->id);
                    unset($detalle->idCotizacion);

                    $detalle->idPoliza = $idVenta;

                    $polizaDetalle = $this->Polizas_model->insertaPolizaDetallesPolizaCreada($detalle);
                }
            }
            */
        }

        if($tipoDeCotizacion==1){
            $tipo = 'Equipos';
        }
        else if($tipoDeCotizacion==2){
            $tipo = 'Consumibles';
        }
        else if ($tipoDeCotizacion==3){
            $tipo = 'Refacciones';
        }
        else if ($tipoDeCotizacion==4){
            $tipo = 'Pólizas';
        }

        if($rentaVentaPoliza==1){
            $rvp = 'ventas';
        }
        else if($rentaVentaPoliza==2){
            $rvp = 'Solcitudes de Contratos Rentas';
        }
        else if ($rentaVentaPoliza==3){
            $rvp = 'Pólizas';
        }

        // Convertir Prospecto a Cliente en caso de ser necesario
        if($idVenta!=0 && $idVenta!=null)
        {
            $datosProspectoCliente = $this->Clientes_model->getClientePorId($datosDeCotizacion[0]->idCliente);
            if($datosProspectoCliente->tipo==2)
            {
                $data = array('tipo' => 1);
                $actualizado = $this->Prospectos_model->update_prospecto($data, $datosDeCotizacion[0]->idCliente);
            }
        }

        $respuestaJSON = array("idVenta"=>$idVenta, "tipo"=>$tipo, "rentaVentaPoliza" => $rvp);
        echo json_encode($respuestaJSON);
    }
    /*
    function autorizarcotizacionequipos(){
        // original la funcion autorizarCotizacion
        $idVenta=0;
        $data = $this->input->post();
        $datosDeCotizacion = $this->Cotizaciones_model->getDatosCotizacion($data['idCotizacion']);
        $rentaVentaPoliza = $datosDeCotizacion[0]->rentaVentaPoliza;
        $tipoDeCotizacion = $datosDeCotizacion[0]->tipo;
        $datosDeCotizacion[0]->servicio = $data['servicio'];
        $datosDeCotizacion[0]->fechaentrega = $data['fechae'];
        if($data['servicio']==1){
            $datosDeCotizacion[0]->servicio_poliza = $data['poliza'];
            $datosDeCotizacion[0]->servicio_servicio = $data['servicio2'];
            $datosDeCotizacion[0]->servicio_direccion = $data['direccion'];
            $datosDeCotizacion[0]->servicio_tipo = $data['tipo'];
            $datosDeCotizacion[0]->servicio_motivo = $data['motivo'];

        }
        // Se actualiza la cotización al estatus de "Aprobada"
            $datosActualizadosCotizacion = array('estatus' => 2);
            $actualizado = $this->Cotizaciones_model->actualizaCotizacion($datosActualizadosCotizacion,$data['idCotizacion']);
        if ($rentaVentaPoliza==1) {
                //log_message('error', 'entra tipoventa: 1');
                // Se elimina el ID del registro de la cotización de los datos que usaremos
                unset($datosDeCotizacion[0]->id);
                // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
                unset($datosDeCotizacion[0]->estatus);
                unset($datosDeCotizacion[0]->tipotoner);
                // Se elimina el tipo de "rentaVentaPoliza"
                unset($datosDeCotizacion[0]->rentaVentaPoliza);
                unset($datosDeCotizacion[0]->propuesta);
                unset($datosDeCotizacion[0]->denegada);
                unset($datosDeCotizacion[0]->motivodenegada);

                // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
                $datosDeCotizacion[0]->estatus = 1;
                $datosDeCotizacion[0]->id_personal = $this->idpersonal;
                // Se indica solo como información, el id de la cotización que va con el proceso
                $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
                $datosDeCotizacion[0]->reg = $this->fechahoyc;
                $idVenta = $this->Ventas_model->insertarVenta($datosDeCotizacion[0]);

                $equipos=$data["equipos"];
                $DATAeq = json_decode($equipos);
                for ($j=0;$j<count($DATAeq);$j++){
                    //$DATAeq[$j]->equipo;
                    //$DATAeq[$j]->cantidad;
                    //$DATAeq[$j]->bodega;
                    $detalleequipo=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos',array('id'=>$DATAeq[$j]->equipo));
                    $detalleequipo=$detalleequipo->result();
                    $detalle=$detalleequipo[0];
                    
                    unset($detalle->id); 
                    unset($detalle->idCotizacion);
                    unset($detalle->tipotoner);
                    $detalle->idVenta = $idVenta;
                    $detalle->serie_bodega = $DATAeq[$j]->bodega;
                    $detalle->cantidad = $DATAeq[$j]->cantidad;

                    $this->Ventas_model->insertarDetallesVentasEquipos($detalle);
                    //$this->ModeloCatalogos->updatestock('equipos','stock','-','stock','id',$detalle->idEquipo);
                }
                $accesorios=$data["accesorios"];
                $DATAac = json_decode($accesorios);
                for ($m=0;$m<count($DATAac);$m++){
                    $detalleaccesorio=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_accesorios',array('id_accesoriod'=>$DATAac[$m]->accesorio));
                    $detalleaccesorio=$detalleaccesorio->result();
                    $detalle=$detalleaccesorio[0];

                    $idaccesorio=$detalle->id_accesorio;

                    $dataacce['id_venta']=$idVenta;
                    $dataacce['id_equipo']=$detalle->id_equipo;
                    $dataacce['id_accesorio']=$detalle->id_accesorio;
                    $dataacce['serie_bodega']=$DATAac[$m]->bodega;
                    $dataacce['cantidad']=$DATAac[$m]->cantidad;
                    $dataacce['costo']=$this->ModeloCatalogos->costoaccesorio($detalle->id_accesorio);
                    $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataacce);
                }
                $consumibles=$data["consumibles"];
                $DATAcon = json_decode($consumibles);
                for ($co=0;$co<count($DATAcon);$co++){
                            $detalleconsumible=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_consumibles',array('id'=>$DATAcon[$co]->consumible));
                            $detalleconsumible=$detalleconsumible->result();
                            $detalle=$detalleconsumible[0];

                          
                            $costo_toner=0;
                            $rendimiento_toner=0;
                            $rendimiento_unidad_imagen=0;

                            
                            $costo_toner =$detalle->costo_toner;
                            $rendimiento_toner =$detalle->rendimiento_toner;
                            $rendimiento_unidad_imagen = $detalle->rendimiento_unidad_imagen;
                            
                            //===============================================
                            $dataaccee['idventa']=$idVenta;
                            $dataaccee['idequipo']=$detalle->id_equipo;
                            $dataaccee['id_consumibles']=$detalle->id_consumibles;
                            $dataaccee['costo_toner']=$costo_toner;
                            $dataaccee['rendimiento_toner']=$rendimiento_toner;
                            $dataaccee['rendimiento_unidad_imagen']=$rendimiento_unidad_imagen;
                            
                            $dataaccee['cantidad']=$DATAcon[$co]->cantidad;
                            $dataaccee['bodega']=$DATAcon[$co]->bodega;
                            $this->ModeloCatalogos->Insert('ventas_has_detallesequipos_consumible',$dataaccee);
                            
                            $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$DATAcon[$co]->cantidad,'consumiblesId',$detalle->id_consumibles,'bodegaId',$DATAcon[$co]->bodega);
                }


        }elseif ($rentaVentaPoliza==2) {
            //log_message('error', 'entra tipoventa: 2 rentas');
            // Se elimina el ID del registro de la cotización de los datos que usaremos
            unset($datosDeCotizacion[0]->id);
            // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
            unset($datosDeCotizacion[0]->estatus);
            // Se elimina el tipo de "rentaVentaPoliza"
            unset($datosDeCotizacion[0]->rentaVentaPoliza);
            // Se elimina el "tipo" , ya que sabemos que es un ingreso de Equipos
            unset($datosDeCotizacion[0]->tipo);
            unset($datosDeCotizacion[0]->propuesta);
                unset($datosDeCotizacion[0]->denegada);
                unset($datosDeCotizacion[0]->motivodenegada);

            // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
            $datosDeCotizacion[0]->estatus = 1;
            // Se indica solo como información, el id de la cotización que va con el proceso
            $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
            $datosDeCotizacion[0]->id_personal=$this->idpersonal;
            $datosDeCotizacion[0]->reg = $this->fechahoyc;
            // Aunque la variable se llame idVenta, es el ID de la renta
            $idVenta = $this->Rentas_model->insertarRenta($datosDeCotizacion[0]);


            $equipos=$data["equipos"];
                $DATAeq = json_decode($equipos);
                for ($j=0;$j<count($DATAeq);$j++){
                    //$DATAeq[$j]->equipo;
                    //$DATAeq[$j]->cantidad;
                    //$DATAeq[$j]->bodega;
                    $detalleequipo=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos',array('id'=>$DATAeq[$j]->equipo));
                    //$detalle=$detalleequipo[0]->result();
                    $detalleequipo=$detalleequipo->result();
                    $detalle=$detalleequipo[0];

                    // Se elimina el ID del registro de la cotización de los datos que usaremos
                    unset($detalle->id);
                    // 
                    unset($detalle->idCotizacion);

                    $detalle->idRenta = $idVenta;
                    $detalle->serie_bodega=$DATAeq[$j]->bodega;
                    $detalle->cantidad = $DATAeq[$j]->cantidad;
                    
                    $ventaDetalle = $this->Rentas_model->insertarDetallesRentasEquipos($detalle);
                }
            $accesorios=$data["accesorios"];
            $DATAac = json_decode($accesorios);
            for ($m=0;$m<count($DATAac);$m++){
                $detalleaccesorio=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_accesorios',array('id_accesoriod'=>$DATAac[$m]->accesorio));
                $detalleaccesorio=$detalleaccesorio->result();
                $detalle=$detalleaccesorio[0];

                $idaccesorio=$detalle->id_accesorio;
                $dataacce['idrentas']=$idVenta;
                $dataacce['id_equipo']=$detalle->id_equipo;
                $dataacce['id_accesorio']=$detalle->id_accesorio;
                $dataacce['serie_bodega']=$DATAac[$m]->bodega;
                $dataacce['cantidad']=$DATAac[$m]->cantidad;
                $dataacce['costo']=$this->ModeloCatalogos->costoaccesorio($idaccesorio);
                $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataacce);
            }  
            $consumibles=$data["consumibles"];
            $DATAcon = json_decode($consumibles);
            for ($co=0;$co<count($DATAcon);$co++){
                $detalleconsumible=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_consumibles',array('id'=>$DATAcon[$co]->consumible));
                $detalleconsumible=$detalleconsumible->result();
                $detalle=$detalleconsumible[0];

              
                $costo_toner=0;
                $rendimiento_toner=0;
                $rendimiento_unidad_imagen=0;

                
                $costo_toner =$detalle->costo_toner;
                $rendimiento_toner =$detalle->rendimiento_toner;
                $rendimiento_unidad_imagen = $detalle->rendimiento_unidad_imagen;
                
                //===============================================
                $dataaccee['idrentas']=$idVenta;
                $dataaccee['idequipo']=$detalle->id_equipo;
                $dataaccee['id_consumibles']=$detalle->id_consumibles;
                $dataaccee['costo_toner']=$costo_toner;
                $dataaccee['rendimiento_toner']=$rendimiento_toner;
                $dataaccee['rendimiento_unidad_imagen']=$rendimiento_unidad_imagen;
                
                $dataaccee['cantidad']=$DATAcon[$co]->cantidad;
                $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$dataaccee);
            } 
        }elseif ($rentaVentaPoliza==3) {
            $equipos=$data["equipos"];
                $DATAeq = json_decode($equipos);
                for ($j=0;$j<count($DATAeq);$j++){
                    $DATAeq[$j]->equipo;
                    $DATAeq[$j]->cantidad;
                    $DATAeq[$j]->bodega;

                    //log_message('error', 'entra tipoventa: 2 rentas');
                    // Se elimina el ID del registro de la cotización de los datos que usaremos
                    unset($datosDeCotizacion[0]->id);
                    // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
                    unset($datosDeCotizacion[0]->estatus);
                    // Se elimina el tipo de "rentaVentaPoliza"
                    unset($datosDeCotizacion[0]->rentaVentaPoliza);
                    // Se elimina el "tipo" , ya que sabemos que es un ingreso de Equipos
                    unset($datosDeCotizacion[0]->tipo);
                    unset($datosDeCotizacion[0]->propuesta);
                unset($datosDeCotizacion[0]->denegada);
                unset($datosDeCotizacion[0]->motivodenegada);
                unset($datosDeCotizacion[0]->reg);

                    // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
                    $datosDeCotizacion[0]->estatus = 1;
                    // Se indica solo como información, el id de la cotización que va con el proceso
                    $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
                    $datosDeCotizacion[0]->reg = $this->fechahoyc;
                    //$datosDeCotizacion[0]->id_personal=$this->idpersonal;
                    // Aunque la variable se llame idVenta, es el ID de la renta
                    $idVenta = $this->Polizas_model->insertaPolizaCreada($datosDeCotizacion[0]);

                    $equipos=$data["equipos"];
                    $DATAeq = json_decode($equipos);
                    for ($j=0;$j<count($DATAeq);$j++){
                        //$DATAeq[$j]->equipo;
                        //$DATAeq[$j]->cantidad;
                        //$DATAeq[$j]->bodega;
                        $detalleequipo=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos',array('id'=>$DATAeq[$j]->equipo));
                        $detalleequipo=$detalleequipo->result();
                        $detalle=$detalleequipo[0];

                        // Se elimina el ID del registro de la cotización de los datos que usaremos
                        unset($detalle->id);
                        // 
                        unset($detalle->idCotizacion);

                        $detalle->idPoliza = $idVenta;
                        //$detalle->serie_bodega=$DATAeq[$j]->bodega;
                        //$detalle->cantidad = $DATAeq[$j]->cantidad;
                        
                        $polizaDetalle = $this->Polizas_model->insertaDetallesPolizaCreada($detalle);
                    }
                }
        }
        if($tipoDeCotizacion==1){
            $tipo = 'Equipos';
        }
        else if($tipoDeCotizacion==2){
            $tipo = 'Consumibles';
        }
        else if ($tipoDeCotizacion==3){
            $tipo = 'Refacciones';
        }
        else if ($tipoDeCotizacion==4){
            $tipo = 'Pólizas';
        }
        if($rentaVentaPoliza==1){
            $rvp = 'ventas';
        }
        else if($rentaVentaPoliza==2){
            $rvp = 'Solcitudes de Contratos Rentas';
        }
        else if ($rentaVentaPoliza==3){
            $rvp = 'Pólizas';
        }
        // Convertir Prospecto a Cliente en caso de ser necesario
        if($idVenta!=0 && $idVenta!=null)
        {
            $datosProspectoCliente = $this->Clientes_model->getClientePorId($datosDeCotizacion[0]->idCliente);
            if($datosProspectoCliente->tipo==2)
            {
                $data = array('tipo' => 1);
                $actualizado = $this->Prospectos_model->update_prospecto($data, $datosDeCotizacion[0]->idCliente);
            }
        }

        $respuestaJSON = array("idVenta"=>$idVenta, "tipo"=>$tipo, "rentaVentaPoliza" => $rvp);
        echo json_encode($respuestaJSON);
    }
    */

    // Función para registrar una nueva cotización
    function nuevaCotizacion(){
        $data = $this->input->post();
        // Saber que se va a cotizar
        $eleccionCotizar = $data['eleccionCotizar'];
        unset($data['eleccionCotizar']);
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }
        
        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }
        

        // Según sea el tipo de Cotización que se va a elegir, se procesará de diferente manera
        switch ($eleccionCotizar){   
            // Equipos
            case 1:
                /*
                // Guardamos el valor para saber si es VENTA, RENTA o PÓLIZA
                $tipoCotizacion = $data["tipo"];
                unset($data['tipo']);
                //$data['rentaVentaPoliza'] = $tipoCotizacion;

                // Saber si es cotización MANUAL o por FAMILIA
                $tipoCotizacionEquipos = $data["cotizar"];
                unset($data['cotizar']);

                // Agisnamos lo que se va a cotizar
                $data['tipo'] = $eleccionCotizar;

                // Se inserta el registro principal
                $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        'tipo' => $data['tipo'],
                                        'rentaVentaPoliza' => $tipoCotizacion,
                                        'estatus' => 1
                                        );
                // Se inserta la cotización principal
                $idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);

                // Si trae el tipo de cotización de equipos con valor 1, es MANUAL
                if($tipoCotizacionEquipos==1){
                    $arregloEquipos = explode(',',$data["equiposIndividuales"]);
                    foreach ($arregloEquipos as $equipoInd) {
                        
                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);

                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1)
                        {
                            $precio = $equipo->costo_venta;
                        }
                        else if ($tipoCotizacion==2)
                        {
                            $precio = $equipo->costo_renta;
                        }
                        else if ($tipoCotizacion==3)
                        {
                            $precio = $equipo->costo_poliza;
                        }
                        //========================================
                        $costo_toner_black = $equipo->costo_toner_black;
                        $costo_toner_cmy = $equipo->costo_toner_cmy;
                        $rendimiento_toner_black = $equipo->rendimiento_toner_black;
                        $rendimiento_toner_cmy = $equipo->rendimiento_toner_cmy;
                        $costo_unidad_imagen = $equipo->costo_unidad_imagen;
                        $rendimiento_unidad_imagen = $equipo->rendimiento_unidad_imagen;
                        $costo_garantia_servicio = $equipo->costo_garantia_servicio;
                        $rendimiento_garantia_servicio = $equipo->rendimiento_garantia_servicio;
                        $costo_pagina_monocromatica = $equipo->costo_pagina_monocromatica;
                        $costo_pagina_color = $equipo->costo_pagina_color;
                        $DATAc = json_decode($consumibles);
                        for ($i=0;$i<count($DATAc);$i++) {
                            if ($equipo->id==$DATAc[$i]->equiposelected) {
                                $idconsumuble=$DATAc[$i]->consumibleselected;
                                log_message('error', 'idconsumible '.$idconsumuble);
                                $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble);
                                foreach ($resultadoaddcon->result() as $item2) {
                                    $costo_toner_black = $item2->costo_toner_black;
                                    $costo_toner_c = $item2->costo_toner_c;
                                    $costo_toner_m = $item2->costo_toner_m;
                                    $costo_toner_y = $item2->costo_toner_y;
                                    $rendimiento_toner_black = $item2->rendimiento_toner_black;
                                    $rendimiento_toner_cmy = $item2->rendimiento_toner_cmy;
                                    $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                    $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                    $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                    $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                    $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                    $costo_pagina_color = $item2->costo_pagina_color;
                                }
                            }
                        }
                        // Se arma el array para insertar los datos
                        $detallesCotizacion = array('idEquipo' => $equipo->id,
                                                    'idCotizacion' => $idCotizacion,
                                                    'modelo' => $equipo->modelo,
                                                    'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                    'precio' => $precio,
                                                    'costo_toner_black' => $costo_toner_black,
                                                    'rendimiento_toner_black' => $rendimiento_toner_black,
                                                    'costo_toner_c' => $costo_toner_c,
                                                    'costo_toner_m' => $costo_toner_m,
                                                    'costo_toner_y' => $costo_toner_y,
                                                    'rendimiento_toner_cmy' => $rendimiento_toner_cmy,
                                                    'costo_unidad_imagen' => $costo_unidad_imagen,
                                                    'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                    'costo_garantia_servicio' => $costo_garantia_servicio,
                                                    'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                    'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                    'costo_pagina_color' => $costo_pagina_color,
                                                    'excedente' => $equipo->excedente,
                                        );
                        // Se insertan en la BD
                        $this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        $datachfc["id_cotizacion"]=$idCotizacion;
                        $datachfc["id_equipo"]=$equipoInd;
                        $datachfc["id_consumibles"]=$idconsumuble;
                        $this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_consumibles',$datachfc);
                    }
                }
                // Si trae el tipo de cotización de equipos con valor 2, es que es por FAMILIA
                else if($tipoCotizacionEquipos==2)
                {
                    $idFamilia = $data["idfamilia"];
                    unset($data['idfamilia']);
                    
                    // Obtenemos los equipos de esa familia
                    $equiposPorFamilia = $this->Equipos_model->getEquiposPorFamilia($idFamilia);                    

                    // Recorremos la familia, para guardar el detalle de la cotización
                    foreach ($equiposPorFamilia as $equipo)                    {
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1){
                            $precio = $equipo->costo_venta;
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $equipo->costo_renta;
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        //========================================
                        $costo_toner_black = $equipo->costo_toner_black;
                        $costo_toner_cmy = $equipo->costo_toner_cmy;
                        $rendimiento_toner_black = $equipo->rendimiento_toner_black;
                        $rendimiento_toner_cmy = $equipo->rendimiento_toner_cmy;
                        $costo_unidad_imagen = $equipo->costo_unidad_imagen;
                        $rendimiento_unidad_imagen = $equipo->rendimiento_unidad_imagen;
                        $costo_garantia_servicio = $equipo->costo_garantia_servicio;
                        $rendimiento_garantia_servicio = $equipo->rendimiento_garantia_servicio;
                        $costo_pagina_monocromatica = $equipo->costo_pagina_monocromatica;
                        $costo_pagina_color = $equipo->costo_pagina_color;
                        $DATAc = json_decode($consumibles);
                        for ($i=0;$i<count($DATAc);$i++) {
                            if ($equipo->id==$DATAc[$i]->equiposelected) {
                                $idconsumuble=$DATAc[$i]->consumibleselected;
                                log_message('error', 'idconsumible '.$idconsumuble);
                                $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble);
                                foreach ($resultadoaddcon->result() as $item2) {
                                    $costo_toner_black = $item2->costo_toner_black;
                                    $costo_toner_c = $item2->costo_toner_c;
                                    $costo_toner_m = $item2->costo_toner_m;
                                    $costo_toner_y = $item2->costo_toner_y;
                                    $rendimiento_toner_black = $item2->rendimiento_toner_black;
                                    $rendimiento_toner_cmy = $item2->rendimiento_toner_cmy;
                                    $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                    $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                    $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                    $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                    $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                    $costo_pagina_color = $item2->costo_pagina_color;
                                }
                            }
                        }
                        // Se arma el array para insertar los datos
                        $detallesCotizacion = array('idEquipo' => $equipo->id,
                                                    'idCotizacion' => $idCotizacion,
                                                    'modelo' => $equipo->modelo,
                                                    'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                    'precio' => $precio,
                                                    'costo_toner_black' => $costo_toner_black,
                                                    'rendimiento_toner_black' => $rendimiento_toner_black,
                                                    'costo_toner_c' => $costo_toner_c,
                                                    'costo_toner_m' => $costo_toner_m,
                                                    'costo_toner_y' => $costo_toner_y,
                                                    'rendimiento_toner_cmy' => $rendimiento_toner_cmy,
                                                    'costo_unidad_imagen' => $costo_unidad_imagen,
                                                    'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                    'costo_garantia_servicio' => $costo_garantia_servicio,
                                                    'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                    'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                    'costo_pagina_color' => $costo_pagina_color,
                                                    'excedente' => $equipo->excedente,
                                        );
                        //var_dump($detallesCotizacion); die();
                        // Se insertan en la BD
                        $this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                    }
                }  
                $DATAa = json_decode($accesorios);
               // log_message('error', 'accesorios:acc'.$accesorios);
                //log_message('error', 'accesorios: data '.$DATAa);
                for ($i=0;$i<count($DATAa);$i++) {
                    
                    $dataas['id_cotizacion']=$idCotizacion;
                    $dataas['id_equipo']=$DATAa[$i]->equiposelected;
                    $dataas['id_accesorio']=$DATAa[$i]->accesorioselected;
                    $this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_accesorios',$dataas);
                    //log_message('error', 'Equipo: '.$equiposelected.' accesorio: '.$accesorioselected);
                }   
                // Se regresa el ID de la cotización
                echo $idCotizacion;
                */
            break;
            // Consumibles
            case 2:
                $equiposSeleccionados = $data['arrayEquipos'];
                unset($data['arrayEquipos']);

                $tonersSeleccionados = $data['arrayToner'];
                unset($data['arrayToner']);

                $piezasSeleccionadas = $data['arrayPiezas'];
                unset($data['arrayPiezas']);

                $presiosSeleccionadas = $data['arrayPrecio'];
                unset($data['arrayPrecio']);

                unset($data['equipoListaConsumibles']);
                unset($data['toner']);

                $data['tipo'] = $eleccionCotizar;

                $equiposArreglo = explode(',', $equiposSeleccionados);
                $tonersArreglo = explode(',', $tonersSeleccionados);
                $piezasArreglo = explode(',', $piezasSeleccionadas);
                $precioArreglo = explode(',', $presiosSeleccionadas);
                

                $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => $data['tipo'],
                                            'rentaVentaPoliza' => 1,
                                            'estatus' => 1,
                                            'id_personal'=>$this->idpersonal
                                            );
                if(isset($data['idcotizacion'])){
                    $idCotizacion=$data['idcotizacion'];
                }else{
                    $idCotizacion=0;
                }
                if($idCotizacion==0){
                    $idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                }else{
                    $idCotizacion=$data['idcotizacion'];
                }
                $datarray_insertconsumibles=array();
                for ($i=0; $i < count($equiposArreglo); $i++){
                    $datosConsumible = $this->Consumibles_model->getConsumibleConPrecios($tonersArreglo[$i]);

                    $total=$precioArreglo[$i]*$piezasArreglo[$i];
                    $detallesCotizacion = array('idCotizacion' => $idCotizacion,
                                            'idEquipo' => $equiposArreglo[$i],
                                            'idConsumible' => $datosConsumible[0]->id,
                                            'piezas' => $piezasArreglo[$i], 
                                            'modelo' => $datosConsumible[0]->modelo,
                                            'rendimiento' =>'',
                                            'descripcion' => $datosConsumible[0]->observaciones,
                                            'precioGeneral' => $precioArreglo[$i],
                                            'totalGeneral' => $total
                                        );
                    $datarray_insertconsumibles[]=$detallesCotizacion;
                    //$this->Cotizaciones_model->insertarDetallesCotizacionConsumibles($detallesCotizacion);
                }
                if(count($datarray_insertconsumibles)>0){
                    $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesConsumibles',$datarray_insertconsumibles);
                }
                echo $idCotizacion;
            break;
            // Refacciones
            case 3:
                $equiposSeleccionados = $data['arrayEquipos'];
                unset($data['arrayEquipos']);

                $arrayRefaccion = $data['arrayRefaccion'];
                unset($data['arrayRefaccion']);
                if(isset($data['seriee'])){
                   $arrayseriee = $data['seriee']; 
               }else{
                $arrayseriee='';
               }
                
                unset($data['seriee']);

                $piezasSeleccionadas = $data['arrayPiezas'];
                unset($data['arrayPiezas']);

                $arrayrprecio = $data['arrayrprecio'];
                unset($data['arrayrprecio']);

                $equiposArreglo = explode(',', $equiposSeleccionados);
                $refaccionesArreglo = explode(',', $arrayRefaccion);
                $serieeArreglo = explode(',', $arrayseriee);
                $piezasArreglo = explode(',', $piezasSeleccionadas);
                $preciosArreglo = explode(',', $arrayrprecio);
                

                $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => $eleccionCotizar,
                                            'rentaVentaPoliza' => 1,
                                            'estatus' => 1,
                                            'propuesta'=>$data['propuesta'],
                                            'id_personal'=>$this->idpersonal
                                            );
                if(isset($data['idcotizacion'])){
                    $idCotizacion=$data['idcotizacion'];
                }else{
                    $idCotizacion=0;
                }
                if($idCotizacion==0){
                    $idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                }else{
                    $idCotizacion=$data['idcotizacion'];
                }
                $datarray_insertrefacciones=array();
                for ($i=0; $i < count($refaccionesArreglo); $i++) 
                {
                    if($refaccionesArreglo[$i]>0){
                    $datosRefaccion = $this->Refacciones_model->getRefaccionConPrecios($refaccionesArreglo[$i]);
                    
                    $detallesCotizacion = array(
                                            'idCotizacion' => $idCotizacion,
                                            'idEquipo' => $equiposArreglo[$i],
                                            'serieequipo' => $serieeArreglo[$i],
                                            'piezas' => $piezasArreglo[$i], 
                                            'modelo' => $datosRefaccion[0]->nombre,
                                            'descripcion' => $datosRefaccion[0]->observaciones,
                                            'precioGeneral' => $preciosArreglo[$i],
                                            'totalGeneral' => ($piezasArreglo[$i]*$preciosArreglo[$i]),
                                            'idRefaccion' => $refaccionesArreglo[$i]
                                        );
                    $datarray_insertrefacciones[]=$detallesCotizacion;
                    
                    //$this->Cotizaciones_model->insertarDetallesCotizacionRefacciones($detallesCotizacion);
                    }
                }
                if(count($datarray_insertrefacciones)>0){
                    $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesRefacciones',$datarray_insertrefacciones);
                }
                echo $idCotizacion;
            break;
            // Pólizas
            case 4:
                /*
                $polizasSeleccionadas = $data['arrayPolizas'];
                unset($data['arrayPolizas']);

                $polizasSeleccionadasDetalles = $data['arrayDetallesPoliza'];
                unset($data['arrayDetallesPoliza']);
                //nueva variable
                $polizasExistentesDetallescostos = $data['arrayDetallesPolizacostos'];
                unset($data['arrayDetallesPolizacostos']);
                //
                $idEquipo = $data['equipoLista'];
                
                $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => $eleccionCotizar,
                                            'rentaVentaPoliza' => 3,
                                            'estatus' => 1
                                            );

                $idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                //===================================================================================================
                $polizasArreglo = explode(',', $polizasSeleccionadas);
                $polizasArregloDetalles = explode(',', $polizasSeleccionadasDetalles);
                //nueva
                $polizasArregloDetallescosto = explode(',', $polizasExistentesDetallescostos);
                //
                for ($i=0; $i < count($polizasArreglo); $i++) 
                {                              
                    $datosPoliza = $this->Polizas_model->getPolizaConUnDetalle($polizasArreglo[$i],$polizasArregloDetalles[$i]);// hace una busqueda
                    if ($polizasArregloDetallescosto[$i]=='local') {
                        $precio_local=$datosPoliza[0]->precio_local;
                        $precio_semi=null;
                        $precio_foraneo=null;
                    }elseif ($polizasArregloDetallescosto[$i]=='foraneo') {
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_foraneo=$datosPoliza[0]->precio_foraneo;
                    }else{
                        $precio_local=null;
                        $precio_semi=$datosPoliza[0]->precio_semi;
                        $precio_foraneo=null;
                    }

                    $detallesCotizacion = array('idPoliza' => $polizasArreglo[$i],
                                            'idEquipo' => $idEquipo,
                                            'idCotizacion' => $idCotizacion,
                                            'nombre' => $datosPoliza[0]->nombre, 
                                            'cubre' => $datosPoliza[0]->cubre,
                                            'vigencia_meses' => $datosPoliza[0]->vigencia_meses,
                                            'vigencia_clicks' => $datosPoliza[0]->vigencia_clicks,
                                            'modelo' => $datosPoliza[0]->modelo,
                                            'precio_local' => $precio_local,
                                            'precio_semi' => $precio_semi,
                                            'precio_foraneo' => $precio_foraneo
                                        );
                    $this->Cotizaciones_model->insertarDetallesCotizacionPoliza($detallesCotizacion);
                }
                //===============================================================================
                echo $idCotizacion;
                */
            break;
            
            default:
                
            break;
        }
    }

    /******************************************************************************/
    /*************************** Cotización de Póliza *****************************/
    /******************************************************************************/

    public function getDatosPolizaporId($idPoliza){
        $detallesPoliza = $this->Polizas_model->getDetallesPoliza($idPoliza);
        $html = '<option selected="selected" disabled="disabled"  data-local="" data-foraneo="" data-semi="" data-especial=""></option>';
        foreach($detallesPoliza as $detallePoliza)
        {
            $html .= $this->creaOptionsp($detallePoliza);
        }
        echo $html;
    }
    public function getDatosPolizaporcostoId($idPoliza){
        /*
        $detallesPolizacosto = $this->Polizas_model->getDetallesPolizacosto($idPoliza);
        */
        $html = '';
        /*
        $html .= '<option selected="selected" disabled="disabled"></option>';
        foreach($detallesPolizacosto as $detallesPolizacosto)
        {
               $local = $detallesPolizacosto->precio_local;
               $semi = $detallesPolizacosto->precio_semi;
               $foraneo = $detallesPolizacosto->precio_foraneo;
        } 
        */
            $html .= '<option value="local">Local</option>';
            $html .= '<option value="semi">Semi</option>';
            $html .= '<option value="foraneo">Foraneo</option>';
        echo $html;
    }

    // Crea la vista para la cotizacion de una Póliza
    function documentoPolizas($idCotizacion) {
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $data['datosCotizacion'] = $datosCotizacion;
        $data['fechareg']=$this->reformateofecha($datosCotizacion[0]->reg);
        
        $nombreUsuario = $this->ModeloPersonal->personalview($datosCotizacion[0]->id_personal);
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;

        $data['datosCotizacionequipos'] = $this->Cotizaciones_model->cotizacionequipospoliza($idCotizacion);
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($data['datosCotizacion'][0]->idCliente);

        $this->load->view('header');
        $this->load->view('cotizacion/documentoPolizasBordes',$data);
        $this->load->view('cotizacion/jscotizar_polizas');
        $this->load->view('footer'); 
    }

    // Regresa los datos a la tabla que se muestra en la vista previa de la impresión de la cotización de Póliza
    /*
    function getTablaDetallesPoliza($idCotizacion){
        $detallesCotizacion = $this->Cotizaciones_model->getDetallesCotizacionPolizaTabla($idCotizacion);
        $json_data = array("data" => $detallesCotizacion);
        echo json_encode($json_data);
    }
    */

    // Actualizar datos del detalles de la Cotización de la Póliza
    function actualizaDetalleCotizacionPoliza(){
        $data = $this->input->post();

        // La variable KEY traerá el ID del registro a actualizar
        $key = key($data["data"]);
        
        // Se actualiza elregistro        
        $idActualizado = $this->Cotizaciones_model->actualizaDetalleCotizacionPoliza($data["data"][$key], $key);
        
        // Se obtienene todos los datos del listado
        $registroActualizado = $this->Cotizaciones_model->getDetalleCotizacionPolizaPorId($key);
        $registroActualizadoAux = (array) $registroActualizado[0];
        
        // Se arma el array
        $dataRetorno = array($registroActualizadoAux); 
        
        // Se crea el data y se regresa en JSON
        $datosRetorno = array("data" => $dataRetorno);

        echo json_encode($datosRetorno);
    }

    /*******************************************************************************/
    /*************************** Cotización de Equipos *****************************/
    /*******************************************************************************/
    function documento($idCotizacion){
        if(isset($_GET['tipocot'])){
            $tipocot=$_GET['tipocot'];
            if($tipocot>0){
                $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('tipocot'=>$tipocot),array('id'=>$idCotizacion));
            }
        }
        $data['perfilid']=$this->perfilid;
        $tipog=0;
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $data['datosCotizacion'] = $datosCotizacion;
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($data['datosCotizacion'][0]->idCliente);
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($datosCotizacion[0]->tipov);
        $nombreUsuario = $this->ModeloPersonal->personalview($datosCotizacion[0]->id_personal);
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;
        $data['fechareg']=$this->reformateofecha($datosCotizacion[0]->reg);
        //$data['accesorios'] = $this->Cotizaciones_model->getcotizacionaccesorios($idCotizacion);
        $data['idCotizacion']=$idCotizacion;
        $data['datoscontacto']=$this->ModeloCatalogos->db10_getselectwheren('cliente_datoscontacto',array('clienteId'=>$datosCotizacion[0]->idCliente,'activo'=>1));
        //================================================================================
            $detallesCotizacion_equipo=$this->Cotizaciones_model->getDetallesCotizacionEquiposTabla($idCotizacion);
            $data['detallesCotizacion_equipo'] = $detallesCotizacion_equipo;

            $detallesCotizacion_consumibles = $this->Cotizaciones_model->getDetallesCotizacionConsumiblesTabla($idCotizacion);
            $data['detallesCotizacion_consumibles'] = $detallesCotizacion_consumibles;

            $detallesCotizacion_refaccion = $this->Cotizaciones_model->getDetallesCotizacionRefaccionesTabla($idCotizacion);
            $data['detallesCotizacion_refaccion'] = $detallesCotizacion_refaccion;

            $datosCotizacionequipospoliza = $this->Cotizaciones_model->cotizacionequipospoliza($idCotizacion);
            $data['datosCotizacionequipospoliza'] = $datosCotizacionequipospoliza;

            $detallesCotizacionacc=$this->Cotizaciones_model->getDetallesCotizacionEquiposaccTabla($idCotizacion);
            $data['detallesCotizacionacc'] = $detallesCotizacionacc;

            if($detallesCotizacion_equipo->num_rows()>0){
                $tipog.=',1';
            }
            if($detallesCotizacion_consumibles->num_rows()>0){
                $tipog.=',2';
            }
            if($detallesCotizacion_refaccion->num_rows()>0){
                $tipog.=',3';
            }
            if($datosCotizacionequipospoliza->num_rows()>0){
                $tipog.=',4';
            }
            if($detallesCotizacionacc->num_rows()>0){
                $tipog.=',5';//accesorios
            }
            $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('tipog'=>$tipog),array('id'=>$idCotizacion));
        //================================================================================

        $tipo=$data['datosCotizacion'][0]->rentaVentaPoliza;
        $data['tipo']=$tipo;
        if ($tipo==1) {
            $data['tipoimg']='<img src="'.base_url().'public/img/kyovent.png" class="onMouseOverDisabled">';
        }elseif ($tipo==2) {

            //redirect('/Cotizaciones/documentoHorizontalConBordesr/'.$idCotizacion);//renta
            //$data['tipoimg']='<img src="'.base_url().'public/img/kyorent.png" class="onMouseOverDisabled">';
            $data['tipoimg']='';
        }elseif ($tipo==3) {
            $data['tipoimg']='<img src="'.base_url().'public/img/kyopo.png" class="onMouseOverDisabled">';
        }else{
            $data['tipoimg']='';
        }
        $tipoig=$data['datosCotizacion'][0]->tiporenta;
            //echo $tipoig.'xxx';
            if ($tipoig==1) {
                redirect('/Cotizaciones/documentoHorizontalConBordesri/'.$idCotizacion);//renta
            }elseif ($tipoig==2) {
                redirect('/Cotizaciones/documentoHorizontalConBordesrg/'.$idCotizacion);//renta
            }
        //$data['button_add_cotizacion']=$datosCotizacion[0]->idCliente.'?idcotizacion='.$idCotizacion.'&eleccionCotizar='.$datosCotizacion[0]->tipo.'&rentaventapoliza='.$datosCotizacion[0]->rentaVentaPoliza;
        $data['button_add_cotizacion']=$datosCotizacion[0]->idCliente.'?idcotizacion='.$idCotizacion.'&tipocot=0&cotrent=0';//tipocot(0 normal venta poliza) (1 solo rentas) / cotrent (solo rentas 1 individual 2 global)
        $this->load->view('header');
        $this->load->view('footer'); 
        $this->load->view('cotizacion/documento_header',$data);
        if($detallesCotizacion_equipo->num_rows()>0){
            $this->load->view('cotizacion/documento_equipos');    
        }
        if($detallesCotizacion_consumibles->num_rows()>0){
            $this->load->view('cotizacion/documento_consumible'); 
        }
        if($detallesCotizacion_refaccion->num_rows()>0){
            $this->load->view('cotizacion/documento_refraccion'); 
        }
        if($datosCotizacionequipospoliza->num_rows()>0){
            $this->load->view('cotizacion/documento_polizas'); 
        }
        if ($detallesCotizacionacc->num_rows()>0) {
            $this->load->view('cotizacion/documento_accesorios'); 
        }
        
        $this->load->view('cotizacion/documento_footer'); 

        
        
    }
    function documentoHorizontalConBordes($idCotizacion){
        $data['perfilid']=$this->perfilid;
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $data['datosCotizacion'] = $datosCotizacion;
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($data['datosCotizacion'][0]->idCliente);
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $nombreUsuario = $this->ModeloPersonal->personalview($datosCotizacion[0]->id_personal);
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;
        $data['fechareg']=$this->reformateofecha($datosCotizacion[0]->reg);
        //$data['accesorios'] = $this->Cotizaciones_model->getcotizacionaccesorios($idCotizacion);
        $data['idCotizacion']=$idCotizacion;
        $detallesCotizacion=$this->Cotizaciones_model->getDetallesCotizacionEquiposTabla($idCotizacion);
        $data['detallesCotizacion'] = $detallesCotizacion;

        $tipo=$data['datosCotizacion'][0]->rentaVentaPoliza;
        $data['tipo']=$tipo;
        if ($tipo==1) {
            $data['tipoimg']='<img src="'.base_url().'public/img/kyovent.png" class="onMouseOverDisabled">';
        }elseif ($tipo==2) {

            //redirect('/Cotizaciones/documentoHorizontalConBordesr/'.$idCotizacion);//renta
            $data['tipoimg']='<img src="'.base_url().'public/img/kyorent.png" class="onMouseOverDisabled">';
        }elseif ($tipo==3) {
            $data['tipoimg']='<img src="'.base_url().'public/img/kyopo.png" class="onMouseOverDisabled">';
        }else{
            $data['tipoimg']='';
        }
        $tipoig=$data['datosCotizacion'][0]->tiporenta;
            //echo $tipoig.'xxx';
            if ($tipoig==1) {
                redirect('/Cotizaciones/documentoHorizontalConBordesri/'.$idCotizacion);//renta
            }elseif ($tipoig==2) {
                redirect('/Cotizaciones/documentoHorizontalConBordesrg/'.$idCotizacion);//renta
            }
        $data['button_add_cotizacion']=$datosCotizacion[0]->idCliente.'?idcotizacion='.$idCotizacion.'&eleccionCotizar='.$datosCotizacion[0]->tipo.'&rentaventapoliza='.$datosCotizacion[0]->rentaVentaPoliza;
        $this->load->view('header');
        $this->load->view('footer'); 
        if($detallesCotizacion->num_rows()>0){
            $this->load->view('cotizacion/documentoBordesHorizontal',$data);    
        }else{
            $detallesCotizacion=$this->Cotizaciones_model->getDetallesCotizacionEquiposaccTabla($idCotizacion);
            $data['detallesCotizacion'] = $detallesCotizacion;
            $this->load->view('cotizacion/documentoBordesHorizontalacc',$data);
        }
        
        $this->load->view('cotizacion/jscotizar_equipos');   
    }
    function documentoHorizontalConBordesri($idCotizacion){
        $data['perfilid']=$this->perfilid;
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $data['datosCotizacion'] = $datosCotizacion;
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($data['datosCotizacion'][0]->idCliente);
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $nombreUsuario = $this->ModeloPersonal->personalview($datosCotizacion[0]->id_personal);
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;

        $data['fechareg']=$this->reformateofecha($datosCotizacion[0]->reg);
        //$data['accesorios'] = $this->Cotizaciones_model->getcotizacionaccesorios($idCotizacion);
        $data['idCotizacion']=$idCotizacion;
        $data['detallesCotizacion'] = $this->Cotizaciones_model->getDetallesCotizacionEquiposTabla($idCotizacion);

        $tipo=$data['datosCotizacion'][0]->rentaVentaPoliza;
        $data['tipo']=$tipo;
        $data['tipoimg']='<img src="'.base_url().'public/img/kyorent.png" class="onMouseOverDisabled">';
        $data['button_add_cotizacion']=$datosCotizacion[0]->idCliente.'?idcotizacion='.$idCotizacion.'&tipocot=1&cotrent=1';//tipocot(0 normal venta poliza) (1 solo rentas) / cotrent (solo rentas 1 individual 2 global)
        $this->load->view('header');
        $this->load->view('footer'); 
        $this->load->view('cotizacion/documentoBordesHorizontal_rentai',$data);
        $this->load->view('cotizacion/jscotizar_equipos');   
    }
    function documentoHorizontalConBordesrg($idCotizacion){
        $data['perfilid']=$this->perfilid;
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $data['datosCotizacion'] = $datosCotizacion;
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($data['datosCotizacion'][0]->idCliente);
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $nombreUsuario = $this->ModeloPersonal->personalview($datosCotizacion[0]->id_personal);
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;

        $data['fechareg']=$this->reformateofecha($datosCotizacion[0]->reg);
        //$data['accesorios'] = $this->Cotizaciones_model->getcotizacionaccesorios($idCotizacion);
        $data['idCotizacion']=$idCotizacion;
        $data['detallesCotizacion'] = $this->Cotizaciones_model->getDetallesCotizacionEquiposTabla($idCotizacion);

        $tipo=$data['datosCotizacion'][0]->rentaVentaPoliza;
        $data['tipo']=$tipo;
        $data['tipoimg']='<img src="'.base_url().'public/img/kyorent.png" class="onMouseOverDisabled">';
        $data['button_add_cotizacion']=$datosCotizacion[0]->idCliente.'?idcotizacion='.$idCotizacion.'&tipocot=1&cotrent=2';//tipocot(0 normal venta poliza) (1 solo rentas) / cotrent (solo rentas 1 individual 2 global)
        $this->load->view('header');
        $this->load->view('footer'); 
        $this->load->view('cotizacion/documentoBordesHorizontal_rentag',$data);
        $this->load->view('cotizacion/jscotizar_equipos');   
    }

    // Regresa los datos a la tabla que se muestra en la vista previa de la impresión de la cotización de Equipos
    function getTablaDetallesEquipos($idCotizacion){
        $detallesCotizacion = $this->Cotizaciones_model->getDetallesCotizacionEquiposTabla($idCotizacion);
        $json_data = array("data" => $detallesCotizacion);
        echo json_encode($json_data);
    }

    // Actualizar datos del detalles de la Cotización de la Póliza
    function actualizaDetalleCotizacionEquipo(){
        $data = $this->input->post();

        // La variable KEY traerá el ID del registro a actualizar
        $key = key($data["data"]);
        
        // Se actualiza elregistro        
        $idActualizado = $this->Cotizaciones_model->actualizaDetalleCotizacionEquipo($data["data"][$key], $key);
        
        // Se obtienene todos los datos del listado
        $registroActualizado = $this->Cotizaciones_model->getDetalleCotizacionEquipoPorId($key);
        $registroActualizadoAux = (array) $registroActualizado[0];
        
        // Se arma el array
        $dataRetorno = array($registroActualizadoAux); 
        
        // Se crea el data y se regresa en JSON
        $datosRetorno = array("data" => $dataRetorno);

        echo json_encode($datosRetorno);
    }

    // Obtener los equipos relacionados a la cotización que se acaba de elegir
    // Y regresar los resultados como opciones para el SELECT donde ese muestran
    public function getDetalleEquiposPorIdCotizacion($idCotizacion){
        $detallesEquipos = $this->Cotizaciones_model->getDetalleEquiposPorIdCotizacion($idCotizacion);
        $html = '<option selected="selected" disabled="disabled"></option>';
        foreach($detallesEquipos as $detalleEquipo)
        {
            $html .= $this->creaOptions($detalleEquipo);
        }
        echo $html;
    }
    function getdatosdecotizacionequipos($idCotizacion){
        $equiposdata='';
        $consumibles='';
        $accesorios='';
        $bodegas='';
        //================================================================================
            //$resultbodegas=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
            $resultbodegas=$this->ModeloCatalogos->view_session_bodegas();
            $bodegas.='<select id="serie_bodegae" name="serie_bodegae" class="browser-default chosen-select" >';
            foreach ($resultbodegas as $itemb) {
                $bodegas.='<option value="'.$itemb->bodegaId.'">'.$itemb->bodega.'</option>';
            }
            $bodegas.='</select>';
        //================================================================================
            $resultequipos=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos',array('idCotizacion'=>$idCotizacion));
            foreach ($resultequipos->result() as $iteme) {
                $stockequipo=$this->ModeloGeneral->stockequipo($iteme->idEquipo);
                $equiposdata.='<tr>
                                    <td>
                                        <input type="checkbox" 
                                                class="equipo_check_coti equipo_check_'.$iteme->idEquipo.'" 
                                                id="equipo_check_coti_'.$iteme->id.'" 
                                                value="'.$iteme->id.'" 
                                                onchange="alertastockequipo('.$iteme->idEquipo.','.$stockequipo.');"
                                        />
                                        <label for="equipo_check_coti_'.$iteme->id.'"></label>
                                    </td>
                                    <td style="width: 140px;">
                                        <input type="text" 
                                            class="equipo_check_coti_cantidad input_cantidad_touspin equ_cant_'.$iteme->idEquipo.'" 
                                            id="cantidad_equipo" value="'.$iteme->cantidad.'"
                                            oninput="alertastockequipo('.$iteme->idEquipo.','.$stockequipo.');"
                                        />
                                    </td>
                                    <td>'.$iteme->modelo.' ('.$stockequipo.')</td>
                                    <td>'.$bodegas.'</td>
                                </tr>';
            }
            $resultconsumibles=$this->ModeloCatalogos->detalle_cotizacion_consumible_equipos($idCotizacion);
            foreach ($resultconsumibles->result() as $itemc) {
                $consumibles.='<tr>
                                    <td>
                                        <input type="checkbox" 
                                        class="consumibles_check_coti consum_check_'.$itemc->id_consumibles.'" 
                                        id="consumibles_check_coti_'.$itemc->id.'" 
                                        value="'.$itemc->id.'"
                                        onchange="alertastocktoner('.$itemc->id_consumibles.');"
                                        "/>
                                        <label for="consumibles_check_coti_'.$itemc->id.'"></label>
                                    </td>
                                    <td style="width: 140px;">
                                        <input type="text" 
                                            class="input_cantidad_touspin consu_cant_'.$itemc->id_consumibles.'" 
                                            id="cantidad_consumibles" 
                                            value="'.$itemc->cantidad.'"
                                            oninput="alertastocktoner('.$itemc->id_consumibles.');"
                                            />
                                        <input type="hidden" id="id_consumibles" value="'.$itemc->id_consumibles.'" />
                                    </td>
                                    <td>'.$itemc->modelo.' <span class="consu_stock_'.$itemc->id_consumibles.'"></span></td>
                                    <td><b>'.$itemc->modeloc.'</b></td>
                                    <td>'.$bodegas.'</td>
                                </tr>';

            }
            $resultaccesorios=$this->ModeloCatalogos->detalle_cotizacion_accesorios_equipos($idCotizacion);
            foreach ($resultaccesorios->result() as $itema) {
                $accesorios.='<tr>
                                    <td>
                                        <input type="checkbox" 
                                            class="accesorio_check_coti acce_check_'.$itema->id.'" 
                                            id="accesorio_check_coti_'.$itema->id_accesoriod.'" 
                                            value="'.$itema->id_accesoriod.'"
                                            onchange="alertastockacces('.$itema->id.');"
                                        />
                                        <label for="accesorio_check_coti_'.$itema->id_accesoriod.'"></label>
                                    </td>
                                    <td style="width: 140px;">
                                        <input type="text" 
                                            class="input_cantidad_touspin access_cant_'.$itema->id.'" 
                                            id="cantidad_accesorio" 
                                            oninput="alertastockacces('.$itema->id.');"
                                            value="'.$itema->cantidad.'"/>
                                    </td>
                                    <td>'.$itema->modelo.' </td>
                                    <td><b>'.$itema->nombre.'</b> <span class="access_stock_'.$itema->id.'"></span></td>
                                    <td>'.$bodegas.'</td>
                                </tr>';
            }



        $array  = array(
            'equipos' => $equiposdata,
            'consumibles' => $consumibles,
            'accesorios' => $accesorios, 
        );
        echo json_encode($array);
    }

    // Obtener las polizas relacionadas a la cotización que se acaba de elegir
    // Y regresar los resultados como opciones para el SELECT donde ese muestran
    public function getDetallePolizaPorIdCotizacion($idCotizacion){
        $equipos=$this->Cotizaciones_model->cotizacionequipospoliza($idCotizacion);
        $html = '';
        foreach ($equipos->result() as $iteme) { 
                    $detallesCotizacion = $this->Cotizaciones_model->getDetallesCotizacionPolizaTablae($iteme->idCotizacion,$iteme->idEquipo);
                    
                    foreach ($detallesCotizacion->result() as $itemed) {
                        $html .='<tr>
                                   <td>
                                      <input type="checkbox" class="polizaselected" id="polizaselected_'.$itemed->id.'" name="polizaselected" value="'.$itemed->id.'"/>
                                      <label for="polizaselected_'.$itemed->id.'"></label>
                                   </td> 
                                   <td>
                                     <input type="number" id="cantidad" placeholder="cantidad" value="'.$itemed->cantidad.'" min="1" class="detallespoliza_e_'.$itemed->id.' detallespoliza_e" onchange="appendseriespolizas('.$itemed->id.')">
                                   </td> 
                                   <td>'.$iteme->modelo.' '.$itemed->nombre.' ('.$itemed->modelo.')</td> 
                                   <td>
                                     <table class="table_series_e_'.$itemed->id.'">
                                        <tbody>
                                            <tr>
                                                <td style="padding-top: 2px;padding-bottom: 2px;">
                                                    <input type="text" id="polizaserie" class="form-control-b v" value="'.$itemed->serie.'">
                                                <td>
                                            <tr>
                                        </tbody>
                                     </table>
                                   </td> 
                                  </tr>';
                    }
                    
        }

        
        echo $html;
    }

    // Verificar que la familia tenga los 3 integrantes
    public function verificaFamilia($idFamilia){
        $busquedaFamilia = $this->Cotizaciones_model->verificaFamilia($idFamilia);
        
        if($busquedaFamilia->total==0)
        {
            // La familia no tiene integrantes
            echo 0;
        }
        else if($busquedaFamilia->total==3)
        {
            // La familia tiene sus 3 integrantes
            echo 1;
        }
        else if($busquedaFamilia->total>3)
        {
            // La familia tiene registrados mas de 3 integrantes
            echo 2;
        }
        else if($busquedaFamilia->total<3)
        {
            // La familia tiene registrados menos de 2 integrantes
            echo 3;
        }
    }

    /***********************************************************************************/
    /*************************** Cotización de Consumibles *****************************/
    /***********************************************************************************/

    public function getDatosConsumiblesPorEquipo($idEquipo,$bodega=0){
        $detallesConsumibles = $this->Consumibles_model->getConsumiblesPorIdEquipo($idEquipo,$bodega);
        $html = '<option selected="selected" disabled="disabled"></option>';
        foreach($detallesConsumibles as $detalleConsumible)
        {
            if ($detalleConsumible->stock>0) {
                $stock=$detalleConsumible->stock;
            }else{
                $stock=0;
            }
            if ($detalleConsumible->stock_resg>0) {
                $stock_resg=$detalleConsumible->stock_resg;
            }else{
                $stock_resg=0;
            }
            $stock=$stock-$stock_resg;
            //$html .= $this->creaOptions($detalleConsumible);
            if($stock<0){
                $stock=0;
            }
            $html .= '<option value="'.$detalleConsumible->id.'" data-stock="'.$stock.'">'.$detalleConsumible->modelo.' ('.$stock.')</option>';
        }
        echo $html;
    }
    public function getDatosConsumiblesPorEquipop($idconsumible){
        $detallesprecios = $this->ModeloCatalogos->db13_getselectwheren('consumibles_costos',array('consumibles_id' =>$idconsumible));
        $html = '';
        foreach($detallesprecios->result() as $item){
            $html.= '<option value="'.$item->general.'">General('.$item->general.')</option>'; //general
            $html.= '<option value="'.$item->frecuente.'">Frecuente('.$item->frecuente.')</option>'; //frecuente
            $html.= '<option value="'.$item->especial.'">Especial('.$item->especial.')</option>'; //especial
            $html.= '<option value="'.$item->poliza.'">Poliza('.$item->poliza.')</option>'; //poliza
        }
        $html .= '<option value="0" class="costocero">0</option>';
        $data_select = array('select_option' => $html,'espe'=>$item->especial);
        echo json_encode($data_select);
    }

    function documentoConsumiblesBordes($idCotizacion){
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $data['datosCotizacion'] = $datosCotizacion;
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($data['datosCotizacion'][0]->idCliente);
        
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        $nombreUsuario = $this->ModeloPersonal->personalview($datosCotizacion[0]->id_personal);
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;

        $data['fechareg']=$this->reformateofecha($datosCotizacion[0]->reg);
        $data['detallesCotizacion'] = $this->Cotizaciones_model->getDetallesCotizacionConsumiblesTabla($idCotizacion);

        $data['button_add_cotizacion']=$datosCotizacion[0]->idCliente.'?idcotizacion='.$idCotizacion.'&eleccionCotizar='.$datosCotizacion[0]->tipo.'&rentaventapoliza='.$datosCotizacion[0]->rentaVentaPoliza;

        $this->load->view('header');
        $this->load->view('cotizacion/documentoConsumiblesBordes',$data);
        $this->load->view('cotizacion/jscotizar_consumibles');
        $this->load->view('footer'); 
    }

    // Regresa los datos a la tabla que se muestra en la vista previa de la impresión de la cotización de Equipos
    function getTablaDetallesConsumibles($idCotizacion){
        $detallesCotizacion = $this->Cotizaciones_model->getDetallesCotizacionConsumiblesTabla($idCotizacion);
        $json_data = array("data" => $detallesCotizacion);
        echo json_encode($json_data);
    }

    // Actualizar datos del detalles de la Cotización de la Póliza
    function actualizaDetalleCotizacionConsumible(){
        $data = $this->input->post();

        // La variable KEY traerá el ID del registro a actualizar
        $key = key($data["data"]);
        
        // Se actualiza elregistro        
        $idActualizado = $this->Cotizaciones_model->actualizaDetalleCotizacionConsumible($data["data"][$key], $key);
        
        // Se obtienene todos los datos del listado
        $registroActualizado = $this->Cotizaciones_model->getDetalleCotizacionConsumiblePorId($key);
        $registroActualizadoAux = (array) $registroActualizado[0];
        
        // Se arma el array
        $dataRetorno = array($registroActualizadoAux); 
        
        // Se crea el data y se regresa en JSON
        $datosRetorno = array("data" => $dataRetorno);

        echo json_encode($datosRetorno);
    }

    /***********************************************************************************/
    /*************************** Cotización de Refacciones *****************************/
    /***********************************************************************************/

    public function getDatosRefaccionesPorEquipo($idEquipo){

        $detallesRefacciones = $this->Refacciones_model->getRefaccionesPorIdEquipo($idEquipo);

        $html = '<option selected="selected" disabled="disabled"></option>';
        foreach($detallesRefacciones as $detalleRefaccion)
        {
            $html .= $this->creaOptions($detalleRefaccion);
        }
        echo $html;
    }

    function documentoRefaccionesBordes($idCotizacion){
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $data['datosCotizacion'] =$datosCotizacion;
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($data['datosCotizacion'][0]->idCliente);
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion(1);
        
        $nombreUsuario = $this->ModeloPersonal->personalview($datosCotizacion[0]->id_personal);
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;

        $data['fechareg']=$this->reformateofecha($datosCotizacion[0]->reg);
        $data['detallesCotizacion'] = $this->Cotizaciones_model->getDetallesCotizacionRefaccionesTabla($idCotizacion);

        $data['button_add_cotizacion']=$datosCotizacion[0]->idCliente.'?idcotizacion='.$idCotizacion.'&eleccionCotizar='.$datosCotizacion[0]->tipo.'&rentaventapoliza='.$datosCotizacion[0]->rentaVentaPoliza;
        
        $this->load->view('header');
        $this->load->view('cotizacion/documentoRefaccionesBordes',$data);
        $this->load->view('cotizacion/jscotizar_refacciones');
        $this->load->view('footer'); 
    }

    // Regresa los datos a la tabla que se muestra en la vista previa de la impresión de la cotización de Equipos
    /*
    function getTablaDetallesRefacciones($idCotizacion){
        $detallesCotizacion = $this->Cotizaciones_model->getDetallesCotizacionRefaccionesTabla($idCotizacion);
        $json_data = array("data" => $detallesCotizacion);
        echo json_encode($json_data);
    }*/

    // Actualizar datos del detalles de la Cotización de la Póliza
    function actualizaDetalleCotizacionRefaccion(){
        $data = $this->input->post();

        // La variable KEY traerá el ID del registro a actualizar
        $key = key($data["data"]);
        
        // Se actualiza elregistro        
        $idActualizado = $this->Cotizaciones_model->actualizaDetalleCotizacionRefaccion($data["data"][$key], $key);
        
        // Se obtienene todos los datos del listado
        $registroActualizado = $this->Cotizaciones_model->getDetalleCotizacionRefaccionPorId($key);
        $registroActualizadoAux = (array) $registroActualizado[0];
        
        // Se arma el array
        $dataRetorno = array($registroActualizadoAux); 
        
        // Se crea el data y se regresa en JSON
        $datosRetorno = array("data" => $dataRetorno);

        echo json_encode($datosRetorno);
    }

    /**************************************************************/
    /******************* FUNCIONES GENERALES **********************/

    // Función para crear "Options" dinamicamente para un SELECT
    function creaOptions($opcion){
        return '<option value="'.$opcion->id.'">'.$opcion->modelo.'</option>';
    }
    function creaOptionsp($opcion){
        $html='';
        $html.= '<option value="'.$opcion->id.'" ';
        $html.='data-local="'.$opcion->precio_local.'" ';
        $html.='data-foraneo="'.$opcion->precio_foraneo.'" ';
        $html.='data-semi="'.$opcion->precio_semi.'" ';
        $html.='data-especial="'.$opcion->precio_especial.'" ';
        $html.='data-modelo="'.$opcion->modelo.'" ';
        $html.='>'.$opcion->modelo.'</option>';
        return $html;
    }
    function editarprecio(){
        $precio = $this->input->post('precio');
        $equipo = $this->input->post('equipo');
        $pass = $this->input->post('pass');
        $idCliente = $this->input->post('idCliente');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion desde cotizaciones con el usuario:'.$item->Usuario.' para la edicion de precios','nombretabla'=>'cotizaciones_has_detallesEquipos','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        if($permiso==0 and $idCliente==597){
            if($pass=='DHL2023' or $pass=='dhl2023'){
                $permiso=1;
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el con DHL2023 para la edicion de precios','nombretabla'=>'cotizaciones_has_detallesEquipos','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }
        if ($permiso==1) {
            $data['precio']=$precio;
            $where = array('id' => $equipo );
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos',$data,$where);
        }

        echo $permiso;
    }
    function editarpiezasequipos(){
        $params = $this->input->post();
        $id = $params['id'];
        $cantidad = $params['cantidad'];
        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos',array('cantidad' => $cantidad ),array('id' => $id ));

        $result=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos',array('id' => $id ));
        foreach ($result->result() as $item) {
            $idEquipo = $item->idEquipo;
            $idCotizacion = $item->idCotizacion;
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos_accesorios',array('cantidad' => $cantidad ),array('id_cotizacion' => $idCotizacion,'id_equipo'=>$idEquipo ));
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos_consumibles',array('cantidad' => $cantidad ),array('id_cotizacion' => $idCotizacion,'id_equipo'=>$idEquipo ));
        }
    } 
    function editarpiezasequiposacc(){
        $params = $this->input->post();
        $id = $params['id'];
        $cantidad = $params['cantidad'];
        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesequipos_accesorios',array('cantidad' => $cantidad ),array('id_accesoriod' => $id ));
    } 
    function obteneraccesorios(){
        $idequipo = $this->input->post('idequipo');
        $where = array('id' => $idequipo);
        $result=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos',$where);
        $equipo=0;
        foreach ($result->result() as $item) {
            $equipo=$item->idEquipo;
            $costo_toner_black=$item->costo_toner_black;
            $costo_toner_c=$item->costo_toner_c;
            $costo_toner_m=$item->costo_toner_m;
            $costo_toner_y=$item->costo_toner_y;
        }
        $optionaccesorio='<option value="0"></option>';
        $idcoti = $this->input->post('idcoti');  
        $accesorios=$this->Cotizaciones_model->getcotizacionaccesorios($idcoti,$equipo); 
        foreach ($accesorios->result() as $item2) {
            $optionaccesorio.='<option value="'.$item2->id.'">'.$item2->nombre.'</option>';
        }
        $optionconsumible='<option value="0"></option>';
        $resultc=$this->ModeloCatalogos->consumibleequipocotizacion($idcoti,$equipo);
        foreach ($resultc->result() as $item3) {
            $optionconsumible.='<option value="'.$item3->id.'">'.$item3->modelo.'</option>';
        }
        $options = array(
                    'accesorios' => $optionaccesorio,
                    'consumible' => $optionconsumible, 
                    'costo_toner_black' => $costo_toner_black,
                    'costo_toner_c' => $costo_toner_c,
                    'costo_toner_m' => $costo_toner_m,
                    'costo_toner_y' => $costo_toner_y
                    );
        echo json_encode($options);
    }
    /*
    function nuevaCotizacionpoliza(){
        $data = $this->input->post();
        $datosCotizacion = array('atencion' => $data['atencion'],
                                            'correo' => $data['correo'],
                                            'telefono' => $data['telefono'],
                                            'idCliente' => $data['idCliente'],
                                            'tipo' => 4,
                                            'rentaVentaPoliza' => 3,
                                            'estatus' => 1,
                                            'propuesta'=>$data['propuesta'],
                                            'id_personal'=>$this->idpersonal
                                            );

        $idCotizacion = $this->ModeloCatalogos->Insert('cotizaciones',$datosCotizacion);
        $arrayPolizas = $data['arrayPolizas'];
        unset($data['arrayPolizas']);
        $DATAc = json_decode($arrayPolizas);
        for ($i=0;$i<count($DATAc);$i++) {
            $datosPoliza = $this->Polizas_model->getPolizaConUnDetalle($DATAc[$i]->poliza,$DATAc[$i]->polizad);

            if ($DATAc[$i]->costo=='local') {
                        $precio_local=$datosPoliza[0]->precio_local;
                        $precio_semi=null;
                        $precio_foraneo=null;
                        $precio_especial=null;
            }elseif ($DATAc[$i]->costo=='foraneo') {
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_especial=null;
                        $precio_foraneo=$datosPoliza[0]->precio_foraneo;
            }elseif($DATAc[$i]->costo=='semi'){
                        $precio_local=null;
                        $precio_semi=$datosPoliza[0]->precio_semi;
                        $precio_foraneo=null;
                        $precio_especial=null;
            }elseif($DATAc[$i]->costo=='especial'){
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_foraneo=null;
                        $precio_especial=$datosPoliza[0]->precio_especial;
            }

            $detallesCotizacion = array('idPoliza' => $DATAc[$i]->poliza,
                                            'idEquipo' => $DATAc[$i]->equipo,
                                            'serie' => $DATAc[$i]->serie,
                                            'cantidad' => $DATAc[$i]->cantidad,
                                            'idCotizacion' => $idCotizacion,
                                            'nombre' => $datosPoliza[0]->nombre, 
                                            'cubre' => $datosPoliza[0]->cubre,
                                            'vigencia_meses' => $datosPoliza[0]->vigencia_meses,
                                            'vigencia_clicks' => $datosPoliza[0]->vigencia_clicks,
                                            'modelo' => $datosPoliza[0]->modelo,
                                            'precio_local' => $precio_local,
                                            'precio_semi' => $precio_semi,
                                            'precio_foraneo' => $precio_foraneo,
                                            'precio_especial' => $precio_especial
                                        );
            //$this->Cotizaciones_model->insertarDetallesCotizacionPoliza($detallesCotizacion);
            $this->ModeloCatalogos->Insert('cotizaciones_has_detallesPoliza',$detallesCotizacion);
            


        }
        echo $idCotizacion;
    }
    */
    function editarpoliza(){
        $idpoliza = $this->input->post('idpoliza');
        $polizacol = $this->input->post('polizacol');
        $pass = $this->input->post('pass');
        $valor = $this->input->post('valor');
        $idCliente = $this->input->post('idCliente');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion desde cotizacion con el usuario:'.$item->Usuario.' para la edicion de precios de poliza','nombretabla'=>'cotizaciones_has_detallesPoliza','idtable'=>$idpoliza,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
            }
        }
        if($permiso==0 and $idCliente==597){
            if($pass=='DHL2023' or $pass=='dhl2023'){
                $permiso=1;
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el con DHL2023 para la edicion de precios de poliza','nombretabla'=>'cotizaciones_has_detallesPoliza','idtable'=>$idpoliza,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }
        if ($permiso==1) {
            $data[$polizacol]=$valor;
            $where = array('id' => $idpoliza );
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesPoliza',$data,$where);
        }

        echo $permiso;
    }
    function autorizarCotizacionpoliza(){
        $data = $this->input->post();
        $idCotizacion=$data['cotizacion'];
        $polizas=$data['polizas'];
        $datosDeCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        //====================== actualiza la cotizacion ==============================
            $wherec= array('id'=>$idCotizacion);
            $datac['estatus']=2;
            //$datac['estatus']=1;
            $this->ModeloCatalogos->updateCatalogo('cotizaciones',$datac,$wherec);
        //=============================================================================
            // Se elimina el ID del registro de la cotización de los datos que usaremos
            unset($datosDeCotizacion[0]->id);
            // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
            unset($datosDeCotizacion[0]->estatus);
            // Se elimina el tipo de "rentaVentaPoliza"
            unset($datosDeCotizacion[0]->rentaVentaPoliza);
            unset($datosDeCotizacion[0]->propuesta);
            unset($datosDeCotizacion[0]->denegada);
            unset($datosDeCotizacion[0]->motivodenegada);
            unset($datosDeCotizacion[0]->reg);
            unset($datosDeCotizacion[0]->tipog);
            unset($datosDeCotizacion[0]->v_ser_tipo);
            unset($datosDeCotizacion[0]->v_ser_id);
            unset($datosDeCotizacion[0]->notifi_precio);
             // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
            $datosDeCotizacion[0]->estatus = 1;
            // Se indica solo como información, el id de la cotización que va con el proceso
            $datosDeCotizacion[0]->idCotizacion = $idCotizacion;
            $datosDeCotizacion[0]->id_personal = $this->idpersonal;
            $polizas = json_decode($polizas);
            //=================================================
                for ($l=0;$l<count($polizas);$l++) {
                    $idpoliza=$polizas[$l]->poliza;
                    $wherecdp=array('id'=>$idpoliza);
                    $detallepoliza=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesPoliza',$wherecdp);
                    foreach ($detallepoliza->result() as $itemp) {
                        if($itemp->viewcont_cant>1){
                            $datosDeCotizacion[0]->viewcont_cant=$itemp->viewcont_cant;
                            $datosDeCotizacion[0]->viewcont=$itemp->viewcont;
                        }
                    }
                }
            //=================================================
            $idVenta = $this->Polizas_model->insertaPolizaCreada($datosDeCotizacion[0]);
        //=============================================================================
            
        for ($i=0;$i<count($polizas);$i++) {
            $idpoliza=$polizas[$i]->poliza;
            $wherecdp=array('id'=>$idpoliza);
            $detallepoliza=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesPoliza',$wherecdp);
            foreach ($detallepoliza->result() as $itemp) {
                unset($itemp->id);
                unset($itemp->viewcont);
                unset($itemp->viewcont_cant);
                unset($itemp->notifi_precio);
                //unset($itemp->idCotizacion);
                $itemp->idPoliza = $idVenta;
                $itemp->cantidad = $polizas[$i]->cantidad;
                //$itemp->serie = $polizas[$i]->serie;
                $idp=$this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza',$itemp);

                $polizasseries = $polizas[$i]->serie;
                for ($s=0;$s<count($polizasseries);$s++) {
                    if (isset($polizasseries[$s]->series)) {
                        $serierow=$polizasseries[$s]->series;
                        $this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$idp,'serie'=>$serierow));
                        //log_message('error', 'seriesdatos----------'.$serierow);
                    }
                    
                }
            }
            


        }
        
        //echo $idVenta;
        $respuestaJSON = array("idVenta"=>$idVenta);
        echo json_encode($respuestaJSON);
        
    }
    function allconsumiblescotizacion(){
       $id = $this->input->post('id'); 
       $html = '';
       $datos = $this->Cotizaciones_model->getalldetallecontizaciones($id);
          $html.='<table id="tableconsumiblep">';
            $html.='<thead>';
              $html.='<tr>';
                $html.='<th>Seleccione</th>';
                $html.='<th>Equipo</th>';
                $html.='<th>Consumibles</th>';
                $html.='<th>Precios</th>';
                $html.='<th>Cantidad</th>';
                
              $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody>';
                foreach ($datos as $item) {
                    $html.='<tr>'; 
                    $html.='<td>
                                <input type="checkbox" name="nombre" class="nombre numchek" id="consumible_'.$item->id.'" value="'.$item->id.'">
                                <label for="consumible_'.$item->id.'"></label>
                            </td>';   
                       $html.='<td>'.$item->equipo.'</td>';
                       $html.='<td>'.$item->modelo.'</td>';
                       $html.='<td>'.$item->precioGeneral.'</td>';
                       $html.='<td>
                                    <input type="number" name="cantidadnew" class="form-control-bmz" id="cantidadnew" value="'.$item->piezas.'">
                       </td>';
                       
                    $html.='</tr>';    
                }
            $html.='</tbody>';
          $html.='</table>';
       echo $html;
    }
    function allrefaccionescotizacion(){
       $id = $this->input->post('id'); 
       $html = '';
       $datos = $this->Cotizaciones_model->getalldetallecontizacionesrefacciones($id);
       //$bodegas=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo' => 1));
       $bodegas=$this->ModeloCatalogos->view_session_bodegas();
          $html.='<table id="tablerefaccionp">';
            $html.='<thead>';
              $html.='<tr>';
                $html.='<th>Seleccione</th>';
                $html.='<th>Equipo</th>';
                $html.='<th>Refaccion</th>';
                $html.='<th>Precios</th>';
                $html.='<th>Cantidad</th>';
                $html.='<th>Bodega</th>';
                
              $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody>';
                foreach ($datos as $item) {
                    $html.='<tr>'; 
                    $html.='<td>
                                <input type="checkbox" name="nombre" class="nombre numchek" id="consumible_'.$item->id.'" value="'.$item->id.'">
                                <label for="consumible_'.$item->id.'"></label>
                            </td>';   
                       $html.='<td>'.$item->equipo.'</td>';
                       $html.='<td>'.$item->modelo.'</td>';
                       $html.='<td>'.$item->precioGeneral.'</td>';
                       $html.='<td>
                                    <input type="number" name="cantidadnew" class="form-control-bmz" id="cantidadnew" value="'.$item->piezas.'">
                                    <input type="hidden" name="serieselect" class="form-control-bmz" id="serieselect" value="'.$item->serieequipo.'">
                                </td>';
                        $html.='<td>
                                    <select id="serie_bodegarr" name="serie_bodegarr" class="browser-default chosen-select form-control-bmz" >';
                                    foreach ($bodegas as $itemb) { 
                                      $html.='<option value="'.$itemb->bodegaId.'">'.$itemb->bodega.'</option>';
                                    } 
                                    $html.='</select>  
                                </td>';
                       
                    $html.='</tr>';    
                }
            $html.='</tbody>';
          $html.='</table>';
       echo $html;
    }
    function nuevaCotizacionequipos(){
        $data = $this->input->post();
        $eleccionCotizar = $data['eleccionCotizar'];
        $tpeq=$data['tipoprecioequipo'];
        unset($data['tipoprecioequipo']);
        unset($data['eleccionCotizar']);
        if(isset($data['accesorios'])){
            $accesorios = $data['accesorios'];
            unset($data['accesorios']);
        }
        
        if(isset($data['consumibles'])){
            $consumibles = $data['consumibles'];
            unset($data['consumibles']);
        }
        //===============================================
        $tipoCotizacion = $data["tipo"];
                unset($data['tipo']);
                //$data['rentaVentaPoliza'] = $tipoCotizacion;
                // Saber si es cotización MANUAL o por FAMILIA
                $tipoCotizacionEquipos = $data["cotizar"];
                unset($data['cotizar']);
                // Agisnamos lo que se va a cotizar
                $data['tipo'] = $eleccionCotizar;
                // Se inserta el registro principal
                if ($tipoCotizacion==1) {
                    $data['tiporenta']=0;
                }
                if(isset($data['eg_compra_consumible'])){
                    $eg_compra_consumible=$data['eg_compra_consumible'];
                }else{
                    $eg_compra_consumible=0;
                }
                $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        'tipo' => $data['tipo'],
                                        'tiporenta' =>$data['tiporenta'],
                                        'eg_rm' =>$data['eg_rm'],
                                        'eg_rvm' =>$data['eg_rvm'],
                                        'eg_rpem' =>$data['eg_rpem'],
                                        'eg_rc' =>$data['eg_rc'],
                                        'eg_rvc' =>$data['eg_rvc'],
                                        'eg_rpec' =>$data['eg_rpec'],
                                        'nccc'=>$eg_compra_consumible,
                                        'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'propuesta'=>$data['propuesta'],
                                        'estatus' => 1);
                // Se inserta la cotización principal
                if($data['idcotizacion']==0){
                    $idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                }else{
                    $idCotizacion=$data['idcotizacion'];
                }
                // Si trae el tipo de cotización de equipos con valor 1, es MANUAL
                //if($tipoCotizacionEquipos==1){
                    $equiposIndividuales=$data["equiposIndividuales"];
                    $DATAeq = json_decode($equiposIndividuales);
                    //foreach ($arregloEquipos as $equipoInd) {
                    //log_message('error', 'numero de equipos: '.count($DATAeq));
                    for ($j=0;$j<count($DATAeq);$j++){
                        $equipoInd=$DATAeq[$j]->equipos;
                        $cantidadeq=$DATAeq[$j]->cantidad;

                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1){
                            if ($tpeq==1) {
                                $precio = $equipo->costo_venta;
                            }elseif ($tpeq==2) {
                                $precio = $equipo->costo_poliza;
                            }else{
                                $precio = $equipo->costo_renta;
                            }
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $DATAeq[$j]->costm+$DATAeq[$j]->costc;
                            $r_costo_m=$DATAeq[$j]->costm;
                            $r_volumen_m=$DATAeq[$j]->volm;
                            $r_costo_c=$DATAeq[$j]->costc;
                            $r_volumen_c=$DATAeq[$j]->volc;
                            if(isset($DATAeq[$j]->nccc)){
                                $nccc=$DATAeq[$j]->nccc;
                            }else{
                                $nccc=0;
                            }
                            
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        //========================================
                            //$costo_toner_black = $equipo->costo_toner_black;
                            //$costo_toner_cmy = $equipo->costo_toner_cmy;
                            //$rendimiento_toner_black = $equipo->rendimiento_toner_black;
                            //$rendimiento_toner_cmy = $equipo->rendimiento_toner_cmy;
                            $costo_unidad_imagen = 0;
                            $rendimiento_unidad_imagen = 0;
                            $costo_garantia_servicio = 0;
                            $rendimiento_garantia_servicio = 0;
                            $costo_pagina_monocromatica = 0;
                            $costo_pagina_color = 0;
                        //========================================
                            $DATAc = json_decode($consumibles);
                            $datainsert_cotiza_eq_consum=array();
                            for ($i=0;$i<count($DATAc);$i++) {
                                
                                if ($equipo->id==$DATAc[$i]->equiposelected) {
                                    $idconsumuble=$DATAc[$i]->consumibleselected;
                                    //log_message('error', 'idconsumible '.$idconsumuble);
                                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id,0);
                                    
                                    foreach ($resultadoaddcon->result() as $item2) {
                                        $tipo = $item2->tipo;
                                        $costo_toner = $item2->poliza;
                                        $rendimiento_toner = $item2->rendimiento_toner;
                                        
                                        if($tipo==5){
                                            if($item2->rendimiento_unidad_imagen>0){
                                                $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                            }
                                            
                                            $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                            $rendimiento_unidad_imagenv=$rendimiento_unidad_imagen;
                                            $costo_unidad_imagenv=$costo_unidad_imagen;
                                        }else{
                                            //$rendimiento_unidad_imagenv=0;
                                            if($item2->rendimiento_unidad_imagen>0){
                                               $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;// se agrego este campo a peticion de julio 
                                            }
                                            $rendimiento_unidad_imagenv=$rendimiento_unidad_imagen;// se modifico este campo a peticion de julio
                                            $costo_unidad_imagenv=0;
                                        }
                                        
                                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                        if($item2->costo_pagina_monocromatica>0){
                                            $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                            //log_message('error', 'costo_pagina_monocromatica: '.$item2->costo_pagina_monocromatica);
                                        }
                                        if ($item2->costo_pagina_color>0) {
                                            $costo_pagina_color = $item2->costo_pagina_color;
                                            //log_message('error', 'costo_pagina_color: '.$item2->costo_pagina_color);
                                        }
                                    }
                                    
                                    $datachfc["id_cotizacion"]=$idCotizacion;
                                        $datachfc["id_equipo"]=$equipoInd;
                                        $datachfc["cantidad"]=$cantidadeq;
                                        $datachfc["id_consumibles"]=$idconsumuble;
                                        $datachfc["costo_toner"]=$costo_toner;
                                        $datachfc["rendimiento_toner"]=$rendimiento_toner;
                                        $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagenv;
                                    $datainsert_cotiza_eq_consum[]= $datachfc; 
                                        //$this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_consumibles',$datachfc);
                                }
                            }
                            if (count($datainsert_cotiza_eq_consum)>0) {
                                $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesEquipos_consumibles',$datainsert_cotiza_eq_consum);
                            }
                            //log_message('error', 'costo_pagina_monocromatica________: '.$costo_pagina_monocromatica);
                            //log_message('error', 'costo_pagina_color________________: '.$costo_pagina_color);
                        //=====================================================
                            $DATAa = json_decode($accesorios);
                            $datainsert_cotiza_eq_accesorio=array();
                            for ($l=0;$l<count($DATAa);$l++) {
                                if ($equipo->id==$DATAa[$l]->equiposelected) {
                                    $dataas['id_cotizacion']=$idCotizacion;
                                    $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                    $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                    $dataas['cantidad']=$cantidadeq;
                                    $datainsert_cotiza_eq_accesorio[]=$dataas;
                                    //$this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_accesorios',$dataas);

                                }
                            }
                            if (count($datainsert_cotiza_eq_accesorio)>0) {
                                $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesEquipos_accesorios',$datainsert_cotiza_eq_accesorio);
                            }

                        //=====================================================
                        // Se arma el array para insertar los datos
                        $detallesCotizacion = array('idCotizacion' => $idCotizacion,
                                                    'idEquipo' => $equipo->id,
                                                    'cantidad'=>$cantidadeq,
                                                    'modelo' => $equipo->modelo,
                                                    'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                    'precio' => $precio,
                                                    'costo_unidad_imagen' => $costo_unidad_imagen,
                                                    'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                    'costo_garantia_servicio' => $costo_garantia_servicio,
                                                    'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                    'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                    'costo_pagina_color' => $costo_pagina_color,
                                                    'excedente' => $equipo->excedente,
                                                    'tipotoner' => $tipo,
                                                    'costo_toner' => $costo_toner,
                                                    'rendimiento_toner' => $rendimiento_toner,           
                                        );
                             if ($tipoCotizacion==2){
                                        $detallesCotizacion["r_costo_m"]=$r_costo_m;
                                        $detallesCotizacion["r_volumen_m"]=$r_volumen_m;
                                        $detallesCotizacion["r_costo_c"]=$r_costo_c;
                                        $detallesCotizacion["r_volumen_c"]=$r_volumen_c;
                                        $detallesCotizacion["nccc"]=$nccc;
                                    }
                        // Se insertan en la BD
                        $this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        
                    }
                //}
                /*
                $DATAa = json_decode($accesorios);
               // log_message('error', 'accesorios:acc'.$accesorios);
                //log_message('error', 'accesorios: data '.$DATAa);
                for ($i=0;$i<count($DATAa);$i++) {
                    
                    $dataas['id_cotizacion']=$idCotizacion;
                    $dataas['id_equipo']=$DATAa[$i]->equiposelected;
                    $dataas['id_accesorio']=$DATAa[$i]->accesorioselected;
                    $this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_accesorios',$dataas);
                    //log_message('error', 'Equipo: '.$equiposelected.' accesorio: '.$accesorioselected);
                }   
                */
                // Se regresa el ID de la cotización
                echo $idCotizacion;
                //==================================================================
    }
    // Consumibles carviar estatus y guardar los check
    function guardarconsumiblescheck(){
        $id = $this->input->post('id');
        $datos = $this->input->post('datos');
        $consumible = $this->input->post('consumible');
        $servicio = $this->input->post('servicio');
       
        
        // obtener los datos de la cotizacion 
        
        $cotizacion = $this->Cotizaciones_model->getDatosCotizacion($id); 
        // Se eliminaran el ID del registro de la cotización, estatus y rentaVentaPoliza 
        unset($cotizacion[0]->id);
        unset($cotizacion[0]->estatus);
        unset($cotizacion[0]->rentaVentaPoliza);
        unset($cotizacion[0]->propuesta);
        unset($cotizacion[0]->denegada);
        unset($cotizacion[0]->motivodenegada);
        unset($cotizacion[0]->tipog);
        $cotizacion[0]->estatus = 1;// El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
        $cotizacion[0]->id_personal = $this->idpersonal;
        $cotizacion[0]->idCotizacion = $id;
        $cotizacion[0]->servicio = $servicio;
        $cotizacion[0]->reg=$this->fechahoyc;
        $cotizacion[0]->fechaentrega = $this->input->post('fechae');
        if($servicio==1){
            $cotizacion[0]->servicio_poliza = $this->input->post('poliza');
            $cotizacion[0]->servicio_servicio = $this->input->post('servicio2');
            $cotizacion[0]->servicio_direccion = $this->input->post('direccion');
            $cotizacion[0]->servicio_tipo = $this->input->post('tipo');
            $cotizacion[0]->servicio_motivo = $this->input->post('motivo');
        }
        $idVenta = $this->Ventas_model->insertarVenta($cotizacion[0]);
        //obtener los datos de cotizacion del id he insertarlos en ventas utlizando el ejemplo comentado 
        $estatus = array('estatus' => 2);//Se actualiza la cotización al estatus de "Aprobada"
        $actualizado = $this->Cotizaciones_model->actualizaCotizacion($estatus,$id);
        $datos = json_decode($datos);
        foreach ($datos as $id_p) {
            $result = $this->Cotizaciones_model->getDatosCotizaciondetalles($id_p->id);
            unset($result[0]->id);
            unset($result[0]->idCotizacion);
            unset($result[0]->piezas);
            $result[0]->piezas=$id_p->cantidad;
            $result[0]->idVentas = $idVenta;
            // insertar los datos de contizaciondetalles y en id venta insertar el id de la tabla de ventas 
            $this->Cotizaciones_model->insertaventadetallesconsumibles($result[0]);
        }
         $DATAc = json_decode($consumible);
        for ($i=0;$i<count($DATAc);$i++) {
            // insertar 
            $datosConsumible = $this->Consumibles_model->getConsumibleConPrecios($DATAc[$i]->toner);
                    $total=$DATAc[$i]->tonerp*$DATAc[$i]->piezas;
                    $detallesCotizacion = array('idVentas' => $idVenta,
                                            'idEquipo' => $DATAc[$i]->equipo,
                                            'idConsumible' => $datosConsumible[0]->id,
                                            'piezas' => $DATAc[$i]->piezas, 
                                            'modelo' => $datosConsumible[0]->modelo,
                                            'rendimiento' =>'',
                                            'descripcion' => $datosConsumible[0]->observaciones,
                                            'precioGeneral' => $DATAc[$i]->tonerp,
                                            'totalGeneral' => $total
                                        );
                    $this->Cotizaciones_model->insertarVentasDetallesCotizacionConsumibles($detallesCotizacion);
        }
    }
    function guardarrefaccionescheck(){
        $id = $this->input->post('id');
        $datos = $this->input->post('datos');
        $consumible = $this->input->post('consumible');
        $servicio = $this->input->post('servicio');
       
        
        // obtener los datos de la cotizacion 
        
        $cotizacion = $this->Cotizaciones_model->getDatosCotizacion($id); 
        // Se eliminaran el ID del registro de la cotización, estatus y rentaVentaPoliza 
        unset($cotizacion[0]->id);
        unset($cotizacion[0]->estatus);
        unset($cotizacion[0]->rentaVentaPoliza);
        unset($cotizacion[0]->propuesta);
        unset($cotizacion[0]->denegada);
        unset($cotizacion[0]->motivodenegada);
        unset($cotizacion[0]->tipog);
        $cotizacion[0]->estatus = 1;// El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
        $cotizacion[0]->id_personal = $this->idpersonal;
        $cotizacion[0]->idCotizacion = $id;
        $cotizacion[0]->servicio = $servicio;
        $cotizacion[0]->reg = $this->fechahoyc;
        $cotizacion[0]->fechaentrega = $this->input->post('fechae');
        if($servicio==1){
            $cotizacion[0]->servicio_poliza = $this->input->post('poliza');
            $cotizacion[0]->servicio_servicio = $this->input->post('servicio2');
            $cotizacion[0]->servicio_direccion = $this->input->post('direccion');
            $cotizacion[0]->servicio_tipo = $this->input->post('tipo');
            $cotizacion[0]->servicio_motivo = $this->input->post('motivo');
        }
        $idVenta = $this->Ventas_model->insertarVenta($cotizacion[0]);
        //obtener los datos de cotizacion del id he insertarlos en ventas utlizando el ejemplo comentado 
        $estatus = array('estatus' => 2);//Se actualiza la cotización al estatus de "Aprobada"
        $actualizado = $this->Cotizaciones_model->actualizaCotizacion($estatus,$id);
        $datos = json_decode($datos);
        foreach ($datos as $id_p) {
            $result = $this->Cotizaciones_model->getDatosCotizaciondetallesrefacciones($id_p->id);
            unset($result[0]->id);
            unset($result[0]->idCotizacion);
            unset($result[0]->piezas);
            $result[0]->piezas=$id_p->cantidad;
            $result[0]->idVentas = $idVenta;
            $result[0]->serie_bodega = $id_p->bodega;
            $result[0]->serie = $id_p->serie;
            // insertar los datos de contizaciondetalles y en id venta insertar el id de la tabla de ventas 
            $this->Cotizaciones_model->insertaventadetallesrefacciones($result[0]);
        }
         $DATAc = json_decode($consumible);
        for ($i=0;$i<count($DATAc);$i++) {
            // insertar 
            $datosConsumible = $this->Consumibles_model->getrefaccionesConPrecios($DATAc[$i]->refaccion);
                    $total=$DATAc[$i]->refaccionp*$DATAc[$i]->piezas;
                    $detallesCotizacion = array('idVentas' => $idVenta,
                                            'idEquipo' => $DATAc[$i]->equipo,
                                            'serie' => $DATAc[$i]->serieequipo,
                                            'idRefaccion' => $datosConsumible[0]->id,
                                            'piezas' => $DATAc[$i]->piezas, 
                                            'modelo' => $datosConsumible[0]->nombre,
                                            'rendimiento' =>'',
                                            'descripcion' => $datosConsumible[0]->observaciones,
                                            'precioGeneral' => $DATAc[$i]->refaccionp,
                                            'totalGeneral' => $total,
                                            'serie_bodega' => $DATAc[$i]->bodega
                                        );
                    $this->Cotizaciones_model->insertaventadetallesrefacciones($detallesCotizacion);
        }
        echo $idVenta;
    }
    // funciona del modal de consumibles de lista
    function mostrarselectorner(){
        $id = $this->input->post('id');
        $detallesConsumibles = $this->Consumibles_model->getConsumiblesPorIdEquipo($id);
        $html = '';
        $html.= '<select id="toner" name="toner" class="browser-default chosen-select toner" onchange="selectprecio()">';
            $html.= '<option value="" disabled selected></option>';
        foreach($detallesConsumibles as $item)
        { 
            $html.='<option value="'.$item->id.'" >'.$item->modelo.'</option>';  
        }
        $html.='</select>';
        echo $html;
    }
    function mostrarselecrefaccionesr(){
        $params = $this->input->post();
        $id = $params['id'];
        if (isset($params['bodega'])) {
            $bodega=$params['bodega'];
        }else{
            $bodega=0;
        }
        $detallesConsumibles = $this->Consumibles_model->getrefaccionesPorIdEquipo($id,$bodega);
        $html = '';
        $html.= '<select id="srefacciones" name="srefacciones" class="browser-default chosen-select srefacciones" onchange="selectprecior($(this))">';
            $html.= '<option value="" disabled selected></option>';
        foreach($detallesConsumibles as $item){ 
            $stock = $item->stock;
            if($item->stock_resg>0){
                $stock_resg = $item->stock_resg;
            }else{
                $stock_resg=0;
            }
            $stock=$stock-$stock_resg;
            $html.='<option value="'.$item->id.'" data-stock="'.$stock.'" data-name="'.$item->nombre.'">'.$item->nombre.' ('.$stock.') </option>';  
        }
        $html.='</select>';
        echo $html;
    }
    // funciona del modal de consumibles de lista
    function mostrarselectprecios(){
        $id = $this->input->post('id');
        $detallesprecios = $this->ModeloCatalogos->db13_getselectwheren('consumibles_costos',array('consumibles_id' =>$id));
        $html = '';
        $html.= '<select id="tonerp" name="tonerp" class="browser-default chosen-select tonerp">';
            $html.= '<option value="" disabled selected></option>';
        foreach ($detallesprecios->result() as $item) {
            $html.= '<option value="'.$item->general.'">General('.$item->general.')</option>'; //general
            $html.= '<option value="'.$item->frecuente.'">Frecuente('.$item->frecuente.')</option>'; //frecuente
            $html.= '<option value="'.$item->especial.'">Especial('.$item->especial.')</option>'; //especial
            $html.= '<option value="'.$item->poliza.'">Poliza('.$item->poliza.')</option>'; //poliza
        }
        $html.= '</select>';
        echo $html;
    }  
    function mostrarselectpreciosr(){
        $id = $this->input->post('id');
        $detallesprecios = $this->ModeloCatalogos->db13_getselectwheren('refacciones_costos',array('refacciones_id' =>$id));
        $html = '';
        $html.= '<select id="refaccionesp" name="refaccionesp" class="browser-default form-control-bmz refaccionesp">';
            
        foreach ($detallesprecios->result() as $item) {
            $html.= '<option value="'.$item->general.'" class="gen">General('.$item->general.')</option>'; //general
            $html.= '<option value="'.$item->frecuente.'" class="fre">Frecuente('.$item->frecuente.')</option>'; //frecuente
            $html.= '<option value="'.$item->especial.'" class="esp">Especial('.$item->especial.')</option>'; //especial
            $html.= '<option value="'.$item->poliza.'" class="pol">Poliza('.$item->poliza.')</option>'; //poliza
        }
            $html.= '<option value="0" class="costocero">0</option>';
        $html.= '</select>';
        $data_select = array('select_option' => $html,'espe'=>$item->especial);
        echo json_encode($data_select);
    }   
    function editarprecioConsumicles(){
        $precio = $this->input->post('precio');
        $equipo = $this->input->post('equipo');
        $pass = $this->input->post('pass');
        $idCliente = $this->input->post('idCliente');
        $totalGeneral = $this->input->post('sumtotal');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion desde cotizacion con el usuario:'.$item->Usuario.' para la edicion de precios de consumibles','nombretabla'=>'cotizaciones_has_detallesConsumibles','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        if($permiso==0 and $idCliente==597){
            if($pass=='DHL2023' or $pass=='dhl2023'){
                $permiso=1;
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el con DHL2023 para la edicion de precios de consumibles','nombretabla'=>'cotizaciones_has_detallesConsumibles','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }
        if ($permiso==1) {
            $arrayData = array('precioGeneral' => $precio );
            $where = array('id' => $equipo );
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesConsumibles',$arrayData,$where);
        }
        echo $permiso;
    }
    function editarpiezasConsumicles(){
        $params = $this->input->post();
        $id = $params['id'];
        $cantidad = $params['cantidad'];
        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesConsumibles',array('piezas' => $cantidad ),array('id' => $id ));
    } 
    function editarpreciorefacciones(){
        $precio = $this->input->post('precio');
        $equipo = $this->input->post('equipo');
        $pass = $this->input->post('pass');
        $idCliente = $this->input->post('idCliente');
        $totalGeneral = $this->input->post('sumtotal');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion desde cotizacion con el usuario:'.$item->Usuario.' para la edicion de precios de refacciones','nombretabla'=>'cotizaciones_has_detallesRefacciones','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        if($permiso==0 and $idCliente==597){
            if($pass=='DHL2023' or $pass=='dhl2023'){
                $permiso=1;
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el con DHL2023 para la edicion de precios de refacciones','nombretabla'=>'cotizaciones_has_detallesRefacciones','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }
        if ($permiso==1) {
            $arrayData = array('precioGeneral' => $precio,'totalGeneral' => $totalGeneral );
            $where = array('id' => $equipo );
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesRefacciones',$arrayData,$where);
        }
        echo $permiso;
    } 
    function editarpiezasefacciones(){
        $params = $this->input->post();
        $id = $params['id'];
        $cantidad = $params['cantidad'];
        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesRefacciones',array('piezas' => $cantidad ),array('id' => $id ));
    } 
    function permisos(){
        $pass = $this->input->post('pass');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el usuario:'.$item->Usuario.'','nombretabla'=>'Permisos','idtable'=>0,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        echo $permiso;
    }
    function permisoscostocero(){
        $pass = $this->input->post('pass');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }

            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el usuario:'.$item->Usuario.' para habilitar costos ceros','nombretabla'=>'Permisos','idtable'=>0,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        echo $permiso;
    }
    function reformateofecha($fecha){
        $dia=date("d",strtotime(date($fecha))); 
        $mes = date("n",strtotime(date($fecha)));
        $yyyy = date("Y",strtotime(date($fecha)));
        if ($mes=='1') { $mesl ="Enero"; }
        if ($mes=='2') { $mesl ="Febrero"; }
        if ($mes=='3') { $mesl ="Marzo"; }
        if ($mes=='4') { $mesl ="Abril"; }
        if ($mes=='5') { $mesl ="Mayo"; }
        if ($mes=='6') { $mesl ="Junio"; }
        if ($mes=='7') { $mesl ="Julio"; }
        if ($mes=='8') { $mesl ="Agosto"; }
        if ($mes=='9') { $mesl ="Septiembre"; }
        if ($mes=='10') { $mesl ="Octubre"; }
        if ($mes=='11') { $mesl ="Noviembre"; }
        if ($mes=='12') { $mesl ="Diciembre"; }
        $fecha=$dia.' de '.$mesl.' de '.$yyyy;
        return $fecha; 
    }
    function cancelarcotizacion(){
        $id = $this->input->post('id');
        $motivo = $this->input->post('motivos');
        $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('denegada'=>1,'motivodenegada'=>$motivo),array('id'=>$id));
    }
    function editarcontacto(){
        $params = $this->input->post();
        $id=$params['id'];
        $atencion = $params['atencion'];
        $correo = $params['correo'];
        $telefono = $params['telefono'];
        $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('atencion'=>$atencion,'correo'=>$correo,'telefono'=>$telefono),array('id'=>$id));
    }
    function editejecutiva(){
        $params = $this->input->post();
        $idCotizacion=$params['idCotizacion'];
        $ejecutiva=$params['ejecutiva'];
        $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('id_personal'=>$ejecutiva),array('id'=>$idCotizacion));
    }
    function editarcondicionesg(){
        $params = $this->input->post();
        $idCotizacion = $params['idCotizacion'];
        unset($params['idCotizacion']);
        $this->ModeloCatalogos->updateCatalogo('cotizaciones',$params,array('id'=>$idCotizacion));
    }
    function editarcondicionesi(){
        $params = $this->input->post();
        $idCotizacion = $params['idCotizacion'];
        unset($params['idCotizacion']);
        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesequipos',$params,array('id'=>$idCotizacion));
    }
    function deleteequipov(){
        $params= $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->getdeletewheren('cotizaciones_has_detallesEquipos',array('id'=>$id));
    }
    function deleteconsumiblev(){
        $params= $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->getdeletewheren('cotizaciones_has_detallesConsumibles',array('id'=>$id));
    }
    function deleterefaccionv(){
        $params= $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->getdeletewheren('cotizaciones_has_detallesRefacciones',array('id'=>$id));
    }
    function pdf($idCotizacion){
        $datosCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $tipov = $datosCotizacion[0]->tipov;


        $data['idCotizacion']=$idCotizacion;
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($tipov);
        //=====================datos generales=========================
        
        $data['datosCotizacion'] = $datosCotizacion;
        $data['datosCliente'] = $this->Clientes_model->getClientePorId($data['datosCotizacion'][0]->idCliente);
        
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($tipov);
        $nombreUsuario = $this->ModeloPersonal->personalview($datosCotizacion[0]->id_personal);
        $nombreAux = $nombreUsuario->result();
        $data['nombreUsuario'] = $nombreAux[0]->nombre.' '.$nombreAux[0]->apellido_paterno.' '.$nombreAux[0]->apellido_materno;
        $data['emailUsuario'] = $nombreAux[0]->email;

        $data['fechareg']=$this->reformateofecha($datosCotizacion[0]->reg);
        $tipo=$datosCotizacion[0]->tipo;//1 - Equipos, 2 - Consumibles, 3 - Refacciones, 4 - Polizas
        $data['tipo']=$tipo;
        //=====================================================================
        $data['detallesCotizacione'] = $this->Cotizaciones_model->getDetallesCotizacionEquiposTabla($idCotizacion);

        
        
        $data['detallesCotizacioncc'] = $this->Cotizaciones_model->getDetallesCotizacionConsumiblesTabla($idCotizacion);
        
        $data['detallesCotizacionref'] = $this->Cotizaciones_model->getDetallesCotizacionRefaccionesTabla($idCotizacion);
        
        
        $data['datosCotizacionpoliza'] = $this->Cotizaciones_model->cotizacionequipospoliza2($idCotizacion);
        
        $data['datosCotizacionacc']=$this->Cotizaciones_model->getDetallesCotizacionEquiposaccTabla($idCotizacion);

        $tipov=$data['datosCotizacion'][0]->tipov;
        $confif=$this->ModeloCatalogos->db10_getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$tipov));
        $data['confif']=$confif->row();
        $data['configuracionCotizacion'] = $this->ConfiguracionDocumentos_Model->getConfiguracion($tipov);
        $data['tipov']=$tipov;
        $this->load->view('Reportes/cotizaciones',$data);
    }
    function editarcostotoner(){
        $params = $this->input->post();
        $id = $params['id'];
        unset($params['id']);
        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesequipos_consumibles',$params,array('id'=>$id));
        $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion de costo toner desde cotizacion','nombretabla'=>'cotizaciones_has_detallesequipos_consumibles','idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
    }
    /*
    function nuevaCotizacionaccesorios(){
        $data = $this->input->post();

        $arrayac = $data['arrayac'];
        unset($data['arrayac']);

        $datosCotizacion = array('atencion' => $data['atencion'],
                                    'correo' => $data['correo'],
                                    'telefono' => $data['telefono'],
                                    'idCliente' => $data['idCliente'],
                                    'tipo' => $data['tipo'],
                                    'estatus' => 1,
                                    'id_personal'=> $this->idpersonal,
                                    'rentaVentaPoliza'=>1
                                    );
                
        $idVenta = $this->ModeloGeneral->getinsert($datosCotizacion,'cotizaciones');
        $datareaccesorioarray=array();
        $DATAc = json_decode($arrayac);       
        for ($i=0;$i<count($DATAc);$i++) {
            
                
                
                    $detallesCotizacion = array(
                                            'id_cotizacion'=>$idVenta,
                                            'id_equipo'=>$DATAc[$i]->equipo,
                                            'id_accesorio'=>$DATAc[$i]->accesorio,
                                            'costo'=>$DATAc[$i]->costo,
                                            'cantidad'=>$DATAc[$i]->cantidad
                                        );
                    $datareaccesorioarray[] =$detallesCotizacion;
                    
            
        }
        if(count($datareaccesorioarray)>0){
            $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesEquipos_accesorios',$datareaccesorioarray);
        }
        echo $idVenta;
    }
    */
    function nuevaCotizaciongeneral(){
        $data = $this->input->post();
        $tipog=0;
        $arrayequipos=$data["arrayequipos"];
        $DATAeq = json_decode($arrayequipos);
        
        $idCotizacion = $data['idcotizacion'];
        unset($data['idcotizacion']);
        if(isset($data['eleccionCotizar'])){
            $eleccionCotizar = $data['eleccionCotizar'];
        }else{
            $eleccionCotizar = 0;
        }
        if(isset($data['tipocot'])){
            $tipocot=$data['tipocot'];
        }else{
            $tipocot=0;
        }
        
        $tpeq=$data['tipoprecioequipo'];
        unset($data['tipoprecioequipo']);
        unset($data['eleccionCotizar']);
        $arrayconsu=$data['arrayconsu'];
        unset($data['arrayconsu']);
        $arrayrefa=$data['arrayrefa'];
        unset($data['arrayrefa']);
        $arrayPolizas = $data['arrayPolizas'];
        unset($data['arrayPolizas']);
        $arrayac = $data['arrayac'];
        unset($data['arrayac']);
        if(isset($data['arrayequipoacce'])){
            $arrayequipoacce = $data['arrayequipoacce'];
            unset($data['arrayequipoacce']);
        }
        
        if(isset($data['arrayequipoconsu'])){
            $arrayequipoconsu = $data['arrayequipoconsu'];
            unset($data['arrayequipoconsu']);
        }
        $DATAacon = json_decode($arrayconsu);
        $DATAaref = json_decode($arrayrefa);
        $DATAp = json_decode($arrayPolizas);
        $DATAac = json_decode($arrayac); 
        //===============================================
                if(isset($data["tipo2"])){
                    $tipoCotizacion = $data["tipo2"];    
                }else{
                    $tipoCotizacion=0;
                }
                if(isset($data['tiporenta'])){
                    $tiporenta=$data['tiporenta'];
                }else{
                    $tiporenta=0;
                }
                
                unset($data['tipo']);
                //$data['rentaVentaPoliza'] = $tipoCotizacion;
                // Saber si es cotización MANUAL o por FAMILIA
                //$tipoCotizacionEquipos = $data["cotizar"];
                unset($data['cotizar']);
                // Agisnamos lo que se va a cotizar
                $data['tipo'] = $eleccionCotizar;
                // Se inserta el registro principal
                if ($tipoCotizacion==1) {
                    $data['tiporenta']=0;
                }
                if(isset($data['eg_compra_consumible'])){
                    $eg_compra_consumible=$data['eg_compra_consumible'];
                }else{
                    $eg_compra_consumible=0;
                }
                if($tiporenta>0){
                    $tipoCotizacion=2;
                }
                if($tipoCotizacion==2 and $tiporenta==0){
                    $tiporenta=2;
                }
                //======================================
                    if(count($DATAeq)>0){
                        $tipog.=',1';
                    }
                    if(count($DATAacon)>0){
                        $tipog.=',2';
                    }
                    if(count($DATAaref)>0){
                        $tipog.=',3';
                    }
                    if(count($DATAp)>0){
                        $tipog.=',4';
                    }
                    if(count($DATAac)>0){
                        $tipog.=',5';//accesorios
                    }
                //======================================
                $datosCotizacion = array('atencion' => $data['atencion'],
                                        'correo' => $data['correo'],
                                        'telefono' => $data['telefono'],
                                        'idCliente' => $data['idCliente'],
                                        'tipo' => $data['tipo'],
                                        'tiporenta' =>$tiporenta,
                                        'eg_rm' =>$data['eg_rm'],
                                        'eg_rvm' =>$data['eg_rvm'],
                                        'eg_rpem' =>$data['eg_rpem'],
                                        'eg_rc' =>$data['eg_rc'],
                                        'eg_rvc' =>$data['eg_rvc'],
                                        'eg_rpec' =>$data['eg_rpec'],
                                        'nccc'=>$eg_compra_consumible,
                                        'rentaVentaPoliza' => $tipoCotizacion,
                                        'id_personal'=>$this->idpersonal,
                                        'propuesta'=>$data['propuesta'],
                                        'tipov'=>$data['tipov'],
                                        'tipog'=>$tipog,
                                        'estatus' => 1,
                                        'v_ser_tipo'=>$data['v_ser_tipo'],
                                        'v_ser_id'=>$data['v_ser_id'],
                                        'prioridad'=>$data['prioridad'],
                                        'tipocot'=>$tipocot
                                    );
                // Se inserta la cotización principal
                if($idCotizacion>0){
                    if($data['v_ser_id']>0){
                        $datosCotizacionedit = array(
                                        'v_ser_tipo'=>$data['v_ser_tipo'],
                                        'v_ser_id'=>$data['v_ser_id'],
                                    );
                        $this->ModeloCatalogos->updateCatalogo('cotizaciones',$datosCotizacionedit,array('id'=>$idCotizacion));
                    }
                }else{
                    $idCotizacion = $this->Cotizaciones_model->insertarCotizacion($datosCotizacion);
                }
                //log_message('error','idCotizacion:'.$idCotizacion);
                if(isset($data['dirinfo'])){
                    if($data['dirinfo']!=''){
                        $this->ModeloCatalogos->Insert('cotizaciones_dir',array('idcotizacion'=>$idCotizacion,'dir'=>$data['dirinfo']));
                    }
                    unset($data['dirinfo']);
                }
                



                //======================================== Equipo  ================================
                    
                    //foreach ($arregloEquipos as $equipoInd) {
                    //log_message('error', 'numero de equipos: '.count($DATAeq));
                    for ($j=0;$j<count($DATAeq);$j++){
                        $equipoInd=$DATAeq[$j]->equipos;
                        $cantidadeq=$DATAeq[$j]->cantidad;
                        if(isset($DATAeq[$j]->gara)){
                            $gara=$DATAeq[$j]->gara;
                            //log_message('error','existe garantia');
                        }else{
                            $gara=0;
                            //log_message('error','no existe garantia');
                        }
                        $equipo = $this->Equipos_model->getEquipoPorId($equipoInd);
                        // Dependiendo si es VENTA, RENTA o PÓLIZA, es el precio que se va a tomar
                        if($tipoCotizacion==1){
                            if ($tpeq==1) {
                                $precio = $equipo->costo_venta;
                            }elseif ($tpeq==2) {
                                $precio = $equipo->costo_poliza;
                            }else{
                                $precio = $equipo->costo_renta;
                            }
                        }
                        else if ($tipoCotizacion==2){
                            $precio = $DATAeq[$j]->costm+$DATAeq[$j]->costc;
                            $r_costo_m=$DATAeq[$j]->costm;
                            $r_volumen_m=$DATAeq[$j]->volm;
                            $r_costo_c=$DATAeq[$j]->costc;
                            $r_volumen_c=$DATAeq[$j]->volc;
                            if(isset($DATAeq[$j]->nccc)){
                                $nccc=$DATAeq[$j]->nccc;
                            }else{
                                $nccc=0;
                            }
                            
                        }
                        else if ($tipoCotizacion==3){
                            $precio = $equipo->costo_poliza;
                        }
                        //========================================
                            $costo_unidad_imagen = 0;
                            $rendimiento_unidad_imagen = 0;
                            $costo_garantia_servicio = 0;
                            $rendimiento_garantia_servicio = 0;
                            $costo_pagina_monocromatica = 0;
                            $costo_pagina_color = 0;
                        //========================================
                            $DATAc = json_decode($arrayequipoconsu);
                            $datainsert_cotiza_eq_consum=array();
                            for ($i=0;$i<count($DATAc);$i++) {
                                
                                if ($equipo->id==$DATAc[$i]->equiposelected) {
                                    $idconsumuble=$DATAc[$i]->consumibleselected;
                                    //log_message('error', 'idconsumible '.$idconsumuble);
                                    $resultadoaddcon=$this->ModeloCatalogos->consumibleagregadoselected($idconsumuble,$equipo->id,0);
                                    
                                    foreach ($resultadoaddcon->result() as $item2) {
                                        $tipo = $item2->tipo;
                                        $costo_toner = $item2->poliza;
                                        $rendimiento_toner = $item2->rendimiento_toner;
                                        
                                        if($tipo==5){
                                            if($item2->rendimiento_unidad_imagen>0){
                                                $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;
                                            }
                                            
                                            $costo_unidad_imagen = $item2->costo_unidad_imagen;
                                            $rendimiento_unidad_imagenv=$rendimiento_unidad_imagen;
                                            $costo_unidad_imagenv=$costo_unidad_imagen;
                                        }else{
                                            //$rendimiento_unidad_imagenv=0;
                                            if($item2->rendimiento_unidad_imagen>0){
                                               $rendimiento_unidad_imagen = $item2->rendimiento_unidad_imagen;// se agrego este campo a peticion de julio 
                                            }
                                            $rendimiento_unidad_imagenv=$rendimiento_unidad_imagen;// se modifico este campo a peticion de julio
                                            $costo_unidad_imagenv=0;
                                        }
                                        
                                        $costo_garantia_servicio = $item2->costo_garantia_servicio;
                                        $rendimiento_garantia_servicio = $item2->rendimiento_garantia_servicio;
                                        if($item2->costo_pagina_monocromatica>0){
                                            $costo_pagina_monocromatica = $item2->costo_pagina_monocromatica;
                                            //log_message('error', 'costo_pagina_monocromatica: '.$item2->costo_pagina_monocromatica);
                                        }
                                        if ($item2->costo_pagina_color>0) {
                                            $costo_pagina_color = $item2->costo_pagina_color;
                                            //log_message('error', 'costo_pagina_color: '.$item2->costo_pagina_color);
                                        }
                                    }
                                    
                                    $datachfc["id_cotizacion"]=$idCotizacion;
                                        $datachfc["id_equipo"]=$equipoInd;
                                        $datachfc["cantidad"]=$cantidadeq;
                                        $datachfc["id_consumibles"]=$idconsumuble;
                                        $datachfc["costo_toner"]=$costo_toner;
                                        $datachfc["rendimiento_toner"]=$rendimiento_toner;
                                        $datachfc["rendimiento_unidad_imagen"]=$rendimiento_unidad_imagenv;
                                    $datainsert_cotiza_eq_consum[]= $datachfc; 
                                        //$this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_consumibles',$datachfc);
                                }
                            }
                            if (count($datainsert_cotiza_eq_consum)>0) {
                                $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesEquipos_consumibles',$datainsert_cotiza_eq_consum);
                            }
                            //log_message('error', 'costo_pagina_monocromatica________: '.$costo_pagina_monocromatica);
                            //log_message('error', 'costo_pagina_color________________: '.$costo_pagina_color);
                        //=====================================================
                            $DATAa = json_decode($arrayequipoacce);
                            $datainsert_cotiza_eq_accesorio=array();
                            for ($l=0;$l<count($DATAa);$l++) {
                                if ($equipo->id==$DATAa[$l]->equiposelected) {
                                    $dataas['id_cotizacion']=$idCotizacion;
                                    $dataas['id_equipo']=$DATAa[$l]->equiposelected;
                                    $dataas['id_accesorio']=$DATAa[$l]->accesorioselected;
                                    $dataas['cantidad']=$cantidadeq;
                                    $datainsert_cotiza_eq_accesorio[]=$dataas;
                                    //$this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_accesorios',$dataas);

                                }
                            }
                            if (count($datainsert_cotiza_eq_accesorio)>0) {
                                $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesEquipos_accesorios',$datainsert_cotiza_eq_accesorio);
                            }

                        //=====================================================
                        // Se arma el array para insertar los datos
                        $detallesCotizacion = array('idCotizacion' => $idCotizacion,
                                                    'idEquipo' => $equipo->id,
                                                    'cantidad'=>$cantidadeq,
                                                    'modelo' => $equipo->modelo,
                                                    'especificaciones_tecnicas' => $equipo->especificaciones_tecnicas,
                                                    'precio' => $precio,
                                                    'costo_unidad_imagen' => $costo_unidad_imagen,
                                                    'rendimiento_unidad_imagen' => $rendimiento_unidad_imagen,
                                                    'costo_garantia_servicio' => $costo_garantia_servicio,
                                                    'rendimiento_garantia_servicio' => $rendimiento_garantia_servicio,
                                                    'costo_pagina_monocromatica' => $costo_pagina_monocromatica,
                                                    'costo_pagina_color' => $costo_pagina_color,
                                                    'excedente' => $equipo->excedente,
                                                    'tipotoner' => $tipo,
                                                    'costo_toner' => $costo_toner,
                                                    'rendimiento_toner' => $rendimiento_toner, 
                                                    'garantia'=>$gara          
                                        );
                             if ($tipoCotizacion==2){
                                        $detallesCotizacion["r_costo_m"]=$r_costo_m;
                                        $detallesCotizacion["r_volumen_m"]=$r_volumen_m;
                                        $detallesCotizacion["r_costo_c"]=$r_costo_c;
                                        $detallesCotizacion["r_volumen_c"]=$r_volumen_c;
                                        $detallesCotizacion["nccc"]=$nccc;
                                    }
                        // Se insertan en la BD
                        $this->Cotizaciones_model->insertarDetallesCotizacionEquipos($detallesCotizacion);
                        
                    }
                //=================================================================================
                //======================================== Consumible =============================
                    
                    $datarray_insertconsumibles=array();
                    for ($l=0;$l<count($DATAacon);$l++){
                        $datosConsumible = $this->Consumibles_model->getConsumibleConPrecios($DATAacon[$l]->toners);

                        $total=$DATAacon[$l]->precio*$DATAacon[$l]->cantidad;
                        $detallesCotizacion = array('idCotizacion' => $idCotizacion,
                                                'idEquipo' => $DATAacon[$l]->equipos,
                                                'idConsumible' => $DATAacon[$l]->toners,
                                                'piezas' => $DATAacon[$l]->cantidad, 
                                                'modelo' => $datosConsumible[0]->modelo,
                                                'rendimiento' =>'',
                                                'descripcion' => $datosConsumible[0]->observaciones,
                                                'precioGeneral' =>$DATAacon[$l]->precio,
                                                'totalGeneral' => $total
                                            );
                        $datarray_insertconsumibles[]=$detallesCotizacion;
                    }
                    if(count($datarray_insertconsumibles)>0){
                        $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesConsumibles',$datarray_insertconsumibles);
                    }
                //======================================== Refacciones  ===========================
                    
                    $datarray_insertrefacciones=array();
                    for ($i=0; $i < count($DATAaref); $i++) {
                        if(isset($DATAaref[$i]->gara)){
                            $gara=$DATAaref[$i]->gara;
                        }else{
                            $gara=0;
                        }
                        $datosRefaccion = $this->Refacciones_model->getRefaccionConPrecios($DATAaref[$i]->refa);
                        
                        $detallesCotizacion = array(
                                                'idCotizacion' => $idCotizacion,
                                                'idEquipo' => $DATAaref[$i]->equipos,
                                                'serieequipo' => $DATAaref[$i]->serie,
                                                'piezas' => $DATAaref[$i]->cantidad, 
                                                'modelo' => $datosRefaccion[0]->nombre,
                                                'descripcion' => $datosRefaccion[0]->observaciones,
                                                'precioGeneral' => $DATAaref[$i]->precio,
                                                'totalGeneral' => ($DATAaref[$i]->cantidad*$DATAaref[$i]->precio),
                                                'idRefaccion' => $DATAaref[$i]->refa,
                                                'garantia'=>$gara
                                            );
                        $datarray_insertrefacciones[]=$detallesCotizacion;
                        
                        //$this->Cotizaciones_model->insertarDetallesCotizacionRefacciones($detallesCotizacion);
                        
                    }
                    if(count($datarray_insertrefacciones)>0){
                        $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesRefacciones',$datarray_insertrefacciones);
                    }
                //=================================================================================
                //======================================== Servicios  =============================

                    $viewcont=0;
                    $viewcont_cant=0;
                    for ($i=0;$i<count($DATAp);$i++) {
                        $datosPoliza = $this->Polizas_model->getPolizaConUnDetalle($DATAp[$i]->poliza,$DATAp[$i]->polizad);
                        $precio_local=null;
                        $precio_semi=null;
                        $precio_foraneo=null;
                        $precio_especial=0;
                        if ($DATAp[$i]->costo=='local') {
                                    $precio_local=$datosPoliza[0]->precio_local;
                                    $precio_semi=null;
                                    $precio_foraneo=null;
                                    $precio_especial=null;
                        }elseif ($DATAp[$i]->costo=='foraneo') {
                                    $precio_local=null;
                                    $precio_semi=null;
                                    $precio_especial=null;
                                    $precio_foraneo=$datosPoliza[0]->precio_foraneo;
                        }elseif($DATAp[$i]->costo=='semi'){
                                    $precio_local=null;
                                    $precio_semi=$datosPoliza[0]->precio_semi;
                                    $precio_foraneo=null;
                                    $precio_especial=null;
                        }elseif($DATAp[$i]->costo=='especial'){
                                    $precio_local=null;
                                    $precio_semi=null;
                                    $precio_foraneo=null;
                                    $precio_especial=$datosPoliza[0]->precio_especial;
                        }
                        if($datosPoliza[0]->vigencia_clicks=='Ilimitadas'){
                            $vigencia=100;

                            $viewcont=1;
                            $viewcont_cant=$vigencia;
                        }else{
                            if(intval($datosPoliza[0]->vigencia_clicks)>1){
                                $vigencia=intval($datosPoliza[0]->vigencia_clicks); 
                                
                                $viewcont=1;
                                $viewcont_cant=$vigencia;
                            }     
                        }

                        $detallesCotizacion = array('idPoliza' => $DATAp[$i]->poliza,
                                                        'idEquipo' => $DATAp[$i]->equipo,
                                                        'serie' => $DATAp[$i]->serie,
                                                        'cantidad' => $DATAp[$i]->cantidad,
                                                        'idCotizacion' => $idCotizacion,
                                                        'nombre' => $datosPoliza[0]->nombre, 
                                                        'cubre' => $datosPoliza[0]->cubre,
                                                        'vigencia_meses' => $datosPoliza[0]->vigencia_meses,
                                                        'vigencia_clicks' => $datosPoliza[0]->vigencia_clicks,
                                                        'modelo' => $datosPoliza[0]->modelo,
                                                        'precio_local' => $precio_local,
                                                        'precio_semi' => $precio_semi,
                                                        'precio_foraneo' => $precio_foraneo,
                                                        'precio_especial' => $precio_especial,
                                                        'viewcont'=>$viewcont,
                                                        'viewcont_cant'=>$viewcont_cant
                                                    );
                        //$this->Cotizaciones_model->insertarDetallesCotizacionPoliza($detallesCotizacion);
                        $this->ModeloCatalogos->Insert('cotizaciones_has_detallesPoliza',$detallesCotizacion);
                        


                    }
                //=================================================================================
                //======================================== Accesorio  =============================

                    $datareaccesorioarray=array();
                          
                    for ($i=0;$i<count($DATAac);$i++) {
                        if(isset($DATAac[$i]->gara)){
                            $gara=$DATAac[$i]->gara;
                        }else{
                            $gara=0;
                        }
                                $detallesCotizacion = array(
                                                        'id_cotizacion'=>$idCotizacion,
                                                        'id_equipo'=>$DATAac[$i]->equipo,
                                                        'id_accesorio'=>$DATAac[$i]->accesorio,
                                                        'costo'=>$DATAac[$i]->costo,
                                                        'cantidad'=>$DATAac[$i]->cantidad,
                                                        'garantia'=>$gara
                                                    );
                                $datareaccesorioarray[] =$detallesCotizacion;
                    }
                    if(count($datareaccesorioarray)>0){
                        $this->ModeloCatalogos->insert_batch('cotizaciones_has_detallesEquipos_accesorios',$datareaccesorioarray);
                    }
                //=================================================================================
                

                echo $idCotizacion;
    }
    function allcotizaciong(){
        $params = $this->input->post();
        $idCotizacion = $params['id'];
            $result_cot=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('id'=>$idCotizacion));
            $reg_cot=$this->fechahoyc;
            foreach ($result_cot->result() as $item) {
                $reg_cot=$item->reg;
                $id_cot_prev =$item->id_cot_prev;
            }
            if($id_cot_prev>0){
                $result_cot=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('id'=>$id_cot_prev));
                foreach ($result_cot->result() as $item) {
                    $reg_cot=$item->reg;
                }
            }
            $reg_cot_masdias = strtotime ( '+3 day', strtotime ( $reg_cot ) ) ;
            $reg_cot_masdias = date ( 'Y-m-d' , $reg_cot_masdias );
        //================================================================================
            //$resultbodegas=$this->ModeloCatalogos->getselectwheren('bodegas',array('activo'=>1));
            $resultbodegas=$this->ModeloCatalogos->view_session_bodegas();
            $bodegas='<select id="serie_bodegae" name="serie_bodegae" class="browser-default form-control-bmz selected_bode_op" >';
            foreach ($resultbodegas as $itemb) {
                $bodegas.='<option value="'.$itemb->bodegaId.'" class="tipo_v_selected_'.$itemb->tipov.'" >'.$itemb->bodega.'</option>';
            }
            $bodegas.='</select>';
        //================================================================================
            $equiposdata='<ul class="collapsible collapsible-accordion" data-collapsible="accordion">';
            $resultequipos=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_detallesEquipos',array('idCotizacion'=>$idCotizacion));
            if($resultequipos->num_rows()>0){
                $equiposdata.='<li>
                                <div class="collapsible-header">
                                    <i class="material-icons">print</i> Equipos</div>
                                <div class="collapsible-body " >
                                <table class="table table-striped bordered" id="tableg_equipos"><tbody>';
                foreach ($resultequipos->result() as $iteme) {
                    $stockequipo=$this->ModeloGeneral->stockequipo($iteme->idEquipo);
                    $equiposdata.='<tr>
                                        <td>
                                            <input type="checkbox" 
                                                    class="equipo_check_coti equipo_check_'.$iteme->idEquipo.'" 
                                                    id="equipo_check_coti_'.$iteme->id.'" 
                                                    value="'.$iteme->id.'" 
                                                    onchange="alertastockequipo('.$iteme->idEquipo.','.$stockequipo.');"
                                            />
                                            <label for="equipo_check_coti_'.$iteme->id.'"></label>
                                        </td>
                                        <td style="width: 140px;">
                                            <input type="text" 
                                                class="equipo_check_coti_cantidad input_cantidad_touspin equ_cant_'.$iteme->idEquipo.' form-control-bmz" 
                                                id="cantidad_equipo" value="'.$iteme->cantidad.'"
                                                oninput="alertastockequipo('.$iteme->idEquipo.','.$stockequipo.');"
                                            />
                                        </td>
                                        <td>'.$iteme->modelo.' ('.$stockequipo.')</td>
                                        <td>
                                            <select id="serie_bodegae" name="serie_bodegae" class="browser-default form-control-bmz selected_bode_op" >
                                                <option value="0">Selecione</option>
                                                ';
                                                foreach ($resultbodegas as $itemb) {
                                                    $stockequipo=$this->ModeloGeneral->stockequipobodega($iteme->idEquipo,$itemb->bodegaId);
                                                    $equiposdata.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stockequipo.'" class="tipo_v_selected_'.$itemb->tipov.'">'.$itemb->bodega.' ('.$stockequipo.')</option>';
                                                }
                                $equiposdata.='</select>
                                        </td>
                                    </tr>';
                }
                $equiposdata.='</tbody></table>
                                </div>
                            </li>';
            }
            $resultconsumibles=$this->ModeloCatalogos->detalle_cotizacion_consumible_equipos($idCotizacion);
            $resultconsumibles2 = $this->Cotizaciones_model->getalldetallecontizaciones($idCotizacion);
            if($resultconsumibles->num_rows()>0 or $resultconsumibles2->num_rows()>0){
                $equiposdata.='<li>
                                <div class="collapsible-header">
                                    <i class="material-icons">toc</i> Consumibles</div>
                                <div class="collapsible-body " >';
                if($resultconsumibles->num_rows()>0){
                    $equiposdata.='<table class="table table-striped bordered" id="tableg_consumiblese"><tbody>';
                    foreach ($resultconsumibles->result() as $itemc) {
                        $equiposdata.='<tr>
                                            <td>
                                                <input type="checkbox" 
                                                class="consumibles_check_coti consum_check_'.$itemc->id_consumibles.'" 
                                                id="consumibles_check_coti_'.$itemc->id.'" 
                                                value="'.$itemc->id.'"
                                                onchange="alertastocktoner('.$itemc->id_consumibles.');"
                                                "/>
                                                <label for="consumibles_check_coti_'.$itemc->id.'"></label>
                                            </td>
                                            <td style="width: 140px;">
                                                <input type="text" 
                                                    class="input_cantidad_touspin consu_cant_'.$itemc->id_consumibles.' form-control-bmz" 
                                                    id="cantidad_consumibles" 
                                                    value="'.$itemc->cantidad.'"
                                                    oninput="alertastocktoner('.$itemc->id_consumibles.');"
                                                    />
                                                <input type="hidden" id="id_consumibles" value="'.$itemc->id_consumibles.'" />
                                            </td>
                                            <td>'.$itemc->modelo.' <span class="consu_stock_'.$itemc->id_consumibles.'"></span></td>
                                            <td><b>'.$itemc->modeloc.'</b></td>
                                            <td><select id="serie_bodegae" name="serie_bodegae" class="browser-default form-control-bmz selected_bode_op" ><option value="0">Selecione</option>';
                                                foreach ($resultbodegas as $itemb) {
                                                    $stock=$this->ModeloGeneral->stockconsumiblesbodega($itemc->id_consumibles,$itemb->bodegaId);
                                                    $equiposdata.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stock.'" class="tipo_v_selected_'.$itemb->tipov.'">'.$itemb->bodega.' ('.$stock.')</option>';
                                                }
                                $equiposdata.='</select></td>
                                        </tr>';

                    }
                    $equiposdata.='</tbody></table>';
                }  
                if($resultconsumibles2->num_rows()>0){
                    $equiposdata.='<table class="table table-striped bordered" id="tableg_consumibles"><tbody>';
                        foreach ($resultconsumibles2->result() as $item) {
                            $equiposdata.='<tr>
                                    <td>
                                        <input type="checkbox" name="nombre" class="nombre numchek" id="consumible_'.$item->id.'" value="'.$item->id.'">
                                        <label for="consumible_'.$item->id.'"></label>
                                    </td>   
                                    <td>'.$item->equipo.'</td>
                                    <td>'.$item->modelo.'</td>
                                    <td>'.$item->precioGeneral.'</td>
                                    <td><input type="number" name="cantidadnew" class="form-control-bmz" id="cantidadnew" value="'.$item->piezas.'"></td>
                                    <td><select id="serie_bodegae" name="serie_bodegae" class="browser-default form-control-bmz selected_bode_op" ><option value="0">Selecione</option>';
                                                foreach ($resultbodegas as $itemb) {
                                                    $stock=$this->ModeloGeneral->stockconsumiblesbodega($item->idConsumible,$itemb->bodegaId);
                                                    $equiposdata.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stock.'" class="tipo_v_selected_'.$itemb->tipov.'">'.$itemb->bodega.' ('.$stock.')</option>';
                                                }
                                $equiposdata.='</select></td>
                                    </tr>';    
                        }
                    $equiposdata.='</tbody></table>';
                } 
                              $equiposdata.='  </div>
                            </li>';
            }
            $resultaccesorios=$this->ModeloCatalogos->detalle_cotizacion_accesorios_equipos($idCotizacion);

            if($resultaccesorios->num_rows()>0){
                $equiposdata.='<li>
                                <div class="collapsible-header">
                                    <i class="material-icons">toc</i> Accesorios</div>
                                <div class="collapsible-body " >
                                <table class="table table-striped bordered" id="tableg_accesorios"><tbody>';
                foreach ($resultaccesorios->result() as $itema) {
                    $equiposdata.='<tr>
                                        <td>
                                            <input type="checkbox" 
                                                class="accesorio_check_coti acce_check_'.$itema->id.' " 
                                                id="accesorio_check_coti_'.$itema->id_accesoriod.'" 
                                                value="'.$itema->id_accesoriod.'"
                                                onchange="alertastockacces('.$itema->id.');"
                                            />
                                            <label for="accesorio_check_coti_'.$itema->id_accesoriod.'"></label>
                                        </td>
                                        <td style="width: 140px;">
                                            <input type="text" 
                                                class="input_cantidad_touspin access_cant_'.$itema->id.' form-control-bmz" 
                                                id="cantidad_accesorio" 
                                                oninput="alertastockacces('.$itema->id.');"
                                                value="'.$itema->cantidad.'"/>
                                        </td>
                                        <td>'.$itema->modelo.' </td>
                                        <td><b>'.$itema->nombre.'</b> <span class="access_stock_'.$itema->id.'"></span></td>
                                        <td><select id="serie_bodegae" name="serie_bodegae" class="browser-default form-control-bmz selected_bode_op" ><option value="0">Selecione</option>';
                                                foreach ($resultbodegas as $itemb) {
                                                    $stock=$this->ModeloGeneral->stockaccesoriobodega($itema->id,$itemb->bodegaId);
                                                    $equiposdata.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stock.'" class="tipo_v_selected_'.$itemb->tipov.'">'.$itemb->bodega.' ('.$stock.')</option>';
                                                }
                                $equiposdata.='</select></td>
                                    </tr>';
                }
                $equiposdata.='</tbody></table>
                                </div>
                            </li>';
            }
            $resultrefacciones = $this->Cotizaciones_model->getalldetallecontizacionesrefacciones($idCotizacion);
            if($resultrefacciones->num_rows()>0){
                $equiposdata.='<li>
                                <div class="collapsible-header">
                                    <i class="material-icons">toc</i> Refacciones</div>
                                <div class="collapsible-body " >
                                <table class="table table-striped bordered" id="tableg_refacciones"><tbody>';
                                foreach ($resultrefacciones->result() as $item) {
                                    $equiposdata.='<tr><td>
                                            <input type="checkbox" name="nombre" class="nombre numchek" id="consumible_'.$item->id.'" value="'.$item->id.'">
                                            <label for="consumible_'.$item->id.'"></label>
                                        </td>  
                                       <td>'.$item->equipo.'</td><td>'.$item->modelo.'</td><td>'.$item->precioGeneral.'</td>
                                       <td>
                                            <input type="number" name="cantidadnew" class="form-control-bmz" id="cantidadnew" value="'.$item->piezas.'">
                                            <input type="hidden" name="serieselect" class="form-control-bmz" id="serieselect" value="'.$item->serieequipo.'">
                                                </td>
                                        <td><select id="serie_bodegae" name="serie_bodegae" class="browser-default form-control-bmz selected_bode_op" ><option value="0">Selecione</option>';
                                                foreach ($resultbodegas as $itemb) {
                                                    $stock=$this->ModeloGeneral->stockrefaccionesbodega($item->idRefaccion,$itemb->bodegaId);
                                                    $equiposdata.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stock.'" class="tipo_v_selected_'.$itemb->tipov.'">'.$itemb->bodega.' ('.$stock.')</option>';
                                                }
                                $equiposdata.='</select></td></tr>';
                                }

                $equiposdata.='</tbody></table>
                                </div>
                            </li>';
            }
            $equipos=$this->Cotizaciones_model->cotizacionequipospoliza($idCotizacion);
            if($equipos->num_rows()){
                $equiposdata.='<li>
                                <div class="collapsible-header">
                                    <i class="material-icons">toc</i> Servicios</div>
                                <div class="collapsible-body " >
                                <table class="table table-striped bordered" id="tableg_servicio"><tbody>';

                                   foreach ($equipos->result() as $iteme) { 
                                        $serie=$iteme->serie;
                                        
                                        $update_monto=0;

                                        $res_ser_ren = $this->ModeloGeneral->get_verificarseriederentas($serie);
                                        if($res_ser_ren->num_rows()==0){
                                            //log_message('error','Entra registro '.$iteme->id);
                                        
                                            if($iteme->precio_local>0 || $iteme->precio_semi>0 || $iteme->precio_foraneo>0 || $iteme->precio_especial>0){
                                                //log_message('error','Entro indica precio');
                                            }else{
                                                //log_message('error',$reg_cot_masdias.'<'.$this->fechahoy);
                                                if($reg_cot_masdias<$this->fechahoy){
                                                    $update_monto=1;
                                                    //log_message('error','update_monto: '.$update_monto);
                                                }
                                            }
                                            if($update_monto==1){
                                                $result_pol=$this->ModeloCatalogos->getselectwheren('polizas_detalles',array('idPoliza'=>$iteme->idPoliza,'modelo'=>$iteme->idEquipo));
                                                $monto_update=0;
                                                foreach ($result_pol->result() as $item_pd) {
                                                    if($item_pd->precio_local>0){
                                                        $monto_update=$item_pd->precio_local;
                                                    }
                                                }
                                                //log_message('error','monto_update: '.$monto_update);
                                                if($monto_update>0){
                                                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesPoliza',array('precio_local'=>$monto_update,'notifi_precio'=>1),array('id'=>$iteme->id));
                                                }else{
                                                    $update_monto=0;
                                                }
                                                //log_message('error','update_monto: '.$update_monto);
                                            }    
                                        }

                                        



                                        $detallesCotizacion = $this->Cotizaciones_model->getDetallesCotizacionPolizaTablae($iteme->idCotizacion,$iteme->idEquipo);
                                        
                                        foreach ($detallesCotizacion->result() as $itemed) {
                                            //=====================================================================
                                            if($itemed->vigencia_clicks>1){
                                                $disabled=' disabled ';
                                            }else{
                                                $disabled='';
                                            }
                                            $equiposdata .='<tr>
                                                       <td>
                                                          <input type="checkbox" class="polizaselected" id="polizaselected_'.$itemed->id.'" name="polizaselected" value="'.$itemed->id.'"/>
                                                          <label for="polizaselected_'.$itemed->id.'"></label>
                                                       </td> 
                                                       <td>
                                                         <input type="number" id="cantidad" placeholder="cantidad" value="'.$itemed->cantidad.'" min="1" class="detallespoliza_e_'.$itemed->id.' detallespoliza_e form-control-bmz" onchange="appendseriespolizas('.$itemed->id.')" '.$disabled.'>
                                                       </td>';
                                                       $equiposdata .='<td>';
                                                       $equiposdata .=$iteme->modelo.' '.$itemed->nombre.' ('.$itemed->modelo.')';
                                                       if($update_monto==1){
                                                        $equiposdata .='<br><p style="color:red">El costo del servicio se a actualizado</p>';
                                                       }
                                                       $equiposdata .='</td>';
                                        $equiposdata .='<td>
                                                         <table class="table_series_e_'.$itemed->id.'">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding-top: 2px;padding-bottom: 2px;">
                                                                        <input type="text" id="polizaserie" class="form-control-b v" value="'.$itemed->serie.'">
                                                                    <td>
                                                                </tr>
                                                            </tbody>
                                                         </table>
                                                       </td> 
                                                      </tr>';
                                        }
                                        
                            } 
                $equiposdata.='</tbody></table>
                                </div>
                            </li>';
            }
        $html = '';
        

            $equiposdata.='</ul>';

            echo $equiposdata;

    }
    function autorizarcotizaciongeneral(){
        $data = $this->input->post();
        if(isset($data['ordenc'])){
            $ordenc = $data['ordenc'];
        }else{
            $ordenc = '';
        }
        
        $idVenta=0;
        
        $idCotizacion=$data['idCotizacion'];
        $datosDeCotizacion = $this->Cotizaciones_model->getDatosCotizacion($idCotizacion);
        $rentaVentaPoliza = $datosDeCotizacion[0]->rentaVentaPoliza;
        $clienteId = $datosDeCotizacion[0]->idCliente;
        unset($datosDeCotizacion[0]->priorizar_cot);
        unset($datosDeCotizacion[0]->tipog);
        unset($datosDeCotizacion[0]->prioridad);
        unset($datosDeCotizacion[0]->tipocot);
        unset($datosDeCotizacion[0]->pre_envio);
        unset($datosDeCotizacion[0]->id_cot_prev);
        //=======================================
            $res_dir=$this->ModeloCatalogos->getselectwheren('cotizaciones_dir',array('idcotizacion'=>$idCotizacion));
            $dirinfo='';
            foreach ($res_dir->result() as $item_dir) {
                $dirinfo=$item_dir->dir;
            }

        //=======================================

        $v_ser_tipo = $datosDeCotizacion[0]->v_ser_tipo;
        $v_ser_id = $datosDeCotizacion[0]->v_ser_id;
        if($data['v_ser_id']>0){
            $v_ser_tipo=$data['v_ser_tipo'];
            $v_ser_id= $data['v_ser_id'];
            $datosDeCotizacion[0]->v_ser_tipo = $v_ser_tipo;
            $datosDeCotizacion[0]->v_ser_id = $v_ser_id;
        }
        //log_message('error','rentaVentaPoliza:'.$rentaVentaPoliza);
        if ($rentaVentaPoliza==0) {
            $rentaVentaPoliza=1;
        }
        $tipoDeCotizacion = $datosDeCotizacion[0]->tipo;
        $datosDeCotizacion[0]->servicio = $data['servicio'];
        $datosDeCotizacion[0]->fechaentrega = $data['fechae'];

        
        
        //$datosDeCotizacion[0]->tipov = $data['tipov'];
        if($data['servicio']==1){
            $datosDeCotizacion[0]->servicio_poliza = $data['poliza'];
            $datosDeCotizacion[0]->servicio_servicio = $data['servicio2'];
            $datosDeCotizacion[0]->servicio_direccion = $data['direccion'];
            $datosDeCotizacion[0]->servicio_tipo = $data['tipo'];
            $datosDeCotizacion[0]->servicio_motivo = $data['motivo'];

        }
        // Se actualiza la cotización al estatus de "Aprobada"
            //$datosActualizadosCotizacion = array('estatus' => 2);
            //$actualizado = $this->Cotizaciones_model->actualizaCotizacion($datosActualizadosCotizacion,$data['idCotizacion']);
            $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('estatus' => 2),array('id'=>$idCotizacion));
        if ($rentaVentaPoliza==1) {
                //log_message('error', 'entra tipoventa: 1');
                // Se elimina el ID del registro de la cotización de los datos que usaremos
                unset($datosDeCotizacion[0]->id);unset($datosDeCotizacion[0]->estatus);unset($datosDeCotizacion[0]->tipotoner);unset($datosDeCotizacion[0]->rentaVentaPoliza);unset($datosDeCotizacion[0]->propuesta);unset($datosDeCotizacion[0]->denegada);unset($datosDeCotizacion[0]->motivodenegada);

                // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
                $datosDeCotizacion[0]->estatus = 1;
                $datosDeCotizacion[0]->id_personal = $this->idpersonal;
                // Se indica solo como información, el id de la cotización que va con el proceso
                $datosDeCotizacion[0]->idCotizacion = $idCotizacion;
                $datosDeCotizacion[0]->reg = $this->fechahoyc;


                $equipos=$data["equipos"];
                $DATAeq = json_decode($equipos);

                $accesorios=$data["accesorios"];
                $DATAac = json_decode($accesorios);

                $consumibles=$data["consumibles"];
                $DATAcon = json_decode($consumibles);

                $refacc=$data["refacc"];
                $DATArefac = json_decode($refacc);

                $arrayconsu = $data['arrayconsu'];
                $darrayconsu = json_decode($arrayconsu);

                $totalitems=count($DATAeq)+count($DATAac)+count($DATAcon)+count($DATArefac)+count($darrayconsu);

                if($totalitems>0){
                    if($ordenc!=''){
                        $datosDeCotizacion[0]->orden_comp=$ordenc;
                    }
                    $idVenta = $this->ModeloCatalogos->Insert('ventas',$datosDeCotizacion[0]);    
                }else{
                    $idVenta = 0;
                }
                

                
                for ($j=0;$j<count($DATAeq);$j++){
                    //$DATAeq[$j]->equipo;
                    //$DATAeq[$j]->cantidad;
                    //$DATAeq[$j]->bodega;
                    $detalleequipo=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos',array('id'=>$DATAeq[$j]->equipo));
                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos',array('cont_confirm'=>1),array('id'=>$DATAeq[$j]->equipo));
                    $detalleequipo=$detalleequipo->result();
                    $detalle=$detalleequipo[0];
                    
                    unset($detalle->id); 
                    unset($detalle->idCotizacion);
                    unset($detalle->tipotoner);
                    unset($detalle->cont_confirm);
                    $detalle->idVenta = $idVenta;
                    $detalle->serie_bodega = $DATAeq[$j]->bodega;
                    $detalle->cantidad = $DATAeq[$j]->cantidad;
                    $detalle->comentario=$dirinfo;

                    $this->Ventas_model->insertarDetallesVentasEquipos($detalle);
                    //$this->ModeloCatalogos->updatestock('equipos','stock','-','stock','id',$detalle->idEquipo);
                }
                
                for ($m=0;$m<count($DATAac);$m++){
                    $detalleaccesorio=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_accesorios',array('id_accesoriod'=>$DATAac[$m]->accesorio));
                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos_accesorios',array('cont_confirm'=>1),array('id_accesoriod'=>$DATAac[$m]->accesorio));
                    $detalleaccesorio=$detalleaccesorio->result();
                    $detalle=$detalleaccesorio[0];

                    $idaccesorio=$detalle->id_accesorio;

                    $dataacce['id_venta']=$idVenta;
                    $dataacce['id_equipo']=$detalle->id_equipo;
                    $dataacce['id_accesorio']=$detalle->id_accesorio;
                    $dataacce['serie_bodega']=$DATAac[$m]->bodega;
                    $dataacce['cantidad']=$DATAac[$m]->cantidad;
                    $dataacce['garantia']=$detalle->garantia;
                    $dataacce['comentario']=$dirinfo;
                    //$dataacce['costo']=$this->ModeloCatalogos->costoaccesorio($detalle->id_accesorio);
                    
                    if($detalle->costo>0){
                        $dataacce['costo']=$detalle->costo;
                    }else{
                        $strq = "SELECT costo FROM catalogo_accesorios WHERE id='$idaccesorio'"; 
                        $query = $this->db->query($strq);
                        $costo=0;
                        foreach ($query->result() as $item) {
                            $costo=$item->costo;
                        }
                        $dataacce['costo']=$costo;
                    }
                    $this->ModeloCatalogos->Insert('ventas_has_detallesEquipos_accesorios',$dataacce);
                }
                
                for ($co=0;$co<count($DATAcon);$co++){
                            $detalleconsumible=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_consumibles',array('id'=>$DATAcon[$co]->consumible));
                            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos_consumibles',array('cont_confirm'=>1),array('id'=>$DATAcon[$co]->consumible));
                            $detalleconsumible=$detalleconsumible->result();
                            $detalle=$detalleconsumible[0];

                          
                            $costo_toner=0;
                            $rendimiento_toner=0;
                            $rendimiento_unidad_imagen=0;

                            
                            $costo_toner =$detalle->costo_toner;
                            $rendimiento_toner =$detalle->rendimiento_toner;
                            $rendimiento_unidad_imagen = $detalle->rendimiento_unidad_imagen;
                            
                            //===============================================
                            $dataaccee['idventa']=$idVenta;
                            $dataaccee['idequipo']=$detalle->id_equipo;
                            $dataaccee['id_consumibles']=$detalle->id_consumibles;
                            $dataaccee['costo_toner']=$costo_toner;
                            $dataaccee['rendimiento_toner']=$rendimiento_toner;
                            $dataaccee['rendimiento_unidad_imagen']=$rendimiento_unidad_imagen;
                            
                            $dataaccee['cantidad']=$DATAcon[$co]->cantidad;
                            $dataaccee['bodega']=$DATAcon[$co]->bodega;
                            $dataaccee['comentario']=$dirinfo;
                            $this->ModeloCatalogos->Insert('ventas_has_detallesequipos_consumible',$dataaccee);
                            
                            $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$DATAcon[$co]->cantidad,'consumiblesId',$detalle->id_consumibles,'bodegaId',$DATAcon[$co]->bodega);
                            
                            //1 equipo, 2 acesorio, 3 refaccion,4 consumible    
                            $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$DATAcon[$co]->cantidad,'modeloId'=>$detalle->id_consumibles,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida para venta '.$idVenta,'serieId'=>0,'clienteId'=>$clienteId,'bodega_o'=>$DATAcon[$co]->bodega));
                }
                
                foreach ($DATArefac as $id_p) {
                    $result = $this->Cotizaciones_model->getDatosCotizaciondetallesrefacciones($id_p->id);
                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesRefacciones',array('cont_confirm'=>1),array('id'=>$id_p->id));
                    $idvinculo=$result[0]->id;
                    unset($result[0]->id);
                    unset($result[0]->idCotizacion);
                    unset($result[0]->piezas);
                    unset($result[0]->cont_confirm);
                    $result[0]->piezas=$id_p->cantidad;
                    $result[0]->idVentas = $idVenta;
                    $result[0]->serie_bodega = $id_p->bodega;
                    $result[0]->serie = $id_p->serie;
                    $inser_refaccion=$result[0];
                    $inser_refaccion->idvinculo=$idvinculo;
                    $inser_refaccion->comentario=$dirinfo;
                    // insertar los datos de contizaciondetalles y en id venta insertar el id de la tabla de ventas 
                    $this->Cotizaciones_model->insertaventadetallesrefacciones($inser_refaccion);
                }
                
                foreach ($darrayconsu as $id_p) {
                    $result = $this->Cotizaciones_model->getDatosCotizaciondetalles($id_p->id);
                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesConsumibles',array('cont_confirm'=>1),array('id'=>$id_p->id));
                    unset($result[0]->id);
                    unset($result[0]->idCotizacion);
                    unset($result[0]->piezas);
                    unset($result[0]->cont_confirm);
                    $result[0]->piezas=$id_p->cantidad;
                    $result[0]->idVentas = $idVenta;
                    $result[0]->bodegas = $id_p->bodega;
                    $result[0]->comentario = $dirinfo;
                    // insertar los datos de contizaciondetalles y en id venta insertar el id de la tabla de ventas 
                    $this->Cotizaciones_model->insertaventadetallesconsumibles($result[0]);

                    $this->ModeloCatalogos->updatestock2('consumibles_bodegas','total','-',$id_p->cantidad,'consumiblesId',$result[0]->idConsumible,'bodegaId',$id_p->bodega);

                    $this->ModeloCatalogos->Insert('bitacora_movimientos',array('id_item'=>0,'cantidad'=>$result[0]->piezas,'modeloId'=>$result[0]->idConsumible,'tipo'=>4,'entrada_salida'=>0,'personal'=>$this->idpersonal,'descripcion_evento'=>'salida para venta '.$idVenta,'serieId'=>0,'clienteId'=>$clienteId,'bodega_o'=>$id_p->bodega));
                }

                $polizas = $data['arrserv'];
                $polizas = json_decode($polizas);
                //log_message('error',$polizas);
                if(count($polizas)>0){
                    if($idVenta>0){
                        $datosDeCotizacion[0]->combinada=1;
                    }else{
                        $datosDeCotizacion[0]->combinada=0;
                    }
                    
                    unset($datosDeCotizacion[0]->servicio);
                    unset($datosDeCotizacion[0]->fechaentrega);
                    
                    unset($datosDeCotizacion[0]->servicio_servicio);
                    unset($datosDeCotizacion[0]->servicio_direccion);
                    unset($datosDeCotizacion[0]->servicio_tipo);
                    unset($datosDeCotizacion[0]->servicio_motivo);

                    unset($datosDeCotizacion[0]->info1);
                    unset($datosDeCotizacion[0]->info2);
                    unset($datosDeCotizacion[0]->info3);
                    unset($datosDeCotizacion[0]->info4);
                    unset($datosDeCotizacion[0]->info5);
                    unset($datosDeCotizacion[0]->servicio_poliza);
                    unset($datosDeCotizacion[0]->v_ser_tipo);
                    unset($datosDeCotizacion[0]->v_ser_id);
                    //unset($datosDeCotizacion[0]->notifi_precio);
                    
                    //=================================================
                        for ($l=0;$l<count($polizas);$l++) {
                            $idpoliza=$polizas[$l]->poliza;
                            $wherecdp=array('id'=>$idpoliza);
                            $detallepoliza=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesPoliza',$wherecdp);
                            foreach ($detallepoliza->result() as $itemp) {
                                if($itemp->viewcont_cant>1){
                                    $datosDeCotizacion[0]->viewcont_cant=$itemp->viewcont_cant;
                                    $datosDeCotizacion[0]->viewcont=$itemp->viewcont;
                                }
                            }
                        }
                    //=================================================
                    if($ordenc!=''){
                        $datosDeCotizacion[0]->orden_comp=$ordenc;
                    }
                    $idpolizav = $this->Polizas_model->insertaPolizaCreada($datosDeCotizacion[0]);
                    //log_message('error','$idpoliza:'.$idpolizav);
                    if($idVenta>0){
                        $datavc=array('equipo'=>$idVenta,'consumibles'=>$idVenta,'refacciones'=>$idVenta,'poliza'=>$idpolizav,'telefono'=>$datosDeCotizacion[0]->telefono,'correo'=>$datosDeCotizacion[0]->telefono,'idCliente'=>$datosDeCotizacion[0]->idCliente,'tipov'=>$datosDeCotizacion[0]->tipov,'id_personal'=>$this->idpersonal,
                            'v_ser_tipo'=>$v_ser_tipo,
                            'v_ser_id'=>$v_ser_id
                            );
                        if($ordenc!=''){
                            $datavc['orden_comp']=$ordenc;
                        }
                        $this->ModeloCatalogos->Insert('ventacombinada',$datavc);
                        $this->ModeloCatalogos->updateCatalogo('ventas',array('combinada'=>1),array('id'=>$idVenta));
                    }
                }
                //=============================================================================
                $visitas=0;
                for ($i=0;$i<count($polizas);$i++) {
                    $idpoliza=$polizas[$i]->poliza;
                    $wherecdp=array('id'=>$idpoliza);
                    $detallepoliza=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesPoliza',$wherecdp);
                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesPoliza',array('cont_confirm'=>1),$wherecdp);
                    
                    foreach ($detallepoliza->result() as $itemp) {
                        $itemp->idvinculo=$itemp->id;
                        if($itemp->viewcont_cant>0){
                            $visitas=$itemp->viewcont_cant;
                        }
                        unset($itemp->id);
                        unset($itemp->viewcont);
                        unset($itemp->viewcont_cant);
                        unset($itemp->cont_confirm);
                        unset($itemp->notifi_precio);
                        //unset($itemp->idCotizacion);
                        $itemp->idPoliza = $idpolizav;
                        $itemp->cantidad = $polizas[$i]->cantidad;
                        $itemp->comentario = $dirinfo;
                        $itemp->direccionservicio = $dirinfo;

                        //$itemp->serie = $polizas[$i]->serie;
                        $idp=$this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza',$itemp);

                        $polizasseries = $polizas[$i]->serie;
                        for ($s=0;$s<count($polizasseries);$s++) {
                            if (isset($polizasseries[$s]->series)) {
                                $serierow=$polizasseries[$s]->series;
                                $this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$idp,'serie'=>$serierow));
                                //log_message('error', 'seriesdatos----------'.$serierow);
                            }
                            
                        }
                    }
                }
                if(intval($visitas)>0){
                    $this->ModeloCatalogos->updateCatalogo('polizasCreadas',array('viewcont'=>1,'viewcont_cant'=>$visitas),array('id'=>$idpolizav));
                }


        }elseif ($rentaVentaPoliza==2) {
            //log_message('error', 'entra tipoventa: 2 rentas');
            // Se elimina el ID del registro de la cotización de los datos que usaremos
            unset($datosDeCotizacion[0]->id);
            // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
            unset($datosDeCotizacion[0]->estatus);
            // Se elimina el tipo de "rentaVentaPoliza"
            unset($datosDeCotizacion[0]->rentaVentaPoliza);
            // Se elimina el "tipo" , ya que sabemos que es un ingreso de Equipos
            unset($datosDeCotizacion[0]->tipo);
            unset($datosDeCotizacion[0]->propuesta);
                unset($datosDeCotizacion[0]->denegada);
                unset($datosDeCotizacion[0]->motivodenegada);
                unset($datosDeCotizacion[0]->tipov);

            // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
            $datosDeCotizacion[0]->estatus = 1;
            // Se indica solo como información, el id de la cotización que va con el proceso
            $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
            $datosDeCotizacion[0]->id_personal=$this->idpersonal;
            $datosDeCotizacion[0]->reg = $this->fechahoyc;
            // Aunque la variable se llame idVenta, es el ID de la renta
            $idVenta = $this->Rentas_model->insertarRenta($datosDeCotizacion[0]);


            $equipos=$data["equipos"];
                $DATAeq = json_decode($equipos);
                for ($j=0;$j<count($DATAeq);$j++){
                    //$DATAeq[$j]->equipo;
                    //$DATAeq[$j]->cantidad;
                    //$DATAeq[$j]->bodega;
                    $detalleequipo=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos',array('id'=>$DATAeq[$j]->equipo));
                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos',array('cont_confirm'=>1),array('id'=>$DATAeq[$j]->equipo));
                    //$detalle=$detalleequipo[0]->result();
                    $detalleequipo=$detalleequipo->result();
                    $detalle=$detalleequipo[0];

                    // Se elimina el ID del registro de la cotización de los datos que usaremos
                    unset($detalle->id);
                    // 
                    unset($detalle->idCotizacion);
                    unset($detalle->cont_confirm);

                    $detalle->idRenta = $idVenta;
                    $detalle->serie_bodega=$DATAeq[$j]->bodega;
                    $detalle->cantidad = 1;
                    for ($a=0;$a<$DATAeq[$j]->cantidad;$a++) {
                        $ventaDetalle = $this->Rentas_model->insertarDetallesRentasEquipos($detalle);
                    }
                }
            $accesorios=$data["accesorios"];
            $DATAac = json_decode($accesorios);
            for ($m=0;$m<count($DATAac);$m++){
                $detalleaccesorio=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_accesorios',array('id_accesoriod'=>$DATAac[$m]->accesorio));
                $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos_accesorios',array('cont_confirm'=>1),array('id_accesoriod'=>$DATAac[$m]->accesorio));
                $detalleaccesorio=$detalleaccesorio->result();
                $detalle=$detalleaccesorio[0];

                $idaccesorio=$detalle->id_accesorio;
                $dataacce['idrentas']=$idVenta;
                $dataacce['id_equipo']=$detalle->id_equipo;
                $dataacce['id_accesorio']=$detalle->id_accesorio;
                $dataacce['serie_bodega']=$DATAac[$m]->bodega;
                $dataacce['cantidad']=1;
                $dataacce['costo']=$this->ModeloCatalogos->costoaccesorio($idaccesorio);
                for ($a=0;$a<$DATAac[$m]->cantidad;$a++) {
                    $this->ModeloCatalogos->Insert('rentas_has_detallesEquipos_accesorios',$dataacce);
                }
            }  
            $consumibles=$data["consumibles"];
            $DATAcon = json_decode($consumibles);
            for ($co=0;$co<count($DATAcon);$co++){
                $detalleconsumible=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesEquipos_consumibles',array('id'=>$DATAcon[$co]->consumible));
                                    $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos_consumibles',array('cont_confirm'=>1),array('id'=>$DATAcon[$co]->consumible));
                $detalleconsumible=$detalleconsumible->result();
                $detalle=$detalleconsumible[0];

              
                $costo_toner=0;
                $rendimiento_toner=0;
                $rendimiento_unidad_imagen=0;

                
                $costo_toner =$detalle->costo_toner;
                $rendimiento_toner =$detalle->rendimiento_toner;
                $rendimiento_unidad_imagen = $detalle->rendimiento_unidad_imagen;
                
                //===============================================
                $dataaccee['idrentas']=$idVenta;
                $dataaccee['idequipo']=$detalle->id_equipo;
                $dataaccee['id_consumibles']=$detalle->id_consumibles;
                $dataaccee['costo_toner']=$costo_toner;
                $dataaccee['rendimiento_toner']=$rendimiento_toner;
                $dataaccee['rendimiento_unidad_imagen']=$rendimiento_unidad_imagen;
                
                $dataaccee['cantidad']=$DATAcon[$co]->cantidad;
                for ($a=0;$a<$DATAcon[$co]->cantidad;$a++) {
                    $this->ModeloCatalogos->Insert('rentas_has_detallesequipos_consumible',$dataaccee);
                }
            } 
        }elseif ($rentaVentaPoliza==3) {
                //$arrserv=$data["arrserv"];
                //$DATAeq = json_decode($arrserv);
                //for ($j=0;$j<count($DATAeq);$j++){
                    //log_message('error', 'entra tipoventa: 2 rentas');
                    // Se elimina el ID del registro de la cotización de los datos que usaremos
                    unset($datosDeCotizacion[0]->id);
                    // Se elimina el estatus que viene de la BD, ya que no corresponde con el campo de estatus de la tabla de ventas
                    unset($datosDeCotizacion[0]->estatus);
                    // Se elimina el tipo de "rentaVentaPoliza"
                    unset($datosDeCotizacion[0]->rentaVentaPoliza);
                    // Se elimina el "tipo" , ya que sabemos que es un ingreso de Equipos
                    unset($datosDeCotizacion[0]->tipo);
                    unset($datosDeCotizacion[0]->propuesta);
                unset($datosDeCotizacion[0]->denegada);
                unset($datosDeCotizacion[0]->motivodenegada);
                unset($datosDeCotizacion[0]->reg);
                unset($datosDeCotizacion[0]->servicio);
                 unset($datosDeCotizacion[0]->fechaentrega);
                 unset($datosDeCotizacion[0]->servicio_poliza);
                    unset($datosDeCotizacion[0]->servicio_servicio);
                    unset($datosDeCotizacion[0]->servicio_direccion);
                    unset($datosDeCotizacion[0]->servicio_tipo);
                    unset($datosDeCotizacion[0]->servicio_motivo);
                    unset($datosDeCotizacion[0]->info1);
                    unset($datosDeCotizacion[0]->info2);
                    unset($datosDeCotizacion[0]->info3);
                    unset($datosDeCotizacion[0]->info4);
                    unset($datosDeCotizacion[0]->info5);
                    unset($datosDeCotizacion[0]->v_ser_tipo);
                    unset($datosDeCotizacion[0]->v_ser_id);
                    //unset($datosDeCotizacion[0]->notifi_precio);

                    // El estatus indicará si la venta está en una fase inicial o ya se ha acompletado 
                    $datosDeCotizacion[0]->estatus = 1;
                    // Se indica solo como información, el id de la cotización que va con el proceso
                    $datosDeCotizacion[0]->idCotizacion = $data['idCotizacion'];
                    $datosDeCotizacion[0]->reg = $this->fechahoyc;
                    //$datosDeCotizacion[0]->id_personal=$this->idpersonal;
                    // Aunque la variable se llame idVenta, es el ID de la renta
                    $arrserv=$data["arrserv"];
                    $polizas = json_decode($arrserv);
                    //=================================================
                        for ($l=0;$l<count($polizas);$l++) {
                            $idpoliza=$polizas[$l]->poliza;
                            $wherecdp=array('id'=>$idpoliza);
                            $detallepoliza=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesPoliza',$wherecdp);
                            foreach ($detallepoliza->result() as $itemp) {
                                if($itemp->viewcont_cant>1){
                                    $datosDeCotizacion[0]->viewcont_cant=$itemp->viewcont_cant;
                                    $datosDeCotizacion[0]->viewcont=$itemp->viewcont;
                                }
                            }
                        }
                    //=================================================

                    if($ordenc!=''){
                        $datosDeCotizacion[0]->orden_comp=$ordenc;
                    }
                    $idVenta = $this->Polizas_model->insertaPolizaCreada($datosDeCotizacion[0]);

                    
                    for ($i=0;$i<count($polizas);$i++) {
                        $idpoliza=$polizas[$i]->poliza;
                        $wherecdp=array('id'=>$idpoliza);
                        $detallepoliza=$this->ModeloCatalogos->db13_getselectwheren('cotizaciones_has_detallesPoliza',$wherecdp);
                        $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesPoliza',array('cont_confirm'=>1),$wherecdp);
                        foreach ($detallepoliza->result() as $itemp) {
                            unset($itemp->id);
                            unset($itemp->viewcont);
                            unset($itemp->viewcont_cant);
                            unset($itemp->cont_confirm);
                            unset($itemp->notifi_precio);
                            //unset($itemp->idCotizacion);
                            $itemp->idPoliza = $idVenta;
                            $itemp->cantidad = $polizas[$i]->cantidad;
                            //$itemp->serie = $polizas[$i]->serie;
                            $idp=$this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza',$itemp);

                            $polizasseries = $polizas[$i]->serie;
                            for ($s=0;$s<count($polizasseries);$s++) {
                                if (isset($polizasseries[$s]->series)) {
                                    $serierow=$polizasseries[$s]->series;
                                    $this->ModeloCatalogos->Insert('polizasCreadas_has_detallesPoliza_series',array('iddetalle'=>$idp,'serie'=>$serierow));
                                    //log_message('error', 'seriesdatos----------'.$serierow);
                                }
                                
                            }
                        }
                        


                    }
                //}
        }
       
        // Convertir Prospecto a Cliente en caso de ser necesario
        if($idVenta!=0 && $idVenta!=null){
            $datosProspectoCliente = $this->Clientes_model->getClientePorId($datosDeCotizacion[0]->idCliente);
            if($datosProspectoCliente->tipo==2){
                $data = array('tipo' => 1);
                $actualizado = $this->Prospectos_model->update_prospecto($data, $datosDeCotizacion[0]->idCliente);
            }
        }

        $respuestaJSON = array("idVenta"=>$idVenta,'rentaVentaPoliza'=>$rentaVentaPoliza);
        echo json_encode($respuestaJSON);
    }
    function quitariva(){
        $pass = $this->input->post('pass');

        $id = $this->input->post('id');
        $tipo = $this->input->post('tipo');
        $sincon = $this->input->post('sincon'); 

        $tipo_view='';
        if($tipo==0){
            $tipo_view='Venta';
        }
        if($tipo==1){
            $tipo_view='Venta Combinada';
        }
        if($tipo==2){
            $tipo_view='Poliza';
        }
        if($tipo==3){
            $tipo_view='ventasd';
        }
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }

            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion para quitar iva a la prefactura:'.$id.' tipo prefactura:'.$tipo.' Usuario:'.$item->Usuario.'','nombretabla'=>$tipo_view,'idtable'=>$id,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }

        if ($permiso==1) {
            if($sincon==0){
                $siniva = 1;
            }else{
                $siniva = 0;
            }
            if($tipo==0){
                //$tipo_view='Venta';
                $this->ModeloCatalogos->updateCatalogo('ventas',array('siniva'=>$siniva),array('id'=>$id));
            }
            if($tipo==1){
                //$tipo_view='Venta Combinada';
                $this->ModeloCatalogos->updateCatalogo('ventacombinada',array('siniva'=>$siniva),array('combinadaId'=>$id));
            }
            if($tipo==2){
                //$tipo_view='Poliza';
                $this->ModeloCatalogos->updateCatalogo('polizascreadas',array('siniva'=>$siniva),array('id'=>$id));
            }
            if($tipo==3){
                //$tipo_view='Venta';
                $this->ModeloCatalogos->updateCatalogo('ventasd',array('siniva'=>$siniva),array('id'=>$id));
            }
        }
        echo $permiso;
    }
    function consumiblesbodegas(){
        $id = $this->input->post('id');
        $bodegai = $this->input->post('bodegai');
        
        $resultbodegas=$this->ModeloCatalogos->view_session_bodegas();
        $equiposdata='';
        foreach ($resultbodegas as $itemb) {
            $stock=$this->ModeloGeneral->stockconsumiblesbodega($id,$itemb->bodegaId);
            if($bodegai==$itemb->bodegaId){
                $b_selected='selected';
                $b_info=$itemb->bodega;
                $stock=1;
            }else{
                $b_selected='';
                $b_info=$itemb->bodega.' ('.$stock.')';
            }
            $equiposdata.='<option value="'.$itemb->bodegaId.'" data-stock="'.$stock.'" '.$b_selected.'>'.$b_info.'</option>';
        }
        echo $equiposdata;
    }
    function clonar(){
        $idc = $this->input->post('idc');
        if($idc>0){
            $resultcoti=$this->ModeloCatalogos->getselectwheren('cotizaciones',array('id'=>$idc));
            $idCotizacion=0;
            foreach ($resultcoti->result() as $itemc) {
                $itemc->id_cot_prev=$itemc->id;
                unset($itemc->reg);
                unset($itemc->id);
                $itemc->id_personal=$this->idpersonal;
                $itemc->denegada=0;
                $itemc->estatus=1;

                $idCotizacion=$this->ModeloCatalogos->Insert('cotizaciones',$itemc);
            }
            if($idCotizacion>0){
                /*
                cotizaciones_has_detallesConsumibles
                cotizaciones_has_detallesEquipos
                cotizaciones_has_detallesEquipos_accesorios
                cotizaciones_has_detallesEquipos_consumibles
                cotizaciones_has_detallesPoliza
                cotizaciones_has_detallesRefacciones
                */
                $result_c=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_detallesConsumibles',array('idCotizacion'=>$idc,'cont_confirm'=>0));
                foreach ($result_c->result() as $itemc) {
                    unset($itemc->id);
                    $itemc->idCotizacion=$idCotizacion;
                    $this->ModeloCatalogos->Insert('cotizaciones_has_detallesConsumibles',$itemc);
                }
                $result_e=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_detallesEquipos',array('idCotizacion'=>$idc,'cont_confirm'=>0));
                foreach ($result_e->result() as $iteme) {
                    unset($iteme->id);
                    $iteme->idCotizacion=$idCotizacion;
                    $this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos',$iteme);
                }
                $result_ea=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_detallesEquipos_accesorios',array('id_cotizacion'=>$idc,'cont_confirm'=>0));
                foreach ($result_ea->result() as $itemea) {
                    unset($itemea->id_accesoriod);
                    $itemea->id_cotizacion=$idCotizacion;
                    $this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_accesorios',$itemea);
                }
                $result_ec=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_detallesEquipos_consumibles',array('id_cotizacion'=>$idc,'cont_confirm'=>0));
                foreach ($result_ec->result() as $itemec) {
                    unset($itemec->id);
                    $itemec->id_cotizacion=$idCotizacion;
                    $this->ModeloCatalogos->Insert('cotizaciones_has_detallesEquipos_consumibles',$itemec);
                }
                $result_p=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_detallesPoliza',array('idCotizacion'=>$idc,'cont_confirm'=>0));
                foreach ($result_p->result() as $itemp) {
                    unset($itemp->id);
                    unset($itemp->notifi_precio);
                    $itemp->idCotizacion=$idCotizacion;
                    $this->ModeloCatalogos->Insert('cotizaciones_has_detallesPoliza',$itemp);
                }
                $result_r=$this->ModeloCatalogos->getselectwheren('cotizaciones_has_detallesRefacciones',array('idCotizacion'=>$idc,'cont_confirm'=>0));
                foreach ($result_r->result() as $itemr) {
                    unset($itemr->id);
                    $itemr->idCotizacion=$idCotizacion;
                    $this->ModeloCatalogos->Insert('cotizaciones_has_detallesRefacciones',$itemr);
                }
            }



        }
    }
    function editarprecioacc(){
        $precio = $this->input->post('precio');
        $equipo = $this->input->post('equipo');
        $pass = $this->input->post('pass');
        $idCliente = $this->input->post('idCliente');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;

            $verificar = password_verify($pass,$item->contrasena);

            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion desde cotizaciones con el usuario:'.$item->Usuario.' para la edicion de precios','nombretabla'=>'cotizaciones_has_detallesEquipos_accesorios','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        if($permiso==0 and $idCliente==597){
            if($pass=='DHL2023' or $pass=='dhl2023'){
                $permiso=1;
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el con DHL2023 para la edicion de precios','nombretabla'=>'cotizaciones_has_detallesEquipos_accesorios','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }
        if ($permiso==1) {
            $data['costo']=$precio;
            $where = array('id_accesoriod' => $equipo );
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos_accesorios',$data,$where);
        }

        echo $permiso;
    }
    function deletepoliza(){
        $params = $this->input->post();
        $id=$params['id'];
        $this->ModeloCatalogos->getdeletewheren('cotizaciones_has_detallesPoliza',array('id'=>$id));
    }
    function editarprecioeqacc(){
        $precio = $this->input->post('precio');
        $equipo = $this->input->post('equipo');
        $pass = $this->input->post('pass');
        $idCliente = $this->input->post('idCliente');
        $permiso=0;
        $permisos=$this->Login_model->permisoadmin();
        foreach ($permisos as $item) {
            $permisotemp=0;
            $verificar = password_verify($pass,$item->contrasena);
            if ($verificar==false) {
                $verificar = password_verify($pass,$item->clavestemp);
                if ($verificar) {
                    $permisotemp=1;
                }
            }
            if ($verificar) {
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion desde cotizaciones con el usuario:'.$item->Usuario.' para la edicion de precios','nombretabla'=>'cotizaciones_has_detallesEquipos','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
                $permiso=1;
                if($permisotemp==1){
                    $this->ModeloCatalogos->updateCatalogo('usuarios',array('clavestemp'=>null),array('UsuarioID'=>$item->UsuarioID));
                }
            }
        }
        if($permiso==0 and $idCliente==597){
            if($pass=='DHL2023' or $pass=='dhl2023'){
                $permiso=1;
                $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una autorizacion con el con DHL2023 para la edicion de precios','nombretabla'=>'cotizaciones_has_detallesEquipos','idtable'=>$equipo,'tipo'=>'Solicitud permiso','personalId'=>$this->idpersonal));
            }
        }
        if ($permiso==1) {
            $data['costo']=$precio;
            $where = array('id_accesoriod' => $equipo );
            $this->ModeloCatalogos->updateCatalogo('cotizaciones_has_detallesEquipos_accesorios',$data,$where);
        }

        echo $permiso;
    }
    function edit_cont(){
        $params = $this->input->post();
        $idc = $params['idc'];
        unset($params['idc']);
        $this->ModeloCatalogos->updateCatalogo('cotizaciones',$params,array('id'=>$idc));
    }
    function editcondicionesr(){
        $params=$this->input->post();
        $id =$params['id'];
        $tipo=$params['tipo'];

        unset($params['id']);
        unset($params['tipo']);

        if($tipo==0){
            $this->ModeloCatalogos->updateCatalogo('cotizaciones',$params,array('id'=>$id));
        }
    }
    function cambiopersonal(){
        $params=$this->input->post();
        $id =$params['id'];
        $eje=$params['eje'];
        $this->ModeloCatalogos->updateCatalogo('cotizaciones',array('id_personal'=>$eje),array('id'=>$id));
        $this->ModeloCatalogos->Insert('bitacora',array('contenido'=>'Se realizo una edicion del ejecutivo en la cotizacion '.$id.', se agrego al ejecutivo con id: '.$eje.'','nombretabla'=>'cotizaciones','idtable'=>$id,'tipo'=>'cambio ejecutivo','personalId'=>$this->idpersonal));
    }
}
?>


