<style type="text/css">
td{font-size: 12px;padding: 3px 7px;  }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bulma-badge.min.css">
<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      
      <div id="table-datatables">
        <h4 class="header">Contratos vencidos</h4>
        
        <div class="row">
          <div class="col s12">
            <table id="tabla_contratos" class="table striped" cellspacing="0">
              <thead>
                <tr>
                  <th>Contrato</th>
                  <th>Folio</th>
                  <th>Empresa</th>
                  <th>Fecha Inicio</th>
                  <th>vigencia</th>
                  <th>Fecha final</th>
                  <th>Fecha final provisional</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>