<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Control_tareas extends CI_Controller {
    public function __construct()     {
        parent::__construct();

        $this->load->model('ModeloCatalogos');
        $this->load->model('Modeloaccesorios');
        $this->load->model('ModeloGeneral');
        $this->load->helper('url');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechac = date('Y-m-d');
        $this->submenu=77;
        if($this->session->userdata('logeado')==true){
            $this->perfilid =$this->session->userdata('perfilid');
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->usuario = $this->session->userdata('usuario');
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,$this->submenu);// 77 es el id del submenu
            if ($permiso==0) {
                //redirect('Sistema');
            }
        }else{
            redirect('Sistema'); 
        }
    }

    function index(){
        redirect('Control_tareas/tareas'); 
    }
    function tareas($viewtipo=0){
        $data['MenusubId']=$this->submenu; 
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        //$data['res_personal'] = $this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['res_personal']=$this->ModeloGeneral->listadopersonal();
        $data['viewtipo']=$viewtipo;
        $this->load->view('header');
        $this->load->view('main');
        $this->load->view('control_tareas/lis',$data);
        $this->load->view('footer');
        $this->load->view('control_tareas/lisjs');
    }
    function iframe($viewtipo=0){
        $data['MenusubId']=$this->submenu; 
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        //$data['res_personal'] = $this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['res_personal']=$this->ModeloGeneral->listadopersonal();
        $data['viewtipo']=$viewtipo;
        $this->load->view('header');
        //$this->load->view('control_tareas/header');
        //$this->load->view('main');
        $this->load->view('control_tareas/lis',$data);
        $this->load->view('control_tareas/footer');
        $this->load->view('control_tareas/lisjs');
    }
    function iframeaddact($viewtipo=0){
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        //$data['res_personal'] = $this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $data['res_personal']=$this->ModeloGeneral->listadopersonal();
        $data['viewtipo']=$viewtipo;
        $this->load->view('control_tareas/iframeaddact',$data);
    }
   
    function registraactividad(){
        $params = $this->input->post();
        log_message('error',json_encode($params));
        $titulo = $params['title'];
        $descripcion = $params['contenido'];
        $asig_personal = $params['personal'];
        $fecha = $params['fecha'];
        $personalplus = $params['personalplus'];
        $tipoac = $params['tipoac'];
        $DATAp = json_decode($personalplus);

        $data=array(
                'titulo'=>$titulo,
                'descripcion'=>$descripcion,
                'asig_personal'=>$asig_personal,
                'creo_personalId'=>$this->idpersonal,
                'vigencia'=>$fecha,
                'actividades'=>$tipoac
        );



        $id_insert = $this->ModeloCatalogos->Insert('actividades',$data);

        for ($i=0;$i<count($DATAp);$i++) {
            $this->ModeloCatalogos->Insert('actividades_otros_personal',array('id_asig'=>$id_insert,'personal'=>$DATAp[$i]->per));
        }

        echo $id_insert;
    }
    function datos_actividades(){
        $params = $this->input->post();
        $visor = $params['visor'];
        $tipo = $params['tipo'];
        $per = $params['per'];
        $viewtipo = $params['viewtipo'];
        
        if($per>0){
            $where_personal=" and (act.asig_personal='$per' or act.creo_personalId='$per' or aop.personal='$per' ) ";
        }else{
            $where_personal='';
        }

        $strq = "SELECT act.id, act.titulo, per.nombre, per.apellido_paterno,act.vigencia,act.status,act.finalizado, group_concat(per2.nombre separator ',') as personalplus,
                per3.nombre as nombrecreo, per3.apellido_paterno as apellido_paternocreo,act.reg
                FROM actividades as act 
                INNER JOIN personal as per on per.personalId=act.asig_personal 
                left join actividades_otros_personal as aop on aop.id_asig=act.id
                left JOIN personal as per2 on per2.personalId=aop.personal 
                left JOIN personal as per3 on per3.personalId=act.creo_personalId
                WHERE act.status='$tipo' AND act.activo=1 and act.actividades='$viewtipo' $where_personal
                GROUP by act.id
                ";
        $query = $this->db->query($strq);
        
        $html='';


        foreach ($query->result() as $item) {
            $back_color='';
            if($item->vigencia!='' && $item->vigencia!=null && $item->vigencia!='0000-00-00'){
                if($item->vigencia<$this->fechac){
                    $back_color=' advertencia ';
                }
            }
            if($item->finalizado==1){
                $back_fin=' act_fin ';
            }else{
                $back_fin='';
            }
            if($item->status==3){
                $btn_fin='<button class="o0opcYgoysGMA4 bxgKMAm3lq5BpA HAVwIqCeMHpVKh SEj5vUdI3VvxDc btn-delete" type="button" data-testid="quick-card-editor-button" aria-label="Editar tarjeta" tabindex="-1" onclick="finaliza('.$item->id.')" style="margin-top: 30px;">';
                        $btn_fin.='<span class="nch-icon A3PtEe1rGIm_yL neoUEAwI0GETBQ fAvkXZrzkeHLoc">';
                            $btn_fin.='<span data-testid="EditIcon" aria-hidden="true" class="css-snhnyn" style="--icon-primary-color: var(--ds-text-accent-gray-bolder, #172B4D); --icon-secondary-color: inherit;">';
                                $btn_fin.='<i class="fas fa-check-circle"></i>';
                            $btn_fin.='</span>';
                        $btn_fin.='</span>';
                    $btn_fin.='</button>';
            }else{
                $btn_fin='';
            }
            $html.='<li class="T9JQSaXUsHTEzk actividad_'.$item->id.'" data-testid="list-card" draggable="true" data-drop-target-for-element="true" ondragstart="arrastrar(event,'.$item->id.')" data-fecha="'.$item->vigencia.'">';
                        $html.='<div class="KWQlnMvysRK4fI ui-droppable '.$back_color.' '.$back_fin.'" data-testid="trello-card" data-card-id="60145bcfdcae614d46dc9ce8" data-drop-target-for-file="true" data-drop-target-for-element="true">';
                            $html.='<div class="amUfYqLTZOvGsn" ondblclick="info_ac('.$item->id.')">';
                                $html.='<a draggable="false" class="NdQKKfeqJDDdX3">';
                                    $html.='<span class="per_titulo">De: '.$item->nombrecreo.' '.$item->apellido_paternocreo.'<br>Para: <b>'.$item->nombre.' '.$item->apellido_paterno.' '.$item->personalplus.'</b></span><br>';
                                    $html.=''.$item->titulo.'';
                                    $html.='<br><span class="per_titulo ">Creado: '.$item->reg.'</span>';
                                    $html.='<br><span class="per_titulo "  onclick="editarlimite('.$item->id.')">Limite: '.$item->vigencia.'</span><br>';
                                $html.='</a>';
                                $html.='<div class="jS9THwzyUow79A" data-testid="card-front-badges"></div>';
                            $html.='</div>';
                            $html.='<button class="o0opcYgoysGMA4 bxgKMAm3lq5BpA HAVwIqCeMHpVKh SEj5vUdI3VvxDc btn-delete" type="button" data-testid="quick-card-editor-button" aria-label="Editar tarjeta" tabindex="-1" onclick="deleteact('.$item->id.')">';
                                $html.='<span class="nch-icon A3PtEe1rGIm_yL neoUEAwI0GETBQ fAvkXZrzkeHLoc">';
                                    $html.='<span data-testid="EditIcon" aria-hidden="true" class="css-snhnyn" style="--icon-primary-color: var(--ds-text-accent-gray-bolder, #172B4D); --icon-secondary-color: inherit;">';
                                        $html.='<i class="fas fa-trash"></i>';
                                    $html.='</span>';
                                $html.='</span>';
                            $html.='</button>'.$btn_fin.'';
                        $html.='</div>';
                    $html.='</li>';
        }
        echo $html;

    }
    function confirmacionmv(){
        $params = $this->input->post();
        $idact = $params['idact'];
        $estado = $params['estado'];


        $this->ModeloCatalogos->updateCatalogo('actividades',array('status'=>$estado),array('id'=>$idact));
        $this->ModeloCatalogos->updateCatalogo('actividades',array('finalizado'=>1),array('id'=>$idact,'actividades'=>0,'status'=>3));
        $this->ModeloCatalogos->updateCatalogo('actividades',array('finalizado'=>0),array('id'=>$idact,'actividades'=>0,'status<'=>3));
    }
    function info_ac($id){
        $data['idreg']=$id;
        $data['perfilid']=$this->perfilid;
        $data['idpersonal']=$this->idpersonal;
        //$data['res_personal'] = $this->ModeloCatalogos->getselectwheren('personal',array('estatus'=>1));
        $this->load->view('control_tareas/info_ac',$data);
    }
    function infoview(){
        $params = $this->input->post();
        $idreg = $params['idreg'];
        /*
        $asig_personal=[];
        $solresult_per=$this->ModeloCatalogos->db13_getselectwheren('actividades_otros_personal',array('id_asig'=>$idreg));
        foreach ($solresult_per->result() as $itemp) {
            $asig_personal[]=$itemp->personal;
        }
        */
        $array_person_g=array();
        $sol_g_vista=$this->ModeloGeneral->consultar_lectura_general($idreg,0);
        $vg_p='';$vg=0;
        $vg_p_num=0;$vg_p_num_v=0;
        foreach ($sol_g_vista->result() as $item_vg) {
            $array_person_g[]=$item_vg->personal;
            $vg_p_num++;
            if($item_vg->id>0){
                $vg_p.=$item_vg->nombre.' '.$item_vg->apellido_paterno.', ';
                $vg_p_num_v++;
            }
        }
        if($vg_p_num_v>0){
            $vg=1;
            if($vg_p_num==$vg_p_num_v){
                $vg=2;
            }
        }
        $solresult=$this->ModeloCatalogos->db13_getselectwheren('actividades',array('id'=>$idreg));

        $solresult_file=$this->ModeloCatalogos->db13_getselectwheren('actividades_files',array('idact'=>$idreg,'tipo'=>0));
        $html='';
        foreach ($solresult->result() as $item) {
            //$asig_personal[]=$item->asig_personal;
                    $img_leido='';
                    if($vg>0){
                        if($vg==2){
                            $img_leido='<img src="'.base_url().'public/img/leido2.png" class="img_leido tooltipped" data-position="top" data-delay="50" data-tooltip="'.$vg_p.'" title="'.$vg_p.'">';
                        }else{
                            $img_leido='<img src="'.base_url().'public/img/leido1.png" class="img_leido tooltipped" data-position="top" data-delay="50" data-tooltip="'.$vg_p.'" title="'.$vg_p.'">';
                        }
                    }

            
                    $html.='<div class="card viewmensajes personal_'.$item->creo_personalId.'">';
                        $html.='<div class="card-header">'.$item->titulo.'</div>';
                        $html.='<div class="card-body">';
                                $html.=$item->descripcion;
                                foreach ($solresult_file->result() as $itemfi) {
                                    $html.='<div class="col s9 col-md-9"><a href="'.base_url().'uploads/otros/'.$itemfi->img.'" target="_blank"><i class="fas fa-paperclip"></i>Archivo adjunto</a></div>';
                                }
                        $html.='</div>';
                        $html.='<div class="card-footer">';
                          $html.='<small class="text-muted">'.$img_leido.$item->reg.'</small>';
                        $html.='</div>';
                    $html.='</div>';
            //if($this->idpersonal==$item->asig_personal){
            if (in_array($this->idpersonal, $array_person_g)) {
                $sol_g_v_p=$this->ModeloGeneral->consultar_lectura_general($idreg,$this->idpersonal);
                if($sol_g_v_p->num_rows()==0){
                    $this->ModeloCatalogos->Insert('actividades_lectura',array('tipo'=>0,'id_act'=>$idreg,'idpersonal'=>$this->idpersonal));
                }
            }
            
        }
        $solresult=$this->ModeloCatalogos->db13_getselectwheren('actividades_detalle',array('id_asig'=>$idreg));
        foreach ($solresult->result() as $item) {
            $solresult_file=$this->ModeloCatalogos->db13_getselectwheren('actividades_files',array('idact'=>$item->id,'tipo'=>1));
            //================================================
                $array_person_dll=array();
                $sol_dll_vista=$this->ModeloGeneral->consultar_lectura_detalle($idreg,$item->id,0);
                $vg_p_dll='';$vg_dll=0;
                $vg_p_num_dll=0;$vg_p_num_v_dll=0;
                foreach ($sol_dll_vista->result() as $item_vg) {
                    $array_person_dll[]=$item_vg->persona;
                    $vg_p_num_dll++;
                    if($item_vg->id_lectura>0){
                        $vg_p_dll.=$item_vg->nombre.' '.$item_vg->apellido_paterno.', ';
                        $vg_p_num_v_dll++;
                    }
                }
                if($vg_p_num_v_dll>0){
                    $vg_dll=1;
                    if($vg_p_num_dll==$vg_p_num_v_dll){
                        $vg_dll=2;
                    }
                }
                $img_leido='';
                if($vg_dll>0){
                    if($vg_dll==2){
                        $img_leido='<img src="'.base_url().'public/img/leido2.png" class="img_leido tooltipped x" data-position="top" data-delay="50" data-tooltip="'.$vg_p_dll.'" title="'.$vg_p_dll.'">';
                    }else{
                        $img_leido='<img src="'.base_url().'public/img/leido1.png" class="img_leido tooltipped x" data-position="top" data-delay="50" data-tooltip="'.$vg_p_dll.'" title="'.$vg_p_dll.'">';
                    }
                }
            //================================================
            
                    $html.='<div class="card viewmensajes personal_'.$item->creo_personalId.'">';
                        $html.='<div class="card-body">';
                                $html.=$item->contenido;
                                foreach ($solresult_file->result() as $itemfi) {
                                    $html.='<div class="col s9 col-md-9"><a href="'.base_url().'uploads/otros/'.$itemfi->img.'" target="_blank"><i class="fas fa-paperclip"></i>Archivo adjunto</a></div>';
                                }
                        $html.='</div>';
                        $html.='<div class="card-footer">';
                          $html.='<small class="text-muted">'.$img_leido.$item->usuario.' '.$item->reg.'</small>';
                        $html.='</div>';
                    $html.='</div>';
            if (in_array($this->idpersonal, $array_person_dll)) {
                $sol_g_v_p=$this->ModeloGeneral->consultar_lectura_detalle($idreg,$item->id,$this->idpersonal);
                if($sol_g_v_p->num_rows()==0){
                    $this->ModeloCatalogos->Insert('actividades_lectura',array('tipo'=>1,'id_act'=>$item->id,'idpersonal'=>$this->idpersonal));
                }
            }
            
        }
        echo $html;
    }
    function agregar_res(){
        $params = $this->input->post();
        $idreg = $params['idreg'];
        $contenido = $params['contenido'];
        $data=array(
            'id_asig'=>$idreg,
            'contenido'=>$contenido,
            'usuario'=>$this->usuario,
            'creo_personalId'=>$this->idpersonal
        );

        $id_insert = $this->ModeloCatalogos->Insert('actividades_detalle',$data);
        echo $id_insert;
    }
    function deleteact(){
        $params = $this->input->post();
        $idact = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('actividades',array('activo'=>0),array('id'=>$idact));
    }
    function imagenes_multiple(){
        $idac=$_POST['idac'];
        $tipo=$_POST['tipo'];
      $data = [];
   
      $count = count($_FILES['files']['name']);
      $output = [];
      for($i=0;$i<$count;$i++){
    
        if(!empty($_FILES['files']['name'][$i])){
    
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];
          $DIR_SUC=FCPATH.'uploads/otros';

          $config['upload_path'] = $DIR_SUC; 
          //$config['allowed_types'] = 'docx|xlsx|pdf|doc|xls|pptx';
          $config['allowed_types'] = '*';

          //$config['allowed_types'] = 'docx|xlsx|pdf|jpg|jpeg|png|gif';
          $config['max_size'] = 5000;
          $file_names=date('ymd_His').'_'.rand(0, 500);
          //$config['file_name'] = $file_names.'_'.str_replace(' ', '_', $_FILES['files']['name'][$i]);
          $config['file_name'] = $file_names;
          //$config['file_name'] = $file_names;
   
          $this->load->library('upload',$config); 
    
          if($this->upload->do_upload('file')){
            $uploadData = $this->upload->data();
            $filename = $uploadData['file_name'];
            $filenametype = $uploadData['file_ext'];
   
            $data['totalFiles'][] = $filename;
            //$this->ModeloGeneral->tabla_inserta('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename,'filetype'=>$filenametype,'usuario'=>$this->idpersonal));
            //$this->ModeloCatalogos->Insert('equipo_imgs',array('idequipo'=>$idequipo,'imagen'=>$filename));
            $this->ModeloCatalogos->Insert('actividades_files',array('idact'=>$idac,'img'=>$filename,'tipo'=>$tipo));
          }else{
            $data = array('error' => $this->upload->display_errors());
            log_message('error', json_encode($data)); 
          }
        }
   
      }
      echo json_encode($output);
    }
    function finaliza(){
        $params = $this->input->post();
        $idact = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('actividades',array('finalizado'=>1),array('id'=>$idact));
    }
    function editarlimiteok(){
        $params = $this->input->post();
        $idact = $params['id'];
        $fecha = $params['fecha'];
        $this->ModeloCatalogos->updateCatalogo('actividades',array('vigencia'=>$fecha),array('id'=>$idact));
    }

    
}
?>
