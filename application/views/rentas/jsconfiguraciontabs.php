<?php 
	$versionscripts=date('YmdGis');
?>
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/fileinput/fileinput.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/loader2/jquery.loader.css">
<script src="<?php echo base_url(); ?>public/plugins/loader2/jquery.loader.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/bootstrap/bootstrap.meterialize.min.css?v=2">
<link href="<?php echo base_url(); ?>public/plugins/select2/select2.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracionrentab1.js?ver=<?php echo $versionscripts; ?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracionrentab2.js?ver=<?php echo $versionscripts; ?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracionrentab3.js?ver=<?php echo $versionscripts; ?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracionrentab4.js?ver=<?php echo $versionscripts; ?>" ></script><!--prefactura-->
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracionrentab4edit.js?ver=<?php echo $versionscripts; ?>" ></script><!--prefactura-->
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracionrentablistfact.js?ver=<?php echo $versionscripts; ?>" ></script><!--prefactura-->
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/floatThead/jquery.floatThead.min.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracionrenta.js?v=<?php echo date('YmdGis');?>" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/notificacion.js?v=<?php echo date('Ymd');?>"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/configuracionrenta_fac.js?ver=<?php echo $versionscripts; ?>"></script>
<?php if($lis_fac_prog->num_rows()>0){ ?>
<script type="text/javascript">
	<?php 
		foreach ($lis_fac_prog->result() as $itemfp) {
			$FacturasId = $itemfp->FacturasId;
			$this->ModeloCatalogos->updateCatalogo('f_facturas',array('program_proc'=>1),array('FacturasId'=>$FacturasId));
			?>
				retimbrar2(<?php echo $FacturasId;?>);
			<?php
		}
	?>
</script>
<?php } ?>