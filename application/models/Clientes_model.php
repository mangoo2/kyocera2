<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Clientes_model extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->DB5 = $this->load->database('other5_db', TRUE);
        $this->DB9 = $this->load->database('other9_db', TRUE);
        $this->DB11 = $this->load->database('other11_db', TRUE);
        $this->DB12 = $this->load->database('other12_db', TRUE);
    }
    public function insertar_cliente($dato){
        $this->db->insert('clientes',$dato);   
        return $this->db->insert_id();
    } 
    public function update_cliente($data, $id) {
        $this->db->set($data);
        $this->db->where('id', $id);
        return $this->db->update('clientes');
    }
    /*
    public function getListadoClientes(){
        $sql = "SELECT
                c.id,
                c.empresa,
                c.email,
                c.estado,
                c.municipio,
                c.giro,
                c.direccion,
                c.observaciones,
                p.nombre as 'vendedor'
                FROM clientes as c
                INNER JOIN usuarios AS u ON c.idUsuario = u.UsuarioID
                INNER JOIN personal AS p ON u.personalId = p.personalId
                WHERE c.tipo=1 AND c.estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getListadoClientesPorUsuario($idUsuario){
        $sql = "SELECT
                c.id,
                c.empresa,
                c.email,
                c.estado,
                c.municipio,
                c.giro,
                c.direccion,
                c.observaciones,
                p.nombre as 'vendedor'
                FROM
                clientes AS c
                INNER JOIN usuarios AS u ON c.idUsuario = u.UsuarioID
                INNER JOIN personal AS p ON u.personalId = p.personalId
                WHERE
                c.tipo = 1 AND
                c.estatus = 1 AND
                c.idUsuario = ".$idUsuario;
        $query = $this->db->query($sql);
        return $query->result();
    }*/
    //==============================================
    function getListadoClientesPorUsuario_asic($params){
        //$idUsuario=$params['idUsuario'];
        $idUsuario=0;
        $columns = array( 
                0=>'c.id',
                1=>'c.empresa',
                2=>'c.email',
                3=>'c.estado',
                4=>'c.municipio',
                5=>'c.giro',
                6=>'c.direccion',
                7=>'c.observaciones',
                8=>'p.nombre',
                9=>'c.bloqueo',
                //10=>'df.razon_social',
                //11=>'GROUP_CONCAT(df.razon_social) as razones'
        );
        $columns_c = array( 
                0=>'c.id',
                1=>'c.empresa',
                2=>'c.email',
                3=>'c.estado',
                4=>'c.municipio',
                5=>'c.giro',
                6=>'c.direccion',
                7=>'c.observaciones',
                8=>'p.nombre',
                9=>'c.bloqueo',
                //10=>'df.razon_social',

        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->DB5->select($select);
        $this->DB5->from('clientes AS c');
        $this->DB5->join('usuarios u', 'c.idUsuario = u.UsuarioID','left');
        $this->DB5->join('personal p', 'u.personalId = p.personalId','left');
        //$this->DB5->join('cliente_has_datos_fiscales df', 'df.idCliente = c.id and df.activo=1','left');
            
            $this->DB5->where('c.tipo',1);
            $this->DB5->where('c.estatus',1);
            //$this->DB5->where('c.viewsistem <=',1);
            if ($idUsuario>1) {
                $this->DB5->where('c.idUsuario',$idUsuario);
            }
            
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB5->group_start();
            foreach($columns_c as $c){
                $this->DB5->or_like($c,$search);
            }
            $this->DB5->group_end();  
        }            
        $this->DB5->group_by("c.id");
        $this->DB5->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->DB5->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB5->get();
        // print_r($query); die;
        return $query;
    }
    function getListadoClientesPorUsuario_asict($params){
        //$idUsuario=$params['idUsuario'];
        $idUsuario=0;
        $columns = array( 
                0=>'c.id',
                1=>'c.empresa',
                2=>'c.email',
                3=>'c.estado',
                4=>'c.municipio',
                5=>'c.giro',
                6=>'c.direccion',
                7=>'c.observaciones',
                8=>'p.nombre',
                //9=>'df.razon_social',
        );
        $this->DB5->select('*');
        $this->DB5->from('clientes AS c');
        $this->DB5->join('usuarios u', 'c.idUsuario = u.UsuarioID','left');
        $this->DB5->join('personal p', 'u.personalId = p.personalId','left');
        //$this->DB5->join('cliente_has_datos_fiscales df', 'df.idCliente = c.id and df.activo=1','left');
            
            $this->DB5->where('c.tipo',1);
            $this->DB5->where('c.estatus',1);
            if ($idUsuario>1) {
                $this->DB5->where('c.idUsuario',$idUsuario);
            }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->DB5->group_start();
            foreach($columns as $c){
                $this->DB5->or_like($c,$search);
            }
            $this->DB5->group_end();  
        }            
        $this->DB5->group_by("c.id");
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->DB5->get();
        // print_r($query); die;
        return $query->num_rows();
    }
    //==============================================
    public function getClientePorId($idCliente){
        $sql = "SELECT * FROM clientes
                WHERE id=".$idCliente;
        $query = $this->DB11->query($sql);
        return $query->row();
    }
    public function getClientedirecPorId($idCliente){
        $sql = "SELECT * FROM clientes_direccion
                WHERE status = 1 AND idcliente=".$idCliente;
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function getClienteContactoPorId($id=0)    {   
        $sql = "SELECT * FROM cliente_has_persona_contacto
                WHERE idCliente = ".$id." LIMIT 1";
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function insertarToCatalogo($data, $catalogo) {
        return $this->DB5->insert('' . $catalogo, $data);
    }
    public function getListadoTel_local($idCliente){
        $sql = "SELECT * FROM cliente_has_telefono WHERE idCliente=".$idCliente;
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function getListadoTel_celular($idCliente){
        $sql = "SELECT * FROM cliente_has_celular WHERE idCliente=".$idCliente;
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function getListadoPersona_contacto($idCliente){
        $sql = "SELECT * FROM cliente_has_persona_contacto WHERE idCliente=".$idCliente;
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function eliminar_tel_local($id) {
        $this->db->where('id', $id);
        return $this->db->delete('cliente_has_telefono');
    }
    public function eliminar_tel_celular($id) {
        $this->db->where('id', $id);
        return $this->db->delete('cliente_has_celular');
    }
    public function eliminar_persona_contacto($id) {
        $this->db->where('id', $id);
        return $this->db->delete('cliente_has_persona_contacto');
    }
    public function getListadoDatosFiscales($idCliente)    {
        $where="";
        //if($idCliente!=0){
            $where = "AND idCliente=".$idCliente;
        //}
        $sql = "SELECT * FROM cliente_has_datos_fiscales WHERE activo=1 ".$where;
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function getListadoDatosFiscalesprefactura($id)    {
        $sql = "SELECT 
                    clidf.*,
                    cli.empresa
                FROM 
                cliente_has_datos_fiscales as clidf
                inner join clientes as cli on cli.id=clidf.idCliente
                WHERE clidf.id=$id";
        $query = $this->DB5->query($sql);
        return $query->result();
    }
    public function getDataFamilia(){
        $sql = "SELECT * FROM familia WHERE status=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function getDataEquipo($id){
        $sql = "SELECT * FROM equipos WHERE idfamilia=$id";
        $query = $this->DB5->query($sql);
        return $query->result();
    } 
    public function getDataEquipo2(){
        $sql = "SELECT eq.*,
                (
                    SELECT count(*) as total
                    FROM series_productos as eqs 
                    WHERE 
                    eqs.productoid=eq.id and 
                    eqs.status<=1 and 
                    eqs.activo=1 and 
                    eqs.bodegaId!=9 and eqs.resguardar_cant=0
                ) as stockequipo
                FROM equipos as eq 
                WHERE eq.estatus=1";
        $query = $this->DB12->query($sql);
        return $query->result();
    } 
    function consultarporbodega($idequipo){
        $sql="SELECT bod.bodega,count(*) as total FROM series_productos as eqs INNER JOIN bodegas as bod on bod.bodegaId=eqs.bodegaId WHERE eqs.productoid=$idequipo and eqs.status<=1 and eqs.activo=1 and eqs.resguardar_cant=0 GROUP BY bod.bodegaId";
        $query = $this->db->query($sql);
        return $query;
    }
    function getexistenciaequipos($equipo,$bodega){
        $sql="SELECT count(*) as total
              FROM series_productos as eqs 
              WHERE 
              eqs.productoid='$equipo' and 
              eqs.bodegaId='$bodega' and 
              eqs.status<=1 and 
              eqs.activo=1";
        $query = $this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
            
        }
        return $total;
    }


    /*
    public function eliminar_datos_fiscales($id) {
        $this->db->where('id', $id);
        return $this->db->delete('cliente_has_datos_fiscales');
    }
    */
}

?>