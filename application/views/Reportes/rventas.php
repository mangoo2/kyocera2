<!-- START CONTENT -->
<section id="content">
  <!--start container-->
  <div class="container">
    <div class="section">
      <input id="base_url" type="hidden" name="base_url" value="<?php echo base_url(); ?>">
      
        <h4 class="header">Reporte Ventas</h4>
        <div class="row">
          <div class="col s12">
            <div class="card-panel">
              <form id="form_reporte">
                <div class="row">
                  <div class="col s2">
                    <label>Tipo fecha</label><select class="form-control-bmz browser-default" id="tipof"><option value="0">Fecha generada</option><option value="1">Fecha pago</option></select>
                  </div>
                  <div class="col s2">
                    <label>Cliente</label><select class="form-control-bmz browser-default" id="idcliente" onchange="recarcular()"></select>
                  </div>
                  <div class="col s2" style="padding: 0 6px;">
                    <label>Fecha inicial</label>
                    <input type="date" id="fechainicio" name="fechainicio" class="form-control-bmz" min="<?php echo date("Y-m-d",strtotime(date('Y-m-d')."- 1 month")); ?>" max="<?php echo date('Y-m-d')?>" onchange="recarcular()" required>
                  </div>
                  <div class="col s2" style="padding: 0 6px;">
                    <label>Fecha final</label>
                    <input type="date" id="fechafinal" name="fechafinal" class="form-control-bmz" value="<?php echo date('Y-m-d')?>" max="<?php echo date('Y-m-d')?>" onchange="recarcular()" required>
                  </div>
                  
                  <div class="col s2">
                    <label>Vendedor</label><select class="form-control-bmz browser-default" id="idvendedor"><option value="0">Todos</option><?php foreach ($rowpersonal->result() as $item) { echo '<option value="'.$item->personalId.'">'.$item->nombre.' '.$item->apellido_paterno.' '.$item->apellido_materno.'</option>';  } ?></select>
                  </div>
                  <div class="col s2">
                    <label>Tipo</label><select class="form-control-bmz browser-default" id="tipovp"><option value="0">Todos</option><option value="1">Venta</option><option value="2">Venta Combinada</option><option value="3">Poliza</option></select>
                  </div>
                </div>
              </form>
              <div class="row">
                <div class="col s2"><button class="btn-bmz cyan waves-effect waves-light" type="button" onclick="generarreporte()">Generar</button></div>
              </div>
            </div>
          </div>
          
        </div>
        
      
    </div>
  </div>
</section>