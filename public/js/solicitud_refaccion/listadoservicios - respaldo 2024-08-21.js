var base_url = $('#base_url').val();
var table;
var editarar=0;
var asignacionId;
var asignacionIde;
var asignaciontipo;
var asigcliente;
var formulario_cotizacion;
var perfilid = $('#perfilid').val();
var serieselected='';
var equiposelected=0;
var tipocot=0;
$(document).ready(function($) {
    $('.modal').modal({
                dismissible: true
            });
	$('.chosen-select').chosen({width: "100%"});
	$('#pcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
          load();
    });	
    $('#idclienteser').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.empresa

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        obtenerservicios(data.id);
        $('.infordelservicio').html('');
    });
    table = $('#tabla_asignar').DataTable();
    load();
    $('.savemodalventa').click(function(event) {
        if (editarar==1) {
            $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Está seguro de Cancelar?<br>Se necesita permisos de administrador<br>'+
                     '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var pass=$('#contrasena').val();
                        if (pass!='') {
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Sistema/solicitarpermiso",
                                data: {
                                    pass:pass
                                },
                                success: function (response){
                                        var respuesta = parseInt(response);
                                        if (respuesta==1) {
                                            //===============
                                                savemodalventa();
                                            //=============
                                        }else{
                                            notificacion(2);
                                        }
                                },
                                error: function(response){
                                    notificacion(1);  
                                     
                                }
                            });
                                        
                        }else{ 
                            notificacion(0);
                        }
                },
                    cancelar: function () {
                    }
                }
            });
        }else{
           savemodalventa(); 
        }
    });
    $('.cancelarmodalventa').click(function(event) {
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            icon: 'fa fa-warning',
            title: 'Atención!',
            content: '¿Está seguro de Cancelar?<br>Se necesita permisos de administrador<br>'+
                 '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/>',
            type: 'red',
            typeAnimated: true,
            buttons: 
            {
                confirmar: function (){
                    var pass=$('#contrasena').val();
                    if (pass!='') {
                        $.ajax({
                            type:'POST',
                            url: base_url+"index.php/Sistema/solicitarpermiso",
                            data: {
                                pass:pass
                            },
                            success: function (response){
                                    var respuesta = parseInt(response);
                                    if (respuesta==1) {
                                        //===============
                                            $.ajax({
                                                  type:'POST',
                                                  url: base_url+'index.php/Solicitud_Refacciones/cancelartotalvs',
                                                  data: {
                                                    tasign:$('#vmidasignacion').val(),
                                                    },
                                                  success:function(data){
                                                      $.alert({
                                                        boxWidth: '30%',
                                                        useBootstrap: false,
                                                        title: 'Atención!',
                                                        content: 'Cancelacion correcta'}
                                                      );
                                                      $('#modalventa').modal('close');
                                                  }
                                              });
                                        //=============
                                    }else{
                                        notificacion(2);
                                    }
                            },
                            error: function(response){
                                notificacion(1);  
                                 
                            }
                        });
                                    
                    }else{
                        notificacion(0);
                    }
            },
                cancelar: function (){
                }
            }
        });
    });
    $('#botonAgregarRefaccion').on('click', function () { 
        agregarRefaccion();
    });
    $('#botonEliminarRefaccion').on('click', function () { 
        eliminarRefaccion();
    });
    $('.savemodalcotizacion').on('click', function ()     {   
        /*
        formulario_cotizacion = $('#formulario_cotizacion');    
        // Validamos que haya elegido que va a cotizar
        if($('#eleccionCotizar').val()=='' || $('#eleccionCotizar').val()==null){
            alertfunction('Atención!','Debe seleccionar que desea Cotizar');  
        }
        // Tipo Refacción
        else if($('#eleccionCotizar').val()==3){
            var validado = validaciones(3);
            if(validado == 1) 
            {
                var equipoListaRefacciones = [];
                $(".equipoListaRefacciones").each(function() {
                    equipoListaRefacciones.push($(this).val()); 
                });
                var seriee = [];
                $(".seriee").each(function() {
                    seriee.push($(this).val()); 
                });
                var refaccion = [];
                $(".refaccion").each(function() {
                    refaccion.push($(this).val()); 
                });

                var piezasRefacion = [];
                $(".piezasRefacion").each(function() {
                    piezasRefacion.push($(this).val()); 
                });
                var refaccionesp =[]
                $(".refaccionesp").each(function() {
                    refaccionesp.push($(this).val()); 
                });
                var datos = formulario_cotizacion.serialize()+'&arrayEquipos='+equipoListaRefacciones+'&seriee='+seriee+'&arrayRefaccion='+refaccion+'&arrayPiezas='+piezasRefacion+'&arrayrprecio='+refaccionesp;
                creaCotizacion(datos);
            }
        }
        */
        btn_cotizar2();
    });
    $('#ventap').change(function(event) {
        console.log('ventap');
        setTimeout(function(){ 
            carga_series_modelo_coti()
        }, 1000);
        setTimeout(function(){ 
            carga_series_modelo_coti()
        }, 2000);
    });
    $('#saveactualizacioncot').click(function(event) {
        var trser = [];
        $("#table_ser_cot tbody > tr").each(function() {
                //var aux = $(this).attr("id").split('_');
                itemp={};
                itemp['ser']=$(this).find("input[id*='ser']").val();
                itemp['serve']=$(this).find("input[id*='serve']").val(); 
                itemp['tipo']=$(this).find("input[id*='tipo']").val();  

                itemp['cot_refaccion']=$(this).find("textarea[id*='cot_refaccion']").val(); 
                itemp['cot_refaccion2']=$(this).find("textarea[id*='cot_refaccion2']").val(); 
                itemp['cot_refaccion3']=$(this).find("textarea[id*='cot_refaccion3']").val(); 
                itemp['prioridad']=$(this).find('.prioridadr:checked').val()                
                trser.push(itemp);
            
        });
        INFOp  = new FormData();
        trserdata   = JSON.stringify(trser);

        $.ajax({
            type:'POST',
            url: base_url+"index.php/Solicitud_Refacciones/saveactualizacioncot",
            data: {
                datos:trserdata
            },
            success: function (response){
                toastr.success('Actualizado correctamente');
                $('#modalinfoequiposser').modal('close');
                // Recargamos la pÃ¡gina
                
                setTimeout(function(){ 
                    load();
                }, 2000); 
            },
            error: function(response){
                notificacion(1); 
            }
        });
    });
});
function carga_series_modelo_coti(){
    $('#polizaserie_1').val(serieselected);
    $('#polizaserie_2').val(serieselected);
    $('#polizaserie_3').val(serieselected);
    $('#polizaserie_4').val(serieselected);
    $('#polizaserie_5').val(serieselected);
    console.log('equiposelected: '+equiposelected);
    if(equiposelected>0){
        $('.equipoListaselected').val(equiposelected).trigger("chosen:updated").change();
    }
}

function load(){
	var serv = $('#serv option:selected').val();
	if (serv==1) {
		loadcontrato();
	}
    if(serv==2){
		loadpoliza();
	}
    if(serv==3){
        loadcliente();
    }
    if(serv==4){
        loadventa();
    }
    if(serv==0){
        loadtodo();
    }
}
function loadtodo(){
    table.destroy();
    var serv = $('#serv option:selected').val();
    var tiposer = $('#tiposer option:selected').val();
    var tecnico = $('#tecnico option:selected').val();
    var status = $('#status option:selected').val();
    var zona = $('#zona option:selected').val();
    var pcliente = $('#pcliente option:selected').val();
    var fechainicial = $('#fechainicial').val();
    var fechafinal = $('#fechafinal').val();
        if(pcliente==undefined){
            pcliente=0;
        }
    //var fechainicio = $('#fechainicio').val();
    //var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Solicitud_Refacciones/getlistaserviciotodos",
            type: "post",
            "data": {
                "tiposer":tiposer,
                "tecnico":tecnico,
                "status":status,
                "zona":zona,
                "pcliente":pcliente,
                "fechainicio":fechainicial,
                "fechafin":fechafinal,
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionIde",
                render: function(data,type,row){ 
                        if(row.cpev==1){//contrato
                            html=row.asignacionIde;
                        }
                        if(row.cpev==2){//poliza
                            html=row.asignacionIde;
                        }
                        if(row.cpev==3){//cliente
                            html=row.asignacionId;
                        }
                        if(row.cpev==4){//venta
                            html=row.asignacionId;
                        }
                    return html;
                }
            },
            {"data": "servicio"},
            {"data": "tecnico",
                render: function(data,type,row){
                    var html='<!--'+row.per_fin+'-->';
                        html+=row.tecnico+'<!--'+row.tecnico2+'-->';
                    if(row.tecnico2!=undefined){
                        html+='<br>'+row.tecnico2;
                    }

                    if(row.per_fin>0){
                        html=row.tecnicof
                    }
                return html;
                }
            },
            {"data": "empresa"},
            {"data": "fecha"},
            
            {"data": "modelo"},
            {"data": "serie"},
            {"data": "cot_refaccion",
                render: function(data,type,row){
                    var html='';
                    if(row.cot_refaccion!='' && row.cot_refaccion!=null){
                        if(row.cot_tipo!='' && row.cot_tipo!=null){
                            html+='<div class="tiporefaccion">'+row.cot_tipo+'</div>';    
                        }else{
                            html+='<div class="tiporefaccion">POR DESGASTE</div>';
                        }
                        html+='<div>'+row.cot_refaccion+'</div>';
                    }
                    if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                        
                        html+='<div class="tiporefaccion">POR DAÑO</div>';
                        
                        html+='<div>'+row.cot_refaccion2+'</div>';
                    }
                    if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                        
                        html+='<div class="tiporefaccion">POR GARANTIA</div>';
                        
                        html+='<div>'+row.cot_refaccion3+'</div>';
                    }
                    if(parseInt(row.cot_contador)>0){
                        html+='<div>Contador: '+row.cot_contador+'</div>';
                    }
                    if (row.cot_status==1){
                        html+='<span class="b-btn b-btn-primary btn_edit_i" onclick="info_edit('+row.asignacionId+','+row.cpev+',0)"><i class="fas fa-pencil-alt"></i></span>';
                    }

                    return html;
                }
            },
            {"data": "cot_tipo",
                render: function(data,type,row){
                    var html='';
                    if(row.cpev==4){
                        if(row.prioridadr>0){
                            html+='Prioridad '+row.prioridadr+' ';
                        }
                    }else{
                        if(row.prioridad>0){
                            html+='Prioridad '+row.prioridad+' ';
                        }
                    }
                        
                        //html+=row.cot_tipo;
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    if (row.cot_status==1){
                        html='<div id="card-alert" class="card red">\
                                    <div class="card-content white-text" align="center">\
                                    <p>Por Cotizar</p>\
                                </div>\
                                </div>';
                    }else if (row.cot_status==2){
                        html='<div id="card-alert" class="card green">\
                                    <div class="card-content white-text">\
                                    <p>Cotizado</p>\
                                </div></div>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                        html='';
                        if(row.priorizar_cot>0){
                            var checked='checked';
                        }else{
                            var checked='';
                        }
                        if (row.cot_status==2){
                            var checked_d='';
                            if(row.cot_referencia>0){
                            }else{
                                var checked_d='disabled';
                            }
                        }else{
                            var checked_d='disabled';
                        }
                        if(perfilid==12){
                            var checked_d='disabled';
                        }
                        var id_row=0;
                        if(row.cpev==1){//cotizacion
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev==2){//poliza
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev==3){//cliente
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev==4){//venta
                            id_row=row.asignacionId;
                        }
                        html+='<input type="checkbox" class="filled-in priorizar_cot_'+row.cpev+'_'+id_row+'" id="priorizar_cot_'+row.cpev+'_'+id_row+'" '+checked+' '+checked_d+' onclick="f_priorizar_cot('+row.cpev+','+id_row+','+row.cot_referencia+')">\
                        <label for="priorizar_cot_'+row.cpev+'_'+id_row+'" ></label>';
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    //==============
                            var tipocotizacion='';
                        if(row.cot_refaccion!='' && row.cot_refaccion!=null){
                            tipocotizacion+='1';
                        }
                        if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                            tipocotizacion+='2';
                        }
                        if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                            tipocotizacion+='3';
                        }
                    //==============
                    var prioridad=0;
                    if(row.cpev==4){
                        if(row.prioridadr>0){
                            prioridad=row.prioridadr;
                        }
                    }else{
                        if(row.prioridad>0){
                            prioridad=row.prioridad;
                        }
                    }
                    if(row.cpev==1){//contratos
                        if (row.cot_status==1){
                            html+='<a class="waves-effect cyan btn-bmz asignacionIde_'+row.asignacionIde+' ser_'+row.asignacionId+'_'+row.asignacionIde+'_1_'+row.id+' equipo_con equipo_con_'+row.idrow+'_'+row.serieId+'" \
                                    onclick="cotizar('+row.asignacionId+','+row.asignacionIde+',1,'+row.id+','+prioridad+')" \
                                    data-equipo="'+row.idEquipo+'"\
                                    data-serie="'+row.serie+'"  \
                                    data-serieid="'+row.serieId+'"  \
                                    data-idrow="'+row.idrow+'"  \
                                    data-tipocot="'+tipocotizacion+'"  \
                                    data-direc="" >\
                                        <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                        }
                        if (row.cot_status==2){
                            if(row.cot_referencia>0){
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/documento/'+row.cot_referencia+'?tipocot='+tipocotizacion+'" target="_blank">\
                                        <i class="fas fa-eye" style="font-size: 20px;"></i></a>';
                            }else{
                            html+='<a class="waves-effect cyan btn-bmz" data-obser="'+row.cot_status_ob+'" onclick="viewcoment($(this))"><i class="fas fa-comment-alt" style="font-size: 20px;"></i></a>';
                        }
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+row.id+'" target="_blank">\
                                        <i class="fas fa-link" style="font-size: 20px;"></i></a>';

                           
                            if (row.cot_referencia_tipo==null) {
                                /*
                                html+='<a class="waves-effect cyan btn-bmz" onclick="autorizar('+row.cot_referencia+','+row.asignacionId+','+row.asignacionIde+',1)">\
                                        <i class="fas fa-check-square" style="font-size: 20px;"></i></a>';
                                */
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz" onclick="detalle('+row.cot_referencia_tipo+',0)">\
                                        <i class="fas fa-file-powerpoint" style="font-size: 20px;"></i></a>';

                            }   
                        }  
                    }
                    if(row.cpev==2){ //poliza
                        if (row.cot_status==1){
                            html+='<a class="waves-effect cyan btn-bmz asignacionIde_'+row.asignacionIde+' ser ser_'+row.asignacionId+'_'+row.asignacionIde+'_2_'+row.id+' " \
                                    onclick="cotizar('+row.asignacionId+','+row.asignacionIde+',2,'+row.id+','+prioridad+')"\
                                    data-equipo="'+row.idEquipo+'"\
                                    data-serie="'+row.serie+'"\
                                    data-direc="'+row.direccionservicio+'"\
                                    data-tipocot="'+tipocotizacion+'"  \
                                    >\
                                        <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                        }
                        if (row.cot_status==2){
                            if(row.cot_referencia>0){
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/documento/'+row.cot_referencia+'?tipocot='+tipocotizacion+'" target="_blank">\
                                        <i class="fas fa-eye" style="font-size: 20px;"></i></a>';
                            }else{
                            html+='<a class="waves-effect cyan btn-bmz" data-obser="'+row.cot_status_ob+'" onclick="viewcoment($(this))"><i class="fas fa-comment-alt" style="font-size: 20px;"></i></a>';
                        }
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+row.id+'" target="_blank">\
                                        <i class="fas fa-link" style="font-size: 20px;"></i></a>';
                            if (row.cot_referencia_tipo==null) {
                                /*
                                html+='<a class="waves-effect cyan btn-bmz" onclick="autorizar('+row.cot_referencia+','+row.asignacionId+','+row.asignacionIde+',2)">\
                                        <i class="fas fa-check-square" style="font-size: 20px;"></i></a>';
                                */
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz" onclick="detalle('+row.cot_referencia_tipo+',0)">\
                                        <i class="fas fa-file-powerpoint" style="font-size: 20px;"></i></a>';
                            }
                            
                            
                        }
                    }
                    if(row.cpev==3){ //cliente
                        if (row.cot_status==1){
                            html+='<a class="waves-effect cyan btn-bmz asignacionIde_'+row.asignacionIde+' ser_'+row.asignacionId+'_'+row.asignacionIde+'_3_'+row.id+'" \
                                    onclick="cotizar('+row.asignacionId+','+row.asignacionIde+',3,'+row.id+','+prioridad+')"\
                                    data-equipo="'+row.idEquipo+'"\
                                    data-serie="'+row.serie+'" \
                                    data-direc="'+row.direccionservicio+'" \
                                    data-tipocot="'+tipocotizacion+'"  \
                                    >\
                                        <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                        }
                        if (row.cot_status==2){
                            if(row.cot_referencia>0){
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/documento/'+row.cot_referencia+'?tipocot='+tipocotizacion+'" target="_blank">\
                                        <i class="fas fa-eye" style="font-size: 20px;"></i></a>';
                            }else{
                            html+='<a class="waves-effect cyan btn-bmz" data-obser="'+row.cot_status_ob+'" onclick="viewcoment($(this))"><i class="fas fa-comment-alt" style="font-size: 20px;"></i></a>';
                        }
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+row.id+'" target="_blank">\
                                        <i class="fas fa-link" style="font-size: 20px;"></i></a>';
                            if (row.cot_referencia_tipo==null) {
                                /*
                                html+='<a class="waves-effect cyan btn-bmz" onclick="autorizar('+row.cot_referencia+','+row.asignacionId+',0,3)">\
                                        <i class="fas fa-check-square" style="font-size: 20px;"></i></a>';
                                */
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz" onclick="detalle('+row.cot_referencia_tipo+',0)">\
                                        <i class="fas fa-file-powerpoint" style="font-size: 20px;"></i></a>';
                            }
                            
                        }
                    }
                    if(row.cpev==4){ //venta
                        if (row.cot_status==1){
                            html+='<a class="waves-effect cyan btn-bmz asignacionIde_'+row.asignacionId+'" \
                                    onclick="cotizar('+row.asignacionId+',0,4,'+row.id+','+prioridad+')" \
                                    data-equipo="'+row.idEquipo+'"\
                                    data-serie="'+row.serie+'" \
                                    data-tipocot="'+tipocotizacion+'"  \
                                    >\
                                        <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                        }
                        if (row.cot_status==2){
                            if(row.cot_referencia>0){
                                html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/documento/'+row.cot_referencia+'?tipocot='+tipocotizacion+'" target="_blank">\
                                        <i class="fas fa-eye" style="font-size: 20px;"></i></a>';
                            }else{
                            html+='<a class="waves-effect cyan btn-bmz" data-obser="'+row.cot_status_ob+'" onclick="viewcoment($(this))"><i class="fas fa-comment-alt" style="font-size: 20px;"></i></a>';
                        }
                            
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+row.id+'" target="_blank">\
                                        <i class="fas fa-link" style="font-size: 20px;"></i></a>';
                            if (row.cot_referencia_tipo==null) {
                                /*
                                html+='<a class="waves-effect cyan btn-bmz" onclick="autorizar('+row.cot_referencia+','+row.asignacionId+',0,3)">\
                                        <i class="fas fa-check-square" style="font-size: 20px;"></i></a>';
                                */
                            }else{
                                html+='<a class="waves-effect cyan btn-bmz" onclick="detalle('+row.cot_referencia_tipo+',0)">\
                                        <i class="fas fa-file-powerpoint" style="font-size: 20px;"></i></a>';
                            }
                            
                        }
                    }
                    if (row.cot_status==1){
                        if(row.cpev==1){//contrato
                            var asigid=row.asignacionIde;
                        }
                        if(row.cpev==2){//poliza
                            var asigid=row.asignacionIde;
                        }
                        if(row.cpev==3){//cliente
                            var asigid=row.asignacionIdd;
                        }
                        if(row.cpev==4){//venta
                            var asigid=row.asignacionId;
                        }

                        html+='<a class="waves-effect cyan btn-bmz" \
                                    onclick="cambiarestatus('+asigid+','+row.cpev+')" title="Cambiar Status a cotizado">\
                                        <i class="fas fa-clipboard-check" style="font-size: 20px;"></i></a>';
                    }
                    if(perfilid==12){
                            var html='';
                    }
                             
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
    }).on('draw',function(){
        verifircar_dir_con();
    });
}
function loadcontrato(){
    table.destroy();
    var serv = $('#serv option:selected').val();
    var tiposer = $('#tiposer option:selected').val();
	var tecnico = $('#tecnico option:selected').val();
	var status = $('#status option:selected').val();
	var zona = $('#zona option:selected').val();
	var pcliente = $('#pcliente option:selected').val();
    var fechainicial = $('#fechainicial').val();
    var fechafinal = $('#fechafinal').val();
		if(pcliente==undefined){
		    pcliente=0;
		}
    //var fechainicio = $('#fechainicio').val();
    //var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Solicitud_Refacciones/getlistaservicioc",
            type: "post",
            "data": {
                "tiposer":tiposer,
				"tecnico":tecnico,
				"status":status,
				"zona":zona,
				"pcliente":pcliente,
                "fechainicio":fechainicial,
                "fechafin":fechafinal,
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionIde"},
            {"data": "servicio"},
            {"data": "tecnico",
                render: function(data,type,row){
                    var html=row.tecnico;
                    if(row.tecnico2!=undefined){
                        html+='<br>'+row.tecnico2;
                    }
                    if(row.per_fin>0){
                        html=row.tecnicof
                    }
                return html;
                }
            },
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": "cot_refaccion",
                render: function(data,type,row){
                    var html='';
                    if(row.cot_refaccion!='' && row.cot_refaccion!=null){
                        if(row.cot_tipo!='' && row.cot_tipo!=null){
                            html+='<div class="tiporefaccion">'+row.cot_tipo+'</div>';    
                        }else{
                            html+='<div class="tiporefaccion">POR DESGASTE</div>';
                        }
                        html+='<div>'+row.cot_refaccion+'</div>';
                    }
                    if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                        
                        html+='<div class="tiporefaccion">POR DAÑO</div>';
                        
                        html+='<div>'+row.cot_refaccion2+'</div>';
                    }
                    if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                        
                        html+='<div class="tiporefaccion">POR GARANTIA</div>';
                        
                        html+='<div>'+row.cot_refaccion3+'</div>';
                    }
                    if(parseInt(row.cot_contador)>0){
                        html+='<div>Contador: '+row.cot_contador+'</div>';
                    }
                    if (row.cot_status==1){
                        html+='<span class="b-btn b-btn-primary btn_edit_i" onclick="info_edit('+row.asignacionId+',1,0)"><i class="fas fa-pencil-alt"></i></span>';
                    }
                    return html;
                }
            },
            {"data": "cot_tipo",
                render: function(data,type,row){
                    var html='';
                        if(row.prioridad>0){
                            html+='Prioridad '+row.prioridad+' ';
                        }
                        //html+=row.cot_tipo;
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    if (row.cot_status==1){
                        html='<div id="card-alert" class="card red">\
                                    <div class="card-content white-text" align="center">\
                                    <p>Por Cotizar</p>\
                                </div>\
                                </div>';
                    }else if (row.cot_status==2){
                        html='<div id="card-alert" class="card green">\
                                    <div class="card-content white-text">\
                                    <p>Cotizado</p>\
                                </div></div>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                        if(row.priorizar_cot>0){
                            var checked='checked';
                        }else{
                            var checked='';
                        }
                        if (row.cot_status==2){
                            var checked_d='';
                            if(row.cot_referencia>0){
                            }else{
                                var checked_d='disabled';
                            }
                        }else{
                            var checked_d='disabled';
                        }
                        if(perfilid==12){
                            var checked_d='disabled';
                        }
                        /*
                        var id_row=0;
                        if(row.cpev=1){
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev=2){
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev=3){
                            id_row=row.asignacionId;
                        }
                        if(row.cpev=4){
                            id_row=row.asignacionId;
                        }
                        */
                        html='<input type="checkbox" class="filled-in priorizar_cot_1_'+row.asignacionIde+'" id="priorizar_cot_1_'+row.asignacionIde+'" '+checked+' '+checked_d+' onclick="f_priorizar_cot(1,'+row.asignacionIde+','+row.cot_referencia+')">\
                        <label for="priorizar_cot_1_'+row.asignacionIde+'" ></label>';
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    //==============
                            var tipocotizacion='';
                        if(row.cot_refaccion!='' && row.cot_refaccion!=null){
                            tipocotizacion+='1';
                        }
                        if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                            tipocotizacion+='2';
                        }
                        if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                            tipocotizacion+='3';
                        }
                    //==============
                    if (row.cot_status==1){
                        html+='<a class="waves-effect cyan btn-bmz asignacionIde_'+row.asignacionIde+' ser_'+row.asignacionId+'_'+row.asignacionIde+'_1_'+row.id+' equipo_con equipo_con_'+row.idrow+'_'+row.serieId+'" \
                                    onclick="cotizar('+row.asignacionId+','+row.asignacionIde+',1,'+row.id+','+row.prioridad+')" \
                                    data-equipo="'+row.idEquipo+'"\
                                    data-serie="'+row.serie+'" \
                                    data-serieid="'+row.serieId+'" \
                                    data-idrow="'+row.idrow+'"  \
                                    data-tipocot="'+tipocotizacion+'"  \
                                    data-direc="" >\
                                        <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                    }
                    if (row.cot_status==2){
                        if(row.cot_referencia>0){
                        html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/documento/'+row.cot_referencia+'?tipocot='+tipocotizacion+'" target="_blank">\
                                    <i class="fas fa-eye" style="font-size: 20px;"></i></a>';
                        }else{
                            html+='<a class="waves-effect cyan btn-bmz" data-obser="'+row.cot_status_ob+'" onclick="viewcoment($(this))"><i class="fas fa-comment-alt" style="font-size: 20px;"></i></a>';
                        }
                        html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+row.id+'" target="_blank">\
                                    <i class="fas fa-link" style="font-size: 20px;"></i></a>';

                       
                        if (row.cot_referencia_tipo==null) {
                            /*
                            html+='<a class="waves-effect cyan btn-bmz" onclick="autorizar('+row.cot_referencia+','+row.asignacionId+','+row.asignacionIde+',1)">\
                                    <i class="fas fa-check-square" style="font-size: 20px;"></i></a>';
                            */
                        }else{
                            html+='<a class="waves-effect cyan btn-bmz" onclick="detalle('+row.cot_referencia_tipo+',0)">\
                                    <i class="fas fa-file-powerpoint" style="font-size: 20px;"></i></a>';

                        }   
                    }   
                    if (row.cot_status==1){
                        var asigid=row.asignacionIde;
                    
                        html+='<a class="waves-effect cyan btn-bmz " \
                                    onclick="cambiarestatus('+asigid+',1)" title="Cambiar Status a cotizado">\
                                        <i class="fas fa-clipboard-check" style="font-size: 20px;"></i></a>';
                    }
                    if(perfilid==12){
                        var html='';
                    }      
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
    });
}
function loadpoliza(){
    table.destroy();
    var serv = $('#serv option:selected').val();
    var tiposer = $('#tiposer option:selected').val();
	var tecnico = $('#tecnico option:selected').val();
	var status = $('#status option:selected').val();
	var zona = $('#zona option:selected').val();
	var pcliente = $('#pcliente option:selected').val();
    var fechainicial = $('#fechainicial').val();
    var fechafinal = $('#fechafinal').val();
		if(pcliente==undefined){
		    pcliente=0;
		}
	//var fechainicio = $('#fechainicio').val();
    //var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Solicitud_Refacciones/getlistaserviciop",
            type: "post",
            "data": {
                "tiposer":tiposer,
				"tecnico":tecnico,
				"status":status,
				"zona":zona,
				"pcliente":pcliente,
                "fechainicio":fechainicial,
                "fechafin":fechafinal,
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionIde"},
            {"data": "servicio"},
            {"data": "tecnico",
                render: function(data,type,row){
                    var html=row.tecnico+'<!--'+row.tecnico2+'-->';
                    if(row.tecnico2!=undefined){
                        html+='<br>'+row.tecnico2;
                    }
                    if(row.per_fin>0){
                        html=row.tecnicof
                    }
                        html+='<!--'+row.per_fin+'-->';
                return html;
                }
            },
            {"data": "empresa"},
            {"data": "fecha"},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": "cot_refaccion",
                render: function(data,type,row){
                    var html='';
                    if(row.cot_refaccion!='' && row.cot_refaccion!=null){
                        if(row.cot_tipo!='' && row.cot_tipo!=null){
                            html+='<div class="tiporefaccion">'+row.cot_tipo+'</div>';    
                        }else{
                            html+='<div class="tiporefaccion">POR DESGASTE</div>';
                        }
                        html+='<div>'+row.cot_refaccion+'</div>';
                    }
                    if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                        
                        html+='<div class="tiporefaccion">POR DAÑO</div>';
                        
                        html+='<div>'+row.cot_refaccion2+'</div>';
                    }
                    if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                        
                        html+='<div class="tiporefaccion">POR GARANTIA</div>';
                        
                        html+='<div>'+row.cot_refaccion3+'</div>';
                    }
                    if(parseInt(row.cot_contador)>0){
                        html+='<div>Contador: '+row.cot_contador+'</div>';
                    }
                    if (row.cot_status==1){
                        html+='<span class="b-btn b-btn-primary btn_edit_i" onclick="info_edit('+row.asignacionId+',2,0)"><i class="fas fa-pencil-alt"></i></span>';
                    }
                    return html;
                }
            },
            {"data": "cot_tipo",
                render: function(data,type,row){
                    var html='';
                        if(row.prioridad>0){
                            html+='Prioridad '+row.prioridad+' ';
                        }
                        //html+=row.cot_tipo;
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                        if(row.priorizar_cot>0){
                            var checked='checked';
                        }else{
                            var checked='';
                        }
                        if (row.cot_status==2){
                            var checked_d='';
                            if(row.cot_referencia>0){
                            }else{
                                var checked_d='disabled';
                            }
                        }else{
                            var checked_d='disabled';
                        }
                        if(perfilid==12){
                            var checked_d='disabled';
                        }
                        /*
                        var id_row=0;
                        if(row.cpev=1){
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev=2){
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev=3){
                            id_row=row.asignacionId;
                        }
                        if(row.cpev=4){
                            id_row=row.asignacionId;
                        }
                        */
                        html='<input type="checkbox" class="filled-in priorizar_cot_2_'+row.asignacionIde+'" id="priorizar_cot_2_'+row.asignacionIde+'" '+checked+' '+checked_d+' onclick="f_priorizar_cot(2,'+row.asignacionIde+','+row.cot_referencia+')">\
                        <label for="priorizar_cot_2_'+row.asignacionIde+'" ></label>';
                    return html;
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    if (row.cot_status==1){
                        html='<div id="card-alert" class="card red">\
                                    <div class="card-content white-text" align="center">\
                                    <p>Por Cotizar</p>\
                                </div>\
                                </div>';
                    }else if (row.cot_status==2){
                        html='<div id="card-alert" class="card green">\
                                    <div class="card-content white-text">\
                                    <p>Cotizado</p>\
                                </div></div>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    //==============
                            var tipocotizacion='';
                        if(row.cot_refaccion!='' && row.cot_refaccion!=null){
                            tipocotizacion+='1';
                        }
                        if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                            tipocotizacion+='2';
                        }
                        if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                            tipocotizacion+='3';
                        }
                    //==============
                    if (row.cot_status==1){
                        html+='<a class="waves-effect cyan btn-bmz asignacionIde_'+row.asignacionIde+'  ser ser_'+row.asignacionId+'_'+row.asignacionIde+'_2_'+row.id+'" \
                                onclick="cotizar('+row.asignacionId+','+row.asignacionIde+',2,'+row.id+','+row.prioridad+')"\
                                data-equipo="'+row.idEquipo+'"\
                                data-serie="'+row.serie+'"\
                                data-direc="'+row.direccionservicio+'"\
                                data-tipocot="'+tipocotizacion+'"  \
                                >\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                    }
                    if (row.cot_status==2){
                        if(row.cot_referencia>0){
                            html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/documento/'+row.cot_referencia+'?tipocot='+tipocotizacion+'" target="_blank">\
                                    <i class="fas fa-eye" style="font-size: 20px;"></i></a>';
                        }else{
                            html+='<a class="waves-effect cyan btn-bmz" data-obser="'+row.cot_status_ob+'" onclick="viewcoment($(this))"><i class="fas fa-comment-alt" style="font-size: 20px;"></i></a>';
                        }
                        html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+row.id+'" target="_blank">\
                                    <i class="fas fa-link" style="font-size: 20px;"></i></a>';
                        if (row.cot_referencia_tipo==null) {
                            /*
                            html+='<a class="waves-effect cyan btn-bmz" onclick="autorizar('+row.cot_referencia+','+row.asignacionId+','+row.asignacionIde+',2)">\
                                    <i class="fas fa-check-square" style="font-size: 20px;"></i></a>';
                            */
                        }else{
                            html+='<a class="waves-effect cyan btn-bmz" onclick="detalle('+row.cot_referencia_tipo+',0)">\
                                    <i class="fas fa-file-powerpoint" style="font-size: 20px;"></i></a>';
                        }
                    }
                    if (row.cot_status==1){
                        var asigid=row.asignacionIde;

                        html+='<a class="waves-effect cyan btn-bmz " \
                                    onclick="cambiarestatus('+asigid+',2)" title="Cambiar Status a cotizado">\
                                        <i class="fas fa-clipboard-check" style="font-size: 20px;"></i></a>';
                    }
                    if(perfilid==12){
                        var html='';
                    }
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
        
    });
    setTimeout(function(){
      if(serv==2 || serv==3){
       table.columns([4]).visible(false);
      }  
    },500);
}
function loadcliente(){
    table.destroy();
    var serv = $('#serv option:selected').val();
    var tiposer = $('#tiposer option:selected').val();
    var tecnico = $('#tecnico option:selected').val();
    var status = $('#status option:selected').val();
    var zona = $('#zona option:selected').val();
    var pcliente = $('#pcliente option:selected').val();
    var fechainicial = $('#fechainicial').val();
    var fechafinal = $('#fechafinal').val();
        if(pcliente==undefined){
            pcliente=0;
        }
    //var fechainicio = $('#fechainicio').val();
    //var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Solicitud_Refacciones/getlistaserviciocl",
            type: "post",
            "data": {
                "tiposer":tiposer,
                "tecnico":tecnico,
                "status":status,
                "zona":zona,
                "pcliente":pcliente,
                "fechainicio":fechainicial,
                "fechafin":fechafinal,
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionId"},
            {"data": "servicio"},
            {"data": "tecnico",
                render: function(data,type,row){
                    var html=row.tecnico;
                    if(row.tecnico2!=undefined){
                        html+='<br>'+row.tecnico2;
                    }
                    if(row.per_fin>0){
                        html=row.tecnicof
                    }
                        html+='<!--'+row.per_fin+'-->';
                return html;
                }
            },
            {"data": "empresa"},
            
            {"data": "fecha"},
            //{"data": null,"visible": false},
            //{"data": null,"visible": false},
            {"data": "modelo"},
            {"data": "serie"},
            {"data": "cot_refaccion",
                render: function(data,type,row){
                    var html='';
                    if(row.cot_refaccion!='' && row.cot_refaccion!='null'){
                        if(row.cot_tipo!='' && row.cot_tipo!=null){
                            html+='<div class="tiporefaccion">'+row.cot_tipo+'</div>';    
                        }else{
                            html+='<div class="tiporefaccion">POR DESGASTE</div>';
                        }
                        html+='<div>'+row.cot_refaccion+'</div>';
                    }
                    if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                        
                        html+='<div class="tiporefaccion">POR DAÑO</div>';
                        
                        html+='<div>'+row.cot_refaccion2+'</div>';
                    }
                    if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                        
                        html+='<div class="tiporefaccion">POR GARANTIA</div>';
                        
                        html+='<div>'+row.cot_refaccion3+'</div>';
                    }
                    if(parseInt(row.cot_contador)>0){
                        html+='<div>Contador: '+row.cot_contador+'</div>';
                    }
                    if (row.cot_status==1){
                        html+='<span class="b-btn b-btn-primary btn_edit_i" onclick="info_edit('+row.asignacionId+',3,0)"><i class="fas fa-pencil-alt"></i></span>';
                    }
                    return html;
                }
            },
            {"data": "cot_tipo",
                render: function(data,type,row){
                    var html='';
                        if(row.prioridad>0){
                            html+='Prioridad '+row.prioridad+' ';
                        }
                        //html+=row.cot_tipo;
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    if (row.cot_status==1){
                        html='<div id="card-alert" class="card red">\
                                    <div class="card-content white-text" align="center">\
                                    <p>Por Cotizar</p>\
                                </div>\
                                </div>';
                    }else if (row.cot_status==2){
                        html='<div id="card-alert" class="card green">\
                                    <div class="card-content white-text">\
                                    <p>Cotizado</p>\
                                </div></div>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                        if(row.priorizar_cot>0){
                            var checked='checked';
                        }else{
                            var checked='';
                        }
                        if (row.cot_status==2){
                            var checked_d='';
                            if(row.cot_referencia>0){
                            }else{
                                var checked_d='disabled';
                            }
                        }else{
                            var checked_d='disabled';
                        }
                        if(perfilid==12){
                            var checked_d='disabled';
                        }
                        /*
                        var id_row=0;
                        if(row.cpev=1){
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev=2){
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev=3){
                            id_row=row.asignacionId;
                        }
                        if(row.cpev=4){
                            id_row=row.asignacionId;
                        }
                        */
                        html='<input type="checkbox" class="filled-in priorizar_cot_3_'+row.asignacionIdd+'" id="priorizar_cot_3_'+row.asignacionIdd+'" '+checked+' '+checked_d+' onclick="f_priorizar_cot(3,'+row.asignacionIdd+','+row.cot_referencia+')">\
                        <label for="priorizar_cot_3_'+row.asignacionIdd+'" ></label>';
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    //==============
                            var tipocotizacion='';
                        if(row.cot_refaccion!='' && row.cot_refaccion!=null){
                            tipocotizacion+='1';
                        }
                        if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                            tipocotizacion+='2';
                        }
                        if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                            tipocotizacion+='3';
                        }
                    //==============
                    if (row.cot_status==1){
                        html+='<a class="waves-effect cyan btn-bmz asignacionIde_'+row.asignacionIdd+' ser_'+row.asignacionId+'_'+row.asignacionIdd+'_3_'+row.id+'" \
                                onclick="cotizar('+row.asignacionId+','+row.asignacionIdd+',3,'+row.id+','+row.prioridad+')"\
                                data-equipo="'+row.idEquipo+'"\
                                data-serie="'+row.serie+'" \
                                data-direc="'+row.direccionservicio+'" \
                                data-tipocot="'+tipocotizacion+'"  \
                                >\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                    }
                    if (row.cot_status==2){
                        if(row.cot_referencia>0){
                        html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/documento/'+row.cot_referencia+'?tipocot='+tipocotizacion+'" target="_blank">\
                                    <i class="fas fa-eye" style="font-size: 20px;"></i></a>';
                        }else{
                            html+='<a class="waves-effect cyan btn-bmz" data-obser="'+row.cot_status_ob+'" onclick="viewcoment($(this))"><i class="fas fa-comment-alt" style="font-size: 20px;"></i></a>';
                        }
                        html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+row.id+'" target="_blank">\
                                    <i class="fas fa-link" style="font-size: 20px;"></i></a>';
                        if (row.cot_referencia_tipo==null) {
                            /*
                            html+='<a class="waves-effect cyan btn-bmz" onclick="autorizar('+row.cot_referencia+','+row.asignacionId+',0,3)">\
                                    <i class="fas fa-check-square" style="font-size: 20px;"></i></a>';
                            */
                        }else{
                            html+='<a class="waves-effect cyan btn-bmz" onclick="detalle('+row.cot_referencia_tipo+',0)">\
                                    <i class="fas fa-file-powerpoint" style="font-size: 20px;"></i></a>';
                        }
                        
                    }
                    if (row.cot_status==1){
                        var asigid=row.asignacionIdd;
                 

                        html+='<a class="waves-effect cyan btn-bmz " \
                                    onclick="cambiarestatus('+asigid+',3)" title="Cambiar Status a cotizado">\
                                        <i class="fas fa-clipboard-check" style="font-size: 20px;"></i></a>';
                    }
                    if(perfilid==12){
                        var html='';
                    }
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
        
    });
    setTimeout(function(){
      if(serv==2 || serv==3){
       //table.columns([4]).visible(false);
      }  
    },500);
}
function loadventa(){
    table.destroy();
    var serv = $('#serv option:selected').val();
    var tiposer = $('#tiposer option:selected').val();
    var tecnico = $('#tecnico option:selected').val();
    var status = $('#status option:selected').val();
    var zona = $('#zona option:selected').val();
    var pcliente = $('#pcliente option:selected').val();
    var fechainicial = $('#fechainicial').val();
    var fechafinal = $('#fechafinal').val();
        if(pcliente==undefined){
            pcliente=0;
        }
    //var fechainicio = $('#fechainicio').val();
    //var fechafin = $('#fechafin').val();
    table = $('#tabla_asignar').DataTable({
        fixedHeader: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "index.php/Solicitud_Refacciones/getlistaserviciovent",
            type: "post",
            "data": {
                "tiposer":tiposer,
                "tecnico":tecnico,
                "status":status,
                "zona":zona,
                "pcliente":pcliente,
                "fechainicio":fechainicial,
                "fechafin":fechafinal,
            },
            error: function() {
                $("#tabla_asignar").css("display", "none");
            }
        },
        "columns": [
            {"data": "asignacionId"},
            {"data": "servicio"},
            {"data": "tecnico",
                render: function(data,type,row){
                    var html=row.tecnico;
                    
                    if(row.per_fin>0){
                        html=row.tecnicof
                    }
                        html+='<!--'+row.per_fin+'-->';
                return html;
                }
            },
            {"data": "empresa"},
            
            {"data": "fecha"},
            
            {"data": 'modelo'},
            {"data": 'serie'},
            {"data": "cot_refaccion",
                render: function(data,type,row){
                    var html='';
                    if(row.cot_refaccion!='' &&  row.cot_refaccion!=null){
                        if(row.cot_tipo!='' && row.cot_tipo!=null){
                            html+='<div class="tiporefaccion">'+row.cot_tipo+'</div>';    
                        }else{
                            html+='<div class="tiporefaccion">POR DESGASTE</div>';
                        }
                        html+='<div>'+row.cot_refaccion+'</div>';
                    }
                    if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                        
                        html+='<div class="tiporefaccion">POR DAÑO</div>';
                        
                        html+='<div>'+row.cot_refaccion2+'</div>';
                    }
                    if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                        
                        html+='<div class="tiporefaccion">POR GARANTIA</div>';
                        
                        html+='<div>'+row.cot_refaccion3+'</div>';
                    }
                    if(parseInt(row.cot_contador)>0){
                        html+='<div>Contador: '+row.cot_contador+'</div>';
                    }
                    if (row.cot_status==1){
                        html+='<span class="b-btn b-btn-primary btn_edit_i" onclick="info_edit('+row.asignacionId+',4,0)"><i class="fas fa-pencil-alt"></i></span>';
                    }
                    return html;
                }
            },
            {"data": "cot_tipo",
                render: function(data,type,row){
                    var html='';
                        if(row.prioridadr>0){
                            html+='Prioridad '+row.prioridadr+' ';
                        }
                        //html+=row.cot_tipo;
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    if (row.cot_status==1){
                        html='<div id="card-alert" class="card red">\
                                    <div class="card-content white-text" align="center">\
                                    <p>Por Cotizar</p>\
                                </div>\
                                </div>';
                    }else if (row.cot_status==2){
                        html='<div id="card-alert" class="card green">\
                                    <div class="card-content white-text">\
                                    <p>Cotizado</p>\
                                </div></div>';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    if(row.priorizar_cot>0){
                            var checked='checked';
                        }else{
                            var checked='';
                        }
                        if (row.cot_status==2){
                            var checked_d='';
                            if(row.cot_referencia>0){
                            }else{
                                var checked_d='disabled';
                            }
                        }else{
                            var checked_d='disabled';
                        }
                        if(perfilid==12){
                            var checked_d='disabled';
                        }
                        /*
                        var id_row=0;
                        if(row.cpev=1){
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev=2){
                            id_row=row.asignacionIde;
                        }
                        if(row.cpev=3){
                            id_row=row.asignacionId;
                        }
                        if(row.cpev=4){
                            id_row=row.asignacionId;
                        }
                        */
                        html='<input type="checkbox" class="filled-in priorizar_cot_4_'+row.asignacionId+'" id="priorizar_cot_4_'+row.asignacionId+'" '+checked+' '+checked_d+' onclick="f_priorizar_cot(4,'+row.asignacionId+','+row.cot_referencia+')">\
                        <label for="priorizar_cot_4_'+row.asignacionId+'" ></label>';
                    return html;
                    return html;
                }
            },
            {"data": null,
                render: function(data,type,row){ 
                    var html = '';
                    //==============
                            var tipocotizacion='';
                        if(row.cot_refaccion!='' && row.cot_refaccion!=null){
                            tipocotizacion+='1';
                        }
                        if(row.cot_refaccion2!='' && row.cot_refaccion2!=null){
                            tipocotizacion+='2';
                        }
                        if(row.cot_refaccion3!='' && row.cot_refaccion3!=null){
                            tipocotizacion+='3';
                        }
                    //==============
                    if (row.cot_status==1){
                        html+='<a class="waves-effect cyan btn-bmz asignacionIde_'+row.asignacionId+'" \
                                onclick="cotizar('+row.asignacionId+',0,4,'+row.id+','+row.prioridadr+')" \
                                data-equipo="'+row.idEquipo+'"\
                                data-serie="'+row.serie+'" \
                                data-tipocot="'+tipocotizacion+'"  \
                                >\
                                    <i class="fas fa-cart-plus" style="font-size: 20px;"></i></a>';
                    }
                    if (row.cot_status==2){
                        if(row.cot_referencia>0){
                        html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/documento/'+row.cot_referencia+'?tipocot='+tipocotizacion+'" target="_blank">\
                                    <i class="fas fa-eye" style="font-size: 20px;"></i></a>';
                        }else{
                            html+='<a class="waves-effect cyan btn-bmz" data-obser="'+row.cot_status_ob+'" onclick="viewcoment($(this))"><i class="fas fa-comment-alt" style="font-size: 20px;"></i></a>';
                        }
                        html+='<a class="waves-effect cyan btn-bmz" href="'+base_url+'index.php/Cotizaciones/listaCotizacionesPorCliente/'+row.id+'" target="_blank">\
                                    <i class="fas fa-link" style="font-size: 20px;"></i></a>';
                        if (row.cot_referencia_tipo==null) {
                            /*
                            html+='<a class="waves-effect cyan btn-bmz" onclick="autorizar('+row.cot_referencia+','+row.asignacionId+',0,3)">\
                                    <i class="fas fa-check-square" style="font-size: 20px;"></i></a>';
                            */
                        }else{
                            html+='<a class="waves-effect cyan btn-bmz" onclick="detalle('+row.cot_referencia_tipo+',0)">\
                                    <i class="fas fa-file-powerpoint" style="font-size: 20px;"></i></a>';
                        }
                        
                    }
                    if (row.cot_status==1){
                        var asigid=row.asignacionId;

                        html+='<a class="waves-effect cyan btn-bmz " \
                                    onclick="cambiarestatus('+asigid+',4)" title="Cambiar Status a cotizado">\
                                        <i class="fas fa-clipboard-check" style="font-size: 20px;"></i></a>';
                    }
                    if(perfilid==12){
                        var html='';
                    }
                    return html;
                }
            },
          ],
        order: [
            [0, "desc"]
          ]
        
    });
    setTimeout(function(){
      if(serv==2 || serv==3){
       table.columns([4]).visible(false);
      }  
    },500);
}
var equiposse;
function cotizar(asi,asie,tipo,cliente,pri){
    $('#prioridad').val(pri);
    var dir='';
    dir=$('.ser_'+asi+'_'+asie+'_'+tipo+'_'+cliente).data('direc');
    tipocot=$('.ser_'+asi+'_'+asie+'_'+tipo+'_'+cliente).data('tipocot');
    
    $('#dirinfo').val(dir);
    $('#ventap').prop('checked',false).change();
    $('#ventar').prop('checked',false).change();
    $('#modalcotizacion').modal('open');
    asignacionId=asi;
    asignacionIde=asie;
    asignaciontipo=tipo;
    asigcliente=cliente
    $('#idCliente').val(cliente);
    obtenerdatoscliente(cliente);
    $('#rowRefacciones2').remove();
    $('#rowRefacciones3').remove();
    $('#rowRefacciones4').remove();
    $('#rowRefacciones5').remove();
    $('#rowRefacciones6').remove();

    if(asie>0){
        if(tipo==3){
            //asie=asi;
        }
        equiposse=$('.asignacionIde_'+asie).data('equipo');
        var serie=$('.asignacionIde_'+asie).data('serie');
        $('#equipoListaRefacciones').val(equiposse).trigger("chosen:updated").change();
        $('#equipo_acce').val(equiposse).trigger("chosen:updated").change();
        $('#seriee').val(serie);
        serieselected=serie;
        equiposelected=equiposse;
    }else{
        equiposse=$('.asignacionIde_'+asi).data('equipo');
        var serie=$('.asignacionIde_'+asi).data('serie');
        $('#equipoListaRefacciones').val(equiposse).trigger("chosen:updated").change();
        $('#equipo_acce').val(equiposse).trigger("chosen:updated").change();
        $('#seriee').val(serie);
        serieselected=serie;
        equiposelected=equiposse;
    }
    consultarcompraservicio(serie);
    deleteser();
} 
function funcionRefacciones(selectRefaccionDetalle){
    var aux = selectRefaccionDetalle.closest('.rowRefacciones');
    cargaRefaccionesPorEquipo(aux);
}
function cargaRefaccionesPorEquipo(aux){
    var idDiv = aux.attr("id");
    var idEquipo = aux.find('.equipoListaRefacciones').val();
    /*
    $.ajax({
        url: base_url+"index.php/Cotizaciones/getDatosRefaccionesPorEquipo/"+idEquipo,
        success: function (response){
            aux.find('.refaccion').html('');
            aux.find('.refaccion').html(response);
            $('.refaccion').trigger("chosen:updated");
        },
        error: function(response){  
            alertfunction('AtenciÃ³n!','Algo saliÃ³ mal, intente de nuevo o contacte al administrador del sistema'); 
        }
    });
    */
    if(idEquipo>0){
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/mostrarselecrefaccionesr",
            data: {
                id:idEquipo
            },
            success: function (data){
                

                aux.find('.refaccion').html('');
                aux.find('.refaccion').html(data);
                $('.refaccion').trigger("chosen:updated");
            },
            error: function(data){
                notificacion(1); 
            }
        });
    }   
}
//(replica de form cotizacion)
function agregarRefaccion(){
    var contadorRefacciones = $('#contadorRefacciones').val();
    var contadorRefaccionesAux = contadorRefacciones;
    contadorRefacciones++;

    $('#contadorRefacciones').val(contadorRefacciones);
    $('.chosen-select').chosen('destroy');

    var $template = $('#rowRefacciones1'),
                        $clone = $template
                        .clone()
                        .show()
                        .removeAttr('id')
                        .attr('id',"rowRefacciones"+contadorRefacciones)
                        .insertAfter("#rowRefacciones"+contadorRefaccionesAux)
                        //.append('')
                        $clone.find("select").val("");
    $('.chosen-select').chosen({width: "100%"});

    $('#rowRefacciones'+contadorRefacciones+' #equipoListaRefacciones').val(equiposse).trigger("chosen:updated").change();
}
//(replica de form cotizacion)
function eliminarRefaccion(){
    var contadorRefacciones = $('#contadorRefacciones').val();
    if(contadorRefacciones>1){
        $('#rowRefacciones'+contadorRefacciones).remove();
        contadorRefacciones--;
        $('#contadorRefacciones').val(contadorRefacciones);
    }
    else if(contadorRefacciones==1){
        alertfunction('Atención!','Debe tener por lo menos una refacción seleccionado'); 
    }
}
//(replica de form cotizacion)
function alertfunction(titlea,contenta){
    $.alert({boxWidth: '30%',useBootstrap: false,title: titlea,theme: 'bootstrap',content: contenta});  
}

function obtenerdatoscliente(cliente){

    $.ajax({
        type:'POST',
        url: base_url+'index.php/Clientes/obtenerdatoscliente',
        data: {
          cliente:cliente
        },
        success:function(data){
            console.log(data);
            var array = $.parseJSON(data);
            $('#idCliente').val(array.id);
            $('#cliente').val(array.empresa);
            $('#atencion').val(array.atencionpara);
            $('#correo').val(array.email);
           
        }
    });
}
//(replica de form cotizacion)
function validaciones(eleccionCotizar){
    var retorno = 1;

    if($('#atencion').val().trim()=='' || $('#correo').val().trim()=='' || $('#telefono').val().trim()==''){ 
        alertfunction('Atención!','Debe llenar los campos de "Atención", "Correo" y "Teléfono". De lo contrario no podrá seguir creando una Cotización.'); 
        retorno = 0;
    }else{
        switch (eleccionCotizar) {
            case 1:
                console.log('Validaciones de equipos');  
            break;
            case 2:
                console.log('Validaciones de consumibles');
            break;
            case 3:
                console.log('Validaciones de refacciones');
                if($('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val()=='' || $('#opcionesConsumiblesRefacciones').find('.tipoConsumibles').val()==null){
                    alertfunction('Atención!','Seleccione que tipo de cotización de Refacciones se realizará.');
                    retorno = 0;
                }
                else if($('.equipoListaRefacciones').val()=='' || $('.equipoListaRefacciones').val()==null){
                    alertfunction('Atención!','Seleccione para que equipo se busca la refaccion.');
                    retorno = 0;
                }
                else if($('.refaccion').val()=='' || $('.refaccion').val()==null){
                    alertfunction('Atención!','Seleccione la refacción deseada.');
                    retorno = 0;
                }
                else if($('.piezasRefacion').val()=='' || $('.piezasRefacion').val()==null || $('.piezasRefacion').val()==0){
                    alertfunction('Atención!','Ingrese la cantidad de piezas de la refacción.');
                    retorno = 0;
                }
            break;
            case 4:
                console.log('Validaciones de polizas');
            break;
            default: 
            break;
        }
    }
    return retorno;
}
// Crear la cotización (replica de form cotizacion)
function creaCotizacion(datos){
    var tipoDocumento = '';
    var idCliente = $('#idCliente').val();
    console.log('idCliente: '+idCliente);
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/nuevaCotizacion",
        data: datos,
        success: function (response){
            var cotizaid =parseInt(response);
            agregarcotizacionacervicio(asignacionId,asignacionIde,asignaciontipo,cotizaid);
            editarpersonalcotizacion(cotizaid);
            alertfunction('Éxito!','Se ha creado la cotización: '+response);

            if($("#eleccionCotizar").val()==1){
                tipoDocumento = 'documentoHorizontalConBordes';
            }
            else if($("#eleccionCotizar").val()==2){
                tipoDocumento = 'documentoConsumiblesBordes';
            }
            else if($("#eleccionCotizar").val()==3){
                tipoDocumento = 'documentoRefaccionesBordes';
            }
            else if($("#eleccionCotizar").val()==4){
                tipoDocumento = 'documentoPolizas';
            }   
            
            setTimeout(function(){ 
                window.open(base_url+"index.php/Cotizaciones/"+tipoDocumento+"/"+response.trim()); 
            }, 2000);
            
            
            setTimeout(function(){ 
                //window.location.href = base_url+"index.php/Cotizaciones/listaCotizacionesPorCliente/"+idCliente;
                load(); 
            }, 2000);
            
        },
        error: function(response){
            notificacion(1); 
        }
    });
}
function agregarcotizacionacervicio(asig,asige,tipo,coti){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Solicitud_Refacciones/actualizarcotizacion',
        data: {
          asig:asig,
          asige:asige,
          tipo:tipo,
          coti:coti
        },
        success:function(data){
           $('#modalcotizacion').modal('close'); 
           
        }
    });
}
function agregarcotizacionacervicioau(asig,asige,tipo,coti){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Solicitud_Refacciones/actualizarcotizaciona',
        data: {
          asig:asig,
          asige:asige,
          tipo:tipo,
          coti:coti
        },
        success:function(data){
           $('#modalcotizacion').modal('close'); 
           
        }
    });
}
function autorizar(idCotizacion,asig,asige,tipo){  
    asignacionId=asig;
    asignacionIde=asige;
    asignaciontipo=tipo;

            //consumibles
            
            // Cargamos los datos de los equipos que vengan en esa cotizaciÃ³n
            // Mostrar el modal
            $('#modalAutorizacionrefacciones').modal('open');
            $('#idCotizacionRefaccion').val(idCotizacion);
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Cotizaciones/allrefaccionescotizacion",
                data: {id:idCotizacion
                },
                success: function (response){
                    $('.tabla_cotizarmostrarre').html(response);    
                },
                error: function(response){
                    notificacion(1); 
                }
            });     
}
/*
var srefaadd=1;
function addrefaccion(){
    var equipoval = $('#equipoListarefacciones option:selected').val();
    var equipotext = $('#equipoListarefacciones option:selected').text();
    var tornerval = $('#srefacciones option:selected').val();
    var tornertext = $('#srefacciones option:selected').text();
    var tornerpval = $('#refaccionesp option:selected').val();
    var tornerptext = $('#refaccionesp option:selected').text();
    var bodegaval = $('#serie_bodegarrs option:selected').val();
    var bodegatext = $('#serie_bodegarrs option:selected').text();
    var piezastext = $('#piezasr').val();
    var tablerow='<tr class="consuelecteddd_row_'+srefaadd+'">\
                    <td>\
                        <input type="hidden" id="equipotx" class="equipotx" value="'+equipoval+'">\
                        '+equipotext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="tonertx" class="tonertx" value="'+tornerval+'">\
                        '+tornertext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="tonerptx" class="tonerptx" value="'+tornerpval+'">\
                        '+tornerptext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="piezastx" class="piezastx" value="'+piezastext+'">\
                        '+piezastext+'\
                    </td>\
                    <td>\
                        <input type="hidden" id="bodegatx" class="piezastx" value="'+bodegaval+'">\
                        '+bodegatext+'\
                    </td>\
                    <td>\
                        <a onclick="deleteconsuadd('+srefaadd+')"style="cursor: pointer;"><i class="material-icons" style="color: red;">delete_forever</i></a>\
                    </td>\
                  </tr>';
    $('.equipotableconsumicle .tablerefacciones tbody').append(tablerow);
    srefaadd++;
}
*/
function deleteconsuadd(id){
    $('.consuelecteddd_row_'+id).remove();
}
function checkrefacciones(){
    // listo 
    var etx = 0;
    var addtp = 0;
    $(".numchek").each(function(){
        if ($(this).is(':checked')) {
            addtp++;
        }  
    }); 
    var tabletr = $('.tablerefacciones tbody > tr').length;       
    var suma = tabletr+addtp;
    if(suma==0){
        $.alert({
                boxWidth: '30%',
                useBootstrap: false,
                title: 'AtenciÃ³n!',
                content: 'Favor de selecionar por lo menos un consumible'});
        retorno = 0;
    }else{
        var nombre = [];
        $("#tablerefaccionp tbody > tr").each(function() {
            if($(this).find("input[name*='nombre']").is(':checked')){
                //var aux = $(this).attr("id").split('_');
                itemp={};
                itemp['id']=$(this).find("input[name*='nombre']").val();
                itemp['cantidad']=$(this).find("input[id*='cantidadnew']").val(); 
                itemp['bodega']=$(this).find("select[id*='serie_bodegarr']").val();                    
                nombre.push(itemp);
            }
        });
        INFOp  = new FormData();
        nombrep   = JSON.stringify(nombre);
        var DATAp  = [];
            var TABLA   = $(".tablerefacciones tbody > tr");
                TABLA.each(function(){         
                item = {};
                item ["equipo"] = $(this).find("input[id*='equipotx']").val();
                item ["refaccion"]   = $(this).find("input[id*='tonertx']").val();
                item ["refaccionp"]  = $(this).find("input[id*='tonerptx']").val();
                item ["piezas"]  = $(this).find("input[id*='piezastx']").val();
                item ["bodega"]  = $(this).find("input[id*='bodegatx']").val();
                DATAp.push(item);
            });
            INFO  = new FormData();
            arrayConsumible   = JSON.stringify(DATAp);
            INFO.append('data', arrayConsumible);

        $.ajax({
            type:'POST',
            url: base_url+"index.php/Cotizaciones/guardarrefaccionescheck",
            data: {datos:nombrep,
                   id:$('#idCotizacionRefaccion').val(),
                   consumible:arrayConsumible
            },
            success: function (response){
                var venta = parseInt(response);
                agregarcotizacionacervicioau(asignacionId,asignacionIde,asignaciontipo,venta);
                $.alert({
                    boxWidth: '30%',
                    useBootstrap: false,
                    title: 'Éxito!',
                    type: 'green',
                    theme: 'bootstrap',
                    content: 'Aceptada Correctamente'
                }); 
                $('#modalAutorizacionrefacciones').modal('close');
                // Recargamos la pÃ¡gina
                
                setTimeout(function(){ 
                    load();
                }, 2000); 
            },
            error: function(response){
                notificacion(1); 
            }
        });
    }        
}
function detalle(id,tipo){
    if (tipo==0) {
        window.open(base_url+"Prefactura/view/"+id, "Prefactura", "width=780, height=612");
    }else{
        //window.open(base_url+"Prefactura/viewc/"+id, "Prefactura", "width=780, height=612");
    }
}
function selectprecior(data){
    var aux = data.closest('.rowRefacciones');
    var id = aux.find('.refaccion').val();
    var stock = aux.find('.refaccion option:selected').data('stock');
    var name = aux.find('.refaccion option:selected').data('name');
    verificarultimascoti(id);
    if(stock>0){}else{solicitarrefaccion(id,name)}
    //var id = $('#refaccion option:selected').val();
    var aaa = $('#aaa').val();
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Cotizaciones/mostrarselectpreciosr",
        data: {id:id},
        success: function (data){
            var array = $.parseJSON(data); 
             aux.find('.refaccionesp_mostrar').html('');
             aux.find('.refaccionesp_mostrar').html(array.select_option);   
             aux.find('.chosen-select').chosen({width: "100%"}); 

            if(aaa==1){
                 aux.find('.refaccionesp').val(array.espe).trigger('chosen:updated');
            }else{
                 aux.find('.refaccionesp').trigger("chosen:updated");    
            }
        },
        error: function(data){ 
            notificacion(1);
        }
    });
}
function editarpersonalcotizacion(idCotizacion){
    var selectejecutiva = $('#selectejecutivas').html();
     $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: 'Desea Editar la ejecutiva?<br>'+
                 '<select class="browser-default form-control-bmz" id="selectejecutivasedit">'+selectejecutiva+'</select>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                
                     $.ajax({
                        type:'POST',
                        url: base_url+"index.php/Cotizaciones/editejecutiva",
                        data: {
                            idCotizacion:idCotizacion,
                            ejecutiva: $('#selectejecutivasedit option:selected').val()
                        },
                        success: function (response){
                                
                                    $.alert({
                                    boxWidth: '30%',
                                    useBootstrap: false,
                                    title: 'Hecho!',
                                    content: 'Personal Agregado'}); 
                                
                        },
                        error: function(response){
                            notificacion(1);
                        }
                    });
            },
            cancelar: function (){
                
            }
        }
    });
}
function solicitarrefaccion(id,name){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Desea solicitar la refaccion <b>'+name+'</b>?<br>Cantidad<br>'+
                 '<input type="number" placeholder="cantidad" id="cantidadsol" class="name form-control-bmz" style="width: 99%;"/>',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                    //=============================================================================
                        var cantidadsol = $('#cantidadsol').val();
                        var datos = 'refaccion='+id+'&cantidad='+cantidadsol;
                        $.ajax({
                            type:'POST',
                            url: base_url+'index.php/Solicitud_Refacciones/solicitar',
                            data: datos,
                            success:function(data){
                                toastr["success"]("Se realizo la solicitud");
                            }
                        });
                    //=============================================================================
                 
                
            },
            cancelar: function () {
                
            }
        }
    });
}
function verificarultimascoti(idr){
    var idcli=$('#idCliente').val();

    $.ajax({
        type:'POST',
        url: base_url+"index.php/Generales/verificarultimascoti",
        data: {
            idcli:idcli,
            idr: idr
        },
        success: function (response){
            var num=parseInt(response);
            console.log('num: '+num);
            if(num>0){
                toastr["error"]("Se ha realizado una cotización <br>previamente");
            }       
        },
        error: function(response){
            notificacion(1);
        }
    });
}
//=================================================================
function btn_cotizar2(){
    formulario_cotizacion = $('#formulario_cotizacion'); 
    var datos = formulario_cotizacion.serialize();
    var tipoprecioequipo = $('#tipoprecioequipo option:selected').val();
    var pasa=1;
    //===========================================
        var nombre = [];
        var TABLA   = $("#tables_equipos_selected tbody > tr");
        TABLA.each(function(){  
            if ($(this).find("input[class*='equipo_check']").is(':checked')) {
                item = {};
                item['equipos'] = $(this).find("input[id*='equipo_selected_all']").val();
                item['cantidad'] = $(this).find("input[id*='equipo_cantidad']").val();

                item['volm'] = $(this).find("input[id*='equipo_r_volm']").val()==undefined?0:$(this).find("input[id*='equipo_r_volm']").val();
                item['costm'] = $(this).find("input[id*='equipo_r_costm']").val()==undefined?0:$(this).find("input[id*='equipo_r_costm']").val();
                item['volc'] = $(this).find("input[id*='equipo_r_volc']").val()==undefined?0:$(this).find("input[id*='equipo_r_volc']").val();
                item['costc'] = $(this).find("input[id*='equipo_r_costc']").val()==undefined?0:$(this).find("input[id*='equipo_r_costc']").val();
                item['nccc'] = $(this).find("input[id*='equipo_r_nccc']").val();
                item['gara'] = $(this).find("input[id*='s_equipo_garantia']").is(':checked')==true?1:0;

                var equiposs =  $(this).find("input[id*='equipo_selected_all']").val();
                var equiposconsumiblelength=$('.equipo_c_'+equiposs+' > div').length;
                if (equiposconsumiblelength==0) {
                    pasa=0;
                }
                nombre.push(item);
            }else{
                var equiposs = $(this).find("input[class*='equipo_check']").val();
                $('.equipo_c_'+equiposs).html('');
                $('.equipo_s_'+equiposs).html('');   
            }
        });
        aInfoe   = JSON.stringify(nombre);
    //===========================================
        var TABLAa   = $(".equipo_accessoriosselected > div");
        var DATAa  = [];
            TABLAa.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelected']").val();
                item ["accesorioselected"]  = $(this).find("input[id*='accesorioselected']").val();
                DATAa.push(item);
            });
            INFOa  = new FormData();
            aInfoa   = JSON.stringify(DATAa);

        var TABLAc   = $(".equipo_consumibleselected > div");
        var DATAc  = [];
            TABLAc.each(function(){         
                item = {};
                item ["equiposelected"]   = $(this).find("input[id*='equiposelectedc']").val();
                item ["consumibleselected"]  = $(this).find("input[id*='consumibleselected']").val();
                DATAc.push(item);
            });
            INFOc  = new FormData();
            aInfoc   = JSON.stringify(DATAc);
    //===========================================
        var consutable = [];
        var TABLAcon   = $("#table-addconsumibles tbody > tr");
        TABLAcon.each(function(){  
            item = {};
            item['equipos'] = $(this).find("input[id*='c_equipo']").val();
            item['toners'] = $(this).find("input[id*='c_toner']").val();
            item['precio'] = $(this).find("input[id*='c_precio']").val();
            item['cantidad'] = $(this).find("input[id*='c_cantidad']").val();
            consutable.push(item);
        });
        aInfotc   = JSON.stringify(consutable);
    //===========================================
        var refatable = [];
        var TABLAref   = $("#table-addrefacciones tbody > tr");
        TABLAref.each(function(){  
            item = {};
            item['equipos'] = $(this).find("input[id*='r_equipo']").val();
            item['serie'] = $(this).find("input[id*='r_serie']").val();
            item['refa'] = $(this).find("input[id*='r_refaccion']").val();
            item['cantidad'] = $(this).find("input[id*='r_cantidad']").val();
            item['precio'] = $(this).find("input[id*='r_precio']").val();
            item['gara'] = $(this).find("input[id*='r_gara']").val();
            refatable.push(item);
        });
        aInfotre   = JSON.stringify(refatable);
    //===========================================
        var DATAp  = [];
        var TABLAp   = $(".tablepolizas tbody > tr");
        TABLAp.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equipoposelected']").val();
            item ["serie"] = $(this).find("input[id*='serieselected']").val();
            item ["poliza"]   = $(this).find("input[id*='polizaval']").val();
            item ["polizad"]  = $(this).find("input[id*='polizadval']").val();
            item ["cantidad"]  = $(this).find("input[id*='polizadcant']").val();
            item ["costo"]  = $(this).find("input[id*='polizacval']").val();
            DATAp.push(item);
        });
        INFO  = new FormData();
        arrayPolizas   = JSON.stringify(DATAp);
    //================================
        var DATAac  = [];
        var TABLAac   = $("#table_venta_accesorios  tbody > tr");
            TABLAac.each(function(){         
            item = {};
            item ["equipo"] = $(this).find("input[id*='equi_acce_selected']").val();
            item ["accesorio"]   = $(this).find("input[id*='acce_acce_selected']").val();
            item ["costo"]   = $(this).find("input[id*='costo_acce_selected']").val();
            item ["cantidad"]  = $(this).find("input[id*='cantidad_acce_selected']").val();
            item ["bodega"]  = $(this).find("input[id*='bodega_acce_selected']").val();
            item ['gara'] = $(this).find("input[id*='garantia_acce_selected']").val();
            DATAac.push(item);
        });
        INFO  = new FormData();
        arrayac   = JSON.stringify(DATAac);
        
    //================================

    datos = datos+'&arrayequipos='+aInfoe+'&arrayequipoacce='+aInfoa+'&arrayequipoconsu='+aInfoc+'&tipoprecioequipo='+tipoprecioequipo+'&arrayconsu='+aInfotc+'&arrayrefa='+aInfotre+'&arrayPolizas='+arrayPolizas+'&arrayac='+arrayac+'&tipov=1&tipocot='+tipocot;
    var tipoDocumento = '';
    var idCliente = $('#idCliente').val();
    console.log('idCliente: '+idCliente);
    if(formulario_cotizacion.valid()){
        if(pasa == 1){
            $.ajax({
                type:'POST',
                url: base_url+"index.php/Cotizaciones/nuevaCotizaciongeneral",
                data: datos,
                success: function (response){
                    var cotizaid =parseInt(response);
                    agregarcotizacionacervicio(asignacionId,asignacionIde,asignaciontipo,cotizaid);
                    editarpersonalcotizacion(cotizaid);
                    alertfunction('Éxito!','Se ha creado la cotización: '+cotizaid); 
                    setTimeout(function(){ 
                        window.open(base_url+"index.php/Cotizaciones/documento/"+cotizaid); 
                    }, 2000);

                },
                error: function(response){
                    notificacion(1); 
                }
            });
        }else{
            alertfunction('Atención!','Falta seleccionar al menos un consumible de un equipo.');
        }
    }
}
function f_priorizar_cot(tipo,idrow,cot_referencia){
    setTimeout(function(){ 
        var pri=$('.priorizar_cot_'+tipo+'_'+idrow).is(':checked')==true?1:0;
        $.ajax({
            type:'POST',
            url: base_url+"index.php/Solicitud_Refacciones/f_priorizar_cot",
            data: {
                tipo:tipo,
                idrow:idrow,
                pri:pri,
                cot_referencia:cot_referencia
            },
            success: function (response){
            },
            error: function(response){
                notificacion(1);
            }
        });
    }, 1000);
}
function cambiarestatus(id,tipo){
    $.confirm({
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fa fa-warning',
                title: 'Atención!',
                content: '¿Está seguro de Cambiar a estatus Cotizado?<br>Se necesita permisos de administrador<br>'+
                     '<input type="password" placeholder="Contraseña" id="contrasena" class="name form-control" required/><br><label>Observaciones</label><textarea class="form-control-bmz" id="obsercs"></textarea>',
                type: 'red',
                typeAnimated: true,
                buttons:{
                    confirmar: function (){
                        var pass=$('#contrasena').val();
                        var obsercs =$('#obsercs').val();
                        if (pass!='') {
                            $.ajax({
                                type:'POST',
                                url: base_url+"index.php/Sistema/solicitarpermiso",
                                data: {
                                    pass:pass
                                },
                                success: function (response){
                                        var respuesta = parseInt(response);
                                        if (respuesta==1) {
                                            //===============
                                                $.ajax({
                                                    type:'POST',
                                                    url: base_url+"index.php/Solicitud_Refacciones/cambiarestatus",
                                                    data: {
                                                        tipo:tipo,
                                                        idrow:id,
                                                        obsercs:obsercs
                                                    },
                                                    success: function (response){
                                                        load();

                                                    },
                                                    error: function(response){
                                                        notificacion(1);
                                                    }
                                                });
                                            //=============
                                        }else{
                                            notificacion(2);
                                        }
                                },
                                error: function(response){
                                    notificacion(1);
                                }
                            });
                                        
                        }else{
                            notificacion(0);
                        }
                },
                cancelar: function () {
                }
                }
            });
}
function viewcoment(data){
    //console.log(data);
    //console.log(data.data('obser'));
    $.alert({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Observacion',
            content: data.data('obser')}); 
}
function consultarcompraservicio(serie){
    $.ajax({
        type:'POST',
        url: base_url+"index.php/Solicitud_Refacciones/consultarcompraservicio",
        data: {
            serie:serie
        },
        success: function (data){
            console.log(data);
            var array = $.parseJSON(data);
            $('.info_compra').html(array.compra);
            $('.info_servicio').html(array.servicio);
        },
        error: function(response){
            notificacion(1);
        }
    });
}
function verifircar_dir_con(){
    var DATAf  = [];
    $(".equipo_con").each(function() {
        item = {};
        item ["equipo"] = $(this).data('idrow');
        item ["serieid"] = $(this).data('serieid');
        DATAf.push(item);
    });
    aInfo   = JSON.stringify(DATAf);
    $.ajax({
        type:'POST',
        url: base_url+"Generales/verifircar_dir_con",
        data: {
            datos:aInfo
        },
        success: function (data){
            //console.log(data);
            var array = $.parseJSON(data);
            console.log(array);
            
            $.each(array, function(index, item) {
                console.log('.equipo_con_'+item.equipo+'_'+item.serieid);
                console.log(item.dir);
                $('.equipo_con_'+item.equipo+'_'+item.serieid).data('direc',item.dir);
            });
            
        }
    });
}

function info_edit(idser,tipo,view){
    $('.tbody_info_eq').html('');
    if(view==0){
        $('#modalinfoequiposser').modal('open');
    }
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Solicitud_Refacciones/infoserdll',
        data: {
          idser:idser,
          tipo:tipo
        },
        success:function(data){
            $('.tbody_info_eq').html(data);
        },
        error: function(response){
            notificacion(1);
        }
    });
    if(view==0){
        $('.optionesservicios').hide();
    }
}
function limpiartr(ser,sere){
    $('.tr_'+ser+'_'+sere+' textarea').val('');
    $('.tr_'+ser+'_'+sere+' .prioridadr').prop('checked',false);
}


//$('.prioridadr:checked').val()
//$('.prioridadr').prop('checked',false);
function agregareditarser(){
    $('#modalinfoequiposser').modal('open');
    $('.optionesservicios').show('show');
    $('.tbody_info_eq').html('');
}
function obtenerservicios(idcli){
    $.ajax({
        type:'POST',
        url: base_url+'index.php/Solicitud_Refacciones/obtenerservicioscli',
        data: {
          idcli:idcli
        },
        success:function(data){
            $('#servcli').html(data);
        },
        error: function(response){
            notificacion(1);
        }
    });
}
function selected_servicio(){
    $('.tbody_info_eq').html('');
    var ser_s = $('#servcli option:selected').data('idser');
    var tipo_s = $('#servcli option:selected').data('tipo');
    if(ser_s>0){
        info_edit(ser_s,tipo_s,1);
        $('.infordelservicio').html('<a class="btn-bmz blue" onclick="obternerdatosservicio('+ser_s+','+tipo_s+')" title="Siguiente"><i class="fas fa-info-circle"></i></a>');
        
    }else{
        $('.infordelservicio').html('');
    }
}