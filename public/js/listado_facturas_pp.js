var base_url = $('#base_url').val();
var tablec_pp;
$(document).ready(function($) {
    tablec_pp = $('#tabla_facturacion_pp').DataTable();
    $('#idcliente_pp').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Ventasi/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    var html='';
                    if(element.empresa==element.razon_social){
                        html=element.empresa;
                    }else{
                        html=element.empresa+' ('+element.razon_social+')';
                    }
                    itemscli.push({
                        id: element.id,
                        text: html

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
    });
    $('#idcliente_rfc_pp').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'index.php/Facturaslis/obtenerrfc3',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.rfc,
                        text: element.rfc+' ('+element.razon_social+')'

                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function (e) {
        //var data = e.params.data;
        //obtenerrfc(data.id)
        //loadtable();
    });    
});
function loadtable_fpp(){
    $('body').loader('show');
    var personals=$('#personals_pp option:selected').val();
    var idcliente=$('#idcliente_pp option:selected').val()==undefined?0:$('#idcliente_pp option:selected').val();
    var idclienterfc=$('#idcliente_rfc_pp option:selected').val()==undefined?0:$('#idcliente_rfc_pp option:selected').val();
    var inicio=$('#finicial_pp').val();
    var fin=$('#ffinal_pp').val();
    var rs_folios=$('#rs_folios_pp option:selected').val();
    tablec_pp.destroy();
    tablec_pp = $('#tabla_facturacion_pp').DataTable({
        pagingType: 'input',
        dom: 'Blfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'pdfHtml5',
        ],
        search: {
                return: true
            }, 
        //stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/Facturaslis/getlistfacturas_pp",
            type: "post",
            "data": {
                'personal':personals,
                'cliente':idcliente,
                'finicio':inicio,
                'ffin':fin,
                'idclienterfc':idclienterfc,
                'rs_folios':rs_folios
            },
        },
        "columns": [
            {"data": "FacturasId",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==1){
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'">\
                              <label for="factura_'+row.FacturasId+'"></label>';
                    }else{
                        html='<input type="checkbox" class="filled-in facturas_checked" id="factura_'+row.FacturasId+'" value="'+row.FacturasId+'" disabled>\
                              <label for="factura_'+row.FacturasId+'"></label>';
                    }
                    return html;
                }
            },
            {"data": "Folio",
                render:function(data,type,row){
                    var html ='';
                        html=row.Folio;
                    return html;
                }
            },
            {"data": null,
                render:function(data,type,row){
                    var html='';
                    if(row.Rfc=='XAXX010101000'){ 
                        html=row.razonsocial+'<br>'+row.empresa;
                    }else{
                        html=row.razonsocial;
                    }
                    return html;
                }
            },
            {"data": "Rfc",},
            {"data": "total",
                render:function(data,type,row){
                    var html='';
                        
                    html=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(row.total);
                    return html;
                }
            },
            {"data": "Estado",
                render:function(data,type,row){
                    var html='';
                    if(row.Estado==0){
                        html='Cancelada';
                        html+='<div class="validarcancelacion '+row.uuid+'" data-uuid="'+row.uuid+'" style="color:red"></div>';
                    }else if(row.Estado==1){
                        html='Facturado';
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            html='Sin Facturar';
                        }
                    }else if(row.Estado==2){
                        html='Sin Facturar';
                    }else{
                        html='';
                    }
                    return html;
                }
            },
            {"data": "fechatimbre"},
            {"data": null,
                render:function(data,type,row){
                    var html='';
                        html=row.personal;
                    return html;
                }
            },
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<div class="btns-factura">';
                    var estadofactura=row.Estado;
                    if(estadofactura==1){
                        if(row.fechatimbre=='0000-00-00 00:00:00'){
                            estadofactura=2;
                        } 
                    }
                    if(row.FormaPago=='PUE'){
                        var classes_pue='consultar_fac_pue consultar_fac_pue_'+row.FacturasId+'';
                    }else{
                        var classes_pue='';
                    }

                    if(estadofactura==0){ // cancelado
                        html+='<a \
                                class="b-btn b-btn-primary tooltipped " \
                                href="'+base_url+'Configuracionrentas/generarfacturas/'+row.FacturasId+'/1" \
                                target="_blank"\
                                data-facturasid="'+row.FacturasId+'" data-motofactura="'+row.total+'"\
                                data-position="top" data-delay="50" data-tooltip="Factura"\
                              ><i class="fas fa-file-alt fa-2x"></i>\
                            </a> ';
                        html+='<a\
                                    class="b-btn b-btn-primary tooltipped" \
                                    href="'+base_url+row.rutaXml+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="XML"\
                                  download ><i class="far fa-file-code fa-2x"></i>\
                                    </a> ';
                        html+='<a\
                                    class="b-btn b-btn-primary tooltipped" \
                                    href="'+base_url+'kyocera/facturas/'+row.rutaAcuseCancelacion+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="Acuse de cancelación"\
                                  download ><i class="fas fa-file-invoice fa-2x" style="color: #ef7e4f;"></i>\
                                    </a> ';
                        if(perfilid==1){
                            html+='<a\
                                    class="b-btn b-btn-primary tooltipped" \
                                    onclick="regresarfacturaavigente('+row.FacturasId+')"  \
                                    target="_blank"\
                                    data-position="top" data-delay="50" title="Regresar a vigente" data-tooltip="Regresar a vigente"\
                                   ><i class="material-icons dp48" style="color: #ef7e4f;">replay</i>\
                                    </a> ';
                        }
                        //html+='<div class="no_tienerelacion no_tienerelacion_'+row.FacturasId+'"></div>';
                    }else if(estadofactura==1){
                        html+='<a \
                                class="b-btn b-btn-primary tooltipped '+classes_pue+'" \
                                href="'+base_url+'Configuracionrentas/generarfacturas/'+row.FacturasId+'/1" \
                                target="_blank"\
                                data-facturasid="'+row.FacturasId+'" data-motofactura="'+row.total+'"\
                                data-position="top" data-delay="50" data-tooltip="Factura"\
                              ><i class="fas fa-file-alt fa-2x"></i>\
                            </a> ';
                        html+='<a\
                                    class="b-btn b-btn-primary tooltipped" \
                                    href="'+base_url+row.rutaXml+'" \
                                    target="_blank"\
                                    data-position="top" data-delay="50" data-tooltip="XML"\
                                  download ><i class="far fa-file-code fa-2x"></i>\
                                </a> ';
                                if(row.FormaPago=='PPD'){
                                    html+='<a\
                                            class="b-btn b-btn-primary tooltipped complementops_files complementops_files_'+row.FacturasId+'" \
                                            onclick="complementop('+row.FacturasId+')"\
                                            data-position="top" data-delay="50" data-tooltip="Complemento de pago" data-complementofac="'+row.FacturasId+'" \
                                          download ><i class="fas fa-receipt fa-2x" ></i>\
                                            </a> ';
                                }
                        html+='<div class="no_tienerelacion no_tienerelacion_'+row.FacturasId+'"></div>';
                    }else if(estadofactura==2){
                        html+='<button type="button"\
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" \
                          onclick="retimbrar('+row.FacturasId+',0)"\
                          data-position="top" data-delay="50" data-tooltip="Retimbrar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="fas fa-file fa-stack-2x"></i>\
                          <i class="fas fa-sync fa-stack-1x" style="color: red;"></i>\
                          </span>\
                        </button> ';
                        html+='<a type="button"\
                            href="'+base_url+'Facturaslis/add/'+row.FacturasId+'"\
                          class="b-btn b-btn-primary tooltipped btn-retimbrar" \
                          data-position="top" data-delay="50" data-tooltip="Editar" id="refacturar">\
                          <span class="fa-stack">\
                          <i class="fas fa-pencil-alt fa-stack-2x"></i>\
                          </span>\
                        </a> ';
                    }else{
                        html+='';
                    }
                    if(estadofactura==1){
                        html+='<a href="'+base_url+'Facturaslis/add/0?idfac='+row.FacturasId+'&uuid='+row.uuid+'" class="waves-effect btn-bmz blue"  title="Refacturar para cancelacion"><i class="fas fa-file-prescription"></i></a>';
                    }
                    html+='</div>';
                    return html;
                }
            }
        ],
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[20,25, 50, 100,200,400,-1], [20,25, 50,100,200,400,'todo']],
        //"lengthMenu": [[25, 50, 100,200,400], [25, 50,100,200,400]],
        //"searching": false
        
    }).on('draw',function(){
        $('.dataTables_filter').append('<i class="fa fa-search buttonsearch" onclick="searchtable_vf()"></i>');
        verificarcomplementos();
        verificarpagos();
        verificarcancelacion();
        $('body').loader('hide');
    });
}