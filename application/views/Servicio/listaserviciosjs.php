<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/plugins/toastr/toastr.css">
<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/toastr/toastr.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>public/plugins/dist/jquery.mark.min.js" ></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
<script src="<?php echo base_url(); ?>/public/js/select2.full.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/listadoservicios.js?v=<?php echo $version;?>" ></script>
<?php if (isset($_GET['tec'])) { ?>
	<script type="text/javascript">
		$(document).ready(function($) {
			setTimeout(function(){ 
				$('#serv').val(1).change();
				$('#serv').trigger("chosen:updated");
			}, 1000);
			setTimeout(function(){ 
				$('#tecnico').val('XXX').change();
				$('#tecnico').trigger("chosen:updated");
			}, 1500);
		});
	</script>
<?php } ?>
<?php if (isset($_GET['servicio'])) { ?>
	<script type="text/javascript">
		$(document).ready(function($) {
			setTimeout(function(){ 
				$('#fechainicio').val('<?php echo $_GET['fecha']?>');
				$('#fechafin').val('<?php echo $_GET['fecha']?>');

				$('#serv').val(1).change();
				$('#serv').trigger("chosen:updated");
			}, 1000);
			
		});
	</script>
<?php } ?>